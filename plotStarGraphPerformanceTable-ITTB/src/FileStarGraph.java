import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class FileStarGraph {

	public  void createFile(List<String> listAlternIDs, byte[] image, File pngFile)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(pngFile);
			fos.write(image);
			fos.close();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public  void createFileMultiplePlots(List<String> listAlternIDs, 
	                                     byte[][] images, String pngFileTemplate)
	{
		for(int i=0;i<listAlternIDs.size();i++)
		{
			File pngFile = new File(String.format(pngFileTemplate, i+1));
			createFile(listAlternIDs, images[i], pngFile);
		}
	}

}
