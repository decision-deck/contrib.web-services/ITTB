import java.io.FileOutputStream;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class StatusMessage
{
	private   Element racine = new Element("XMCDA");		 
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");		 
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");
	private org.jdom2.Document document = new Document(racine);

	public void save(String fichier)
	{
		try
		{	        
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fichier));
		}
		catch (java.io.IOException e){}
	}

	public void createErrorMessage(String errorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		Element logMessage = new Element("errorMessage");
		methodMessages.addContent(logMessage);

		Attribute executionStatus = new Attribute("name","executionStatus");
		logMessage.setAttribute(executionStatus);

		Element text = new Element("text");
		text.setText("Execution failed");
		logMessage.addContent(text); 
		if(errorMessage!="")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error= new Attribute("name","Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(errorMessage);
			fatalMess.addContent(text2); 
		}
	}

	public  void createLogMessage(String warningMessage, String errorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);
		Attribute executionStatus = new Attribute("name","executionStatus");
		logMessage.setAttribute(executionStatus);

		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);  

		if(errorMessage!="")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error= new Attribute("name","Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(errorMessage);
			fatalMess.addContent(text2); 
		}

		if(warningMessage!="")
		{
			Element warningMess = new Element("message");
			methodMessages.addContent(warningMess);
			Attribute error= new Attribute("name","Warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			//text2.setText("Attention!!!");
			warningMess.addContent(text2); 
		}
	}

}
