import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_PerformanceTable;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class DrawStarGraph {

	protected static Polygon starGraph(Point p, Dimension size, int numberOfCriter, float translation )
	{
		// Les coordonn�es des diff�rents points du polygone
		int [] abscissa = new int[numberOfCriter]; 
		int [] ordonates = new int[numberOfCriter];

		// L'angle de r�f�rence entre 2 pointes du polygone r�gulier
		double delta = Math.PI * 2 / numberOfCriter; 
		double angle = 0;
		for ( int i = 0; i < numberOfCriter; i++ ) {
			//On trace un polyg�ne r�gulier
			angle = delta * i + Math.PI / 2;
			abscissa[i] = (int)(size.width * Math.cos(angle) + 0.5) + p.x;
			ordonates[i] = p.y - (int)(size.height * Math.sin(angle) + 0.5)+(int)translation;
		}

		return new Polygon(abscissa,ordonates, numberOfCriter);
	}

	protected static void drawLines (Graphics2D g,Point p, Dimension size, PerformanceTable perfTable,Color colorToUse, Alternative alternative,
	                                 ArrayList<String > listTitlesCriteria, float translation, String []parsePreferenceDirectin) {
		// Les coordonn�es des diff�rents points du polygone
		int numberOfCriter= perfTable.nbCriteria();
		int [] abscissa = new int[numberOfCriter]; 
		int [] ordonates = new int[numberOfCriter];

		// L'angle de r�f�rence entre 2 pointes du polygone r�gulier
		double delta = Math.PI * 2 / numberOfCriter; 
		double angle = 0;
		for ( int i = 0; i < numberOfCriter; i++ ) {
			//On trace un polyg�ne r�gulier
			angle = delta * i + Math.PI / 2;
			abscissa[i] = (int)(size.width * Math.cos(angle) + 0.5) + p.x;
			ordonates[i] = p.y - (int)(size.height * Math.sin(angle) + 0.5);
		}

		Font fontText = new Font("Arial", Font.BOLD, 12);
		g.setFont(fontText);

		for ( int i = 0; i < numberOfCriter; i++ )
		{
			g.drawLine(p.x,p.y+(int)translation, abscissa[i],  ordonates[i]+(int)translation);
			if( abscissa[i]>p.x && ordonates[i]<=p.y)
				g.drawString(listTitlesCriteria.get(i), abscissa[i] ,(int)translation+ordonates[i]-10);
			else if( abscissa[i]>p.x && ordonates[i]>p.y)
				g.drawString(listTitlesCriteria.get(i), abscissa[i]+5, (int)translation+ordonates[i]+10);
			else if( abscissa[i]<p.x && ordonates[i]<=p.y)
				g.drawString(listTitlesCriteria.get(i), abscissa[i]-85, (int)translation+ordonates[i]-10);
			else if( abscissa[i]<=p.x && ordonates[i]>p.y)
				g.drawString(listTitlesCriteria.get(i), abscissa[i]-100, (int)translation+ordonates[i]-5);

			else if( abscissa[i]==p.x && ordonates[i]==0)
			{
				g.drawString(listTitlesCriteria.get(i), abscissa[i]+25, (int)translation+ordonates[i]+10);
			}
		}

		if( numberOfCriter==2)
		{
			g.drawLine(p.x-5,p.y-5+(int)translation,p.x+5,p.y+5+(int)translation);
			g.drawLine(p.x-5,p.y+5+(int)translation,p.x+5,p.y-5+(int)translation);
		}

		g.setColor(colorToUse);  
		g.setStroke(new BasicStroke(3F));  
		// Dessiner les valeurs de la perfTable pour l'alternative 

		int [] abscissaPerfTable = new int[numberOfCriter];
		int [] ordonatesPerfTable = new int[numberOfCriter];
		List<Criterion> criteria = perfTable.criteria();
		for ( int i = 0; i < numberOfCriter; i++ ) {
			//chercher le min et le max pour chaque crit�re
			float min=getMinimum(perfTable,criteria.get(i));
			float max=getMaximum(perfTable,criteria.get(i));

			// Transformer l element courant pour avoir un x et un y
			float courant=perfTable.getValue(alternative, criteria.get(i));
			if(abscissa[i]!=p.x)
			{
				float a=(float) ( (max-min)/(abscissa[i]-(double)p.x) );
				if (abscissa[i]<p.x)
					a=-a;

				//System.out.println("pente ="+a);
				if (abscissa[i]>p.x)
				{
					abscissaPerfTable [i]=  (int) ( (double)p.x+((courant-min)/a) );
				}
				else
				{
					abscissaPerfTable [i]=  (int) ( (double)p.x+((courant-max)/a) );
				}

				float pente=(float) ( ((double)p.y-ordonates[i])/((double)p.x- abscissa[i]) );
				ordonatesPerfTable[i]= (int) ( (double)p.y+((abscissaPerfTable [i]-p.x)*pente) )  ;
			} 
			else
			{
				abscissaPerfTable [i]=p.x;	
				if(ordonates[i]!=p.y)
				{
					float a=(float) ( (max-min)/(ordonates[i]-(double)p.y) );
					if (ordonates[i]<p.y)
						a=-a;
					if (ordonates[i]>p.y)
						ordonatesPerfTable[i]=  (int) ( (double)p.y+((courant-min)/a) );
					else
						ordonatesPerfTable[i]=  (int) ( (double)p.y+((courant-max)/a) );
				}
				else
					ordonatesPerfTable[i]= p.y;
			}

			// Chercher les coordonn�es du milieu pour faire la sym�trie par rapport � ce point dans le cas abscissa <500
			double abscissaMilieu=((double)p.x+(double)abscissa[i])/2.;
			double ordonateMilieu=((double)p.y+(double)ordonates[i])/2.;
			double symetrieX=2.*abscissaMilieu-(double)abscissaPerfTable[i];
			double symetrieY=2.*ordonateMilieu-(double)ordonatesPerfTable[i];

			if(abscissa[i]<p.x ||(abscissa[i]== p.x && ordonatesPerfTable[i]>ordonates[i]&& ordonatesPerfTable[i]<=p.y) ||
					(abscissa[i]== p.x && ordonatesPerfTable[i]==ordonates[i]&& ordonatesPerfTable[i]==0) )
			{
				abscissaPerfTable[i]=(int) symetrieX;
				ordonatesPerfTable[i]=(int) symetrieY;
			}
		}

		// Maintenant, je vais m'occuper des prefrences directions, si c'est max: je ne fais rien,
		//mais si preference direction est min, il faudra faire la sym�trie axiale par rapport au milieu
		for ( int i = 0; i < numberOfCriter; i++ )
		{
			if (parsePreferenceDirectin[i].equals("min"))
			{
				// Chercher les coordonn�es du milieu pour faire la sym�trie par rapport � ce point dans le cas abscissa <500
				double abscissaMilieu=((double)p.x+(double)abscissa[i])/2.;
				double ordonateMilieu=((double)p.y+(double)ordonates[i])/2.;
				double symetrieX=2.*abscissaMilieu-(double)abscissaPerfTable[i];
				double symetrieY=2.*ordonateMilieu-(double)ordonatesPerfTable[i];
				abscissaPerfTable[i]=(int) symetrieX;
				ordonatesPerfTable[i]=(int) symetrieY;
			}
			ordonatesPerfTable[i]+= translation;
		}

		Polygon testPoly= new Polygon(abscissaPerfTable,ordonatesPerfTable,numberOfCriter)   ; 	 
		g.drawPolygon(testPoly);

		//g.fillPolygon(testPoly);

		for ( int i = 0; i < numberOfCriter; i++ ) {
			/*Ellipse2D.Double circle = new Ellipse2D.Double(abscissaPerfTable [i], ordonatesPerfTable[i],5, 5);
			   g.fill(circle);*/

			//ordonatesPerfTable[i]+=translation;

			g.fillOval(abscissaPerfTable [i]-5, ordonatesPerfTable[i]-5, 10,10);

			/*if(i<numberOfCriter-1)
			   g.drawLine((int)abscissaPerfTable [i],(int)ordonatesPerfTable[i],
			              (int)abscissaPerfTable [i+1],(int)ordonatesPerfTable[i+1]);
			   else

				   g.drawLine((int)abscissaPerfTable [i],(int)ordonatesPerfTable[i],
				              (int)abscissaPerfTable [0],(int)ordonatesPerfTable[0]);*/
		}
	}

	protected static float getMaximum(PerformanceTable performanceTable, Criterion criterion)
	{
		float max = Float.NEGATIVE_INFINITY;
		for (Alternative alternative: performanceTable.alternatives())
		{
			float v = performanceTable.getValue(alternative, criterion);
			if ( v > max )
				max = v;
		}
		return max;
	}

	protected static float getMinimum(PerformanceTable performanceTable, Criterion criterion)
	{
		float min = Float.POSITIVE_INFINITY;
		for (Alternative alternative: performanceTable.alternatives())
		{
			float v = performanceTable.getValue(alternative, criterion);
			if ( v < min )
				min = v;
		}
		return min;
	}

	protected static ArrayList<String> getTitlesForMultiplePlots(Alternatives alter, List<String> listintersectionIds)
	{
		ArrayList<String> listintersectionTitles=new ArrayList<String>();

		for(int i=0; i<listintersectionIds.size();i++)
		{
			if (alter.getIDs().
					contains(listintersectionIds.get(i)))
			{
				int index=alter.getIDs().indexOf(listintersectionIds.get(i));
				if(alter.getNames().get(index)!=null)
					listintersectionTitles.add(alter.getNames().get(index)+" ("+listintersectionIds.get(i)+")");
				else listintersectionTitles.add(listintersectionIds.get(i));
			}
		}

		return listintersectionTitles;
	}

	protected static ArrayList<String> getTitlesCriteria(Criteria criter, List<String> listintersectionIds)
	{
		ArrayList<String> listintersectionTitles=new ArrayList<String>();

		for(int i=0; i<listintersectionIds.size();i++)
		{
			if (criter.getCriteriaIDs().
					contains(listintersectionIds.get(i)))
			{
				int index=criter.getCriteriaIDs().indexOf(listintersectionIds.get(i));
				if(criter.getListCriteriaNames().get(index)!=null)
					listintersectionTitles.add(criter.getListCriteriaNames().get(index)+" ("+listintersectionIds.get(i)+")");
				else listintersectionTitles.add(listintersectionIds.get(i));
			}
		}

		return listintersectionTitles;
	}

	protected static byte[] createStarGraph(PerformanceTable perfTable,Color colorToUse, String plotsOrganisation,
	                                        ArrayList<String > listTitles,ArrayList<String > listTitlesCriteria,
	                                        String order, String increaseDecrease, String[]parsePreferenceDirectin) {
		int numberOfCriter=perfTable.nbCriteria();
		int numberOfAlternatives = perfTable.nbAlternatives();
		int width = 450, height = 320;
		float transaltion=40;
		ByteArrayOutputStream []chart_png_image = new ByteArrayOutputStream[ numberOfAlternatives ];
		for (int indexAlternative=0;indexAlternative< numberOfAlternatives ;indexAlternative++)
		{
			Alternative alternative = perfTable.alternatives().get(indexAlternative);

			BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

			Graphics2D g = bi.createGraphics();  
			g.setColor(Color.black); 
			Font font = new Font("TimesRoman", Font.BOLD, 20);
			g.setFont(font); 

			// Tracer un premier polygone (� l'ext�rieur)
			Polygon p1=starGraph(new Point(width/2,130),new Dimension(130, 130),numberOfCriter,transaltion);

			// Tracer un 2�me polygone (au milieu)--> donc les dimensions sont /2
			Polygon p2=starGraph(new Point(width/2,130) ,new Dimension(65, 65),numberOfCriter,transaltion);

			g.drawPolygon(p1);
			g.drawPolygon(p2);

			chart_png_image [indexAlternative]= new ByteArrayOutputStream();
			drawLines(g,new Point(width/2,130),new Dimension(130, 130),perfTable,colorToUse,
			          alternative,listTitlesCriteria,transaltion,parsePreferenceDirectin);

			g.setColor(Color.black);  
			Font fontText = new Font("Arial", Font.BOLD, 16);
			g.setFont(fontText);
			String title=listTitles.get(indexAlternative);
			int abscissaTitle=(width/2)-4* title.length();
			g.drawString( title+":", abscissaTitle, transaltion/2);

			g.dispose();
			// Save the png
			File file;	   
			try {
				file=File.createTempFile("starGraph_", ".png", new File(System.getProperty("java.io.tmpdir")));
				ImageIO.write(bi, "PNG", file);
				FileInputStream input= new FileInputStream(file);		 
				BufferedImage image = ImageIO.read(input);
				ImageIO.write(image, "png", chart_png_image [indexAlternative]);
				chart_png_image[indexAlternative].close();
				input.close();
				if(!file.delete())
					System.err.println("Warning: " + file.getAbsolutePath() + " could not be deleted!");
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}

		// Mettre dans l'ordre souhaite
		orderStarGraphs(order,increaseDecrease,perfTable.alternativesIDs(),listTitles,chart_png_image);

		InputStream[]inputStream = new InputStream[numberOfAlternatives];
		for (int i=0;i<numberOfAlternatives;i++)
			inputStream[i]= new ByteArrayInputStream(chart_png_image[i].toByteArray());

		String encodedImage="";
		if(plotsOrganisation.equals("Line"))
			return convertImageByLine(inputStream,"png",numberOfAlternatives);

		else if(plotsOrganisation.equals("Column"))
			return convertImageByColumn(inputStream,"png",numberOfAlternatives);

		else  if(plotsOrganisation.equals("Grid"))
			return convertImageByGrid(inputStream,"png",numberOfAlternatives);

		return null;
	}

	protected static void orderStarGraphs(String order,String increaseDecrease,
	                                      List<String> listAlternativeIDs,ArrayList<String > listTitles,
	                                      ByteArrayOutputStream []chart_png_image)
	{
		int numberOfAlternatives=listAlternativeIDs.size();
		String[]listNewTitles=new String[listTitles.size()];

		if(order.equals("by name"))
			for (int i=0;i<listNewTitles.length;i++)
				listNewTitles[i]=listTitles.get(i);

		else if(order.equals("by id"))
			for (int i=0;i<listNewTitles.length;i++)
				listNewTitles[i]=listAlternativeIDs.get(i);

		if(increaseDecrease.equals("increasing"))
		{
			for (int i=0;i< numberOfAlternatives-1 ;i++)
				for (int j=i+1;j< numberOfAlternatives ;j++)
					if(listNewTitles[j].toLowerCase().compareTo(listNewTitles[i].toLowerCase())<0)
					{
						exchange (chart_png_image,i,j);  
						exchangeTitles(listNewTitles,i,j);
					}
		}
		else if(increaseDecrease.equals("decreasing"))
		{
			for (int i=0;i< numberOfAlternatives-1 ;i++)
				for (int j=i+1;j< numberOfAlternatives ;j++)
					if(listNewTitles[j].toLowerCase().compareTo(listNewTitles[i].toLowerCase())>0)
					{
						exchange(chart_png_image,i,j);  
						exchangeTitles(listNewTitles,i,j);
					}
		}
	}

	protected static void orderMultipleStarGraphs(String order, String increaseDecrease,
	                                              List<String> listAlternativeIDs, List<String> listTitles,
	                                              byte[][] chart_png_image)
	{
		int numberOfAlternatives = listAlternativeIDs.size();
		String[] listNewTitles = new String[listTitles.size()];

		if (order.equals("by name"))
			for (int i = 0; i < listNewTitles.length; i++)
				listNewTitles[i] = listTitles.get(i);
		else if (order.equals("by id"))
			for (int i = 0; i < listNewTitles.length; i++)
				listNewTitles[i] = listAlternativeIDs.get(i);

		if(increaseDecrease.equals("increasing"))
		{
			for (int i = 0; i < numberOfAlternatives - 1; i++)
				for (int j = i + 1; j < numberOfAlternatives; j++)
					if (listNewTitles[j].toLowerCase().compareTo(listNewTitles[i].toLowerCase()) < 0)
					{
						exchangeImages(chart_png_image, i, j);
						exchangeTitles(listNewTitles, i, j);
					}
		}
		else if(increaseDecrease.equals("decreasing"))
		{
			for (int i = 0; i < numberOfAlternatives - 1; i++)
				for (int j = i + 1; j < numberOfAlternatives; j++)
					if (listNewTitles[j].toLowerCase().compareTo(listNewTitles[i].toLowerCase()) > 0)
					{
						exchangeImages(chart_png_image, i, j);
						exchangeTitles(listNewTitles, i, j);
					}
		}
	}

	protected static void exchange (ByteArrayOutputStream[] chart_png_image, int i, int j)
	{
		ByteArrayOutputStream temp=chart_png_image[j];
		chart_png_image[j]=chart_png_image[i];
		chart_png_image[i]=temp;
	}

	protected static void exchangeImages(byte[][] images, int i, int j)
	{
		byte[] temp=images[j];
		images[j]=images[i];
		images[i]=temp;
	}

	protected static void exchangeTitles(String[] titles, int i, int j)
	{
		String temp=titles[j];
		titles[j]=titles[i];
		titles[i]=temp;
	}

	protected static byte[] convertImageByLine (java.io.InputStream[] inputStream, String format, int size) 
	{
		String encodedImage="";	
		BufferedImage []images=new BufferedImage [size]; 
		for (int i=0;i<size;i++)
			try{		 	
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {		
				//e.printStackTrace();
			}

		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			w+=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{
			g.drawImage(images[i], x, y, null);
			x+=images[i].getWidth();
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(combined, format, baos);
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return baos.toByteArray();
	}  

	protected static byte[] convertImageByColumn (java.io.InputStream []inputStream,String format, int size) 
	{
		String encodedImage="";
		BufferedImage []images=new BufferedImage [size];
		for (int i=0;i<size;i++)
			try{			 	
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {
				e.printStackTrace();
			}

		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			h+=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			y+=images[i].getHeight();
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(combined, format, baos);
		} catch (IOException e1) {
			//e1.printStackTrace();
		}
		return baos.toByteArray();
	}  

	protected static byte[] convertImageByGrid (java.io.InputStream []inputStream,String format, int size) 
	{
		BufferedImage []images=new BufferedImage [size];

		for (int i=0;i<size;i++)
			try{
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {	
				e.printStackTrace();
			}

		// create the new image, canvas size is the square of the size
		int maxNumberOfImagesByLine=(int) Math.ceil(Math.sqrt(size));
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();
		int maxWidth=w;
		w=w*maxNumberOfImagesByLine;

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		int maxHeight=h;
		h=h*maxNumberOfImagesByLine;

		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;

		for (int i=0;i<size;i++)					 	
		{
			g.drawImage(images[i], x, y, null);
			x+=maxWidth;
			if(x>=w)
			{
				x=0;
				y+=maxHeight;
			}
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(combined, format, baos);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			ImageIO.write(combined, format, baos);
		} catch (IOException e1) {
			//e1.printStackTrace();
		}
		return baos.toByteArray();
	}

	protected static byte[] createMultipleStarGraph(PerformanceTable perfTable,Color colorToUse, 
	                                                Alternative alternative, String title,ArrayList<String > listTitlesCriteria,
	                                                String[]parsePreferenceDirectin) {
		int numberOfCriter=perfTable.nbCriteria();
		int width = 450, height = 320;
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bi.createGraphics();  
		g.setColor(Color.black); 
		Font font = new Font("TimesRoman", Font.BOLD, 20);
		g.setFont(font);
		int transaltion=40;
		// Tracer un premier polygone (� l'ext�rieur)
		Polygon p1=starGraph(new Point(width/2,130),new Dimension(130, 130),numberOfCriter,transaltion);
		// Tracer un 2�me polygone (au milieu)--> donc les dimensions sont /2
		Polygon p2=starGraph(new Point(width/2,130) ,new Dimension(65, 65),numberOfCriter,transaltion);
		g.drawPolygon(p1);
		g.drawPolygon(p2);

		drawLines(g,new Point(width/2,130),new Dimension(130, 130),perfTable,colorToUse,
		          alternative,listTitlesCriteria,transaltion,parsePreferenceDirectin);

		g.setColor(Color.black);  
		Font fontText = new Font("Arial", Font.BOLD, 16);
		g.setFont(fontText);
		int abscissaTitle=(width/2)-4*title.length();
		g.drawString(title+":", abscissaTitle, transaltion/2);
		g.dispose();

		// Save the png
		File file;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", baos);
			baos.close();
			return baos.toByteArray();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return null;
	}

	protected static Color getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		Color color = Color.black;
		if (black_and_white)
			return color;

		Color c = null;
		try
		{
			c = (Color)Color.class.getField(colorToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c==null )
			return color;

		color = c;
		return color;
	}

	public static void main(String[] argv) {	  
		boolean stopRunning = false;
		String tempErrorMessage = "";
		String warningMessage = "";

		if (argv.length != 6)
		{
			System.err.println("Usage: alternatives.xml criteria.xml performanceTable.xml parameters.xml performanceTable.png messages.xml");
			System.err.println("       The 4 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		if (!new File(argv[0]).exists() || !new File(argv[1]).exists() || !new File(argv[2]).exists()
				|| !new File(argv[3]).exists()) stopRunning = true;

		if (stopRunning)
			tempErrorMessage = "Error: unable to run the algorithm. "
					+ "Reason: missing one (or more) entry file(s). "
					+ "Please check your entry files.";

		if (stopRunning == false)
		{
			Main mainFunction = new Main();
			PerformanceTable test = new PerformanceTable();

			String pathAlternatives = argv[0];
			String pathCriteria = argv[1];
			String pathPerfTable = argv[2];
			String pathOptions=argv[3];

			String errParam=mainFunction.checkEntryFile(pathOptions);			
			String errAltern = mainFunction.checkEntryFile(pathAlternatives);
			String errCriter = mainFunction.checkEntryFile(pathCriteria);
			String errPerfTable = mainFunction.checkEntryFile(pathPerfTable);

			if(errAltern!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the alternatives " +
						"tag is not a valid XMCDA document.");

			if(errCriter!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteria " +
						"tag is not a valid XMCDA document.");

			if(errPerfTable!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the performanceTable " +
						"tag is not a valid XMCDA document.");

			if(errParam!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the parameters " +
						"is not a valid XMCDA document.");

			if(errParam!=""||errAltern!=""||errCriter!=""||errPerfTable!="")
			{
				//In this case: stopRunning=true, we send the messages.xml output file
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createErrorMessage(tempErrorMessage);			
				String pathOutputMessage=argv[argv.length - 1];
				xmcdaMess.enregistre(pathOutputMessage);
			}
			else
			{
				//ParseMethodParameters
				OptionsParameters parseMethodParam=new OptionsParameters();
				mainFunction.PrepareParsing (pathOptions);
				boolean statusParseMethod=parseMethodParam.parse(mainFunction.getRacine());
				if(parseMethodParam.getWarning()!="")
					warningMessage=warningMessage.concat(parseMethodParam.getWarning());

				if(statusParseMethod==false)
				{
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal error: the file containing the parameters " +
								"is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" methodParameters tag is erroneous or empty.");
				}

				boolean uniquePlot=parseMethodParam.getUniquePlot();
				String plotsOrganisation=parseMethodParam.getPlotsOrganisation();
				boolean colorBoolean=parseMethodParam.getColor();
				String colorFromOptions=parseMethodParam.getColorToUse();
				Color colorToUse=getColorFromOptions(!colorBoolean,colorFromOptions);

				String order=parseMethodParam.getOrder();
				String maxMin=parseMethodParam.getIncreaseDecreaseOrder();
				boolean prefDirec=parseMethodParam.getPrefDirection();

				// Test d'erreur
				JDOM_Criteria jdomCriter = new JDOM_Criteria();
				mainFunction.PrepareParsing(pathCriteria);
				boolean parseCriterNull = false;
				Criteria criter = jdomCriter.parseCriteriaXML(mainFunction.getRacine());
				String[] parsePreferenceDirectin = null ;
				if(criter!=null)
				{
					parsePreferenceDirectin = new String[criter.getCriteriaIDs().size()];
					for (int i=0;i<criter.getCriteriaIDs().size();i++)
						parsePreferenceDirectin[i]="max";
				}

				if(prefDirec && criter !=null)
				{
					parsePreferenceDirectin = new String[criter.getCriteriaIDs().size()];
					int idx = 0;
					LinkedHashMap<Criterion, String> m = jdomCriter.exploreCriteriaXML(mainFunction.getRacine(),criter);
					// The following is based on the fact that the Set is in fact a LinkedKeySet, so the order of the criteria
					// supplied in 'criter' is preserved and its index and the index in criteria correspond.
					Set<Criterion> s = m.keySet();
					for (Criterion c: s)
						parsePreferenceDirectin[idx++] = m.get(c);
				}

				if (jdomCriter.getWarning() != "")
				{
					if (warningMessage.equals(""))
						warningMessage = "\n" + jdomCriter.getWarning();
					else
						warningMessage = warningMessage.concat("\n" + jdomCriter.getWarning());
				}
				if (criter != null)
				{}
				else
				{
					parseCriterNull = true;
					if (tempErrorMessage.equals(""))
						tempErrorMessage = "\nFatal Error: the file containing the criteria "
								+ "tag is erroneous or empty.";
					else
						tempErrorMessage = tempErrorMessage.concat("\nFatal Error: the file containing the"
								+ " criteria tag is erroneous or empty.");
				}

				// Test d'erreur
				mainFunction.PrepareParsing(pathAlternatives);
				JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
				boolean parseAlternativesNull = false;
				Alternatives alter = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
				if (jdomAlter.getWarning() != "")
				{
					if (warningMessage.equals(""))
						warningMessage = "\n" + jdomAlter.getWarning();
					else
						warningMessage = warningMessage.concat("\n" + jdomAlter.getWarning());
				}
				if (alter != null)
				{}
				else
				{
					parseAlternativesNull = true;
					// Error message
					if (tempErrorMessage.equals(""))
						tempErrorMessage = "\nFatal Error: the file containing the alternatives "
								+ "tag is erroneous or empty.";
					else
						tempErrorMessage = tempErrorMessage.concat("\nFatal Error: the file containing the"
								+ " alternatives tag is erroneous or empty.");
				}

				// Test d'erreur
				mainFunction.PrepareParsing (pathPerfTable);
				JDOM_PerformanceTable jdomPerfTable= new JDOM_PerformanceTable();
				boolean parsePerfTableNull=false;
				PerformanceTable perfTable=jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());
				if(jdomPerfTable.getErrorMessage()!="")
					warningMessage= warningMessage.concat("\n"+jdomPerfTable.
					                                      getErrorMessage());

				if(jdomPerfTable.getWarningMessage()!="")
					warningMessage=warningMessage.concat(jdomPerfTable.
					                                     getWarningMessage());

				if(perfTable!=null){}
				else
				{
					parsePerfTableNull=true;
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the performanceTable " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" performanceTable tag is erroneous or empty.");
				}

				if(parseCriterNull==false && parseAlternativesNull==false && parsePerfTableNull==false && statusParseMethod==true)
				{
					/*
					mainFunction.PrepareParsing (pathCriteria);
					Criteria criteria = jdomCriter.parseCriteriaXML(mainFunction.getRacine());
					
					mainFunction.PrepareParsing(pathAlternatives);
					Alternatives alternatives = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());

					mainFunction.PrepareParsing(pathPerfTable);
					test = jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());
					*/
					test=mainFunction.run(perfTable,criter,alter);

					warningMessage=warningMessage.concat(mainFunction.getWarningMessage());
					tempErrorMessage=mainFunction.getErrorMessage();

					byte[][] encodedImage = new byte[perfTable.alternativesIDs().size()][];

					if(test.nbAlternatives()!=0 && test.nbCriteria()!=0)
					{
						String pathOutputFile=argv[4];
						// D�rouler l'algorithme
						FileStarGraph outputFile=new FileStarGraph();
						ArrayList<String > listTitlesCriteria=getTitlesCriteria(criter,test.criteriaIDs());
						ArrayList<String > listTitles=getTitlesForMultiplePlots(alter,test.alternativesIDs());
						if (!uniquePlot)
						{
							for (int i=0;i<perfTable.alternativesIDs().size();i++)
							{
								Alternative alternative = perfTable.alternatives().get(i);
								encodedImage[i]=createMultipleStarGraph(test,colorToUse,alternative,listTitles.get(i),listTitlesCriteria,parsePreferenceDirectin);
							}

							final String template = pathOutputFile.substring(0, pathOutputFile.indexOf('.')) + "-%02d.png";
							orderMultipleStarGraphs(order,maxMin,perfTable.alternativesIDs(),listTitles,encodedImage);

							outputFile.createFileMultiplePlots(test.alternativesIDs(), encodedImage, template);
						}
						else // one unique plot
						{
							byte[] image = createStarGraph(test,colorToUse, plotsOrganisation,listTitles,listTitlesCriteria, order, maxMin,parsePreferenceDirectin);
							outputFile.createFile(test.alternativesIDs(), image, new File(pathOutputFile));
						}

						// Output Log Message File
						StatusMessage xmcdaMess = new StatusMessage();
						xmcdaMess.createLogMessage(warningMessage,tempErrorMessage);

						String pathOutputMessage = argv[argv.length - 1];
						xmcdaMess.save(pathOutputMessage);
					}
					else
					{
						tempErrorMessage=tempErrorMessage.concat("\n We cannot run the algorithm " +
								"since the criteria list or the alternatives list " +
								"of the PROJECT is empty.\n");
						// Output Log Message File
						StatusMessage xmcdaMess = new StatusMessage();
						xmcdaMess.createErrorMessage(tempErrorMessage);
						String pathOutputMessage = argv[argv.length - 1];
						xmcdaMess.save(pathOutputMessage);
					}
				}
				else
				{
					// Output Log Message File
					StatusMessage xmcdaMess = new StatusMessage();
					xmcdaMess.createErrorMessage(tempErrorMessage);
					String pathOutputMessage = argv[argv.length - 1];
					xmcdaMess.save(pathOutputMessage);
				}
			}
		}// StopRunning =false

		else
		{
			// StopRunning =true
			// Output Log Message File
			StatusMessage xmcdaMess = new StatusMessage();
			xmcdaMess.createErrorMessage(tempErrorMessage);
			String pathOutputMessage = argv[argv.length - 1];
			xmcdaMess.save(pathOutputMessage);
		}
	}

}