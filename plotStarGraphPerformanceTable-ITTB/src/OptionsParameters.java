import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class OptionsParameters {

	static org.jdom2.Document document;
	private boolean color;
	private String colorToUse;
	private String arrangement;
	private boolean uniquePlot;
	private String order;
	private String warning;
	private String maxMin;
	private boolean preferenceDirection;

	public  OptionsParameters(){
		color=false;	
		uniquePlot=true;
		colorToUse="";
		arrangement ="By column";
		warning="";
		order="";
		maxMin="";
		preferenceDirection=false;
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		Element racine = document.getRootElement();

		return racine;
	}

	public boolean parse(Element racine)
	{	      
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");

			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("preference_direction"))
					{
						if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("true")))
							preferenceDirection=true;
						else if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							preferenceDirection= false;
						else
						{
							// Default value
							preferenceDirection= false;
							warning=warning.concat("\nThe parameter 'Take preferenceDirections into account?' is set to the default value 'false'. " +
									"Check the entry file containing the methodParameters tag.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("unique_plot"))
					{
						if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("true")))
							uniquePlot=true;
						else if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							uniquePlot= false;
						else
						{
							// Default value
							uniquePlot= true;
							warning=warning.concat("\nThe parameter 'Number of images' is set to the default value 'Multiple images'. " +
									"Check the entry file containing the methodParameters tag.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("plots_display"))
					{
						if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("line")))
							arrangement= "Line";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("column")))
							arrangement= "Column";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("grid")))
							arrangement= "Grid";	
						else
						{
							arrangement= "Column";
							warning=warning.concat("\nThe parameter 'Plots arrangement' is set to the default value 'Column'. " +
									"Check the entry file containing the methodParameters tag.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order_by"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("name"))
							order= "by name";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("id"))
							order= "by id";
						else
						{
							// �mettre un message d'erreur et dire que c'est by id pard�faut 
							order= "by id";
							warning=warning.concat("\nThe parameter 'Order:' is set to the default value 'By id'. " +
									"Check the entry file containing the methodParameters tag.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("increasing"))
							maxMin= "increasing";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("decreasing"))
							maxMin= "decreasing";
						else
						{
							// �mettre un message d'erreur 
							warning=warning.concat("\nThe parameter 'maxMin' is set to the default value 'Ascending order'. " +
									"Check the entry file containing the methodParameters tag.");
							maxMin= "increasing";
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("use_color"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							color=false;
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) &&
								(courant.getChild("value").
										getChild("label").getValue().equals("true")))
							color=true;
						else
						{
							color=false;
							warning=warning.concat("\nThe parameter 'Use color:' is set to the default value 'false'. " +
									"Check the entry file containing the methodParameters tag.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("selected_color"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) )
							colorToUse=courant.getChild("value").
							getChild("label").getValue();
					}
					else {// we have only the previous parameters in method parameters}
						//System.out.println("id:"+courant.getAttributeValue("id"));
						warning=warning.concat("Error in the file containing the methodParameters tag: " +
								"you have not entered the correct parameters." );
						statusOK=false;
					}
				}
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;
		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}

	public String getColorToUse()
	{
		return colorToUse;
	}

	public String getPlotsOrganisation()
	{
		return arrangement;
	}
	public boolean getUniquePlot()
	{
		return uniquePlot;
	}
	public String getOrder()
	{
		return order;
	}
	public String getIncreaseDecreaseOrder()
	{
		return maxMin;
	}
	public String getWarning()
	{
		return warning;
	}

	public boolean getPrefDirection()
	{
		return preferenceDirection;
	}

}