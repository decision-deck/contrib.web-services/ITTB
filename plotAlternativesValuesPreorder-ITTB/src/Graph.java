import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;

public class Graph
{
	private static String DOT ="dot";

	private StringBuilder graph = new StringBuilder();

	public String getDotSource() {
		return graph.toString();
	}

	public void add(String line) {
		graph.append(line);
	}

	public void addln(String line) {
		graph.append(line + "\n");
	}

	public void addln() {
		graph.append('\n');
	}

	public int writeGraphToFile(byte[] img, File to)
	{
		try {
			FileOutputStream fos = new FileOutputStream(to);
			fos.write(img);
			fos.close();
		} catch (java.io.IOException ioe) { return -1; }
		return 1;
	}

	public byte[] getGraph(String dot_source, String type)
	{
		File dot;
		byte[] img_stream = null;

		try {
			dot = writeDotSourceToFile(dot_source);
			if (dot != null)
			{
				img_stream = get_img_stream(dot, type);
				if (dot.delete() == false) 
					System.err.println("Warning: " + dot.getAbsolutePath() + " could not be deleted!");
				return img_stream;
			}
			else
				return null;
		} catch (java.io.IOException ioe) { return null; }
	}

	private byte[] get_img_stream(File dot, String type)
	{
		File img;
		byte[] img_stream = null;
		Process p=null;
		try {
			img = File.createTempFile("graph_", "."+type, new File(System.getProperty("java.io.tmpdir")));

			String[] args = {DOT, "-T"+type, dot.getAbsolutePath(), "-o"+img.getAbsolutePath()};
			Runtime rt = Runtime.getRuntime();
			p = rt.exec(args);  
			p.waitFor(); 

			FileInputStream in = new FileInputStream(img.getAbsolutePath());

			img_stream = new byte[in.available()];
			in.read(img_stream);
			// Close it if we need to
			if( in != null ) in.close();

			if (img.delete() == false) 
				System.err.println("Warning: " + img.getAbsolutePath() + " could not be deleted!");

			p.destroy();

		}
		catch (java.io.IOException ioe) {
			System.err.println("Error:    in I/O processing of tempfile in java.io.tmpdir \n");
			System.err.println("       or in calling external command");
			ioe.printStackTrace();
		}
		catch (java.lang.InterruptedException ie) {
			System.err.println("Error: the execution of the external program was interrupted");
			ie.printStackTrace();
		}
		return img_stream;
	}

	/**
	 * Writes the source of the graph in a file, and returns the written file
	 * as a File object.
	 * @param str Source of the graph (in dot language).
	 * @return The file (as a File object) that contains the source of the graph.
	 */
	private File writeDotSourceToFile(String str) throws java.io.IOException
	{
		File temp;
		try {
			temp = File.createTempFile("graph_", ".dot.tmp",new File(System.getProperty("java.io.tmpdir")));
			FileWriter fout = new FileWriter(temp);  
			fout.write(str);
			fout.close();
		}
		catch (Exception e) {
			System.err.println("Error: I/O error while writing the dot source to temp file!");
			return null;
		}
		return temp;
	}

	/**
	 * Returns a string that is used to start a graph.
	 * @return A string to open a graph.
	 */
	public String start_graph(String color, String node_shape) {
		return "digraph G {\n node [shape="+node_shape+", color="+color+"];\n edge [color="+color+"];";
	}
	public String end_graph() {
		return "}";
	}

} // end of class Graph


