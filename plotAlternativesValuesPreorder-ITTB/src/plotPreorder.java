import java.io.File;
import java.util.ArrayList;

import eu.telecom_bretagne.utils.GraphViz;
import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.AlternativeValue;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesValues;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_AlternativesValues;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class plotPreorder {

	private static String warningMessage = "";

	public boolean checkFiles(Alternatives alter,AlternativesValues alternVal)
	{
		boolean result=true;
		int diff=0;

		if (alter.getIDs().size() != alternVal.size())
		{
			warningMessage += "\nPlease check the file containing the alternatives tag:" +
					"incoherence with the number of alternatives given in the file containing the alternativesValues tag.";
			return false;
		}
		ArrayList<String> list = new ArrayList<String>();
		for (AlternativeValue av: alternVal)
			list.add(av.getAlternativeValueId());

		for (String s : alter.getIDs())
		{
			if(!list.contains(s))
			{
				result=false;
				diff++;
			}
		}
		if(diff>1)
			warningMessage += "\nPlease check the file containing the alternatives tag:" +
					"incoherence with the alternatives given " +
					"in the file containing the alternativesValues tag. There are "+diff+" different alternatives.";
		if(diff==1)
			warningMessage += "\nPlease check the file containing the alternatives tag:" +
					"incoherence with the alternatives given " +
					"in the file containing the alternativesValues tag. There is one different alternative.";
		return result;		
	}
	public Alternatives getIntersectionAlternativesIDs(Alternatives alter,AlternativesValues alternVal)
	{
		Alternatives intersection = new Alternatives();
		for (AlternativeValue av: alternVal) 
			if(alter.contains(av.getAlternative()))
			{
				Alternative a = alter.get(av.getAlternativeValueId());
				if ( a.isActive() )
					intersection.add(a);
			}
		return intersection;
	}

	public ArrayList<String > getIntersectionAlternativesNames(Alternatives alter,AlternativesValues alternVal,
	                                                           ArrayList<String > listintersectionIds)
	                                                           {
		ArrayList<String > listintersectionNames=new ArrayList<String >();

		for(int i=0; i< listintersectionIds.size();i++)
		{
			if (alter.getIDs().
					contains(listintersectionIds.get(i)))
			{
				final int index=alter.getIDs().indexOf(listintersectionIds.get(i));
				if(alter.getNames().get(index)!=null)
					listintersectionNames.add(alter.getNames().get(index));
				else listintersectionNames.add(null);
			}
			else listintersectionNames.add(null);
		}

		return listintersectionNames;
	                                                           }

	protected static void echange(double [] values ,int element1, int element2)
	{
		double temp= values[element1];
		values[element1]=values[element2];
		values[element2]=temp;
	}

	protected static void exchangeIDs(String []listIds,int index1, int index2)
	{
		String temp= listIds[index1];
		listIds[index1]=listIds[index2];
		listIds[index2]=temp;
	}

	public GraphViz createGraph(String []listintersectionIds,
	                            String []listintersectionNames,
	                            String color,String node_shape,boolean namesToPlace,int size)
	{
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph(color, node_shape));

		char guillemet = '"';
		String temp[]=new String[size];
		for (int i=0;i<size;i++)
		{
			temp[i]=listintersectionIds[i];
			if(listintersectionIds[i].contains(", "))
				temp[i]=temp[i].replaceAll(", ", "");         		
		}
		if(!namesToPlace)
			for (int i=0;i<size;i++)
			{
				gv.addln(temp[i]+" [label = "+guillemet+listintersectionIds[i]+guillemet+", fontcolor="+color+"];");
			}
		else
		{
			for (int i=0;i<size;i++)
			{
				if( listintersectionNames[i]!=null)
					/*gv.addln(listintersectionIds[i]+ "[label="+
       			          listintersectionNames[i]+", fontcolor="+color+"]");*/
					gv.addln(temp[i]+ "[label="+guillemet+
					         listintersectionNames[i]+guillemet+", fontcolor="+color+"]");
				else
				{
					//gv.addln(listintersectionIds[i]+ "[fontcolor="+color+"]");
					gv.addln(temp[i]+" [label = "+guillemet+listintersectionIds[i]+guillemet+", fontcolor="+color+"];");
					warningMessage= warningMessage.concat("\n Warning: the attribute 'name' of the alternative ID: "+listintersectionIds[i]+" does " +
							"not exist. Please check the entry file containing the alternatives tag.");
				}
			}
		}

		for (int i=0;i<size-1;i++)
			gv.addln(temp[i]+ " -> "+temp[i+1]+";");

		gv.addln(gv.end_graph());

		return gv;
	}

	protected static int existsInTab( double[] alternValuesToUse,int size,double value)
	{
		int exists=-1;
		int i=0;
		while((i<size) && (exists==-1))
		{
			if (alternValuesToUse[i]==value)
				exists=i;
			i++;
		}
		return exists; 
	}

	public GraphViz generatePlot(AlternativesValues alternativesValuesToPlot,Alternatives alter,
	                             Alternatives intersection,
	                             String tempErrorMessage,
	                             String order,String color, 
	                             String node_shape, boolean namesToPlace)
	{
		/* Representation of the AlternativesValues */
		// TODO XXXXXX revoir tout �a
		double[] ySeriesAlternValues=new double[intersection.size()];

		String tempId="";
		String tempName="";

		String [] listOrderIds=new String[intersection.size()];
		String [] listOrderNames=new String[intersection.size()];
		int idx=0;
		for( Alternative a: intersection )
		{
			if (alternativesValuesToPlot.getListAlternativesIDs().
					contains(a.id()))
			{
				ySeriesAlternValues[idx] = alternativesValuesToPlot.getListAlternativesValues().
						get(alternativesValuesToPlot.getListAlternativesIDs().indexOf(a.id())).
						getRealValue();

				tempId=alternativesValuesToPlot.getListAlternativesIDs().
						get(alternativesValuesToPlot.getListAlternativesIDs().
						    indexOf(a.id()));

				listOrderIds[idx]=tempId;

				tempName=a.name();
				listOrderNames[idx]=tempName;
			}
			idx++;
		}

		// Check pour les m�mes valeurs qui seront � repr�senter dans une m�me case
		double[] alternValuesToUse=new double[intersection.size()];
		String [] listIdsToUse=new String[intersection.size()];
		String [] listNamesToUse=new String[intersection.size()];

		int index=0;
		for(int i=0;i<intersection.size();i++)
		{
			int existTest=existsInTab(alternValuesToUse,index,ySeriesAlternValues[i]);
			if(existTest==-1) // �a n'existe pas dans le tableau 
			{
				alternValuesToUse[index]=ySeriesAlternValues[i];
				listIdsToUse[index]=listOrderIds[i];
				listNamesToUse[index]=listOrderNames[i];
				index++;  
			}
			else // �a existe d�j� dans le tableau
			{
				listIdsToUse[existTest]=listIdsToUse[existTest].concat(", "+listOrderIds[i]) ;
				if(listOrderNames[i]!=null)
					listNamesToUse[existTest]=listNamesToUse[existTest].concat(", "+listOrderNames[i]);
			}
		}

		if(order.equals("increasing")) 
		{
			for(int i=0;i<index-1;i++)
			{
				for(int j=i+1;j<index;j++)
				{
					if(alternValuesToUse[j]<alternValuesToUse[i])
					{
						echange(alternValuesToUse,j,i);
						exchangeIDs(listIdsToUse,j,i);
						exchangeIDs(listNamesToUse,j,i);
					}
				}
			}
		}
		else
		{
			// Default value: decreasing order
			for(int i=0;i<index-1;i++)
			{
				for(int j=i+1;j<index;j++)
				{
					if(alternValuesToUse[j]>alternValuesToUse[i])
					{
						echange(alternValuesToUse,j,i);
						exchangeIDs(listIdsToUse,j,i);
						exchangeIDs(listNamesToUse,j,i);
					}
				}
			}
		}
		return createGraph(listIdsToUse,listNamesToUse,color,node_shape,namesToPlace, index);
	}

	protected static String getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		String color = "";
		if (black_and_white)
			color = "black";
		else
			color = colorToUse.toLowerCase();

		return color;
	}

	public static void main(String[] argv)
	{
		if ( argv.length != 6 )
		{
			System.err.println("Usage: alternatives.xml alternativesValues.xml parameters.xml alternativesValuesPreorder.dot alternativesValuesPreorder.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathAlternatives = argv[0];
		final String pathAlternativesValues = argv[1];
		final String pathOptions = argv[2];
		final File pathOutputDotFile = new File(argv[3]);
		final File pathOutputFilePlot = new File(argv[4]);
		final String pathOutputMessage = argv[5];

		String tempErrorMessage="";
		if( !new File(pathOptions).exists()||!new File(pathAlternatives).exists()||!new File(pathAlternativesValues).exists())
		{
			tempErrorMessage="Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction=new Main();

		String errMethodParameters=mainFunction.checkEntryFile(pathOptions);

		if(errMethodParameters!="")
			tempErrorMessage += "\nFatal Error: the file containing the parameters is not a valid XMCDA document.";

		String errAlternatives=mainFunction.checkEntryFile(pathAlternatives);
		if(errAlternatives!="")
			tempErrorMessage += "\nFatal Error: the file containing the alternatives tag is not a valid XMCDA document.";

		String errAlternativesValues=mainFunction.checkEntryFile(pathAlternativesValues);
		if(errAlternativesValues!="")
		{
			tempErrorMessage += "\nFatal Error: the file containing the alternativesValues tag is not a valid XMCDA document.";
		}

		if( !"".equals(errMethodParameters) || ! "".equals(errAlternatives) || ! "".equals(errAlternativesValues) )
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			//.createLogMessageForAlternativesValPlot(errAlternatives, errAlternativesValues,plot.warningMessage, tempErrorMessage)
			return;
		}
		plotPreorder plot =new plotPreorder();
		if(mainFunction.getWarningMessage()!="")
			warningMessage=warningMessage.concat(mainFunction.getWarningMessage());

		//Parsing XML file for Alternatives
		mainFunction.PrepareParsing (pathAlternatives);
		Alternatives alternativesToUse=new Alternatives();
		JDOM_Alternatives jdomAlter= new JDOM_Alternatives();
		alternativesToUse=jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if(alternativesToUse!=null){/*This is OK*/}
		else
		{
			if(tempErrorMessage.equals(""))
				tempErrorMessage="\nFatal Error: the file containing the alternatives " +
						"tag is erroneous or empty.";
			else
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
						" alternatives tag is erroneous or empty.");
		}

		if(jdomAlter.getWarning()!="")
			warningMessage= warningMessage.concat("\n"+jdomAlter.getWarning());

		//Parsing XML file for AlternativesValues
		mainFunction.PrepareParsing (pathAlternativesValues);
		AlternativesValues alternativesValuesToUse=new AlternativesValues();
		JDOM_AlternativesValues jdomAlternVal= new JDOM_AlternativesValues();
		alternativesValuesToUse=jdomAlternVal.parseAlternativesValuesXML(mainFunction.getRacine());

		if (alternativesValuesToUse == null)
		{
			tempErrorMessage += "\nFatal Error: the file containing the alternativesValues tag is erroneous or empty.";
		}
		if (jdomAlternVal.getWarningMessage() != "")
		    warningMessage = warningMessage.concat("\n" + jdomAlternVal.getWarningMessage());

		//ParseMethodParameters
		Options options=new Options();
		mainFunction.PrepareParsing (pathOptions);
		boolean statusOptions=options.parse(mainFunction.getRacine());

		if(statusOptions==false)
		{
			tempErrorMessage += "\nFatal Error: the file containing the parameters is erroneous or empty.";
		}
		if (alternativesValuesToUse == null || alternativesToUse == null || !statusOptions)
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		plot.checkFiles(alternativesToUse,alternativesValuesToUse);

		//Intersection list
		Alternatives intersection = plot.getIntersectionAlternativesIDs(alternativesToUse,alternativesValuesToUse);

		// Si l'intersection est vide on prend ce qu'on a dans la alternativesValues.xml
		if (intersection.isEmpty())
		{
			warningMessage= warningMessage.concat("\nOnly the alternatives IDs " +
					"in the file containing the alternativesValues tag were considered. " +
					"Please check your input file containing the alternatives tag.");

			for (AlternativeValue av: alternativesValuesToUse)
				intersection.add(av.getAlternative());
		}

		if (intersection.isEmpty())
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess= new XmcdaMessage();

			String errorMessage="Failure to run the algorithm because it seems that the file containing " +
					"the alternativesValues tag is empty or erroneous.";
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
		}
		else
		{
			if (options.getWarning()!="")	
				warningMessage= warningMessage.concat(options.getWarning());

			String plotTitle=options.getTitle();
			String order=options.getOrder();
			boolean color = options.getColor();
			String colorOptions= options.getColorToUse();
			if(!color && colorOptions!="Black")
				warningMessage= warningMessage.concat("\nWarning: the selected color '"+colorOptions+"' was ignored " +
						"because you have chosen a black and white plot.");

			String colorToUse = getColorFromOptions(!color, colorOptions);
			String node_shape=options.getShape();
			boolean namesToPlace=options.getNamesOrIDs();

			GraphViz gv = null;
			if( ! intersection.isEmpty() )
			{
				// Run the algorithm
				gv = plot.generatePlot(alternativesValuesToUse, alternativesToUse, intersection,
				                                                         tempErrorMessage, order,
				                                                         colorToUse, node_shape, namesToPlace);
			}
			else
			{
				tempErrorMessage += "\n We cannot run the algorithm since the alternatives list of the PROJECT is empty.\n";
			}

			// Output XMCDA File to plot the alternatives values preorder
			if(plotTitle.length() ==0)
			{
				plotTitle= "Alternatives values preorder plot";
				warningMessage += "\n The title field is empty or erroneous. The title was set " +
						"to 'Alternatives values preorder plot'. " +
						"You can check the file containing the method parameters tag.";
			}

			// Output Log Message File
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createLogMessageForAlternativesValPlot(errAlternatives,errAlternativesValues,
			                                                 warningMessage,tempErrorMessage);

			xmcdaMess.enregistre(pathOutputMessage);

			if(intersection.size()!=0 && gv!=null)
			{
				PlotPreorderFile.createFiles(intersection, gv, plotTitle, pathOutputDotFile, pathOutputFilePlot);
			}
		}
	}

}
