import java.io.File;

import eu.telecom_bretagne.utils.GraphViz;
import eu.telecom_bretagne.xmcda.Alternatives;

public class PlotPreorderFile {

	public static void createFiles(Alternatives alternatives, GraphViz gv, String commentToPut, File dotFile, File imageFile)
	{
		gv.createDotFile(dotFile);
		gv.createImage(dotFile, "png", imageFile);
	}

}
