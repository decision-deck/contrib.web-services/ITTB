import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.TreeSet;

import eu.telecom_bretagne.utils.GraphViz;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesComparisons;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;


public class transReduc {

	private String warningMessage = "";

	public boolean checkFiles(Alternatives alter,AlternativesComparisons alternComp)
	{
		boolean result=true;
		int diff=0;
		int size1=alter.getAlternativesIDs().size();
		int size2=alternComp.getListAlternativesIDs().size();

		if(size1 == size2)
		{
			ArrayList<String > list=new ArrayList<String >();
			for(int i=0; i<size2;i++)
				list.add(alternComp.getListAlternativesIDs().
				         get(i));
			for (String s : alter.getAlternativesIDs())
			{
				if(list.contains(s)){/*This is OK*/}
				else
				{
					result=false;
					diff++;
				}
			}
			if(diff>1)
				warningMessage=warningMessage.concat("\nPlease check the file containing the alternatives tag:" +
						"incoherence with the alternatives given " +
						"in the file containing the alternativesComparisons tag. There are "+diff+" different alternatives.");
			if(diff==1)
				warningMessage=warningMessage.concat("\nPlease check the file containing the alternatives tag:" +
						"incoherence with the alternatives given " +
						"in the file containing the alternativesComparisons tag. There is one different alternative.");
		}
		else 
		{
			result=false;
			warningMessage=warningMessage.concat("\nPlease check the file containing the alternatives tag:" +
					"incoherence with the number of alternatives given " +
					"in the file containing the alternativesComparisons tag.");
		}
		return result;
	}
	public ArrayList<String > getIntersectionAlternativesIDs(Alternatives alter,AlternativesComparisons alternComp)
	{
		ArrayList<String > listintersection=new ArrayList<String >();

		for (String t : alternComp.getListAlternativesIDs()) {
			if(alter.getAlternativesIDs().contains(t)) {
				listintersection.add(t);
			}
		}
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>
		(new TreeSet<String>(listintersection));
		return listintersection;
	}

	public GraphViz createGraphiz(ArrayList<String >AlternIDs,AlternativesComparisons alternComp)
	{
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph());

		for (int i=0;i<AlternIDs.size();i++)
		{
			int index_matrix_line=alternComp.getListAlternativesIDs().
					indexOf(AlternIDs.get(i));

			for (int j=0;j<alternComp.getListAlternativesIDs().size();j++)								
				if(AlternIDs.contains(alternComp.getListAlternativesIDs().get(j)))
					if(alternComp.getMatrix()[index_matrix_line][j]!=-2000)
						gv.addln(AlternIDs.get(i)+ " -> "+alternComp.getListAlternativesIDs().get(j)+";");
		}
		gv.addln(gv.end_graph());

		return gv;
	}

	public AlternativesComparisons parseDotFile(File dot)
	{
		String  initial_ID,  final_ID;
		AlternativesComparisons altComp=new AlternativesComparisons();
		try
		{
			FileInputStream fis = new FileInputStream(dot.getAbsolutePath());
			DataInputStream dis = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(dis));

			StringTokenizer st = new StringTokenizer(br.readLine());
			int i=0;
			// Cr�er un tableau � la taille du nombre de mots � extraire�
			String mot[] = new String[st.countTokens()];

			// Parcourir l'ensemble des mots � extraire
			while (st.hasMoreTokens()) {
				// Les m�moriser dans un tableau
				mot[i] = st.nextToken();
				i++;
			}

			// Enlever les ; dans les cases correspondantes
			int r=5;
			while (r<mot.length)
			{
				mot[r]=mot[r].replaceAll(";", "");
				r+=3;
			}

			// Enlever le symbole de fin }
			mot[mot.length-1]=mot[mot.length-1].replaceAll("}", "");

			int debut=3;
			int fin=5;
			while (debut< mot.length && fin <mot.length)
			{
				initial_ID=mot[debut];						   
				final_ID=mot[fin];

				if (!altComp.getListAlternativesIDs().contains( initial_ID))
					altComp.addAlternativeID(initial_ID);

				if (!altComp.getListAlternativesIDs().contains( final_ID))
					altComp.addAlternativeID(final_ID);
				//Matrix Update
				altComp.updateMatrix(altComp.getListAlternativesIDs().indexOf(initial_ID),
				                     altComp.getListAlternativesIDs().indexOf( final_ID));
				debut+=3;
				fin+=3;
			}
			br.close();
			dis.close();
			fis.close();
		} 
		catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return altComp;
	}

	//Main function
	public static void main(String[] argv)
	{
		String pathAlternatives=argv[0];
		String pathAlternativesComparisons=argv[1];

		boolean stopRunning=false;
		String errorMessage="";
		if(argv[0]==null ||argv[1]==null ||argv.length!=4||
				!new File(argv[0]).exists()||!new File(argv[1]).exists())
		{
			stopRunning =true;
			errorMessage="Error: unable to run the algorithm. Reason: one (or more) missing entry file(s). " +
					"Please check your entry files.";
		}

		if (stopRunning==false)
		{
			Main mainFunction=new Main();

			String errAlternatives=mainFunction.checkEntryFile(pathAlternatives);
			if(errAlternatives!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the alternatives " +
						"tag is not a valid XMCDA document.");

			String errAlternativesComparisons=mainFunction.checkEntryFile(pathAlternativesComparisons);
			if(errAlternativesComparisons!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the alternativesComparisons " +
						"tag is not a valid XMCDA document.");

			if(errAlternatives!=""||errAlternativesComparisons!="")
			{
				//In this case: stopRunning=true, we send the messages.xml output file
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createErrorMessage(errorMessage);			
				String pathOutputMessage=argv[3];
				xmcdaMess.enregistre(pathOutputMessage);
			}
			else
			{
				transReduc transitiveReduction = new transReduc ();

				if(mainFunction.getWarningMessage()!="")
					transitiveReduction.warningMessage=transitiveReduction.warningMessage.concat(mainFunction.getWarningMessage());

				//Parsing XML file for Alternatives
				mainFunction.PrepareParsing (pathAlternatives);
				Alternatives alternativesToUse=new Alternatives();
				JDOM_Alternatives jdomAlter= new JDOM_Alternatives();
				boolean parseAlternativesNull=false;
				alternativesToUse=jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
				if(alternativesToUse!=null)
				{
					/*This is OK*/
				}
				else
				{
					parseAlternativesNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the alternatives " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" alternatives tag is erroneous or empty.");
				}

				if(jdomAlter.getWarning()!="")
					transitiveReduction.warningMessage= transitiveReduction.warningMessage.concat("\n"+jdomAlter.getWarning());

				//Parsing XML file for AlternativesComparisons
				mainFunction.PrepareParsing (pathAlternativesComparisons);
				AlternativesComparisons alternCompaToUse=new AlternativesComparisons();
				JDOM_Alternatives_Comparisons jdomAlterComp= new JDOM_Alternatives_Comparisons();
				boolean parseAlternCompNull=false;
				alternCompaToUse=jdomAlterComp.exploreAlternativesComparisonsXML(mainFunction.getRacine());
				if(alternCompaToUse!=null)
				{
					/*This is OK*/
				}
				else
				{
					parseAlternCompNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the alternativesComparisons " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" alternativesComparisons tag is erroneous or empty.");
				}

				if(jdomAlterComp.getWarningMessage()!="")
					transitiveReduction.warningMessage= 
					transitiveReduction.warningMessage.concat("\n"+jdomAlterComp.getWarningMessage());

				if(parseAlternCompNull==false && parseAlternativesNull==false)
				{
					transitiveReduction.checkFiles(alternativesToUse,alternCompaToUse);
					//Intersection list
					ArrayList<String > listintersectionAlternIDs=
							transitiveReduction.getIntersectionAlternativesIDs(alternativesToUse,alternCompaToUse);

					// Si l'intersection est vide on prend ce qu'on a dans alternativesComparisons.xml
					if (listintersectionAlternIDs.isEmpty())
					{
						transitiveReduction.warningMessage= transitiveReduction.warningMessage.concat("\nOnly the alternatives IDs " +
								"in the file containing the alternativesComparisons tag will be considered. " +
								"Please check your input file containing the alternatives tag.");
						for (int i=0; i<alternCompaToUse.getListAlternativesIDs().size();i++)
							listintersectionAlternIDs.add(alternCompaToUse.getListAlternativesIDs().get(i));
					}

					if (listintersectionAlternIDs.isEmpty())
					{
						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();
						errorMessage= errorMessage.concat("Failure to run the algorithm because it seems that the file containing " +
								"the alternativesComparisons tag is empty or erroneous.");
						xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives,errAlternativesComparisons,
						                                                      transitiveReduction.warningMessage,errorMessage);

						String pathOutputMessage=argv[3];
						xmcdaMess.enregistre(pathOutputMessage);
					}
					else
					{
						// D�rouler l'algorithme de r�duction transtive
						String type = "dot";

						GraphViz initialGraph=transitiveReduction.createGraphiz(listintersectionAlternIDs,alternCompaToUse);

						File reduction=initialGraph.getGraph(initialGraph.getDotSource(), type);
						AlternativesComparisons alterCompAfterReduction=
								transitiveReduction.parseDotFile(reduction);

						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();

						xmcdaMess.createLogMessagePlotAlternativesComparisons
						(errAlternatives,errAlternativesComparisons,
						 transitiveReduction.warningMessage,errorMessage);

						String pathOutputMessage=argv[3];
						//String pathOutputMessage="C:/temp/messTrans.xml";
						xmcdaMess.enregistre(pathOutputMessage);

						String pathOutputFile=argv[2];
						//String pathOutputFile="C:/temp/TransReduc.xml";
						String commentToPut="Alternatives comparisons transitive reduction" ;  

						OutputTransReduc outputFile= new OutputTransReduc();
						outputFile.createFile(alterCompAfterReduction,commentToPut);
						outputFile.saveFile(pathOutputFile);
					}
				}
				else
				{
					// Output Log Message File
					XmcdaMessage xmcdaMess= new XmcdaMessage();
					xmcdaMess.createErrorMessage(errorMessage);
					String pathOutputMessage=argv[3];
					xmcdaMess.enregistre(pathOutputMessage);
				}
			}
		}
		else
		{
			//In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=argv[3];
			xmcdaMess.enregistre(pathOutputMessage);
		}
	}

}
