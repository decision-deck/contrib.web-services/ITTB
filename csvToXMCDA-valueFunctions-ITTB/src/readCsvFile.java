import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class readCsvFile
{
	private String errorMessage;
	private String warningMessage;
	private   Element racine = new Element("XMCDA");		 
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");		 
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");
	private org.jdom2.Document document = new Document(racine);

	public readCsvFile()
	{
		errorMessage="";
		warningMessage="";
	}

	protected  void createOutputFile(String[] criteriaId,String[] criteriaName, int[] numberOfpoints,
	                                 ArrayList<Float> listpoints,int length)  
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		Element criteria = new Element("criteria");
		racine.addContent(criteria);
		Attribute mcdaConcept =new Attribute("mcdaConcept","criteria");
		criteria.setAttribute(mcdaConcept);

		int indexPoints=0;

		for(int i=0;i<length;i++)
		{
			Element criterion = new Element("criterion");
			criteria.addContent(criterion);
			Attribute id =new Attribute("id", criteriaId[i]);
			criterion.setAttribute(id);
			Attribute name =new Attribute("name", criteriaName[i]);
			criterion.setAttribute(name);

			Element active=new Element ("active");
			criterion.addContent(active);
			active.setText("true");

			Element  criterionFunction=new Element ("criterionFunction");
			criterion.addContent(criterionFunction);

			Element points=new Element ("points");
			criterionFunction.addContent(points);
			int number=numberOfpoints[i];
			for(int j=0;j<number;j++)
			{
				Element point=new Element ("point");
				points.addContent(point);

				Element abscissa=new Element ("abscissa");
				point.addContent(abscissa);
				Element real=new Element ("real");
				abscissa.addContent(real);
				real.setText(Float.toString(listpoints.get(indexPoints)));
				indexPoints++;

				Element ordinate=new Element ("ordinate");
				point.addContent(ordinate);
				Element real2=new Element ("real");
				ordinate.addContent(real2);
				real2.setText(Float.toString(listpoints.get(indexPoints)));
				indexPoints++;
			}
		}
	}

	public void save(String fichier)
	{
		try
		{	        
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fichier));
		}
		catch (java.io.IOException e){}
	}

	protected static String statisticsAboutSeparator(String chaine)
	{

		int nbrVirgule=0,nbrPointVirgule=0, nbrPoint=0;
		for (int i=0;i<chaine.length();i++)
		{
			if(chaine.charAt(i)==',')
				nbrVirgule++;
			else if (chaine.charAt(i)==';')
				nbrPointVirgule++;
			else if (chaine.charAt(i)=='.')
				nbrPoint++;
			else{}
		}

		// Le s�parateur est le max
		String separator=",";
		int max=nbrVirgule;
		//Comparer � ";"
		if(nbrPointVirgule>max)
		{
			max=nbrPointVirgule;
			separator=";";
		}

		//Comparer � "."
		if(nbrPoint>max)
		{
			max=nbrPoint;
			separator=".";
		}

		return separator;
	}

	public boolean extractInformation(String path)
	{	
		boolean status=true;
		String criteriaId[] = new String[1000];
		String criteriaName[] = new String[1000];

		int[] numberOfpoints=new int[1000];
		for(int j=0;j<numberOfpoints.length;j++)
			numberOfpoints[j]=0;

		ArrayList<Float> listpoints = new ArrayList<Float>();
		int indexExtract=0;
		int indexTab=-1;
		try
		{
			BufferedReader fichier_source = new BufferedReader(new FileReader(path));
			String chaine;
			try
			{
				while(status && (chaine = fichier_source.readLine())!= null)
				{
					//Faire des statistiques pour choisir le s�parateur
					String separator=statisticsAboutSeparator(chaine);

					String[] tabChaine = chaine.split(separator);
					//J'effectue les traitements avec les donn�es contenues dans le tableau
					//La premi�re information se trouve � l'indice 0

					if(tabChaine.length!=4)
						status=false;
					if(status)
					{
						if(!tabChaine[0].isEmpty() && !tabChaine[1].isEmpty()) 
						{
							indexTab++;
							criteriaId[indexExtract]=tabChaine[0];
							criteriaName[indexExtract]=tabChaine[1];
							indexExtract++; 
							numberOfpoints[indexTab]+=1;
						}
						else
							numberOfpoints[indexTab]+=1;

						listpoints.add(Float.parseFloat(tabChaine[2].replace(',', '.')));
						listpoints.add(Float.parseFloat(tabChaine[3].replace(',', '.')));
					}
				}
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try
			{
				fichier_source.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Le fichier est introuvable !");
		}

		if(indexExtract>0)
			createOutputFile(criteriaId, criteriaName,numberOfpoints,listpoints ,indexExtract)  ;
		else
			status=false;
		return status;
	}

	public static void main(String[] argv)
	{
		readCsvFile parseCSV=new readCsvFile();
		boolean stopRunning = false;

		if (!new File(argv[0]).exists()|| argv.length != 3) 
			stopRunning = true;

		if (stopRunning)
			parseCSV.errorMessage =parseCSV.errorMessage.concat("Error: unable to run the algorithm: "
					+ "the entry file may be inexistant. Check your execution command. ");

		if (stopRunning == false)
		{
			String pathCSV = argv[0];
			boolean check =parseCSV.extractInformation(pathCSV);	
			if(!check)
			{
				parseCSV.errorMessage =parseCSV.errorMessage.concat("The csv entry file is empty or erroneous.");
				// StopRunning =true
				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createErrorMessage(parseCSV.getError());
				String pathOutputMessage = argv[argv.length-1];
				xmcdaMess.save(pathOutputMessage);
			}
			else
			{
				// Output XMCDA File for the CategoriesProfiles
				String pathOutputFile = argv[argv.length - 2];
				parseCSV.save(pathOutputFile);			

				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createLogMessage(parseCSV.getError(), parseCSV.getWarning());
				String pathOutputMessage = argv[argv.length - 1];
				xmcdaMess.save(pathOutputMessage);
			}
		}// StopRunning =false
		else
		{
			// StopRunning =true
			// Output Log Message File
			OutputMessage xmcdaMess = new OutputMessage();
			xmcdaMess.createErrorMessage(parseCSV.getError());
			String pathOutputMessage = argv[argv.length-1];
			xmcdaMess.save(pathOutputMessage);
		}
	}

	public String getError()
	{
		return errorMessage;
	}

	public String getWarning()
	{
		return warningMessage;
	}
}
