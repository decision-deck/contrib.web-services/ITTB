﻿================================================================
 The web service « csvToXMCDA-valueFunctions-ITTB »
================================================================

How to run the java program in command line?
===================================================
::

java –Djava.awt.headless =true -jar csvToXMCDA-valueFunctions-ITTB.jar valueFunctions.csv valueFunctions.xml messages.xml 

where:

- The entry file `` valueFunctions.csv ``
- The output file `` valueFunctions.xml `` is an XMCDA compliant file
- The output file ``messages.xml`` 

How to run the tests?
=====================

You have to provide the entry file `` valueFunctions.csv `` (check everything is OK)
