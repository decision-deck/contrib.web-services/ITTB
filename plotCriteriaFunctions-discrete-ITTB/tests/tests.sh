#! /bin/sh

JAR=../build/plotValueFunctions-ITTB.jar

OUT=out
TMPF=$("tempfile")

unset DISPLAY

for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar $JAR $IN/criteria.xml $IN/valueFunctions.xml $IN/parameters.xml $OUT/valueFunctions.png $OUT/messages.xml
  diff -Bqrw out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
    #meld out out${IN#in} # uncomment 'unset DISPLAY' above as well!
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done
