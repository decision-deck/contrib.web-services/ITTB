import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.Point;

public class ParseFileCriterionFunction
{

	private String warning = "";

	public Element getRacine(String path)
	{
		org.jdom2.Document document = null;
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch (Exception e)
		{}

		Element racine = document.getRootElement();
		return racine;
	}


	public Criteria exploreCriterionFunction(Element racine)
	{
		if (racine.getChild("criteria") == null)
			return null;
		Element criteria = racine.getChild("criteria");
		if (criteria.getChildren("criterion") == null)
			return null;
		Iterator<Element> i = criteria.getChildren("criterion").iterator();

		Criteria         criteriaToTest = new Criteria();
		Criterion criter = null;

		while ( i.hasNext() )
		{
			criter = null;
			Element courant = (Element) i.next();
			if (courant.getAttributeValue("id") != null && courant.getAttributeValue("name") != null)
			{

				if (courant.getChild("active") != null && courant.getChild("active").getValue().equals("false"))
				{
					// Le criterion est inactif, on l'ignore
					warning = warning.concat("The criterion " + courant.getAttributeValue("id")
					                         + " was ignored " + "because it is not active: active=false.\n");
				}
				else
				{
					criter = new Criterion(courant.getAttributeValue("id"), courant.getAttributeValue("name"));
					criteriaToTest.add(criter);
				}
			}
			else if (courant.getAttributeValue("id") != null && courant.getAttributeValue("name") == null)
			{
				if (courant.getChild("active") != null && courant.getChild("active").getValue().equals("false"))
				{
					// Le criterion est inactif, on l'ignore
					warning = warning.concat("The criterion " + courant.getAttributeValue("id")
					                         + " was ignored " + "because it is not active: active=false.\n");
				}
				else
				{
					criter = new Criterion(courant.getAttributeValue("id"));
					criteriaToTest.add(criter);
				}
			}
			else
			{
				// Warning, on n'a pas d'ID pour le criterion
			}

			if (courant.getChild("criterionFunction") != null)
			{
				Element criterionFunction = courant.getChild("criterionFunction");
				if (criterionFunction.getChild("points") != null)
				{
					Element points = criterionFunction.getChild("points");

					if (points.getChildren("point") != null)
					{
						Iterator<Element> j = points.getChildren("point").iterator();

						while ( j.hasNext() && criter != null )
						{
							Element courantPoint = (Element) j.next();
							if (courantPoint.getChild("abscissa") != null
									&& courantPoint.getChild("ordinate") != null)
							{
								if (courantPoint.getChild("abscissa").getChild("real") != null
										&& courantPoint.getChild("ordinate").getChild("real") != null)
								{
									if (courantPoint.getChild("abscissa") != null
											&& courantPoint.getChild("ordinate") != null)
									{
										if (courantPoint.getChild("abscissa").getChild("real") != null
												&& courantPoint.getChild("ordinate").getChild("real") != null)
										{
											if (courantPoint.getChild("abscissa").getValue() != null
													&& courantPoint.getChild("ordinate").getValue() != null)
											{

												Point p = new Point(Float.parseFloat(courantPoint
												                                     .getChild("abscissa").getValue()),
												                                     Float.parseFloat(courantPoint
												                                                      .getChild("ordinate").getValue()));
												criter.getCriterionFunction().addPoint(p);
											}
											else
											{
												warning = warning
														.concat("\n Error in the file containing the criterion function tag."
																+ " The file is not well-formed: the value for an 'abscissa' or an 'ordinate' "
																+ "tag must IS EMPTY. Please check the corresponding "
																+ "criterion: ("
																+ courant.getAttributeValue("id") + ").");
											}
										}
										else
										{
											warning = warning
													.concat("\n Error in the file containing the criterion function tag."
															+ " The file is not well-formed: the value for an 'abscissa' or an 'ordinate' "
															+ "tag must be REAL. Please check the corresponding "
															+ "criterion: ("
															+ courant.getAttributeValue("id") + ").");
										}
									}
									else
									{
										warning = warning
												.concat("\n Error in the file containing the criterion function tag."
														+ " The file is not well-formed and an 'abscissa' "
														+ "or an 'ordinate' tag is required for criterion: ("
														+ courant.getAttributeValue("id") + ").");
									}
								}
								else
								{
									// Warning la valeur n'est pas un r�el!!!
									warning = warning
											.concat("\n Error in the file containing the criterion function tag."
													+ " The 'value' tag of an abscissa or ordinate is EMPTY or "
													+ "the type is not correct (only 'real' is accepted)."
													+ " We have eliminated the corresponding point for criterion: "
													+ courant.getAttributeValue("id") + ".");
								}
							}
						}
					}
					else
					{
						// remove the criterion Id because no point Tags
						criteriaToTest.remove(criter);
						warning = warning
								.concat("\nThe criterion ("
										+ courant.getAttributeValue("id")
										+ ") was eliminated. There is nothing to plot since no 'point' tag is available."
										+ " If necessary, check the file containing the criterionFunction tag.");
					}
				}
				else
				{
					// remove the criterion Id because no points Tag
					criteriaToTest.remove(criter);
					warning = warning
							.concat("\nThe criterion (" + courant.getAttributeValue("id")
							        + ") was eliminated because the corresponding 'points' tag is empty."
							        + " If necessary, check the file containing the criterionFunction tag.");
				}
			}
			else
			{
				// remove the criterion Id because no criterionFunction Tag
				criteriaToTest.remove(criter);

				warning = warning
						.concat("\nThe criterion ("
								+ courant.getAttributeValue("id")
								+ ") was eliminated because the corresponding 'criterion function' tag is empty."
								+ " If necessary, check the file containing the criterionFunction tag.");
			}

			if (criter.getCriterionFunction().getPoints().size() == 0)
			{
				criteriaToTest.remove(criter);

				warning = warning
						.concat("\nThe criterion (" + courant.getAttributeValue("id")
						        + ") was eliminated because no points were finally added after parsing "
						        + "the file containing the criterion function tag."
						        + " If necessary, check this entry file.");
			}
		}

		if (criteriaToTest.getCriteriaIDs().size() == 0)
			return null;
		else
			return criteriaToTest;
	}

	public String getWarning()
	{
		return warning;
	}

}
