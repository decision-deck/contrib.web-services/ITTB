import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class methodParameters
{
	static org.jdom2.Document document;

	private boolean          color;

	private boolean          linearInterpolation;

	private String           colorToUse;

	private String           arrangement;

	private boolean          uniquePlot;

	private boolean          addBars;

	public methodParameters()
	{
		color = false; // the default plot is a color plot
		linearInterpolation = true;
		addBars = false;
		uniquePlot = true;
		colorToUse = "";
		arrangement = "By column";
	}

	public Element getRacine(String path)
	{
		org.jdom2.Document document = null;
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch (Exception e)
		{}

		Element racine = document.getRootElement();

		return racine;
	}

	public boolean parse(Element racine)
	{
		boolean statusOK = true;
		if (racine.getChild("methodParameters") == null)
			return false;
		Element methodParameters = racine.getChild("methodParameters");

		if (methodParameters.getChildren("parameter") == null)
			return false;
		Iterator<Element> i = methodParameters.getChildren("parameter").iterator();

		while ( i.hasNext() )
		{
			Element courant = (Element) i.next();

			if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("unique_plot"))
			{
				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("true") ))
					uniquePlot = true;

				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("false") ))
					uniquePlot = false;
				else
				{
					// Default value
					uniquePlot = true;
				}
			}

			if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("plots_display"))
			{
				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("line") ))
					arrangement = "By line";
				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("column") ))
					arrangement = "By column";
				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("grid") ))
					arrangement = "By square";
				else
				{
					arrangement = "By column";
				}
			}
			else if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("linear_interpolation"))
			{
				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("true") ))
					linearInterpolation = true;
				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("false") ))

					linearInterpolation = false;
				else
				{
					linearInterpolation = false;
				}
			}
			else if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("vertical_lines"))
			{
				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("true") ))
					addBars = true;
				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("false") ))
					addBars = false;
				else
				{
					addBars = false;
				}
			}
			else if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("use_color"))
			{
				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("false") ))
					color = false;
				else if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null )
						&& ( courant.getChild("value").getChild("label").getValue().equals("true") ))
					color = true;
				else
				{
					color = false;
				}
			}
			else if (courant.getAttributeValue("id") != null
					&& courant.getAttributeValue("id").equals("selected_color"))
			{

				if (( courant.getChild("value") != null )
						&& ( courant.getChild("value").getChild("label") != null ))
					colorToUse = courant.getChild("value").getChild("label").getValue();
			}
			else
			{// we have only the previous parameters in method parameters}
			}
		}
		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}

	public boolean getInterpolation()
	{
		return linearInterpolation;
	}

	public String getColorToUse()
	{
		return colorToUse;
	}

	public String getPlotsOrganisation()
	{
		return arrangement;
	}

	public boolean getUniquePlot()
	{
		return uniquePlot;
	}

	public boolean getAddBars()
	{
		return addBars;
	}

}
