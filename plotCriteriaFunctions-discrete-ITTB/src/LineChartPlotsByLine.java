import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import eu.telecom_bretagne.xmcda.Criterion;


/**
 * A simple demonstration application showing how to create a line chart using data from a
 * {@link CategoryDataset}.
 */
public class LineChartPlotsByLine  {

	private JFreeChart chart;

	public LineChartPlotsByLine(final String title, String chartTitle, String XaxisLabel, Criterion criterion,
	                            boolean interpolation, Color colorToUse, boolean addBars)
	{
		final XYDataset dataset = createDataset(criterion,addBars);
		chart = createChart(dataset, chartTitle, XaxisLabel, interpolation, colorToUse, criterion, addBars);
	}

	private XYDataset createDataset(Criterion criter,boolean addBars){

		final XYSeries series1 = new XYSeries("");
		int numberOfPoints= criter.getCriterionFunction().getPoints().size();

		final XYSeries[] series = new XYSeries[numberOfPoints];
		for (int j=0;j<numberOfPoints;j++)
			series[j] = new XYSeries(j);

		for (int j=0;j<numberOfPoints;j++)
		{
			float x=criter.getCriterionFunction().getPoints().get(j).getAbscissa();
			float y=criter.getCriterionFunction().getPoints().get(j).getOrdinate();
			//tab[indexCriter].add(x,y);
			series1.add(x,y);

			series[j].add(x,0);
			series[j].add(x,y);
		}
		final XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series1);

		if(addBars==true)
		{
			for (int j=0;j<numberOfPoints;j++)
				dataset.addSeries(series[j]);
		}

		return dataset;
	}

	/**
	 * Creates a sample chart.
	 * 
	 * @param dataset  a dataset.
	 * 
	 * @return The chart.
	 */
	private JFreeChart createChart(final XYDataset dataset,
	                               String chartTitle, String XaxisLabel,boolean interpolation,
	                               Color colorToUse,Criterion criterion,boolean addBars) {
		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart(
		                                                        chartTitle,       // chart title
		                                                        XaxisLabel,                    // domain axis label
		                                                        "",                   // range axis label
		                                                        dataset,                   // data
		                                                        PlotOrientation.VERTICAL,  // orientation
		                                                        false,                      // include legend
		                                                        true,                      // tooltips
		                                                        false                      // urls
				);


		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		Font font = new Font("Arial", Font.BOLD, 12);

		final ValueAxis domainAxis = plot.getDomainAxis();
		domainAxis.setTickLabelFont(font);
		domainAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));

		// customize the range axis...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setTickLabelFont(font);
		rangeAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));


		// Customize the renderer...
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

		renderer.setSeriesPaint(0, colorToUse);
		renderer.setSeriesShape(0, new Ellipse2D.Double(-3.0, -3.0, 6.0, 6.0));


		if (interpolation==true)
		{
			renderer.setSeriesLinesVisible(0,true);
			renderer.setSeriesShapesVisible(0, true);
		}
		else
		{
			renderer.setSeriesLinesVisible(0,false);
			renderer.setSeriesShapesVisible(0, true);
		}

		if(addBars==true)
		{
			for (int j=0;j<criterion.getCriterionFunction().getPoints().size();j++)
			{
				renderer.setSeriesLinesVisible(j+1,true);
				renderer.setSeriesShapesVisible(j+1, false);
				renderer.setSeriesPaint(j+1, colorToUse);
			}
		}

		plot.setRenderer(renderer);
		return chart;
	}

	public JFreeChart getChart()
	{
		return chart;
	}

	public void saveChart(OutputStream outputStream)
	{
		try
		{
			ChartUtilities.writeChartAsPNG(outputStream, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveChart(String filepath)
	{
		File file= new File(filepath);
		try
		{
			ChartUtilities.saveChartAsPNG(file, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
