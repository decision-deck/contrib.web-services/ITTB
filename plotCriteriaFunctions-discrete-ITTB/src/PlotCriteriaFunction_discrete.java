import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaValues;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.Point;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class PlotCriteriaFunction_discrete
{

	private static final String BY_SQUARE = "By square";
	private static final String BY_LINE = "By line";
	private static final String BY_COLUMN = "By column";
	private String warningMessage = "";

	private static String title(Criterion criterion)
	{
		if (criterion.name() != null)
			return criterion.name() + " (" + criterion.id() + ")";
		return criterion.id();
	}

	public boolean checkFiles(Criteria criter,Criteria criteriaValFunc)
	{
		boolean result=true; // Coherent lists
		int criterCommun=0;
		if(criter.getCriteriaIDs().size()!=criteriaValFunc.getCriteriaIDs().size())
		{
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the number of criteria given " +
					"in the file containing the criterionFunction tag.");
			return false;
		}

		for(int i=0;i<criteriaValFunc.getCriteriaIDs().size();i++)
		{
			if(criteriaValFunc.getCriteriaIDs().contains(criter.getCriteriaIDs().get(i)))
			{
				// OK
				criterCommun++;
			}
			else
			{
				result=false;
			}
		}
		if(criterCommun>1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the criteria given " +
					"in the file containing the criterionFunction tags. There are only "
					+criterCommun+" identical criteria in both files.");

		if(criterCommun==1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the criteria given " +
					"in the file containing the criterionFunction tags. " +
					"There is one identical criterion in both files.");

		return result;		
	}

	public Criteria getIntersectionCriteria(Criteria criteria,Criteria criteriaValFunc)
	{
		ArrayList<String > listintersection=new ArrayList<String >();
		
		for(int i=0; i<criteria.getCriteriaIDs().size();i++)
		{
			final String id = criteria.getCriteriaIDs().get(i);
			if (!listintersection.contains(id))
				listintersection.add(id);
		}
		listintersection.retainAll(criteriaValFunc.getCriteriaIDs());
		// listintersection contains the ids common to both sets, respecting the same order as in criteria
		
		Criteria criteriaintersection=new Criteria();

		for (int i=0; i< listintersection.size();i++)
		{
			Criterion criterion=new Criterion (listintersection.get(i));
			criteriaintersection.add(criterion);
			
			final int criterion_idx = criteriaValFunc.getCriteriaIDs().indexOf(listintersection.get(i));
			final ArrayList<Point> points = criteriaValFunc.getListCriteria().get(criterion_idx).getCriterionFunction()
			        .getPoints();
			final int numberOfPoints = points.size();
			for (int j=0; j<numberOfPoints;j++)
			{
				float x = points.get(j).getAbscissa();
				float y = points.get(j).getOrdinate();
				criterion.getCriterionFunction().addPoint(new Point(x,y));
			}
		}

		// Si l'intersection est vide on prend ce qu'on a dans la valueFunctions.xml
		if (criteriaintersection.getCriteriaIDs().size()==0)
		{
			warningMessage=warningMessage.concat("\nOnly the criteriaIDs " +
					"in the file containing the criterionFunction tag will be considered. " +
					"Please check your input file containing the criteria tag."); 		 
			criteriaintersection=criteriaValFunc; 		
		}

		return criteriaintersection;	
	}

	public Criteria getCriteriaFromValueFunctions(Criteria criteriaValFunc)
	{
		ArrayList<String > listintersection=new ArrayList<String >();
		for(int i=0; i<criteriaValFunc.getCriteriaIDs().size();i++)
		{
			final String id = criteriaValFunc.getCriteriaIDs().get(i);
			if (!listintersection.contains(id))
				listintersection.add(id);
		}

		Criteria criteriaintersection=new Criteria();

		for (int i=0; i< listintersection.size();i++)
		{
			Criterion criterion=new Criterion (listintersection.get(i));
			criteriaintersection.add(criterion);

			final int criterion_idx = criteriaValFunc.getCriteriaIDs().indexOf(listintersection.get(i));
			final ArrayList<Point> points = criteriaValFunc.getListCriteria().get(criterion_idx).getCriterionFunction()
			        .getPoints();
			final int numberOfPoints = points.size();
			for (int j=0; j<numberOfPoints;j++)
			{
				float x = criteriaValFunc.getListCriteria().
						get(i).getCriterionFunction().getPoints().get(j).getAbscissa();
				float y = criteriaValFunc.getListCriteria().
						get(i).getCriterionFunction().getPoints().get(j).getOrdinate();

				criterion.getCriterionFunction().addPoint(new Point(x,y));
			}
		}
		return criteriaintersection;	
	}

	public void generatedPlot(Criterion criterion, boolean interpolation, Color color, String tempErrorMessage,
	                          boolean addBars, OutputStream outputStream)
	{
		LineChart line = new LineChart("", title(criterion), "", criterion, interpolation, color, addBars);
		line.saveChart(outputStream);
	}

	public void generatePlotByColumn(Criteria criteria, boolean interpolation, Color color,
	                                 String tempErrorMessage, int size, boolean addBars, OutputStream outputStream)
	{
		ByteArrayOutputStream[] chart_png_image = new ByteArrayOutputStream[size];

		int index = 0;
		for (Criterion criterion : criteria) {
			chart_png_image[index] = new ByteArrayOutputStream();
			LineChartPlotsByLine plotsInLine = new LineChartPlotsByLine("", title(criterion), "", criterion,
					interpolation, color, addBars);
			plotsInLine.saveChart(chart_png_image[index]);
			index++;
		}

		ImageGenerated imageConvert = new ImageGenerated();

		InputStream[] inputStream = new InputStream[size];
		for (int i = 0; i < size; i++)
			inputStream[i] = new ByteArrayInputStream(chart_png_image[i].toByteArray());

		imageConvert.convertImageByColumn(inputStream, "png", size, outputStream);
		if (imageConvert.getImageErrorMessage() != "")
			tempErrorMessage = tempErrorMessage.concat(imageConvert.getImageErrorMessage());
	}

	public void generatePlotByLine(Criteria criteria, boolean interpolation, Color color,
	                                  String tempErrorMessage, int size,boolean addBars, OutputStream outputStream)
	{
		ByteArrayOutputStream[] chart_png_image = new ByteArrayOutputStream[size];
		int index = 0;
		for (Criterion criterion : criteria) {
			chart_png_image[index] = new ByteArrayOutputStream();
			LineChartPlotsByLine plotsInLine = new LineChartPlotsByLine("", title(criterion), "", criterion,
					interpolation, color, addBars);
			plotsInLine.saveChart(chart_png_image[index]);
			index++;
		}

		ImageGenerated imageConvert = new ImageGenerated();
		InputStream[] inputStream = new InputStream[size];
		for (int i = 0; i < size; i++)
			inputStream[i] = new ByteArrayInputStream(chart_png_image[i].toByteArray());

		imageConvert.convertImageByLine(inputStream, "png", size, outputStream);
		if (imageConvert.getImageErrorMessage() != "")
			tempErrorMessage = tempErrorMessage.concat(imageConvert.getImageErrorMessage());
	}

	public void generatePlotBySquare(Criteria criteria, boolean interpolation, Color color,
	                                 String tempErrorMessage, int size, boolean addBars,
	                                 OutputStream outputStream)
	{
		ByteArrayOutputStream[] chart_png_image = new ByteArrayOutputStream[size];
		int index = 0;
		for (Criterion criterion : criteria) {
			chart_png_image[index] = new ByteArrayOutputStream();
			LineChartPlotsByLine plotsInLine = new LineChartPlotsByLine("", title(criterion), "", criterion,
					interpolation, color, addBars);
			plotsInLine.saveChart(chart_png_image[index]);
			index++;
		}
		ImageGenerated imageConvert = new ImageGenerated();
		InputStream[] inputStream = new InputStream[size];
		for (int i = 0; i < size; i++)
			inputStream[i] = new ByteArrayInputStream(chart_png_image[i].toByteArray());
		imageConvert.convertImageBySquare(inputStream, "png", size, outputStream);
		if (imageConvert.getImageErrorMessage() != "")
			tempErrorMessage = tempErrorMessage.concat(imageConvert.getImageErrorMessage());
	}

	protected static Color getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		if (black_and_white)
			return Color.black;

		Color c = null;
		try
		{
			c = (Color)Color.class.getField(colorToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c==null )
			return Color.black;
		return c;
	}

	public static void main(String[] argv)
	{
		String errorMessage="";
		if(argv.length!=5)
		{
			System.err.println("Usage: criteria.xml criteriaFunctions.xml parameters.xml criteriaFunctions.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.err.println("       criteria.xml may not exist.");
			System.exit(-1);
		}

		final String in_criteria = new File(argv[0]).exists() ? argv[0] : null;
		final String in_criteriaFunctions = argv[1];
		final String in_parameters = argv[2];
		final String out_criteriaFunctionsPlot = argv[3];
		final String out_messages = argv[4];

		if( !new File(in_parameters).exists() || !new File(in_criteriaFunctions).exists() )
		{
			errorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s)."
					+ "Please check your entry files.";
			XmcdaMessage.writeErrorMessage(errorMessage, out_messages);
			return;
		}
		
		String pathMethodParameters=in_parameters;
		String pathValueFunctions=in_criteriaFunctions;

		Main mainFunction=new Main();

		String errMethodParameters=mainFunction.checkEntryFile(pathMethodParameters);
		if(errMethodParameters!="")
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the parameters " +
					"is not a valid XMCDA document.");

		String errCriteria="";
		if (in_criteria!=null && new File(in_criteria).exists())
		{
			String pathCriteria=in_criteria;
			errCriteria=mainFunction.checkEntryFile(pathCriteria);
			if(errCriteria!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteria " +
						"is not a valid XMCDA document.");
		}

		String errValueFunctions=mainFunction.checkEntryFile(pathValueFunctions);
		if ( errValueFunctions!="" )
		{
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteria " +
					"functions is not a valid XMCDA document.");
		}

		if ( errMethodParameters!="" || errCriteria!="" || errValueFunctions!="" )
		{
			XmcdaMessage.writeErrorMessage(errorMessage, out_messages);
			return;
		}
		PlotCriteriaFunction_discrete plotValFunc =new PlotCriteriaFunction_discrete();
		if(mainFunction.getWarningMessage()!="")
			plotValFunc.warningMessage=plotValFunc.warningMessage.concat(mainFunction.getWarningMessage());

		//Parsing XML file for Criteria
		Criteria criteria = new Criteria();
		handling_criteria_parameter:
			if ( in_criteria != null )
			{
				if ( !new File(in_criteria).exists() )
				{
					plotValFunc.warningMessage += "\nCriteria argument specified but not present";
					break handling_criteria_parameter;
				}
				String pathCriteria=in_criteria; // c'est criteria
				mainFunction.PrepareParsing (pathCriteria);
				JDOM_Criteria jdomCriter= new JDOM_Criteria();
				criteria=jdomCriter.parseCriteriaXML(mainFunction.getRacine());
				if(criteria==null)
				{
					errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteria is erroneous or empty.");
				}

				if(jdomCriter.getWarning()!="")
					plotValFunc.warningMessage= plotValFunc.warningMessage.concat("\n"+jdomCriter.getWarning());
			}

		//Parsing XML file for ValueFunctions
		ParseFileCriterionFunction parsing= new ParseFileCriterionFunction ();
		mainFunction.PrepareParsing (pathValueFunctions);
		Criteria criteriaFromValueFunctions=new Criteria();
		criteriaFromValueFunctions=parsing.exploreCriterionFunction(mainFunction.getRacine());

		if(criteriaFromValueFunctions==null)
		{
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteria functions is erroneous or empty.");
		}

		if(parsing.getWarning()!="")
			plotValFunc.warningMessage=plotValFunc.warningMessage.concat("\n"+parsing.getWarning());


		//ParseMethodParameters
		methodParameters methodParam=new methodParameters();
		mainFunction.PrepareParsing (pathMethodParameters);

		boolean statusParseMethod=methodParam.parse(mainFunction.getRacine());

		if(statusParseMethod==false)
		{
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the parameters is erroneous or empty.");
		}

		if ( criteriaFromValueFunctions==null || criteria==null || ! statusParseMethod )
		{
			XmcdaMessage.writeErrorMessage(errorMessage, out_messages);
			return;
		}

		//Check the files after the parsing 
		//plotValFunc.checkFiles(criteriaToUse,criteriaFromValueFunctions);

		//Intersection list
		Criteria intersectionCriteria= new Criteria();

		if( in_criteria != null )
			intersectionCriteria=plotValFunc.getIntersectionCriteria(criteria,criteriaFromValueFunctions);
		else
			intersectionCriteria=plotValFunc.getCriteriaFromValueFunctions(criteriaFromValueFunctions);

		boolean color =methodParam.getColor();
		boolean linearInterpolation=methodParam.getInterpolation();
		String colorToUse=methodParam.getColorToUse();
		String plotsOrganisation=methodParam.getPlotsOrganisation();
		boolean uniquePlot=methodParam.getUniquePlot();
		boolean addBars=methodParam.getAddBars();

		Color colorPlot=Color.black;
		// Merci Sébastien !!!!
		colorPlot = getColorFromOptions(!color, colorToUse);


		if ( ! uniquePlot )
			plotsOrganisation = BY_COLUMN;

		if ( linearInterpolation )
			addBars = false;

		int size = intersectionCriteria.getListCriteria().size();

		if(intersectionCriteria.size()!=0 )
		main:
		{
			//Run the algorithm
			if(!uniquePlot)
			{
				final String template = out_criteriaFunctionsPlot.substring(0, out_criteriaFunctionsPlot.indexOf('.')) + "-%02d.png";
				//ManyImages
				//La seule possibilité de faire c'est By Column
				int idx = 1;
				for (Criterion criterion: intersectionCriteria)
				{
					final FileOutputStream pngFOS;
					final File pngFile = new File(String.format(template, idx++));
					try {
						pngFOS = new FileOutputStream(pngFile);
					}
					catch (IOException ioe) {
						errorMessage=errorMessage.concat("\nFatal Error: could not create output file "+pngFile.toString());
						continue;
					}
					plotValFunc.generatedPlot(criterion, linearInterpolation, colorPlot, errorMessage, addBars,
					                          pngFOS);
				}
			}
			else
				//(uniquePlot==true)
				// J'ai le choix entre "By column" "By line" "By square"
			{
				final FileOutputStream pngFOS;
				try {
					pngFOS = new FileOutputStream(new File(out_criteriaFunctionsPlot));
				} catch (IOException ioe) {
					errorMessage = errorMessage.concat("\nFatal Error: could not create output file " + out_criteriaFunctionsPlot);
					break main;
				}
				//On a le choix entre plusieurs 
				if(plotsOrganisation.equals(BY_COLUMN))
				{
					plotValFunc.generatePlotByColumn(intersectionCriteria, linearInterpolation, colorPlot,
					                                 errorMessage, size, addBars, pngFOS);
				}
				else if(plotsOrganisation.equals(BY_LINE))
				{
						plotValFunc.generatePlotByLine(intersectionCriteria, linearInterpolation, colorPlot,
						                               errorMessage, size, addBars, pngFOS);
				}
				else if(plotsOrganisation.equals(BY_SQUARE))
				{
						plotValFunc.generatePlotBySquare(intersectionCriteria, linearInterpolation, colorPlot,
						                                 errorMessage, size, addBars, pngFOS);
				}
			}
		}
		else
		{
			errorMessage=errorMessage.concat("\n We cannot run the algorithm " +
					"since the criteria list of the PROJECT is empty.\n");
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess= new XmcdaMessage();
		xmcdaMess.createLogMessagePlotCriteriaFunctions_discrete(errCriteria, errValueFunctions,
		                                                         plotValFunc.warningMessage, errorMessage);

		String pathOutputMessage=out_messages;
		xmcdaMess.enregistre(pathOutputMessage);

	}

}
