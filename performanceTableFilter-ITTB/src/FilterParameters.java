import org.jdom2.Element;

import eu.telecom_bretagne.xmcda.CriteriaValues;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.CriterionValue;
import eu.telecom_bretagne.xmcda.Interval;


public class FilterParameters
{
	private String warning = "";
	private CriteriaValues intervals = new CriteriaValues();

	public boolean parse(Element racine)
	{
		Float lowerBound, upperBound;
		if (racine.getChild("methodParameters")==null)
			return false;
		Element methodParameters = racine.getChild("methodParameters");
		if(methodParameters.getChild("parameter")==null)
			return false;
		Element parameter = methodParameters.getChild("parameter");
		if(parameter.getChildren("value")==null || parameter.getChildren("value").size()==0 || 
				!parameter.getAttributeValue("id").equals("criteria_filter"))
			return false;

		for (Element courant: parameter.getChildren("value"))
		{
			if (courant.getAttribute("id")==null)
			{
				/*
				 //(courant.getChild("value")= null)
				 // Indiquer que je n'ai pas de upper Bound
				 upperBound[listCriter.indexOf(courant.getAttributeValue("id"))]=-1;
				 // Indiquer que je n'ai pas de lower Bound
				 lowerBound[listCriter.indexOf(courant.getAttributeValue("id"))]=-1;
				// Message pour ignorer le filtrage pour le crit�re
		 			warning=warning.concat("\nWarning: The filter for criterion ("+courant.getAttributeValue("id")+")" +
		 					" was ignored. Check your entry file containing the method parameters tag.");
				 */
				continue;
			}
			Criterion c = new Criterion(courant.getAttributeValue("id"));
			if (intervals.getCriteriaIDs().contains(c))
			{
				// Error message
				warning=warning.concat("\nWarning: duplicate criterionID("+courant.getAttributeValue("id")+
						") in the entry file containing the method parameters tag. Only the first provided filter is considered.");
				continue;
			}
			if (courant.getChild("interval")==null) 
			{
				// Message pour ignorer le filtrage pour le crit�re
				warning=warning.concat("\nWarning: The filter for criterion ("+courant.getAttributeValue("id")+")" +
						" was ignored. Check your entry file containing the method parameters tag.");
				continue;
			}
			
			lowerBound = null;
			final Element xml_lowerBound = courant.getChild("interval").getChild("lowerBound");
			if (xml_lowerBound != null)
				if (xml_lowerBound.getChild("real")!=null)
					lowerBound = Float.parseFloat(xml_lowerBound.getChild("real").getValue());

			upperBound = null;
			final Element xml_upperBound = courant.getChild("interval").getChild("upperBound");
			if (xml_upperBound != null)
				if (xml_upperBound.getChild("real")!=null)
					upperBound = Float.parseFloat(xml_upperBound.getChild("real").getValue());

			intervals.add(new CriterionValue(c, new Interval(lowerBound, upperBound)));
		}

		// Si on n'a rien rajout� au final: (�a peut arriver)
		if (intervals.size()==0)
		{
			warning=warning.concat("\nWarning: an error occured while " +
					"trying to get the transformation for each criterion. " +
					"Check the entry file containing the method parameters tag. It seems to be empty or erroneous.");
			return false;
		}
		return true;
	}

	public CriteriaValues getIntervals()
	{
		return intervals;
	}
	
	public String getWarning()
	{
		return warning;
	}

}
