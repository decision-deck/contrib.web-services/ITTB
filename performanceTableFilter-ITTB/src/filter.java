import java.io.File;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaValues;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.CriterionValue;
import eu.telecom_bretagne.xmcda.Interval;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_PerformanceTable;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class filter
{
	public static PerformanceTable filterCriteria(PerformanceTable performanceTable, CriteriaValues intervals)
	{
		PerformanceTable filtered = new PerformanceTable();

		for (Alternative alternative: performanceTable.alternatives())
		{
			for (Criterion criterion: performanceTable.criteria())
			{
				CriterionValue crit_interval = intervals.getCriterionValue(criterion);
				final float value = performanceTable.getValue(alternative, criterion);
				if ( crit_interval != null && ! ( (Interval) crit_interval.getObject()).isWithin(value) )
				{
					filtered.removeRow(alternative);
					break;
				}
				filtered.addAlternative(alternative, true);
				filtered.addCriterion(criterion, true);
				filtered.setValue(alternative, criterion, value);
			}
		}
		return filtered;
	}
	

	public static void main(String[] argv)
	{
		String error = "";
		String warning = "";

		if ( argv.length != 6 )
		{
			System.err.println("6 parameters needed: methodParameters.xml alternatives.xml criteria.xml performanceTable.xml transformedPerformanceTable.xml messages.xml");
			System.exit(-1);
		}
		
		final String in_methodParameters   = argv[0];
		final String in_alternatives       = argv[1];
		final String in_criteria           = argv[2];
		final String in_performanceTable   = argv[3];
		final String out_performanceTable  = argv[4];
		final String out_messages          = argv[5];
		
		if (!new File(in_methodParameters).canRead() || !new File(argv[1]).canRead() || !new File(argv[2]).canRead()
		    || !new File(argv[3]).canRead())
		{
			error = "Error: unable to run the algorithm. " + "Reason: missing one (or more) entry file(s). "
					+ "Please check your entry files.";
			XmcdaMessage.writeErrorMessage(error, argv[5]);
			return;
		}
		Main mainFunction = new Main();

		String errParam = mainFunction.checkEntryFile(in_methodParameters);
		String errAltern = mainFunction.checkEntryFile(in_alternatives);
		String errCriter = mainFunction.checkEntryFile(in_criteria);
		String errPerfTable = mainFunction.checkEntryFile(in_performanceTable);

		if (errParam != "")
			error = error.concat("\nFatal Error: the file containing the methodParameters tag is not a valid XMCDA document.");

		if (errAltern != "")
			error = error.concat("\nFatal Error: the file containing the alternatives tag is not a valid XMCDA document.");

		if (errCriter != "")
			error = error.concat("\nFatal Error: the file containing the criteria tag is not a valid XMCDA document.");

		if (errPerfTable != "")
			error = error.concat("\nFatal Error: the file containing the performanceTable tag is not a valid XMCDA document.");

		if (errParam != "" || errAltern != "" || errCriter != "" || errPerfTable != "")
		{
			XmcdaMessage.writeErrorMessage(error, out_messages);
			return;
		}
		// ParseMethodParameters
		FilterParameters parseMethodParam = new FilterParameters();
		mainFunction.PrepareParsing(in_methodParameters);
		boolean statusParseMethod = parseMethodParam.parse(mainFunction.getRacine());

		if (parseMethodParam.getWarning() != "")
			warning = warning.concat(parseMethodParam.getWarning());

		if (!statusParseMethod)
		{
			error = error.concat("\nFatal Error: the file containing the methodParameters tag is erroneous or empty.");
		}

		// Criteria
		JDOM_Criteria jdomCriter = new JDOM_Criteria();
		mainFunction.PrepareParsing(in_criteria);
		Criteria criteria = jdomCriter.parseCriteriaXML(mainFunction.getRacine());

		if (jdomCriter.getWarning() != "")
		{
			if (warning.equals(""))
				warning = "\n" + jdomCriter.getWarning();
			else
				warning = warning.concat("\n" + jdomCriter.getWarning());
		}

		if (criteria == null)
		{
			error = error.concat("\nFatal Error: the file containing the criteria tag is erroneous or empty.");
		}

		// Alternatives
		mainFunction.PrepareParsing(in_alternatives);
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		Alternatives alternatives = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (jdomAlter.getWarning() != "")
		{
			warning = warning.concat("\n" + jdomAlter.getWarning());
		}
		if (alternatives == null)
		{
			error = error.concat("\nFatal Error: the file containing the alternatives tag is erroneous or empty.");
		}

		// Performance table
		mainFunction.PrepareParsing(in_performanceTable);
		JDOM_PerformanceTable jdomPerfTable = new JDOM_PerformanceTable();
		PerformanceTable perfTable = jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());

		if (jdomPerfTable.getErrorMessage() != "")
			warning = warning.concat("\n" + jdomPerfTable.getErrorMessage());

		if (jdomPerfTable.getWarningMessage() != "")
			warning = warning.concat(jdomPerfTable.getWarningMessage());

		if (perfTable == null)
		{
			error = error.concat("\nFatal Error: the file containing the performanceTable tag is erroneous or empty.");
		}
		

		if ( criteria == null || alternatives == null || perfTable == null || !statusParseMethod )
		{
			XmcdaMessage.writeErrorMessage(error, out_messages);
			return;
		}

		perfTable = mainFunction.run(perfTable, criteria, alternatives);

		warning = warning.concat(mainFunction.getWarningMessage());
		error = error.concat(mainFunction.getErrorMessage());

		PerformanceTable filterPerfTable = new PerformanceTable();
		if (perfTable.nbRows() != 0 && perfTable.nbColumns() != 0)
		{
			// D�rouler l'algorithme
			filterPerfTable = filterCriteria(perfTable, parseMethodParam.getIntervals());
		}
		else
		{
			error = error.concat("\n We cannot run the algorithm since the criteria list or the alternatives list of the PROJECT is empty.\n");
		}

		// Output XMCDA File
		if (filterPerfTable.nbRows() != 0 && filterPerfTable.nbColumns() != 0)
		{
			OutputFilterFile outputPerfTable = new OutputFilterFile();
			outputPerfTable.createOutputFile(filterPerfTable, "Filtered performance table");
			outputPerfTable.save(out_performanceTable);
		}
		else
		{
			error = error.concat("Error: The filtered performance table is empty.");
		}
		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessage(errCriter, errAltern, errPerfTable, warning, error);
		xmcdaMess.enregistre(out_messages);
	}

}
