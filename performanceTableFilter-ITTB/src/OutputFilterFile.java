import java.io.FileOutputStream;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.PerformanceTable;


public class OutputFilterFile
{

	static Element           racine   = new Element("XMCDA");

	private Namespace        xsi      = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" + "XMLSchema-instance");

	private Namespace        xmcda    = Namespace.getNamespace("xmcda", "http://www.decision-deck.org"
			+ "/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public void save(String fichier)
	{

		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos = new FileOutputStream(fichier);
			sortie.output(document, fos);
			fos.close();

		}
		catch (java.io.IOException e)
		{}
	}

	public void createOutputFile(PerformanceTable performanceTable, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		List<Content> contents = racine.getContent();
		contents.clear();

		Element performanceTable_xml = new Element("performanceTable");
		racine.addContent(performanceTable_xml);
		Attribute name = new Attribute("mcdaConcept", commentToPut);
		performanceTable_xml.setAttribute(name);

		for (Alternative alternative: performanceTable.alternatives())
		{
			Element alternativePerformances = new Element("alternativePerformances");
			performanceTable_xml.addContent(alternativePerformances);

			Element alternativeID = new Element("alternativeID");
			alternativePerformances.addContent(alternativeID);
			alternativeID.setText(alternative.id());
			for (Criterion criterion: performanceTable.criteria())
			{
				if ( ! performanceTable.isSet(alternative, criterion) )
					break; // TODO valeur N/A ? check
				Element performance = new Element("performance");
				alternativePerformances.addContent(performance);
				Element criterionID = new Element("criterionID");
				performance.addContent(criterionID);
				criterionID.setText(criterion.id());
				Element value = new Element("value");
				performance.addContent(value);
				Element real = new Element("real");
				value.addContent(real);
				String index = String.valueOf(performanceTable.getValue(alternative, criterion));
				real.setText(index);
			}
		}
	}

}
