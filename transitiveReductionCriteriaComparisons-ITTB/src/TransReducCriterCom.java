import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.TreeSet;

import eu.telecom_bretagne.utils.GraphViz;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaComparisons;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_Criteria_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;


public class TransReducCriterCom {

	private String warningMessage = "";

	public boolean checkFiles(Criteria criter,CriteriaComparisons criterComp)
	{
		boolean result=true;
		int diff=0;
		int size1=criter.getCriteriaIDs().size();
		int size2=criterComp.getListCriteriaIDs().size();

		if(size1 == size2)
		{
			ArrayList<String > list=new ArrayList<String >();
			for(int i=0; i<size2;i++)
				list.add(criterComp.getListCriteriaIDs().
				         get(i));
			for (String s : criter.getCriteriaIDs())
			{
				if(list.contains(s)){/*This is OK*/}
				else
				{
					result=false;
					diff++;
				}
			}
			if(diff>1)
				warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
						"incoherence with the criteria given " +
						"in the file containing the criteriaComparisons tag. There are "+diff+" different criteria.");
			if(diff==1)
				warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
						"incoherence with the criteria given " +
						"in the file containing the criteriaComparisons tag. There is one different criterion.");
		}
		else 
		{
			result=false;
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the number of criteria given " +
					"in the file containing the criteriaComparisons tag.");
		}
		return result;
	}

	public ArrayList<String > getIntersectionCriteriaIDs(Criteria criter,CriteriaComparisons criterComp)
	{
		ArrayList<String > listintersection=new ArrayList<String >();

		for (String t : criterComp.getListCriteriaIDs()) {
			if(criter.getCriteriaIDs().contains(t)) {
				listintersection.add(t);
			}
		}
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>
		(new TreeSet<String>(listintersection));
		return listintersection;
	}

	public GraphViz createGraphiz(ArrayList<String >CriteriaIDs,CriteriaComparisons criterComp)
	{
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph());

		for (int i=0;i<CriteriaIDs.size();i++)
		{
			int index_matrix_line=criterComp.getListCriteriaIDs().indexOf(CriteriaIDs.get(i));

			for (int j=0;j<criterComp.getListCriteriaIDs().size();j++)								
				if(CriteriaIDs.contains(criterComp.getListCriteriaIDs().get(j)))
					if(criterComp.getMatrix()[index_matrix_line][j]!=-2000)
						gv.addln(CriteriaIDs.get(i)+ " -> "+criterComp.getListCriteriaIDs().get(j)+";");
		}
		gv.addln(gv.end_graph());
		return gv;
	}

	public CriteriaComparisons parseDotFile(File dot)
	{
		String  initial_ID,  final_ID;
		CriteriaComparisons criterComp=new CriteriaComparisons();
		try
		{
			FileInputStream fis = new FileInputStream(dot.getAbsolutePath());
			DataInputStream dis = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(dis));

			StringTokenizer st = new StringTokenizer(br.readLine());
			int i=0;
			// Cr�er un tableau � la taille du nombre de mots � extraire�
			String mot[] = new String[st.countTokens()];

			// Parcourir l'ensemble des mots � extraire
			while (st.hasMoreTokens()) {
				// Les m�moriser dans un tableau
				mot[i] = st.nextToken();
				i++;
			}

			// Enlever les ; dans les cases correspondantes
			int r=5;
			while (r<mot.length)
			{
				mot[r]=mot[r].replaceAll(";", "");
				r+=3;
			}

			// Enlever le symbole de fin }
			mot[mot.length-1]=mot[mot.length-1].replaceAll("}", "");

			int debut=3;
			int fin=5;
			while (debut< mot.length && fin <mot.length)
			{
				initial_ID=mot[debut];						   
				final_ID=mot[fin];

				if (!criterComp.getListCriteriaIDs().contains( initial_ID))
					criterComp.addCriterionID(initial_ID);

				if (!criterComp.getListCriteriaIDs().contains( final_ID))
					criterComp.addCriterionID(final_ID);

				//Matrix Update
				criterComp.updateMatrix(criterComp.getListCriteriaIDs().indexOf(initial_ID),
				                        criterComp.getListCriteriaIDs().indexOf( final_ID));
				debut+=3;
				fin+=3;
			}

			br.close();
			dis.close();
			fis.close();
		} 
		catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return criterComp;
	}

	//Main function
	public static void main(String[] argv)
	{
		String pathCriteria=argv[0];
		String pathCriteriaComparisons=argv[1];

		boolean stopRunning=false;
		String errorMessage="";
		if(argv[0]==null ||argv[1]==null ||argv.length!=4||
				!new File(argv[0]).exists()||!new File(argv[1]).exists())
		{
			stopRunning =true;
			errorMessage="Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). " +
					"Please check your entry files.";
		}

		if (stopRunning==false)
		{
			Main mainFunction=new Main();

			String errCriteria=mainFunction.checkEntryFile(pathCriteria);
			if(errCriteria!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteria " +
						"tag is not a valid XMCDA document.");

			String errCriteriaComparisons=mainFunction.checkEntryFile(pathCriteriaComparisons);

			if(errCriteriaComparisons!="")
			{
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the criteriaComparisons " +
						"tag is not a valid XMCDA document.");
				System.out.println("error :"+errCriteriaComparisons);
			}
			if(errCriteria!=""||errCriteriaComparisons!="")
			{
				//In this case: stopRunning=true, we send the messages.xml output file
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createErrorMessage(errorMessage);			
				String pathOutputMessage=argv[3];
				xmcdaMess.enregistre(pathOutputMessage);
			}
			else
			{
				TransReducCriterCom transitiveReduction = new TransReducCriterCom ();

				if(mainFunction.getWarningMessage()!="")
					transitiveReduction.warningMessage=transitiveReduction.warningMessage.concat(mainFunction.getWarningMessage());

				//Parsing XML file for Criteria
				mainFunction.PrepareParsing (pathCriteria);
				Criteria criteriaToUse=new Criteria();
				JDOM_Criteria jdomCriter= new JDOM_Criteria();
				boolean parseCriteriaNull=false;
				criteriaToUse=jdomCriter.parseCriteriaXML(mainFunction.getRacine());
				if(criteriaToUse!=null)
				{
					/*This is OK*/
				}
				else
				{
					parseCriteriaNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the criteria " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" criteria tag is erroneous or empty.");
				}

				if(jdomCriter.getWarning()!="")
					transitiveReduction.warningMessage= transitiveReduction.warningMessage.concat("\n"+jdomCriter.getWarning());

				//Parsing XML file for CriteriaComparisons
				mainFunction.PrepareParsing (pathCriteriaComparisons);
				CriteriaComparisons criterCompaToUse=new CriteriaComparisons();
				JDOM_Criteria_Comparisons jdomCriterComp= new JDOM_Criteria_Comparisons();
				boolean parseCriterCompNull=false;
				criterCompaToUse=jdomCriterComp.exploreCriteriaComparisonsXML(mainFunction.getRacine());
				if(criterCompaToUse!=null)
				{
					/*This is OK*/
				}
				else
				{
					parseCriterCompNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the criteriaComparisons " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" criteriaComparisons tag is erroneous or empty.");
				}

				if(jdomCriterComp.getWarningMessage()!="")
					transitiveReduction.warningMessage= 
					transitiveReduction.warningMessage.concat("\n"+jdomCriterComp.getWarningMessage());

				if(parseCriterCompNull==false && parseCriteriaNull==false)
				{
					transitiveReduction.checkFiles(criteriaToUse,criterCompaToUse);
					//Intersection list
					ArrayList<String > listintersectionCriterIDs=
							transitiveReduction.getIntersectionCriteriaIDs(criteriaToUse,criterCompaToUse);

					// Si l'intersection est vide on prend ce qu'on a dans criteriaComparisons.xml
					if (listintersectionCriterIDs.isEmpty())
					{
						transitiveReduction.warningMessage= transitiveReduction.warningMessage.concat("\nOnly the criteria IDs " +
								"in the file containing the criteriaComparisons tag will be considered. " +
								"Please check your input file containing the criteria tag.");
						for (int i=0; i<criterCompaToUse.getListCriteriaIDs().size();i++)
							listintersectionCriterIDs.add(criterCompaToUse.getListCriteriaIDs().get(i));
					}

					if (listintersectionCriterIDs.isEmpty())
					{
						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();
						errorMessage= errorMessage.concat("Failure to run the algorithm because it seems that the file containing " +
								"the criteriaComparisons tag is empty or erroneous.");
						xmcdaMess.createLogMessagePlotCriteriaComparisons(errCriteria,errCriteriaComparisons,
						                                                  transitiveReduction.warningMessage,errorMessage);

						String pathOutputMessage=argv[3];
						xmcdaMess.enregistre(pathOutputMessage);
					}
					else
					{
						// D�rouler l'algorithme de r�duction transitive
						String type = "dot";

						GraphViz initialGraph=transitiveReduction.createGraphiz
								(listintersectionCriterIDs,criterCompaToUse);

						File reduction=initialGraph.getGraph(initialGraph.getDotSource(), type);
						CriteriaComparisons criterCompAfterReduction=
								transitiveReduction.parseDotFile(reduction);

						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();
						xmcdaMess.createLogMessagePlotCriteriaComparisons
						(errCriteria,errCriteriaComparisons,
						 transitiveReduction.warningMessage,errorMessage);

						String pathOutputMessage=argv[3];
						xmcdaMess.enregistre(pathOutputMessage);

						String pathOutputFile=argv[2];
						String commentToPut="transitive reduction of criteria comparisons" ;  

						OutputCriteriaCompReduc outputFile= new OutputCriteriaCompReduc();
						outputFile.createFile(criterCompAfterReduction,commentToPut);
						outputFile.saveFile(pathOutputFile);
					}
				}
				else
				{
					// Output Log Message File
					XmcdaMessage xmcdaMess= new XmcdaMessage();
					xmcdaMess.createErrorMessage(errorMessage);
					String pathOutputMessage=argv[3];
					xmcdaMess.enregistre(pathOutputMessage);
				}
			}
		}
		else
		{
			//In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=argv[3];
			xmcdaMess.enregistre(pathOutputMessage);
		}
	}

}
