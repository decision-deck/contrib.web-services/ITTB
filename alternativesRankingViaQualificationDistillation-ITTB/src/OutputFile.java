import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.AlternativesComparisons;


public class OutputFile
{

	static Element           racine   = new Element("XMCDA");

	private Namespace        xsi      = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" + "XMLSchema-instance");

	private Namespace        xmcda    = Namespace.getNamespace("xmcda", "http://www.decision-deck.org"
	                                                                    + "/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public OutputFile()
	{

	}

	public void printFile()
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e)
		{}
	}

	public void saveFile(String file)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos = new FileOutputStream(file);
			sortie.output(document, fos);
			fos.close();
		}
		catch (java.io.IOException e)
		{}
	}

	public void createOutputFile(ArrayList<Set<Alternative>> distil, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
		                                      + "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		List<Content> contents = racine.getContent();
		contents.clear();

		Element alternativesValues = new Element("alternativesValues");
		racine.addContent(alternativesValues);

		Attribute name = new Attribute("mcdaConcept", commentToPut);
		alternativesValues.setAttribute(name);

		float rank = 1f;
		for (Set<Alternative> set: distil)
		{
			Iterator<Alternative> a = set.iterator();
			while ( a.hasNext() )
			{
				Element alternativeValue = new Element("alternativeValue");
				alternativesValues.addContent(alternativeValue);

				Element alternativeID = new Element("alternativeID");
				alternativeValue.addContent(alternativeID);
				alternativeID.setText(a.next().id());

				Element value = new Element("value");
				alternativeValue.addContent(value);
				Element real = new Element("real");
				value.addContent(real);
				real.setText(Float.toString(rank));
			}
			rank += set.size();
		}
	}

	public void createIntersection(AlternativesComparisons alternComp, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
		                                      + "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		List<Content> contents = racine.getContent();
		contents.clear();

		Element alternativesComparisons = new Element("alternativesComparisons");
		racine.addContent(alternativesComparisons);

		Attribute name = new Attribute("mcdaConcept", commentToPut);
		alternativesComparisons.setAttribute(name);


		Element pairs = new Element("pairs");
		alternativesComparisons.addContent(pairs);

		for (Alternative alternative_i: alternComp.rows())
		{
			for (Alternative alternative_j: alternComp.rows())
			{
				if (!alternComp.isSet(alternative_i, alternative_j)
				    || alternComp.getValue(alternative_i, alternative_j) != 1)
				    continue;

				Element pair = new Element("pair");
				pairs.addContent(pair);

				Element initial = new Element("initial");
				pair.addContent(initial);
				Element alternativeID = new Element("alternativeID");
				initial.addContent(alternativeID);
				alternativeID.setText(alternative_i.id());

				Element terminal = new Element("terminal");
				pair.addContent(terminal);
				Element alternativeID2 = new Element("alternativeID");
				terminal.addContent(alternativeID2);
				alternativeID2.setText(alternative_j.id());
			}
		}
	}

}
