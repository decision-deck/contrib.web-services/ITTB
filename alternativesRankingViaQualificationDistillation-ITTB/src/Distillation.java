import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.AlternativeValue;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesComparisons;
import eu.telecom_bretagne.xmcda.AlternativesValues;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;


public class Distillation
{

	private String warningMessage;

	public Distillation()
	{
		warningMessage = "";
	}

	public ArrayList<Set<Alternative>> createDownDistillation(AlternativesComparisons alternComp)
	{
		return createDistillation(alternComp, new Comparator<AlternativeValue>()
		{
			@Override
			public int compare(AlternativeValue a1, AlternativeValue a2)
			{
				return Float.compare(a2.getRealValue(), a1.getRealValue());
			}
		}, false);
	}

	public ArrayList<Set<Alternative>> createUpDistillation(AlternativesComparisons alternComp)
	{
		return createDistillation(alternComp, new Comparator<AlternativeValue>()
		{
			@Override
			public int compare(AlternativeValue a1, AlternativeValue a2)
			{
				return Float.compare(a1.getRealValue(), a2.getRealValue());
			}
		}, true);
	}

	protected ArrayList<Set<Alternative>> createDistillation(AlternativesComparisons alternComp, Comparator<AlternativeValue> comparator, final boolean inverse)
	{
		AlternativesComparisons currentAltComp = new AlternativesComparisons(alternComp);

		ArrayList<Set<Alternative>> distillation = new ArrayList<Set<Alternative>>();

		while ( currentAltComp.nbRows() > 0 )
		{
			ArrayList<AlternativeValue> qualifications = getQualification(currentAltComp).getListAlternativesValues();
			Collections.sort(qualifications, comparator);
			float first = qualifications.get(0).getRealValue();

			Set<Alternative> set = new HashSet<Alternative>();
			for (AlternativeValue av: qualifications)
			{
				if (av.getRealValue() != first)
					break; // qualifications is sorted, no need to examine the remaining ones
				set.add(av.getAlternative());
				currentAltComp.removeAlternative(av.getAlternative());
			}
			if (inverse)
				distillation.add(0, set);
			else
				distillation.add(set);
		}
		return distillation;
	}

	protected static AlternativesValues getQualification(AlternativesComparisons alternativesComparisons)
	{
		AlternativesValues qualification = new AlternativesValues();
		// puissance = |{ b in A / aSb }|
		// faiblesse = |{ b in A / bSa }|

		for (Alternative a: alternativesComparisons.rows())
		{
			float puissance = 0, faiblesse = 0;
			for (Alternative b: alternativesComparisons.rows())
			{
				if (alternativesComparisons.isSet(a, b)) // aSb
					puissance += alternativesComparisons.getValue(a, b);
				if (alternativesComparisons.isSet(b, a)) // bSa
					faiblesse += alternativesComparisons.getValue(b, a);
			}
			qualification.addAlternative( new AlternativeValue(a, puissance - faiblesse) );
		}
		return qualification;
	}

	public AlternativesComparisons getPreorder(AlternativesComparisons alternativesComparisons,
	                                           ArrayList<Set<Alternative>> downDistill,
	                                           ArrayList<Set<Alternative>> upDistill)
	{
		AlternativesComparisons preorder = new AlternativesComparisons();
		for (Alternative alternative: alternativesComparisons.columns())
		{
			preorder.addAlternative(alternative, false);
		}


		Map<Alternative, Integer> orderDown = new HashMap<Alternative, Integer>();
		Map<Alternative, Integer> orderUp = new HashMap<Alternative, Integer>();

		int rank=1;
		for (Set<Alternative> s: downDistill)
		{
			Iterator<Alternative> a = s.iterator();
			while (a.hasNext())
				orderDown.put(a.next(), rank);
			rank += s.size();
		}
		rank=1;
		for (Set<Alternative> s: upDistill)
		{
			Iterator<Alternative> a = s.iterator();
			while (a.hasNext())
				orderUp.put(a.next(), rank);
			rank += s.size();
		}
		// Pour chaque alternative, trouver l'ordre avec toutes les autres alternatives
		for (Alternative a_i: alternativesComparisons.rows())
		{
			for (Alternative a_j: alternativesComparisons.rows())
			{
				if ( a_i==a_j ) // yes, we mean '==' here, not .equals()
					continue;

				boolean i_preferred_to_j_in_down  = orderDown.get(a_i) <  orderDown.get(a_j);
				boolean i_preferred_to_j_in_up    = orderUp  .get(a_i) <  orderUp  .get(a_j);
				boolean i_equivalent_to_j_in_down = orderDown.get(a_i) == orderDown.get(a_j);
				boolean i_equivalent_to_j_in_up   = orderUp  .get(a_i) == orderUp  .get(a_j);

				if ( i_preferred_to_j_in_up && (i_preferred_to_j_in_down || i_equivalent_to_j_in_down) )
				{
					preorder.setValue(a_i, a_j, 1f);
					continue;
				}
				if ( i_equivalent_to_j_in_up )
				{
					if ( i_preferred_to_j_in_down )
						preorder.setValue(a_i, a_j, 1f);
					else if ( i_equivalent_to_j_in_down)
						preorder.setValue(a_i, a_j, 1f);
				}
			}
		}
		return preorder;
	}

	// Main function
	public static void main(String[] argv)
	{
		String errorMessage = "";
		if (argv.length != 6 || !new File(argv[0]).exists() || !new File(argv[1]).exists())
		{
			errorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). "
			               + "Please check your entry files.";
			// In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage = argv[5];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}
		String pathAlternatives = argv[0];
		String pathAlternativesComparisons = argv[1];


		Main mainFunction = new Main();

		Distillation distillation = new Distillation();

		String errAlternatives = mainFunction.checkEntryFile(pathAlternatives);
		if (errAlternatives != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternatives "
			                                   + "tag is not a valid XMCDA document.");

		String errAlternativesComparisons = mainFunction.checkEntryFile(pathAlternativesComparisons);
		if (errAlternativesComparisons != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternativesComparisons "
			                                   + "tag is not a valid XMCDA document.");

		if (errAlternatives != "" || errAlternativesComparisons != "")
		{
			// In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage = argv[5];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}
		if (mainFunction.getWarningMessage() != "")
			distillation.warningMessage = distillation.warningMessage.concat(mainFunction.getWarningMessage());

		// Parsing XML file for Alternatives
		mainFunction.PrepareParsing(pathAlternatives);
		Alternatives alternativesToUse = new Alternatives();
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		boolean parseAlternativesNull = false;
		alternativesToUse = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (alternativesToUse == null)
		{
			parseAlternativesNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternatives " + "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
				                                   + " alternatives tag is erroneous or empty.");
		}

		if (jdomAlter.getWarning() != "")
			distillation.warningMessage = distillation.warningMessage.concat("\n" + jdomAlter.getWarning());


		// Parsing XML file for AlternativesComparisons
		mainFunction.PrepareParsing(pathAlternativesComparisons);

		JDOM_Alternatives_Comparisons jdomAlterComp = new JDOM_Alternatives_Comparisons();

		boolean parseAlternCompNull = false;
		AlternativesComparisons alternCompaToUse =
				jdomAlterComp.exploreAlternativesComparisonsXML(mainFunction.getRacine(), alternativesToUse,
				                                                true);

		if (alternCompaToUse == null)

		{
			parseAlternCompNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternativesComparisons "
				               + "tag (outrankingRelation) is erroneous or empty.";
			else
				errorMessage = errorMessage
				        .concat("\nFatal Error: the file containing the"
				                + " alternativesComparisons tag (outrankingRelation) is erroneous or empty.");
		}

		if (jdomAlterComp.getWarningMessage() != "")
			distillation.warningMessage = distillation.warningMessage.concat("\n" + jdomAlterComp.getWarningMessage());


		if (parseAlternCompNull || parseAlternativesNull)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			// if(new File(argv[3]).exists())
			{
				String pathOutputMessage = argv[5];
				xmcdaMess.enregistre(pathOutputMessage);
			}
		}

		if (alternCompaToUse.nbRows() == 0)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			errorMessage = errorMessage
			        .concat("Failure to run the algorithm because it seems that the file containing "
			                + "the alternativesComparisons tag (outrankingRelation) is empty or erroneous.");
			xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
			                                                      distillation.warningMessage, errorMessage);

			String pathOutputMessage = argv[2];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		// R�cup�rer les param�tres et d�rouler l'agorithme
		ArrayList<Set<Alternative>> downDistil = distillation.createDownDistillation(alternCompaToUse);

		ArrayList<Set<Alternative>> upDistil = distillation.createUpDistillation(alternCompaToUse);

		AlternativesComparisons intersDistill = distillation.getPreorder(alternCompaToUse, downDistil, upDistil);

		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
		                                                      distillation.warningMessage, errorMessage);
		String pathOutputMessage = argv[5];
		xmcdaMess.enregistre(pathOutputMessage);

		// Output files: downwardsDistillation, upwardsDistillation and intersectionDistillation
		String pathDownDistil = argv[2];
		OutputFile downOutputFile = new OutputFile();
		downOutputFile.createOutputFile(downDistil, "downwards distillation");
		downOutputFile.saveFile(pathDownDistil);

		String pathUpDistil = argv[3];
		OutputFile upOutputFile = new OutputFile();
		upOutputFile.createOutputFile(upDistil, "upwards distillation");
		upOutputFile.saveFile(pathUpDistil);

		String pathInterDistil = argv[4];
		OutputFile intersOutputFile = new OutputFile();
		intersOutputFile.createIntersection(intersDistill, "intersection of upwards and downwards distillation");
		intersOutputFile.saveFile(pathInterDistil);
	}

}
