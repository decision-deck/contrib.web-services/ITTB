#! /bin/sh

JAR=../build/plotCriteriaValues-ITTB.jar

OUT=out
TMPF=$("tempfile")

unset DISPLAY

for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar $JAR $IN/criteria.xml $IN/criteriaValues.xml $IN/parameters.xml $OUT/criteriaValues.png $OUT/messages.xml
  diff -Bqrw --exclude=alternativesValuesPlot.xml out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done


# How to extract the images from the XMCDA output:
# grep '<image>' out/alternativesComparisonsPlot.xml | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 2.png
