			========================
				INSTALL 
			========================

StartWithDiviz/jdom-1.1.2.jar: 	JDOM 			http://jdom.org/ 	 		v1.1.2		http://www.jdom.org/docs/faq.html#a0030

StartWithDiviz/eclipse.jar:	  	ECLIPSE		http://www.eclipse.org/			v3.7.2		Eclipse Public License (EPL)
	
StartWithDiviz/junit.jar: 		JUNIT 		http://www.junit.org/	 		v4.10		IBM Common Public License

StartWithDiviz/swt.jar: 		SWT 		http://www.eclipse.org/swt/		v3.7.2		Eclipse Public License (EPL)
	
StartWithDiviz/resolver.jar: RESOLVER 			http://xerces.apache.org/		v2.11.0		The Apache Software Foundation

StartWithDiviz/xml-apis.jar: XML-APIS 			http://xerces.apache.org/		v2.11.0		The Apache Software Foundation

StartWithDiviz/serializer.jar: SERIALIZER 		http://xerces.apache.org/		v2.11.0		The Apache Software Foundation

StartWithDiviz/xercesImpl.jar: XercesImpl 		http://xerces.apache.org/		v2.11.0		The Apache Software Foundation

StartWithDiviz/xercesSamples.jar: XercesSamples		http://xerces.apache.org/		v2.11.0		The Apache Software Foundation

StartWithDiviz/org.eclipse.jface.jar:  			http://www.eclipse.org/			v3.7.2		Eclipse Public License (EPL)

StartWithDiviz/org.eclipse.swt-3.1.jar:			http://www.eclipse.org			v3.7.2		Eclipse Public License (EPL)
	
StartWithDiviz/org.swtchart.jar: 			http://www.swtchart.org/ 		v0.7.0		Eclipse Public License (EPL)

StartWithDiviz/eclipse.jface-2.1.0.jar: 		http://www.eclipse.org			v3.7.2		Eclipse Public License (EPL)

jfreechart-1.0.14.jar: 		JFreeChart		http://www.jfree.org/jfreechart/	v1.0.14		GNU Lesser General Public Licence

jcommon-1.0.17.jar: 		JCommon 		http://www.jfree.org/jcommon/		v1.0.17 	GNU Lesser General Public Licence



	
	
