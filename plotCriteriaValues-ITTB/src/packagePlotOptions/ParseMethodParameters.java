package packagePlotOptions;
import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class ParseMethodParameters {

	static org.jdom2.Document document;
	private boolean color;
	private String chartType;
	private String color1ToUse;
	private String color2ToUse;
	private String chartTitle;
	private String XaxisLabel;
	private String YaxisLabel;
	private String error;
	private String order;
	private String maxMin;

	// Constructor
	public  ParseMethodParameters (){
		color=false;			//the default plot is a color plot
		chartType= "bc";		// bar chart is the default configuration
		color1ToUse="Black";
		color2ToUse="Black";
		chartTitle="";
		XaxisLabel="";
		YaxisLabel="";
		error="";
		order="by values";
		maxMin="increasing";
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}
		return document.getRootElement();
	}

	public boolean parse(Element racine)
	{	      
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator();

				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					/*if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Bar chart or pie chart?"))*/
					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("chart_type"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("barChart"))
							chartType= "bc";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("pieChart"))

							chartType= "pc";
						else
						{
							// �mettre un message d'erreur et dire que c'est bar chart par d�faut 
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order_by"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("name"))
							order= "by name";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("id"))
							order= "by id";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("values"))

							order= "by values";
						else
						{
							// �mettre un message d'erreur et dire que c'est by values d�faut 
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("increasing"))
							maxMin= "increasing";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("decreasing"))

							maxMin= "decreasing";
						else
						{
							// �mettre un message d'erreur 
							maxMin= "increasing";
						}
					}

					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Color chart?"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("use_color"))
					{
						if (courant.getChild("value").
								getChild("label").getValue().equals("false"))
							color=false;
						else if (courant.getChild("value").
								getChild("label").getValue().equals("true"))
							color=true;
						else color=false;
					}
					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Initial color:"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("initial_color"))
					{
						color1ToUse=courant.getChild("value").
								getChild("label").getValue();
					}
					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Final color:"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("final_color"))
					{
						color2ToUse=courant.getChild("value").
								getChild("label").getValue();
					}

					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Enter chart title:"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("chart_title"))
					{
						chartTitle=courant.getChild("value").
								getChild("label").getValue();
					}

					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Enter the domain axis label:"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("domain_axis"))
					{
						XaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}
					/*else if (courant.getAttributeValue("name")!= null && courant.getAttributeValue("name").
		        		 equals("Enter the range axis label:"))*/
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("range_axis"))
					{
						YaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}

					else {// we have only two parameters in method parameters}
						error="Error: you have not entered the correct parameters. " +
								"The default generated plot is a black and white bar chart.";
					}
				}  
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;

		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}
	public String getChartType()
	{
		return chartType;
	}

	public String getInitialColor()
	{
		return color1ToUse;
	}
	public String getFinalColor()
	{
		return color2ToUse;
	}
	public String getChartTitle()
	{
		return chartTitle;
	}
	public String getDomainAxisLabel()
	{
		return XaxisLabel;
	}
	public String getRangeAxisLabel()
	{
		return YaxisLabel;
	}
	public String getError()
	{
		return error;
	}

	public String getOrder()
	{
		return order;
	}
	public String getMaxMin()
	{
		return maxMin;
	}

}