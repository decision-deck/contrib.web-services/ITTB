package packagePlotOptions;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;

import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaValues;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_CriteriaValues;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class Plot {

	private String warningMessage = "";

	public boolean checkFiles(Criteria criter, CriteriaValues criterVal)

	{
		boolean result=true;// les listes sont coh�rentes
		int diff=0;
		if(criter.getCriteriaIDs().size()!=criterVal.size())
		{
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the number of criteria given " +
					"in the file containing the criteriaValues tag.");
			return false;
		}

		for (String s : criterVal.getCriteriaIDs())
		{
			if( ! criter.getCriteriaIDs().contains(s) )
			{
				result=false;
				diff++;
			}
		}
		if(diff>1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the criteria given " +
					"in the file containing the criteriaValues tag. There are "+diff+"different criteria.");
		if(diff==1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the criteria tag:" +
					"incoherence with the criteria given " +
					"in the file containing the criteriaValues tag. There is one different criterion.");

		return result;		
	}

	public ArrayList<String> getIntersectionCriteriaIDs(Criteria criter,CriteriaValues criterVal)
	{
		Set<String> intersection=new HashSet<String>(); // no duplicates

		for (String t : criterVal.getCriteriaIDs()) 
			if(criter.getCriteriaIDs().contains(t)) 
				intersection.add(t);
		return new ArrayList<String>(intersection);
	}

	public ArrayList<String > getIntersectionCriteriaNames(Criteria criter,CriteriaValues criterVal,
	                                                       ArrayList<String > listintersectionIds)
	{
		ArrayList<String > listintersectionNames=new ArrayList<String >();

		for(int i=0; i< listintersectionIds.size();i++)
		{
			if (criter.getCriteriaIDs().
					contains(listintersectionIds.get(i)))
			{
				int index=criter.getCriteriaIDs().indexOf(listintersectionIds.get(i));
				if(criter.getListCriteriaNames().get(index)!=null)
					listintersectionNames.add(criter.getListCriteriaNames().get(index));
				else listintersectionNames.add(null);
			}
			else listintersectionNames.add(null);
		}
		return listintersectionNames;
	}

	public void plotCriteriaValuesBarChart(CriteriaValues criteriaValuesToPlot,Criteria criter,
	                                         ArrayList <String>listintersectionCriteria,
	                                         int format,String tempErrorMessage,
	                                         boolean color, Color color1, Color color2,
	                                         String chartTitle, String XaxisLabel, String YaxisLabel,
	                                         ArrayList  <String>listintersectionNames, String order, String maxMin,
	                                         File pngFile)
	{
		/* Bar chart representation of the CriteriaValues */

		double[] ySeriesCriterValues=new double[listintersectionCriteria.size()];
		// Read these values from the file criteriaValues.xml

		for(int i=0;i<listintersectionCriteria.size();i++)
			if (criteriaValuesToPlot.getCriteriaIDs().
					contains(listintersectionCriteria.get(i)))

				ySeriesCriterValues[i]= criteriaValuesToPlot.getListCriteriaValues().
				get(criteriaValuesToPlot.getCriteriaIDs().indexOf(listintersectionCriteria.get(i))).
				getRealValue();

		BarChartDemo3 barChartCriterVal= new  BarChartDemo3("Bar Chart Demo 3",
		                                                    ySeriesCriterValues, 
		                                                    color1, color2,
		                                                    chartTitle, XaxisLabel, YaxisLabel,criter,listintersectionCriteria,
		                                                    listintersectionNames, order,maxMin);
		barChartCriterVal.saveChart(pngFile);
	}

	public void plotCriteriaValuesPieChart(CriteriaValues criteriaValuesToPlot,Criteria criter,
	                                         ArrayList <String>listintersectionCriteria,
	                                         int format,String tempErrorMessage,
	                                         boolean color, Color color1, Color color2,
	                                         String chartTitle,ArrayList  <String>listintersectionNames, String order,String maxMin,
	                                         File pngFile)
	{
		/* Pie chart representation of the CriteriaValues */

		double[] ySeriesCriterValues=new double[listintersectionCriteria.size()];
		// Read these values from the file criteriaValues.xml

		for(int i=0;i<listintersectionCriteria.size();i++) 
			if (criteriaValuesToPlot.getCriteriaIDs().
					contains(listintersectionCriteria.get(i)))

				ySeriesCriterValues[i]= criteriaValuesToPlot.getListCriteriaValues().
				get(criteriaValuesToPlot.getCriteriaIDs().indexOf(listintersectionCriteria.get(i))).
				getRealValue();

		PieChartDemo4 pieChartCriterVal=new PieChartDemo4("PieChartDemo4",
		                                                  ySeriesCriterValues, criteriaValuesToPlot,criter,
		                                                  color1, color2,chartTitle,listintersectionCriteria,listintersectionNames,order,maxMin);
		pieChartCriterVal.saveChart(pngFile);
	}

	protected static Color[] getColorsFromOptions(boolean black_and_white, String colors1ToUse, String colors2ToUse)
	{
		Color[] colors = { Color.black, Color.black };
		if (black_and_white)
			return colors;

		Color c1 = null, c2 = null;
		try
		{
			c1 = (Color)Color.class.getField(colors1ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c1==null )
			return colors;

		try
		{
			c2 = (Color)Color.class.getField(colors2ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c2==null )
			return colors;

		colors[0] = c1;
		colors[1] = c2;
		return colors;
	}

	public static void main(String[] argv)
	{
		String tempErrorMessage="";
		if ( argv.length != 5 )
		{
			System.err.println("Usage: criteria.xml criteriaValues.xml parameters.xml criteriaValues.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathCriteria=argv[0];
		final String pathCriteriaValues=argv[1];
		final String pathParameters=argv[2];
		final String pathOutputFilePlot=argv[3];
		final String pathOutputMessage=argv[4];

		if( !new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists() )
		{
			tempErrorMessage="Error: unable to run the algorithm. Reason: missing one (or more)entry file(s). Please check your entry files";
			XmcdaMessage.writeErrorMessage(tempErrorMessage, pathOutputMessage);
			return;
		}

		Main mainFunction=new Main();

		String errMethodParameters=mainFunction.checkEntryFile(pathParameters);

		if(errMethodParameters!="")
			tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the parameters " +
					"is not a valid XMCDA document.");

		String errCriteria=mainFunction.checkEntryFile(pathCriteria);
		if(errCriteria!="")
			tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteria " +
					"tag is not a valid XMCDA document.");

		String errCriteriaValues=mainFunction.checkEntryFile(pathCriteriaValues);
		if(errCriteriaValues!="")
		{
			//System.out.println("errAlternativesValues: "+errAlternativesValues);
			tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteriaValues " +
					"tag is not a valid XMCDA document.");
		}

		if(errMethodParameters!="" ||errCriteria!=""||errCriteriaValues!="")
		{
			XmcdaMessage.writeErrorMessage(tempErrorMessage, pathOutputMessage);
			return;
		}
		Plot plot = new Plot();
		if(mainFunction.getWarningMessage()!="")
			plot.warningMessage=plot.warningMessage.concat(mainFunction.getWarningMessage());

		//Parsing XML file for Criteria
		mainFunction.PrepareParsing (pathCriteria);
		Criteria criteria = new Criteria();
		JDOM_Criteria jdomCriter= new JDOM_Criteria();
		criteria=jdomCriter.parseCriteriaXML(mainFunction.getRacine());

		if (criteria == null)
		{
			//Error message
			tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteria tag is erroneous or empty.");
		}
		if(jdomCriter.getWarning()!="")
			plot.warningMessage= plot.warningMessage.concat("\n"+jdomCriter.getWarning());

		//Parsing XML file for CriteriaValues
		mainFunction.PrepareParsing(pathCriteriaValues);
		CriteriaValues criteriaValues = new CriteriaValues();
		JDOM_CriteriaValues jdomCriterVal = new JDOM_CriteriaValues();
		criteriaValues=jdomCriterVal.parseCriteriaValuesXML(mainFunction.getRacine());

		if(criteriaValues == null)
		{
			tempErrorMessage = tempErrorMessage.concat("\nFatal Error: the file containing the criteriaValues tag is erroneous or empty.");
		}
		if(jdomCriterVal.getWarningMessage()!="")
			plot.warningMessage = plot.warningMessage.concat("\n"+jdomCriterVal.getWarningMessage());

		//ParseMethodParameters
		ParseMethodParameters parseMethodParam=new ParseMethodParameters();
		mainFunction.PrepareParsing (pathParameters);
		boolean statusParseMethod=parseMethodParam.parse(mainFunction.getRacine());

		if ( ! statusParseMethod )
		{
			tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the parameters is erroneous or empty.");
		}

		if( criteriaValues == null || criteria == null || ! statusParseMethod )
		{
			final XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createErrorMessage(tempErrorMessage);		
			xmcdaMess.enregistre(argv[4]);
			return;
		}

		plot.checkFiles(criteria,criteriaValues);

		//Intersection list
		ArrayList<String > listintersectionCriteria=
				plot.getIntersectionCriteriaIDs(criteria,criteriaValues);

		// Si l'intersection est vide on prend ce qu'on a dans la criteriaValues.xml
		if (listintersectionCriteria.isEmpty())
		{
			plot.warningMessage= plot.warningMessage.concat("\nOnly the criteriaIDs " +
					"in the file containing the criteriaValues tag were considered. " +
					"Please check your input file containing the criteria tag.");
			listintersectionCriteria=criteriaValues.getCriteriaIDs();
		}

		boolean color =parseMethodParam.getColor();
		String chartType=parseMethodParam.getChartType();
		if (parseMethodParam.getError()!="")	
			plot.warningMessage= plot.warningMessage.concat(parseMethodParam.getError());

		String colors1ToUse=parseMethodParam.getInitialColor();
		String colors2ToUse=parseMethodParam.getFinalColor();

		Color[] colors = getColorsFromOptions(!color, colors1ToUse, colors2ToUse);

		int format=SWT.IMAGE_PNG;
		String chartTitle=parseMethodParam.getChartTitle();
		String XaxisLabel=parseMethodParam.getDomainAxisLabel();
		String YaxisLabel=parseMethodParam.getRangeAxisLabel();
		String order=parseMethodParam.getOrder();
		String maxMin=parseMethodParam.getMaxMin();

		if( listintersectionCriteria.size()!=0 )
		{
			// D�rouler l'algorithme du plot
			if(chartType.equals("bc"))	
				plot.plotCriteriaValuesBarChart(criteriaValues,criteria
				                                             ,listintersectionCriteria,format,tempErrorMessage,
				                                             color, colors[0],colors[1],
				                                             chartTitle,XaxisLabel, YaxisLabel,
				                                             plot.getIntersectionCriteriaNames(criteria,criteriaValues, 
				                                                                               listintersectionCriteria),
				                                             order, maxMin, new File(pathOutputFilePlot));
			// D�rouler l'algorithme du plot
			else if(chartType.equals("pc"))	
				plot.plotCriteriaValuesPieChart(criteriaValues,criteria,listintersectionCriteria,
				                                             format,tempErrorMessage,
				                                             color, colors[0],colors[1],
				                                             chartTitle,
				                                             plot.getIntersectionCriteriaNames(criteria,criteriaValues, 
				                                                                               listintersectionCriteria),
				                                             order, maxMin, new File(pathOutputFilePlot));
			else {
				tempErrorMessage=tempErrorMessage.concat("\nError in chart type. " +
						"Only Pie chart or Bar chart are available plots.");
			}
		}
		else
		{
			tempErrorMessage=tempErrorMessage.concat("\n We cannot run the algorithm " +
					"since the criteria list  " +
					"of the PROJECT is empty.\n");
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess= new XmcdaMessage();
		xmcdaMess.createLogMessageForCriteriaValPlot(errCriteria,errCriteriaValues,
		                                             plot.warningMessage,tempErrorMessage);

		xmcdaMess.enregistre(pathOutputMessage);
	}

}
