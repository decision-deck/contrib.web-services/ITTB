package packagePlotOptions;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class OutputPlotFile {
	//Nous allons commencer notre arborescence en cr�ant la racine XML
	//qui sera ici "xmcda:XMCDA".
	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");

	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	//On cr�e un nouveau Document JDOM bas� sur la racine que l'on vient de cr�er
	static org.jdom2.Document document = new Document(racine);

	public  void printFile()
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e){}
	}

	public void saveFile(String file)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos=new FileOutputStream(file);
			sortie.output(document,  fos);
			fos.close();     
		}
		catch (java.io.IOException e){}
	}

	public  void createOutputFile(ArrayList<String > listintersectionCriteria, 
	                              String encodedImage, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);
		List<Content> contents= racine.getContent();
		contents.clear();

		Element criterionValue= new Element("criterionValue");
		racine.addContent(criterionValue);

		Attribute name =new Attribute("mcdaConcept",commentToPut);
		criterionValue.setAttribute(name);

		Element criteriaSet= new Element("criteriaSet");
		criterionValue.addContent(criteriaSet);

		for(int i=0;i<listintersectionCriteria.size();i++)
		{
			Element element = new Element("element");
			criteriaSet.addContent(element);

			Element criterionID = new Element("criterionID");
			element.addContent(criterionID);
			criterionID.setText(listintersectionCriteria.
			                    get(i).toString());
		}  
		Element value=new Element ("value");
		criterionValue.addContent(value);

		Element image=new Element ("image");
		value.addContent(image);
		if(encodedImage!="")
			image.setText(encodedImage);
		else System.out.println ("encoded Image not detected");

	}

}
