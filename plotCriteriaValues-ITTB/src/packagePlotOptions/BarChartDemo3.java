package packagePlotOptions;

import java.awt.Color;
import java.awt.Paint;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.awt.Font;
import org.jfree.chart.ChartFactory;
//import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;

import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;

import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;
//import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.TextAnchor;

import eu.telecom_bretagne.xmcda.Criteria;

/**
 * A bar chart that uses a custom renderer to display different colors within a series.
 * No legend is displayed because there is only one series but the colors are not consistent.
 *
 */
public class BarChartDemo3 {

	private int size1;
	private int size2;
	private JFreeChart chart;
	/**
	 * A custom renderer that returns a different color for each item in a single series.
	 */

	class CustomRenderer extends BarRenderer {

		private static final long serialVersionUID = 1449043462068613652L;
		/** The colors. */
		private Paint[] colors;

		public CustomRenderer(final Paint[] colors) {
			this.colors = colors;
			super.setShadowVisible(false);
			super.setBarPainter(new StandardBarPainter());
		}

		public Paint getItemPaint(final int row, final int column) {
			return this.colors[column % this.colors.length];
		}
	}

	public BarChartDemo3(final String title,double[] ySeries, 
	                     Color c1, Color c2,
	                     String chartTitle, String XaxisLabel, String YaxisLabel, Criteria criter,
	                     ArrayList <String>listintersectionCriteria,
	                     ArrayList  <String>listintersectionNames, String order,String maxMin) {
		this.setSize1(500);
		this.setSize2(270);
		final CategoryDataset dataset = createDataset(ySeries, criter,
		                                              listintersectionCriteria, listintersectionNames,order,maxMin); 

		chart = createChart(dataset,ySeries.length, 
		                    c1,c2,chartTitle, XaxisLabel, YaxisLabel);

	}

	public void saveChart(OutputStream outputStream)
	{
		try
		{
			ChartUtilities.writeChartAsPNG(outputStream, chart, this.getSize1(), this.getSize2());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveChart(File file)
	{
		try
		{
			ChartUtilities.saveChartAsPNG(file, chart, this.getSize1(), this.getSize2());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveChart(String filepath)
	{
		File file= new File(filepath);
		try
		{
			ChartUtilities.saveChartAsPNG(file, chart, this.getSize1(), this.getSize2());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Creates a sample dataset.
	 * 
	 * @return a sample dataset.
	 */
	private CategoryDataset createDataset(double[] ySeries, Criteria criter,
	                                      ArrayList  <String>listIntersectionIds, ArrayList  <String>listIntersectionNames,
	                                      String order,String maxMin) {
		final double[][] data = new double[1][ySeries.length]; 

		for (int i=0;i<ySeries.length;i++)
			data[0][i]=ySeries[i];

		String[] rowKeys=new  String[1];
		rowKeys[0]="Series";
		String[] columnKeys =new  String[ySeries.length];	
		for(int i=0;i<ySeries.length;i++){columnKeys[i]="" ;}

		String[] listIds=new  String[ySeries.length];
		for (int i=0;i<ySeries.length;i++)
			listIds[i]=listIntersectionIds.get(i);

		String[] listNames=new  String[ySeries.length];
		for (int i=0;i<ySeries.length;i++)
			listNames[i]=listIntersectionNames.get(i);

		// Initialisation
		for(int i=0;i<ySeries.length;i++)
		{
			if(order.equals("by id"))
			{

				if (listIntersectionNames.get(i)!=null && listIntersectionIds!=null)
					columnKeys[i]="("+listIntersectionIds.get(i)+") "+listIntersectionNames.
					get(i);
				else
					columnKeys[i]=listIntersectionIds.get(i);
			}
			else
			{
				if (listIntersectionNames.get(i)!=null && listIntersectionIds!=null)

					columnKeys[i]=listIntersectionNames.
					get(i)+" ("+listIntersectionIds.get(i)+")";

				else
					columnKeys[i]=listIntersectionIds.get(i);
			}
		}

		if(order.equals("by id")||order.equals("by name"))
		{
			// Ordonner les valeurs
			for (int i=0;i<columnKeys.length -1;i++)
			{
				for (int j=i+1;j<columnKeys.length;j++)
				{
					if(maxMin.equals("increasing"))
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))<0)
						{

							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    			
						}
					}
					else //(maxMin.equals("decreasing")
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))>0)
						{

							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    			
						}
					}
				}
			}
		}

		/*else if(order.equals("by name"))
        {
        	/// Ordonner les valeurs
        	for (int i=0;i<columnKeys.length -1;i++)
        	{
        		for (int j=i+1;j<columnKeys.length;j++)
        		{
        			if(maxMin.equals("increasing"))
        			{
        				if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))<0)
        				{
        				exchange (columnKeys,i,j);       				
        				exchange (ySeries,i,j);    
        				}
        			}

        			else //(maxMin.equals("decreasing"))
        			{
        				if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))>0)
        				{
        				exchange (columnKeys,i,j);       				
        				exchange (ySeries,i,j);    
        				}
        			}
        		}
        	}
        }*/

		else if(order.equals("by values"))
		{
			/// Ordonner les valeurs
			for (int i=0;i<ySeries.length -1;i++)
			{
				for (int j=i+1;j<ySeries.length;j++)
				{
					if (maxMin.equals("increasing"))
					{
						if(ySeries[j]<ySeries[i])
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    
						}
					}
					else //(maxMin.equals("decreasing"))
					{
						if(ySeries[j]>ySeries[i])
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    

						}
					}
				}
			}
		}
		else
		{
		}

		for (int i=0;i<ySeries.length;i++)
			data[0][i]=ySeries[i];

		CategoryDataset dataset =DatasetUtilities.createCategoryDataset(
		                                                                rowKeys,
		                                                                columnKeys,
		                                                                data
				);
		return dataset;
	}

	protected static void exchange (String[]columnKeys, int i, int j)
	{
		String tempKey=columnKeys[j];
		columnKeys[j]=columnKeys[i];
		columnKeys[i]=tempKey;
	}

	protected static void exchange (double[]ySeries, int i, int j)
	{
		double tempVal=ySeries[j];
		ySeries[j]=ySeries[i];
		ySeries[i]=tempVal;
	}

	/**
	 * Creates a sample chart.
	 * 
	 * @param dataset  the dataset.
	 * 
	 * @return a sample chart.
	 */
	@SuppressWarnings("deprecation")
	private JFreeChart createChart(final CategoryDataset dataset, int ySeriesLength, 
	                               Color c1, Color c2,String chartTitle, String XaxisLabel, String YaxisLabel) {
		final JFreeChart chart = ChartFactory.createBarChart(
		                                                     chartTitle,       // chart title
		                                                     XaxisLabel,               // domain axis label
		                                                     YaxisLabel,                  // range axis label
		                                                     dataset,                  // data
		                                                     PlotOrientation.VERTICAL, // the plot orientation
		                                                     false,                    // include legend
		                                                     true,
		                                                     false
				);

		chart.setBackgroundPaint(Color.white);

		// get a reference to the plot for further customisation...
		final CategoryPlot plot = chart.getCategoryPlot();
		plot.setNoDataMessage("NO DATA!");      
		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		Paint []paint= new Paint [ySeriesLength];

		int red1 = c1.getRed();
		int green1 = c1.getGreen();
		int blue1 = c1.getBlue();

		int red2 = c2.getRed();
		int green2 = c2.getGreen();
		int blue2 = c2.getBlue(); 

		int LARGEUR=ySeriesLength;
		for(int i=0; i<ySeriesLength; i++) {
			paint[i]=new Color((red1*(LARGEUR-i)+red2*i)/LARGEUR,
			                   (green1*(LARGEUR-i)+green2*i)/LARGEUR,
			                   (blue1*(LARGEUR-i)+blue2*i)/LARGEUR);
		} 

		final CategoryItemRenderer renderer = new CustomRenderer(paint);
		for(int i=0; i<ySeriesLength; i++)
			renderer.setSeriesPaint(i,paint[i]);

		renderer.setSeriesItemLabelsVisible(0, Boolean.FALSE);
		final ItemLabelPosition p = new ItemLabelPosition(
		                                                  ItemLabelAnchor.CENTER, TextAnchor.CENTER, TextAnchor.CENTER, 45.0
				);
		renderer.setPositiveItemLabelPosition(p);
		plot.setRenderer(renderer);
		plot.setShadowGenerator(null);

		Font font = new Font("Arial", Font.BOLD, 12);

		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

		domainAxis.setTickLabelFont(font);
		domainAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));

		// change the margin at the top of the range axis...
		final ValueAxis rangeAxis = plot.getRangeAxis();

		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		rangeAxis.setLowerMargin(0.15);
		rangeAxis.setUpperMargin(0.15);
		rangeAxis.setTickLabelFont(font);
		rangeAxis.setTickLabelsVisible(true);
		rangeAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));

		return chart;
	}

	public int getSize1()
	{
		return size1;
	}
	public void setSize1(int val)
	{
		size1=val;
	}

	public int getSize2()
	{
		return size2;
	}
	public void setSize2(int val)
	{
		size2=val;
	}

}

