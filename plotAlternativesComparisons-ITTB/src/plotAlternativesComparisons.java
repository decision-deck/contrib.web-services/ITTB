import java.io.File;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesComparisons;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class plotAlternativesComparisons
{

	private String  warningMessage = "";

	public GraphViz createGraphiz(AlternativesComparisons alternComp, String color, boolean valuedGraph,
	                              boolean transitiveReduction, String node_shape)
	{
		GraphViz gv = new GraphViz();
		gv.setTransitiveReduction(transitiveReduction);
		gv.addln(gv.start_graph(color, node_shape));

		for (Alternative alternative_i: alternComp.rows())
		{
			for (Alternative alternative_j: alternComp.columns())
			{
				if (!alternComp.isSet(alternative_i, alternative_j))
				    continue;

				if (valuedGraph == false)
					gv.addln(alternative_i + " -> " + alternative_j + ";");
				else
				{
					String label = Float.toString(alternComp.getValue(alternative_i, alternative_j));
					gv.addln(alternative_i + " -> " + alternative_j + " [label=" + label + ", fontcolor=" + color
					         + "];");
				}
			}
		}

		for (Alternative alternative: alternComp.rows())
			gv.addln(alternative.id() + "[fontcolor=" + color + "]");

		gv.addln(gv.end_graph());

		return gv;
	}

	protected static String getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		String color = "";
		if (black_and_white)
			color = "black";
		else
			color = colorToUse.toLowerCase();

		return color;
	}

	// Main function
	public static void main(String[] argv)
	{
		if ( argv.length != 5 &&  argv.length != 6 )
		{
			System.err.println("Usage: alternatives.xml alternativesComparisons.xml parameters.xml [alternativesValuesPlot.dot] alternativesValuesPlot.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathAlternatives = argv[0];
		final String pathAlternativesComparisons = argv[1];
		final String pathMethodParameters = argv[2];
		final File pathOutputDotFile = argv.length==6 ? new File(argv[3]) : null;
		final File pathOutputFile = new File(argv[argv.length-2]);
		final String pathOutputMessage = argv[argv.length-1];
		
		String errorMessage = "";
		
		if (!new File(pathMethodParameters).exists() || !new File(pathAlternatives).exists() || !new File(pathAlternativesComparisons).exists())
		{
			errorMessage = "Error: unable to run the algorithm. Reason: one (or more) missing entry file. "
			               + "Please check your entry files.";
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);

			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}
		Main mainFunction = new Main();
		String errMethodParameters = mainFunction.checkEntryFile(pathMethodParameters);

		if (errMethodParameters != "")
		    errorMessage = errorMessage.concat("\nFatal Error: the file containing the parameters "
		                                       + "is not a valid XMCDA document.");

		String errAlternatives = mainFunction.checkEntryFile(pathAlternatives);
		if (errAlternatives != "")
		    errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternatives "
		                                       + "tag is not a valid XMCDA document.");

		String errAlternativesComparisons = mainFunction.checkEntryFile(pathAlternativesComparisons);
		if (errAlternativesComparisons != "")
		    errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternativesComparisons "
		                                       + "tag is not a valid XMCDA document.");

		if (errMethodParameters != "" || errAlternatives != "" || errAlternativesComparisons != "")
		{
			// In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}
		plotAlternativesComparisons plot = new plotAlternativesComparisons();
		if (mainFunction.getWarningMessage() != "")
		    plot.warningMessage = plot.warningMessage.concat(mainFunction.getWarningMessage());
		// Parsing XML file for Alternatives
		// System.out.println("Parsing the file alternatives.xml:");
		mainFunction.PrepareParsing(pathAlternatives);
		Alternatives alternativesToUse = new Alternatives();
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		boolean parseAlternativesNull = false;
		alternativesToUse = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (alternativesToUse == null)
		{
			parseAlternativesNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternatives " + "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
				                                   + " alternatives tag is erroneous or empty.");
		}

		if (jdomAlter.getWarning() != "")
		    plot.warningMessage = plot.warningMessage.concat("\n" + jdomAlter.getWarning());
		// Parsing XML file for AlternativesComparisons
		mainFunction.PrepareParsing(pathAlternativesComparisons);
		AlternativesComparisons alternCompaToUse = new AlternativesComparisons();
		JDOM_Alternatives_Comparisons jdomAlterComp = new JDOM_Alternatives_Comparisons();

		boolean parseAlternCompNull = false;
		alternCompaToUse = jdomAlterComp.exploreAlternativesComparisonsXML(mainFunction.getRacine(), alternativesToUse);
		if (alternCompaToUse == null)
		{
			parseAlternCompNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternativesComparisons "
				               + "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
				                                   + " alternativesComparisons tag is erroneous or empty.");
		}

		if (jdomAlterComp.getWarningMessage() != "")
		    plot.warningMessage = plot.warningMessage.concat("\n" + jdomAlterComp.getWarningMessage());
		// ParseMethodParameters
		MethodParamAlternCompa parseMethodParam = new MethodParamAlternCompa();
		mainFunction.PrepareParsing(pathMethodParameters);
		boolean statusParseMethod = parseMethodParam.parse(mainFunction.getRacine());

		if (parseMethodParam.getWarning() != "")
		    plot.warningMessage = plot.warningMessage.concat(parseMethodParam.getWarning());

		if (!statusParseMethod)
		{
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the parameters "
				               + "is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
				                                   + " parameters is erroneous or empty.");
		}
		if (parseAlternCompNull || parseAlternativesNull || !statusParseMethod)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		// Si l'intersection est vide on prend ce qu'on a dans la alternativesComparisons.xml
		if (alternCompaToUse.nbRows() == 0)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			errorMessage = errorMessage
			        .concat("Failure to run the algorithm because it seems that the file containing "
			                + "the alternativesComparisons tag is empty or erroneous.");
			xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
			                                                      plot.warningMessage, errorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		// R�cup�rer les param�tres et d�rouler l'agorithme

		boolean color = parseMethodParam.getColor();
		String colorToUse = parseMethodParam.getColorToUse();
		// Merci S�bastien !!!!
		colorToUse = getColorFromOptions(!color, colorToUse);
		boolean valuedGraph = parseMethodParam.getValuedGraph();
		boolean transitiveReduction = parseMethodParam.getTransitiveReduction();

		// Parameter dependency
		if (valuedGraph == true)
		    transitiveReduction = false;
		// On ne fait pas de r�duction transitive sur les graphes valu�s
		String node_shape = parseMethodParam.getShape();
		GraphViz gv = plot.createGraphiz(alternCompaToUse, colorToUse, valuedGraph, transitiveReduction, node_shape);
		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();

		xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
		                                                      plot.warningMessage, errorMessage);

		xmcdaMess.enregistre(pathOutputMessage);

		String commentToPut = "Alternatives comparisons plot";

		OutputAlterComp.createFiles(alternCompaToUse.rows(), gv, commentToPut, pathOutputFile, pathOutputDotFile);
	}
}
