import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.telecom_bretagne.xmcda.Alternative;


public class OutputAlterComp
{

	private static final Namespace xsi   = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

	private static final Namespace xmcda = Namespace.getNamespace("xmcda",
	                                                              "http://www.decision-deck.org/2009/XMCDA-2.0.0");

	public static void createFiles(List<Alternative> alternatives, GraphViz gv, String commentToPut, File imageFile,
			File dotFile)
	{
		try {
			FileWriter fw = new FileWriter(dotFile);
			fw.write(gv.getDotSource());
			fw.close();
		} catch (java.io.IOException e) {
			e.printStackTrace();
		}
		gv.createImage(dotFile, "png", imageFile);
	}

	public static void old_createFiles(List<Alternative> alternatives, GraphViz gv, String commentToPut, File xmlFile,
	                               File dotFile)
	{
		Element racine = new Element("XMCDA");
		Document document = new Document(racine);
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
		                                      + "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		List<Content> contents = racine.getContent();
		contents.clear();
		Element alternativeValue = new Element("alternativeValue");
		racine.addContent(alternativeValue);

		Attribute name = new Attribute("mcdaConcept", commentToPut);
		alternativeValue.setAttribute(name);
		Element alternativesSet = new Element("alternativesSet");
		alternativeValue.addContent(alternativesSet);
		// System.out.println(listintersection.getCriteriaIDs().size());
		for (Alternative alternative: alternatives)
		{
			Element element = new Element("element");
			alternativesSet.addContent(element);

			Element alternativeID = new Element("alternativeID");
			element.addContent(alternativeID);
			alternativeID.setText(alternative.id());
		}

		Element value = new Element("value");
		alternativeValue.addContent(value);
		Element image = new Element("image");
		value.addContent(image);
		image.setText(Base64.encode(gv.getGraph(gv.getDotSource(), "png")));
		
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos = new FileOutputStream(xmlFile);
			sortie.output(document, fos);
			fos.close();
		}
		catch (java.io.IOException e)
		{
			e.printStackTrace();
		}
		
		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), xmlFile);

		if (dotFile != null)
		{
		    try
		    {
			    FileWriter fw = new FileWriter(dotFile);
			    fw.write(gv.getDotSource());
			    fw.close();
		    }
		    catch (java.io.IOException e)
		    {
			    e.printStackTrace();
		    }
		}
	}

}
