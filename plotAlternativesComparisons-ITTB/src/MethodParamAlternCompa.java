import java.util.Iterator;

import org.jdom2.Element;

public class MethodParamAlternCompa {
	private boolean color;
	private String colorToUse;
	private boolean valuedGraph;
	private boolean transitive;
	private String shape;
	private String warning;
	// Constructor
	public MethodParamAlternCompa()
	{
		color=false;
		colorToUse="Black";
		valuedGraph=false;
		transitive=false;
		shape="Rectangle";
		warning="";
		
	}

    public boolean parse(Element racine)
	{
		boolean statusOK=true;
		 if (racine.getChild("methodParameters")!=null)
		   {
			 Element methodParameters = racine.getChild("methodParameters");
			 if(methodParameters.getChildren("parameter")!=null)
			 {
				 Iterator<Element> i = methodParameters.getChildren("parameter").iterator(); 
				 while(i.hasNext())
				 {
					 Element courant = (Element)i.next();
					 
					 if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
			        		 equals("valued_graph"))
			         {
			        	 if ( (courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
				        	 		getChild("label")!=null) &&
				        	 		(courant.getChild("value").
				        	 		getChild("label").getValue().equals("true")))
			        		 valuedGraph=true;
			        	 
			        	 else if ( (courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
				        	 		getChild("label")!=null) &&
				        	 		(courant.getChild("value").
				        	 		getChild("label").getValue().equals("false")))
			        		 valuedGraph= false;
			        	 else
			        	 {
			        		 // Default value
			        		 valuedGraph= false;
			        		 warning=warning.concat("\nWarning: an error occured while trying to get parameter 'A valued graph?'" +
			        		 		". The default value is set to 'false'.");
			        	 }
			         }
					 else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
			        		 equals("transitive_reduction"))
			         {
			        	 if ( (courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
				        	 		getChild("label")!=null) &&
				        	 		(courant.getChild("value").
				        	 		getChild("label").getValue().equals("true")))
			        		 transitive=true;
			        	 
			        	 else if ( (courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
				        	 		getChild("label")!=null) &&
				        	 		(courant.getChild("value").
				        	 		getChild("label").getValue().equals("false")))
			        		 transitive= false;
			        	 else
			        	 {
			        		 // Default value
			        		 transitive= false;
			        		 warning=warning.concat("\nWarning: an error occured while trying to get" +
			        		 		" the parameter 'With transitive reduction?'" +
		        		 		". The default value is set to 'false'.");
			        	 }
			         }

					 else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
			        		 equals("node_shape"))
			        		 {
			        	 		if ((courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
						        	 		getChild("label")!=null) )
			        	 		shape=courant.getChild("value").
			        	 			getChild("label").getValue();
			        	 		
			        	 		else
			        	 		{
			        	 			 warning=warning.concat("\nWarning: an error occured while " +
			        	 			 		"trying to get the parameter 'Shape of the nodes?'" +
				        		 		". The default value is set to 'Rectangle'.");
			        	 		}
			        		 }
					 else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
			        		 equals("use_color"))
			        		 {
			        	 		if ((courant.getChild("value")!= null) && 
					        			 (courant.getChild("value").
								        	 		getChild("label")!=null) &&
			        	 				(courant.getChild("value").
			        	 		getChild("label").getValue().equals("false")))
			        	 		color=false;
			        	 		else if ((courant.getChild("value")!= null) && 
					        			 (courant.getChild("value").getChild("label")!=null) &&
								        	 		(courant.getChild("value").
					        	 		getChild("label").getValue().equals("true")))
					        	 		color=true;
			        	 		else
			        	 		{
			        	 			// Default value
			        	 			color=false;
			        	 			warning=warning.concat("\nWarning: an error occured while " +
			   			        	 			 		"trying to get the parameter 'Colored graph?'" +
			   				        		 		". The default value is set to 'false'.");
			        	 		}
			        		 }
					 else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
			        		 equals("selected_color"))
			        		 {
			        	 		if ((courant.getChild("value")!= null) && 
			        			 (courant.getChild("value").
						        	 		getChild("label")!=null) )
			        	 		colorToUse=courant.getChild("value").
			        	 			getChild("label").getValue();
			        	 		else
			        	 		{
			        	 			warning=warning.concat("\nWarning: an error occured while " +
				   			        	 			 		"trying to get the parameter 'Choose color:'" +
				   				        		 		". The default value is set to 'Black'.");
			        	 		}
			        		 }
					 else {// we have only the previous parameters in method parameters}
		        	 }
				 }
			 }
			 else 
				   statusOK=false;
		   }
		 else 
			 statusOK=false;
		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}
	public String getColorToUse()
	{
		return colorToUse;
	}
	public boolean getValuedGraph()
	{
		return valuedGraph;
	}
	
	public boolean getTransitiveReduction()
	{
		return transitive;
	}
	public String getShape()
	{
		return shape.toLowerCase();
	}
	
	public String getWarning()
	{
		return warning;
	}
	
}
