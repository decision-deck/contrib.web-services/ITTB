import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class readCsv
{
	private String errorMessage;
	private String warningMessage;
	private   Element racine = new Element("XMCDA");		 
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");		 
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");
	private org.jdom2.Document document = new Document(racine);



	public readCsv()
	{
		errorMessage="";
		warningMessage="";
	}

	protected  void createOutputFile(String[] alternativeId,String[] lowCategory,String[] upCategory,int length)  
	{

		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		Element projectReference = new Element("projectReference");
		racine.addContent(projectReference);

		Element title = new Element("title");
		projectReference.addContent(title);
		title.setText("Categories profiles");

		Element categoriesProfiles = new Element("categoriesProfiles");
		racine.addContent(categoriesProfiles);

		for (int i=0;i<length;i++)
		{
			Element categoryProfile = new Element("categoryProfile");
			categoriesProfiles.addContent(categoryProfile);
			Element alternativeID = new Element("alternativeID");
			categoryProfile.addContent(alternativeID);
			alternativeID.setText(alternativeId[i]);

			Element limits = new Element("limits");
			categoryProfile.addContent(limits);

			Element lowerCategory = new Element("lowerCategory");
			limits.addContent(lowerCategory);
			Element categoryID = new Element("categoryID");
			lowerCategory.addContent(categoryID);
			categoryID.setText(lowCategory[i]);

			Element upperCategory = new Element("upperCategory");
			limits.addContent(upperCategory);
			Element categoryID2 = new Element("categoryID");
			upperCategory.addContent(categoryID2);
			categoryID2.setText(upCategory[i]);
		}
	}

	public void save(String fichier)
	{
		try
		{	        
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fichier));
		}
		catch (java.io.IOException e){}
	}

	public boolean extractInformation(String path)
	{	
		boolean status=true;
		String alternativeId[] = new String[1000];
		String lowerCategory[] = new String[1000];
		String upperCategory[] = new String[1000];
		int indexExtract=0;
		try
		{

			BufferedReader fichier_source = new BufferedReader(new FileReader(path));
			String chaine;

			try
			{
				while(status && (chaine = fichier_source.readLine())!= null)
				{
					String separator=",";
					if(chaine.contains(";"))
						separator=";";

						String[] tabChaine = chaine.split(separator);
						//J'effectue les traitements avec les donn�es contenues dans le tableau
						//La premi�re information se trouve � l'indice 0
						if(tabChaine.length==3)
						{
							alternativeId[indexExtract]=tabChaine[0];
							lowerCategory[indexExtract]=tabChaine[1];
							upperCategory[indexExtract]=tabChaine[2];
							indexExtract++;   
						}
						else
							status=false;
				}
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try
			{
				fichier_source.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Le fichier est introuvable !");
		}

		if(indexExtract>0)
			createOutputFile(alternativeId,lowerCategory,upperCategory,indexExtract)  ;
		else
			status=false;

		return status;
	}

	public static void main(String[] argv)
	{
		readCsv parseCSV=new readCsv();
		boolean stopRunning = false;

		if (!new File(argv[0]).exists()|| argv.length != 3) 
			stopRunning = true;

		if (stopRunning)
			parseCSV.errorMessage =parseCSV.errorMessage.concat("Error: unable to run the algorithm: "
					+ "the entry file may be inexistant. Check your execution command. ");

		if (stopRunning == false)
		{
			String pathCSV = argv[0];

			boolean check =parseCSV.extractInformation(pathCSV);	
			if(!check)
			{
				parseCSV.errorMessage =parseCSV.errorMessage.concat("The csv entry file is empty or erroneous.");
				// StopRunning =true
				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createErrorMessage(parseCSV.getError());
				String pathOutputMessage = argv[argv.length-1];
				xmcdaMess.save(pathOutputMessage);
			}
			else
			{
				// Output XMCDA File for the CategoriesProfiles
				String pathOutputFile = argv[argv.length - 2];
				parseCSV.save(pathOutputFile);			

				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createLogMessage(parseCSV.getError(), parseCSV.getWarning());
				String pathOutputMessage = argv[argv.length - 1];
				xmcdaMess.save(pathOutputMessage);
			}
		}// StopRunning =false
		else
		{
			// StopRunning =true
			// Output Log Message File
			OutputMessage xmcdaMess = new OutputMessage();
			xmcdaMess.createErrorMessage(parseCSV.getError());
			String pathOutputMessage = argv[argv.length-1];
			xmcdaMess.save(pathOutputMessage);
		}
	}

	public String getError()
	{
		return errorMessage;
	}

	public String getWarning()
	{
		return warningMessage;
	}
}
