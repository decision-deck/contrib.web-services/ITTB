﻿================================================================
 The web service « csvToXMCDA-categoriesProfiles-ITTB »
================================================================

How to run the java program in command line?
===================================================
::

java –Djava.awt.headless =true -jar csvToXMCDA-categoriesProfiles-ITTB.jar categoriesProfiles.csv categories_profiles.xml messages.xml 

where:

- The entry file `` categoriesProfiles.csv ``
- The output file `` categories_profiles.xml `` is an XMCDA compliant file
- The output file ``messages.xml`` 

How to run the tests?
=====================

You have to provide the entry file `` categoriesProfiles.csv `` (check everything is OK)
