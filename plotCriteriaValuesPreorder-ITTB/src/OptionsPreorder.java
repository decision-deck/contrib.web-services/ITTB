import java.util.Iterator;

import org.jdom2.Element;


public class  OptionsPreorder {
	private boolean color;
	private String colorToUse;
	private String shape;
	private String warning;
	boolean place_names;
	private String title;
	private String order;

	public OptionsPreorder()
	{
		title="";
		color=false;
		colorToUse="Black";
		shape="Rectangle";
		place_names=false;
		warning="";
		order="decreasing";
	}

	public boolean parse(Element racine)
	{
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator(); 
				while(i.hasNext())
				{
					Element courant = (Element)i.next();

					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("plot_title"))
					{		
						title=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("plot_order"))
					{		
						order=courant.getChild("value").
								getChild("label").getValue();
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("increasing")))
							order="increasing";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) &&
								(courant.getChild("value").
										getChild("label").getValue().equals("decreasing")))
							order="decreasing";
						else
						{
							// Default value
							order="decreasing";
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Order'" +
									". The default value is set to 'decreasing'.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("use_color"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							color=false;
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) &&
								(courant.getChild("value").
										getChild("label").getValue().equals("true")))
							color=true;
						else
						{
							// Default value
							color=false;
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Use colors?'" +
									". The default value is set to 'false'.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("selected_color"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) )
							colorToUse=courant.getChild("value").
							getChild("label").getValue();
						else
						{
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Choose color:'" +
									". The default value is set to 'Black'.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("node_shape"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) )
							shape=courant.getChild("value").
							getChild("label").getValue();
						else
						{
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Shape of the nodes?'" +
									". The default value is set to 'Rectangle'.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("show_names"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							place_names=false;
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) &&
								(courant.getChild("value").
										getChild("label").getValue().equals("true")))
							place_names=true;
						else
						{
							// Default value
							place_names=false;
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Alternative name or id?'" +
									". The default value is set to 'id'.");
						}
					}
					else {// we have only the previous parameters in method parameters}
					}
				}
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;
		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}
	public String getColorToUse()
	{
		return colorToUse;
	}
	public String getTitle()
	{
		return title;
	}

	public boolean getNamesOrIDs()
	{
		return place_names;
	}
	public String getShape()
	{
		return shape.toLowerCase();
	}

	public String getWarning()
	{
		return warning;
	}

	public String getOrder()
	{
		return order;
	}

}
