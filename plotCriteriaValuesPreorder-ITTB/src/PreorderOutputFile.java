import java.io.File;

import eu.telecom_bretagne.utils.GraphViz;

public class PreorderOutputFile
{
	public static void createFiles(GraphViz gv, File dotFile, File pngFile)
	{
		gv.createDotFile(dotFile);
		gv.createImage(dotFile, "png", pngFile);
	}
}
