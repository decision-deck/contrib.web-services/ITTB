import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.imageio.ImageIO;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.telecom_bretagne.utils.GraphViz;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaValues;
import eu.telecom_bretagne.xmcda.CriterionValue;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_CriteriaValues;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class PlotCritValPreorder
{

	private static String warningMessage = "";

	public boolean checkFiles(Criteria criter, CriteriaValues criterVal)
	{
		boolean result = true;// les listes sont coh�rentes
		int diff = 0;
		if (criter.getCriteriaIDs().size() != criterVal.size())
		{
			warningMessage += "\nPlease check the file containing the criteria tag: incoherence with the number "
			                  + "of criteria given in the file containing the criteriaValues tag.";
			return false;
		}
		for (String s: criterVal.getCriteriaIDs())
		{
			if (!criter.getCriteriaIDs().contains(s))
			{
				result = false;
				diff++;
			}
		}
		if (diff > 1)
			warningMessage += "\nPlease check the file containing the criteria tag: incoherence with the criteria given "
					+ "in the file containing the criteriaValues tag. There are " + diff + "different criteria.";
		if (diff == 1)
			warningMessage += "\nPlease check the file containing the criteria tag: incoherence with the criteria given "
					+ "in the file containing the criteriaValues tag. There is one different criterion.";
		return result;
	}

	public ArrayList<String> getIntersectionCriteriaIDs(Criteria criter, CriteriaValues criterVal)
	{
		ArrayList<String> listintersection = new ArrayList<String>();

		for (String t: criterVal.getCriteriaIDs())
			if (criter.getCriteriaIDs().contains(t))
			    listintersection.add(t);
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>(new TreeSet<String>(listintersection));

		return listintersection;
	}

	public ArrayList<String> getIntersectionCriteriaNames(Criteria criter, CriteriaValues criterVal,
	                                                      ArrayList<String> listintersectionIds)
	{
		ArrayList<String> listintersectionNames = new ArrayList<String>();

		for (int i = 0; i < listintersectionIds.size(); i++)
		{
			if (criter.getCriteriaIDs().contains(listintersectionIds.get(i)))
			{
				int index = criter.getCriteriaIDs().indexOf(listintersectionIds.get(i));
				if (criter.getListCriteriaNames().get(index) != null)
					listintersectionNames.add(criter.getListCriteriaNames().get(index));
				else
					listintersectionNames.add(null);
			}
			else
				listintersectionNames.add(null);

		}
		return listintersectionNames;
	}

	protected static void echange(double[] values, int element1, int element2)
	{
		double temp = values[element1];
		values[element1] = values[element2];
		values[element2] = temp;
	}

	protected static void exchangeIDs(String[] listIds, int index1, int index2)
	{
		String temp = listIds[index1];
		listIds[index1] = listIds[index2];
		listIds[index2] = temp;
	}

	protected static String convertImage(File image_png, String format)
	{
		String encodedImage = "";
		try
		{
			BufferedImage image = ImageIO.read(image_png);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		if (image_png.delete() == false)
		    System.err.println("Warning: " + image_png.getAbsolutePath() + " could not be deleted!");
		return encodedImage;
	}

	public GraphViz createGraph(String[] listintersectionIds, String[] listintersectionNames, String color,
	                            String node_shape, boolean namesToPlace, int size,
	                            String title)
	{
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph(color, node_shape));

		String temp[] = new String[size];
		for (int i = 0; i < size; i++)
		{
			temp[i] = listintersectionIds[i];
			if (listintersectionIds[i].contains(", "))
			{
				temp[i] = temp[i].replaceAll(", ", "");
			}
		}

		if (!namesToPlace)
			for (int i = 0; i < size; i++)
			{
				gv.addln(temp[i] + " [label = \"" + listintersectionIds[i] + "\", fontcolor=" + color + "];");
			}
		else
		{
			for (int i = 0; i < size; i++)
			{
				if (listintersectionNames[i] != null)
					/*
					 * gv.addln(listintersectionIds[i]+ "[label="+ listintersectionNames[i]+", fontcolor="+color+"]");
					 */
					gv.addln(temp[i] + "[label=\"" + listintersectionNames[i] + "\", fontcolor=" + color + "]");
				else
				{
					// gv.addln(listintersectionIds[i]+ "[fontcolor="+color+"]");
					gv.addln(temp[i] + " [label = \"" + listintersectionIds[i] + "\", fontcolor="+ color + "];");
					warningMessage += "\n Warning: the attribute 'name' of the criterion ID: " + listintersectionIds[i]
					                + " does not exist. Please check the entry file containing the criteria tag.";
				}
			}
		}
		for (int i = 0; i < size - 1; i++)
			gv.addln(temp[i] + " -> " + temp[i + 1] + ";");

		gv.add(gv.title(title));
		gv.addln(gv.end_graph());
		return gv;
	}

	protected static int existsInTab(double[] alternValuesToUse, int size, double value)
	{
		int exists = -1;
		int i = 0;
		while ( ( i < size ) && ( exists == -1 ) )
		{
			if (alternValuesToUse[i] == value)
			    exists = i;
			i++;
		}
		return exists;
	}

	public GraphViz generatePlot(CriteriaValues criteriaValuesToPlot, Criteria criter,
	                           ArrayList<String> listintersectionIds, ArrayList<String> listintersectionNames,
	                           String tempErrorMessage, String order, String color, String node_shape,
	                           boolean namesToPlace, String title)
	{
		/* Representation of the CriteriaValuesPreorder */

		double[] ySeries = new double[listintersectionIds.size()];

		String tempId = "";
		String tempName = "";
		String[] listOrderIds = new String[listintersectionIds.size()];
		String[] listOrderNames = new String[listintersectionIds.size()];

		for (int i = 0; i < listintersectionIds.size(); i++)
		{
			if (criteriaValuesToPlot.getCriteriaIDs().contains(listintersectionIds.get(i)))
			{
				ySeries[i] = criteriaValuesToPlot.getListCriteriaValues()
				        .get(criteriaValuesToPlot.getCriteriaIDs().indexOf(listintersectionIds.get(i))).getRealValue();


				tempId = criteriaValuesToPlot.getCriteriaIDs().get(criteriaValuesToPlot.getCriteriaIDs()
				                                                           .indexOf(listintersectionIds.get(i)));

				listOrderIds[i] = tempId;

				tempName = listintersectionNames.get(listintersectionIds.indexOf(tempId));
				listOrderNames[i] = tempName;
			}
		}


		// Check pour les m�mes valeurs qui seront � repr�senter dans une m�me case

		double[] alternValuesToUse = new double[listintersectionIds.size()];
		String[] listIdsToUse = new String[listintersectionIds.size()];
		String[] listNamesToUse = new String[listintersectionIds.size()];

		int index = 0;
		for (int i = 0; i < listintersectionIds.size(); i++)
		{

			int existTest = existsInTab(alternValuesToUse, index, ySeries[i]);
			if (existTest == -1) // �a n'existe pas dans le tableau
			{
				alternValuesToUse[index] = ySeries[i];
				listIdsToUse[index] = listOrderIds[i];
				listNamesToUse[index] = listOrderNames[i];
				index++;
			}
			else
			// �a existe d�j� dans le tableau
			{
				listIdsToUse[existTest] = listIdsToUse[existTest].concat(", " + listOrderIds[i]);
				if (listOrderNames[i] != null)
				    listNamesToUse[existTest] = listNamesToUse[existTest].concat(", " + listOrderNames[i]);
			}
		}

		if (order.equals("increasing"))
		{
			for (int i = 0; i < index - 1; i++)
			{
				for (int j = i + 1; j < index; j++)
				{
					if (alternValuesToUse[j] < alternValuesToUse[i])
					{
						echange(alternValuesToUse, j, i);
						exchangeIDs(listIdsToUse, j, i);
						exchangeIDs(listNamesToUse, j, i);
					}
				}
			}
		}
		else
		{
			// Default value: decreasing order
			for (int i = 0; i < index - 1; i++)
			{
				for (int j = i + 1; j < index; j++)
				{
					if (alternValuesToUse[j] > alternValuesToUse[i])
					{
						echange(alternValuesToUse, j, i);
						exchangeIDs(listIdsToUse, j, i);
						exchangeIDs(listNamesToUse, j, i);
					}
				}
			}
		}
		return createGraph(listIdsToUse, listNamesToUse, color, node_shape, namesToPlace, index, title);
	}

	protected static String getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		String color = "";
		if (black_and_white)
			color = "black";
		else
			color = colorToUse.toLowerCase();

		return color;
	}

	public static void main(String[] argv)
	{
		if (argv.length != 6)
		{
			System.err
			        .println("Usage: criteria.xml criteriaValues.xml parameters.xml criteriaValuesPreorder.dot criteriaValuesPerorder.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathCriteria = argv[0];
		final String pathCriteriaValues = argv[1];
		final String pathOptions = argv[2];
		final File pathOutputDotFile = new File(argv[3]);
		final File pathOutputFilePlot = new File(argv[4]);
		final String pathOutputMessage = argv[5];

		String tempErrorMessage = "";
		if (!new File(pathOptions).exists() || !new File(pathCriteria).exists() || !new File(pathCriteria).exists())
		{
			tempErrorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction = new Main();

		String errMethodParameters = mainFunction.checkEntryFile(pathOptions);

		if (errMethodParameters != "")
		    tempErrorMessage += "\nFatal Error: the file containing the parameters is not a valid XMCDA document.";

		String errCriteria = mainFunction.checkEntryFile(pathCriteria);
		if (errCriteria != "")
		    tempErrorMessage += "\nFatal Error: the file containing the criteria tag is not a valid XMCDA document.";

		String errCriteriaValues = mainFunction.checkEntryFile(pathCriteriaValues);
		if (errCriteriaValues != "")
		{
			// System.out.println("errAlternativesValues: "+errAlternativesValues);
			tempErrorMessage += "\nFatal Error: the file containing the criteriaValues tag is not a valid XMCDA document.";
		}

		if (!"".equals(errMethodParameters) || !"".equals(errCriteria) || !"".equals(errCriteriaValues))
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		PlotCritValPreorder plot = new PlotCritValPreorder();
		if (mainFunction.getWarningMessage() != "")
		    warningMessage += mainFunction.getWarningMessage();


		// Parsing XML file for Criteria
		mainFunction.PrepareParsing(pathCriteria);
		Criteria criteriaToUse = new Criteria();
		JDOM_Criteria jdomCrit = new JDOM_Criteria();
		criteriaToUse = jdomCrit.parseCriteriaXML(mainFunction.getRacine());
		if (criteriaToUse == null)
		{
			tempErrorMessage += "\nFatal Error: the file containing the criteria tag is erroneous or empty.";
		}

		if (jdomCrit.getWarning() != "")
		    warningMessage += "\n" + jdomCrit.getWarning();

		// Parsing XML file for CriteriaValues
		mainFunction.PrepareParsing(pathCriteriaValues);
		CriteriaValues criteriaValuesToUse = new CriteriaValues();
		JDOM_CriteriaValues jdomCriterVal = new JDOM_CriteriaValues();
		criteriaValuesToUse = jdomCriterVal.parseCriteriaValuesXML(mainFunction.getRacine());
		if (criteriaValuesToUse == null)
		{
			tempErrorMessage += "\nFatal Error: the file containing the criteriaValues tag is erroneous or empty.";
		}
		if (jdomCriterVal.getWarningMessage() != "")
		    warningMessage = warningMessage.concat("\n" + jdomCriterVal.getWarningMessage());

		// ParseMethodParameters
		OptionsPreorder options = new OptionsPreorder();
		mainFunction.PrepareParsing(pathOptions);
		boolean statusOptions = options.parse(mainFunction.getRacine());

		if (!statusOptions)
		{
			tempErrorMessage += "\nFatal Error: the file containing the parameters is erroneous or empty.";
		}

		if (criteriaToUse == null || criteriaValuesToUse == null || !statusOptions)
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		plot.checkFiles(criteriaToUse, criteriaValuesToUse);

		// Intersection list
		ArrayList<String> listintersectionCriteria = plot
		        .getIntersectionCriteriaIDs(criteriaToUse, criteriaValuesToUse);

		// Si l'intersection est vide on prend ce qu'on a dans la criteriaValues.xml
		if (listintersectionCriteria.isEmpty())
		{
			warningMessage = warningMessage.concat("\nOnly the criteria IDs "
			                                       + "in the file containing the criteriaValues tag were considered. "
			                                       + "Please check your input file containing the criteria tag.");
			for (CriterionValue criterionValue: criteriaValuesToUse)
				listintersectionCriteria.add(criterionValue.getCriterion().id());
		}

		if (listintersectionCriteria.isEmpty())
		{
			// Output Log Message File
			String errorMessage = "Failure to run the algorithm because it seems that the file containing "
			                      + "the criteriaValues tag is empty or erroneous.";
			new XmcdaMessage().createErrorMessage(errorMessage).enregistre(pathOutputMessage);
			return;
		}

		if (options.getWarning() != "")
		    warningMessage = warningMessage.concat(options.getWarning());

		String plotTitle = options.getTitle();
		String order = options.getOrder();
		boolean color = options.getColor();
		String colorOptions = options.getColorToUse();
		if (!color && colorOptions != "Black")
		    warningMessage = warningMessage.concat("\nWarning: the selected color (" + colorOptions + ") was ignored "
		                                           + "because you have chosen a black and white plot.");

		String colorToUse = getColorFromOptions(!color, colorOptions);
		String node_shape = options.getShape();
		boolean namesToPlace = options.getNamesOrIDs();

		GraphViz gv = null;

		if (listintersectionCriteria.size() != 0)
		{
			// Run the algorithm
			gv = plot.generatePlot(criteriaValuesToUse, criteriaToUse, listintersectionCriteria, plot
			                                         .getIntersectionCriteriaNames(criteriaToUse, criteriaValuesToUse,
			                                                                       listintersectionCriteria),
			                                 tempErrorMessage, order, colorToUse, node_shape, namesToPlace, plotTitle);
		}
		else
		{
			tempErrorMessage += "\n We cannot run the algorithm since the criteria list of the PROJECT is empty.\n";
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessageForCriteriaValPlot(errCriteria, errCriteriaValues, warningMessage, tempErrorMessage);
		xmcdaMess.enregistre(pathOutputMessage);

		if (listintersectionCriteria.size() != 0 && gv != null)
		{
			PreorderOutputFile.createFiles(gv, pathOutputDotFile, pathOutputFilePlot);
		}

	}

}
