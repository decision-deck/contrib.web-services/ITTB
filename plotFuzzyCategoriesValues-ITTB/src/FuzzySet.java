import java.util.ArrayList;


public class FuzzySet
{
	private ArrayList <String> listValuesIDs;
	private ArrayList <FuzzyNumber> fuzzyNumbers;

	public FuzzySet()
	{
		listValuesIDs=new ArrayList <String>();
		fuzzyNumbers=new ArrayList <FuzzyNumber>();
	}

	public void addValueID(String id)
	{
		listValuesIDs.add(id);
	}

	public void addPointToFuzzyNumber(String id, Point p)
	{
		int index=listValuesIDs.indexOf(id);
		fuzzyNumbers.get(index).addPoint(p);
	}

	public void addXYToFuzzyNumber(String id, double x, double y)
	{
		int index=listValuesIDs.indexOf(id);
		fuzzyNumbers.get(index).addXY(x,y);
	}

	public ArrayList <String> getIDsList()
	{
		return listValuesIDs;
	}

	public void addAFuzzyNumber(FuzzyNumber fz)
	{
		fuzzyNumbers.add(fz);
	}

	public ArrayList <FuzzyNumber> getFuzzyNumbers()
	{
		return fuzzyNumbers;
	}

}
