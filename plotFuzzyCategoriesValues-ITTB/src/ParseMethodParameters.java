import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class ParseMethodParameters {

	static org.jdom2.Document document;
	private boolean color;
	private String chartTitle;
	private String XaxisLabel;
	private String YaxisLabel;

	public  ParseMethodParameters (){
		color=false;
		chartTitle="";
		XaxisLabel="";
		YaxisLabel="";
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		return document.getRootElement();
	}

	public boolean parse(Element racine)
	{	      
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("chart_title"))
					{
						chartTitle=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("x_axis"))
					{
						XaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("y_axis"))
					{
						YaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("use_color"))
					{
						if (courant.getChild("value").
								getChild("label").getValue().equals("false"))
							color=false;
						else if (courant.getChild("value").
								getChild("label").getValue().equals("true"))
							color=true;
						else color=false;
					}
					/*else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("selected_color"))
		         		{
		        	 		colorToUse=courant.getChild("value").
		        	 		getChild("label").getValue();
		        		 }*/
					else {
						statusOK=false;
					}
				}  
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;

		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}

	public String getChartTitle()
	{
		return chartTitle;
	}
	public String getDomainAxisLabel()
	{
		return XaxisLabel;
	}
	public String getRangeAxisLabel()
	{
		return YaxisLabel;
	}

}