import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class mainFuzzyPlot
{
	private   Element racine = new Element("XMCDA");		 
	private org.jdom2.Document document = new Document(racine);

	private static final String XMCDA_2_0_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.0.0";

	private static final String XMCDA_2_0_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd";

	private static final String XMCDA_2_1_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.1.0";

	private static final String XMCDA_2_1_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd";

	private static final String XMCDA_2_2_0_NAMESPACE     = "http://www.decision-deck.org/2012/XMCDA-2.2.0";

	private static final String XMCDA_2_2_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd";

	private static final String ACCEPTED_SCHEMA_LOCATIONS = XMCDA_2_0_0_NAMESPACE + " " + XMCDA_2_0_0_SCHEMA + " "
			+ XMCDA_2_1_0_NAMESPACE + " " + XMCDA_2_1_0_SCHEMA +" "
			+ XMCDA_2_2_0_NAMESPACE + " " + XMCDA_2_2_0_SCHEMA;

	public  mainFuzzyPlot()
	{
	}

	public void PrepareParsing(String path)
	{
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){
			//System.out.println(e.getStackTrace());
		}
		racine = document.getRootElement();
	}

	protected static String checkEntryFile(String path)
	{
		String error="";
		try {
			parseXML(path);

		} catch (JDOMException e) {

			error=error.concat("File is not well-formed: "+e.getMessage());
		} catch (IOException e) {

			error=error.concat("Could not check file because: "+e.getMessage());
		}
		return error;
	}

	protected static void parseXML(final String xml) throws JDOMException, IOException {
		SAXBuilder builder = 
				new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
		builder.setFeature(
		                   "http://apache.org/xml/features/validation/schema", true);

		builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
		                    ACCEPTED_SCHEMA_LOCATIONS);

		@SuppressWarnings("unused")
		Document doc= builder.build(xml);
	}

	protected static Color getColorFromOptions(boolean black_and_white, String colorsToUse)
	{
		Color color = Color.black;
		if (black_and_white)
			return color;

		Color c = null;
		try
		{
			c = (Color)Color.class.getField(colorsToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c==null )
			return color;
		color = c;
		return color;
	}

	public String generatedPlot(String title, String XaxisLabel,String YaxisLabel,FuzzySet fuzzySet,boolean color)
	{
		ByteArrayOutputStream chart_png_image = new ByteArrayOutputStream();
		generatePlot(chart_png_image, title, XaxisLabel,YaxisLabel,fuzzySet, color);

		ImageGenerated imageConvert=new ImageGenerated();
		String encodedImage=imageConvert.convertImage(
		                                              new ByteArrayInputStream(chart_png_image.toByteArray()),"png");
		/*if (imageConvert.getImageErrorMessage()!="")
	                			tempErrorMessage=tempErrorMessage.concat(imageConvert.getImageErrorMessage());*/
		return encodedImage;
	}

	public void generatePlot(OutputStream outputStream, String title, String XaxisLabel,String YaxisLabel,FuzzySet fuzzySet,boolean color)
	{
		FuzzyPlot fuzzyPlot=new FuzzyPlot(title, XaxisLabel,YaxisLabel,fuzzySet, color);
		fuzzyPlot.saveChart(outputStream);
	}

	public static void main(String[] argv)
	{
		mainFuzzyPlot plot=new  mainFuzzyPlot();
		String errorMessage="";
		String warningMessage = "";

		boolean stopRunning=false;

		if (argv.length != 4)
		{
			System.err
			        .println("Usage: fuzzyCategoriesValues.xml parameters.xml fuzzyCategoriesValues.png messages.xml");
			System.err.println("       The 2 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		if(argv[0]==null ||argv[1]==null ||
				!new File(argv[0]).exists()||!new File(argv[1]).exists())
		{
			stopRunning =true;
			errorMessage="Error: unable to run the algorithm. Reason: missing one (or more) entry file(s)." +
					"Please check your entry files.";
		}
		
		if (!stopRunning)
		{
			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=argv[argv.length - 1];
			xmcdaMess.save(pathOutputMessage);
			return;
		}

		//ParseMethodParameters
		String pathFuzzy=argv[0];
		String pathOptions=argv[1];
		final String fuzzyCategoriesValues_png = argv[2];
		final String results_xml = argv[3];
		String errParameters = checkEntryFile(pathOptions);
		String errFuzzy =checkEntryFile(pathFuzzy);

		if(errParameters!="")
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the parameters " +
					"is not a valid XMCDA document: "+errParameters);

		if(errFuzzy!="")
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the 'categoriesValues' " +
					"tag (fuzzy numbers) is not a valid XMCDA document: "+errFuzzy);

		if(errParameters!="" ||errFuzzy!="")
		{
			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=results_xml;
			xmcdaMess.save(pathOutputMessage);
			return;
		}

		//ParseMethodParameters
		ParseMethodParameters parseMethodParam=new ParseMethodParameters();
		plot.PrepareParsing (pathOptions);
		boolean statusParseMethod=parseMethodParam.parse(plot.racine);

		if(!statusParseMethod)
		{
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
					" parameters is erroneous or empty.");
		}

		//Parsing XML file for categoriesValues (fuzzy numbers)
		plot.PrepareParsing (pathFuzzy);
		FuzzySet fuzzySet=new FuzzySet();
		ParseCategoriesValues categVal= new ParseCategoriesValues ();
		boolean parseCategValNull=false;
		fuzzySet=categVal.exploreCategoriesValues(plot.racine);
		if(fuzzySet==null)
		{	
			parseCategValNull=true;
			errorMessage=errorMessage.concat("\nFatal Error: the file containing the 'categoriesValues' " +
					"tag (fuzzy numbers) is erroneous or empty.");
		}

		if(categVal.getWarning()!="")
			warningMessage= warningMessage.concat("\n"+categVal.getWarning());

		if(parseCategValNull || !statusParseMethod)
		{
			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=results_xml;
			xmcdaMess.save(pathOutputMessage);
			return;
		}

		String chartTitle=parseMethodParam.getChartTitle();
		String XaxisLabel=parseMethodParam.getDomainAxisLabel();
		String YaxisLabel=parseMethodParam.getRangeAxisLabel();

		boolean color =parseMethodParam.getColor();
		try {
			plot.generatePlot(new FileOutputStream(new File(fuzzyCategoriesValues_png)), chartTitle, XaxisLabel,YaxisLabel,fuzzySet,color);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Output Log Message File
		StatusMessage xmcdaMess = new StatusMessage();
		xmcdaMess.createLogMessage(warningMessage,errorMessage);		
		String pathOutputMessage = results_xml;
		xmcdaMess.save(pathOutputMessage);
	}

}
