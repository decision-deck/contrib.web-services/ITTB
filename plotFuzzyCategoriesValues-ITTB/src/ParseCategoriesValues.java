import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class ParseCategoriesValues {

	static org.jdom2.Document document;
	private String warning = "";

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}
		return document.getRootElement();
	}

	public FuzzySet exploreCategoriesValues(Element racine)
	{	      
		FuzzySet fuzzySet=new FuzzySet();
		if(racine.getChild("categoriesValues")!=null)
		{
			Element categoriesValues = racine.getChild("categoriesValues");	  
			if(categoriesValues.getChild("categoryValue")!=null)
			{
				Element categoryValue= categoriesValues.getChild("categoryValue");   	  
				if(categoryValue.getChild("values")!=null)
				{
					Element values=categoryValue.getChild("values");
					if(values.getChildren("value")!=null)
					{
						Iterator<Element> i = values.getChildren("value").iterator();

						boolean numeric=true;
						while(i.hasNext() && numeric)
						{ 
							Element courant = (Element)i.next();
							String id=courant.getAttributeValue("id");
							if(fuzzySet.getIDsList().contains(id))
							{
								String tempWarning="Duplicated id ("+id+") in the file containing the 'categoriesValues' tag. " +
										"Only the first occurence is considered.";
								if(warning.equals(""))
									warning=tempWarning;
								else
									warning=warning.concat(tempWarning);
							}
							else
							{
								fuzzySet.addValueID(id);
								Element fuzzyNumber=courant.getChild("fuzzyNumber");
								Element trapezoidal_triangular;
								int  nbrOfPoints=0;
								if(fuzzyNumber.getChild("trapezoidal")!=null)
								{
									trapezoidal_triangular=fuzzyNumber.getChild("trapezoidal");
									nbrOfPoints=4;
								}
								else
								{
									trapezoidal_triangular=fuzzyNumber.getChild("triangular");
									nbrOfPoints=3;
								}

								FuzzyNumber fz =new FuzzyNumber();
								for(int indexPoint=1;indexPoint<=nbrOfPoints;indexPoint++)
								{
									if(numeric)
									{
										String temp="point"+Integer.toString(indexPoint);
										Element point=trapezoidal_triangular.getChild(temp);
										double x = 0, y = 0;
										if(point.getChild("abscissa").getChild("integer")!=null)					    			 
											x=Double.parseDouble(point.getChild("abscissa").getChild("integer").getValue());
										else if (point.getChild("abscissa").getChild("real")!=null)	
											x=Double.parseDouble(point.getChild("abscissa").getChild("real").getValue());
										else
										{
											//erreur
											numeric =false;
											String tempWarning="\n Error in the file containing the 'categoriesValues' tag." +
													" The value of 'abscissa' for ("+id+") must be real or integer." ;
											if(warning.equals(""))
												warning=tempWarning;
											else
												warning=warning.concat(tempWarning);						    			 							    			 
										}
										if(numeric)
										{
											if(point.getChild("ordinate").getChild("integer")!=null)					    			 
												y=Double.parseDouble(point.getChild("ordinate").getChild("integer").getValue());
											else if (point.getChild("ordinate").getChild("real")!=null)	
												y=Double.parseDouble(point.getChild("ordinate").getChild("real").getValue());
											else
											{
												//erreur
												numeric =false;
												String tempWarning="\n Error in the file containing the 'categoriesValues' tag." +
														" The value of 'ordinate' for ("+id+") must be real or integer." ;
												if(warning.equals(""))
													warning=tempWarning;
												else
													warning=warning.concat(tempWarning);
											}
											fz.addXY(x, y);
										}
									}					    		
								}
								if(numeric)
									fuzzySet.addAFuzzyNumber(fz);
							}				    	   				    	  
						}
						return fuzzySet;
					}
					else
						return null; //value null
				}
				else
					return null;// values null
			}
			else  	   
				return null;//categoryValue null

		}
		else
			return null;// categoriesValues null

	}

	public String getWarning()
	{
		return warning;
	}

}
