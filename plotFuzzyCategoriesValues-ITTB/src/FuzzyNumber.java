import java.util.ArrayList;


public class FuzzyNumber
{
	private ArrayList <Point> points;
	public FuzzyNumber()
	{
		points=new ArrayList <Point>();
	}

	public void addPoint(Point p)
	{
		points.add(p);
	}

	public void addXY (double x, double y)
	{
		points.add(new Point(x,y));
	}

	public ArrayList <Point> getPoints()
	{
		return points;
	}

}
