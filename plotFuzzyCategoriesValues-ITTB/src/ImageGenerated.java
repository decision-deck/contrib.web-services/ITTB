import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class ImageGenerated {
	private String  errorMessage = "";

	public String convertImage(java.io.InputStream inputStream, String format)
	{
		String encodedImage = "";
		try
		{
			BufferedImage image = ImageIO.read(inputStream);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		}
		catch (Exception e)
		{
			errorMessage = errorMessage.concat("\n Error in the image conversion: " + e.getMessage());
		}
		return encodedImage;
	}

	public String convertImage(String pathImage,String format) 
	{  
		String encodedImage="";
		File fileTodelete=new File(pathImage);
		try{
			BufferedImage image = ImageIO.read(fileTodelete);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		} catch(Exception e) {
			//e.printStackTrace();
			errorMessage=errorMessage.concat("\n Error in the image conversion: "+e.getMessage());
		}
		fileTodelete.delete();
		return encodedImage;
	}  

	public String getImageErrorMessage()
	{
		return errorMessage;
	}

}
