import java.awt.BasicStroke;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.awt.Font;
//import java.awt.geom.Ellipse2D;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class FuzzyPlot {

	private JFreeChart chart;
	public FuzzyPlot(
	                 String chartTitle, String XaxisLabel,String YaxisLabel,FuzzySet fuzzySet,boolean color) {

		final XYDataset dataset = createDataset(fuzzySet);
		chart = createChart(dataset,chartTitle, XaxisLabel,YaxisLabel,
		                    color,fuzzySet);

	}

	public void saveChart(OutputStream outputStream)
	{
		try
		{
			ChartUtilities.writeChartAsPNG(outputStream, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveChart(String filepath)
	{
		File file= new File(filepath);
		try
		{
			ChartUtilities.saveChartAsPNG(file, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private XYDataset createDataset(FuzzySet fuzzySet){

		int size= fuzzySet.getIDsList().size();

		final XYSeries []series=new XYSeries [size];
		for (int j=0;j<size;j++)
			series[j] = new XYSeries(fuzzySet.getIDsList().get(j));

		final XYSeriesCollection dataset = new XYSeriesCollection();
		for (int j=0;j<size;j++)
		{
			int numberOfPoints=fuzzySet.getFuzzyNumbers().get(j).getPoints().size();
			for(int i=0;i<numberOfPoints;i++)
			{
				double x=fuzzySet.getFuzzyNumbers().get(j).getPoints().get(i).getX();
				double y=fuzzySet.getFuzzyNumbers().get(j).getPoints().get(i).getY();
				series[j].add(x,y);	 
			}
			dataset.addSeries(series[j]);
		}
		return dataset;           
	}

	private JFreeChart createChart(final XYDataset dataset,
	                               String chartTitle, String XaxisLabel,String YaxisLabel,
	                               boolean color,FuzzySet fuzzySet) {
		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart(
		                                                        chartTitle,       // chart title
		                                                        XaxisLabel,                    // domain axis label
		                                                        YaxisLabel,                   // range axis label
		                                                        dataset,                   // data
		                                                        PlotOrientation.VERTICAL,  // orientation
		                                                        true,                      // include legend
		                                                        true,                      // tooltips
		                                                        false                     // urls
				);
		chart.setBackgroundPaint(Color.white);
		chart.getLegend().setItemFont(new Font("Arial", Font.BOLD, 12));
		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.white);
		//plot.setDomainGridlinePaint(Color.white);
		//plot.setRangeGridlinePaint(Color.white);
		plot.setDomainGridlinesVisible(true);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.gray);
		plot.setDomainGridlinePaint(Color.gray);

		Font font = new Font("Arial", Font.BOLD, 12);

		final ValueAxis domainAxis = plot.getDomainAxis();       
		domainAxis.setTickLabelFont(font);       
		domainAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));

		// customize the range axis...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();      
		rangeAxis.setTickLabelFont(font);
		rangeAxis.setLabelFont(new Font("Arial", Font.BOLD, 16));

		// Customize the renderer...
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();      

		int maxNumber=30; //au plus 30 s�ries
		for(int i=0;i<maxNumber;i++)
		{       	 
			// bold lines
			renderer.setSeriesStroke( i, new BasicStroke(2) );
			if(!color)
				renderer.setSeriesPaint(i, Color.black);   

			//Interpolation 
			renderer.setSeriesLinesVisible(i,true);
			renderer.setSeriesShapesVisible(i, true);
			//renderer.setSeriesItemLabelsVisible(i, true);

		}
		plot.setRenderer(renderer);
		return chart;
	}

}
