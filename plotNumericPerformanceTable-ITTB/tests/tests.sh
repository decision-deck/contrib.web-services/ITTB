#! /bin/sh

JAR=../build/plotNumericPerformanceTable-ITTB.jar

OUT=out
TMPF=$("tempfile")

unset DISPLAY

for IN in in[0-9]*; do
  echo -n "$IN / out${IN#in} "
  /bin/rm -rf $OUT
  mkdir $OUT
  java -jar $JAR $IN/alternatives.xml $IN/criteria.xml $IN/performanceTable.xml $IN/parameters.xml $OUT/performanceTable.png $OUT/messages.xml
  diff -Bqrw out out${IN#in} > $TMPF 2>&1
  if [ $? -ne 0 ]; then
    echo "FAILED"
    cat $TMPF >&2
  else
    echo "SUCCESS"
  fi
  /bin/rm $TMPF
done


# How to extract the images from the XMCDA output: (multiple images)
# i=0; grep '<image>' out/performanceTablePlot.xml |while read line; do echo $line | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 2_$i.png; i=$((i+1)); done

#i=0; grep '<image>' out/performanceTablePlot.xml |while read line; do echo $line | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 2_$i.png; i=$((i+1)); done
#i=0; grep '<image>' out${IN#in}/performanceTablePlot.xml |while read line; do echo $line | sed -e 's|.*<image>\(.*\)</image>.*|\1|g'|base64 -d > 1_$i.png; i=$((i+1)); done
#
#for i in 1_*.png; do gthumb $i 2_${i##[0-9]*_}; done
