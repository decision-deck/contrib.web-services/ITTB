import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.IOException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.SortOrder;

public class PieChartPlot {
	private JFreeChart chart;

	public PieChartPlot(double[] ySeries, 
	                    Color c1, Color c2,
	                    String chartTitle, String XaxisLabel, String YaxisLabel,
	                    String []listIds, String []listNames,
	                    String order, String maxMin) {
		String[] columnKeys =new  String[ySeries.length];	
		for(int i=0;i<ySeries.length;i++){columnKeys[i]="" ;}

		// Initialization
		for(int i=0;i<ySeries.length;i++)
		{
			if(order.equals("by id"))
			{
				if (listNames[i]!=null && listIds[i]!=null)
					columnKeys[i]="("+listIds[i]+") "+listNames[i];
				else
					columnKeys[i]=listIds[i];
			}
			else
			{
				if (listNames[i]!=null && listIds[i]!=null)
					columnKeys[i]=listNames[i]+" ("+listIds[i]+")";
				else
					columnKeys[i]=listIds[i];
			}
		}

		if(order.equals("by id"))
		{
			for (int i=0;i<columnKeys.length -1;i++)
			{
				for (int j=i+1;j<columnKeys.length;j++)
				{
					if(maxMin.equals("increasing"))
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))<0)
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    			
						}
					}
					else //(maxMin.equals("decreasing")
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))>0)
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    			
						}
					}
				}
			}
		}
		else if(order.equals("by name"))
		{
			for (int i=0;i<columnKeys.length -1;i++)
			{
				for (int j=i+1;j<columnKeys.length;j++)
				{
					if(maxMin.equals("increasing"))
					{
						if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))<0)
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    
						}
					}
					else //(maxMin.equals("decreasing"))
					{
						if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))>0)
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    
						}
					}
				}
			}
		}
		else if(order.equals("by values"))
		{
			for (int i=0;i<ySeries.length -1;i++)
			{
				for (int j=i+1;j<ySeries.length;j++)
				{
					if (maxMin.equals("increasing"))
					{
						if(ySeries[j]<ySeries[i])
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    
						}
					}
					else //(maxMin.equals("decreasing"))
					{
						if(ySeries[j]>ySeries[i])
						{
							exchange (columnKeys,i,j);       				
							exchange (ySeries,i,j);    
						}
					}

				}
			}
		}
		DefaultPieDataset pieDataset = new DefaultPieDataset(); 
		for(int i=0;i<ySeries.length;i++)
			pieDataset.setValue(columnKeys[i], ySeries[i]);

		if(order.equals("by name") ||order.equals("by id"))
		{
			if(maxMin.equals("increasing"))
				pieDataset.sortByKeys(SortOrder.ASCENDING);
			else
				pieDataset.sortByKeys(SortOrder.DESCENDING);
		}
		else if (order.equals("by values"))
		{
			if(maxMin.equals("increasing"))
				pieDataset.sortByValues(SortOrder.ASCENDING);
			else
				pieDataset.sortByValues(SortOrder.DESCENDING);
		}
		// create the chart...
		chart = ChartFactory.createPieChart(
		                                    chartTitle,  // chart title
		                                    pieDataset,             // dataset
		                                    false,               // include legend
		                                    true,
		                                    false
				);

		chart.setBackgroundPaint(Color.white);

		final PiePlot plot = (PiePlot) chart.getPlot();
		plot.setBackgroundPaint(Color.white);
		plot.setCircular(true);
		plot.setLabelFont(new Font("Arial", Font.BOLD, 12));

		Paint []paint= new Paint [ySeries.length];

		int red1 = c1.getRed();
		int green1 = c1.getGreen();
		int blue1 = c1.getBlue();

		int red2 = c2.getRed();
		int green2 = c2.getGreen();
		int blue2 = c2.getBlue(); 

		int LARGEUR=ySeries.length;
		for(int i=0; i<ySeries.length; i++) {
			paint[i]=new Color((red1*(LARGEUR-i)+red2*i)/LARGEUR,
			                   (green1*(LARGEUR-i)+green2*i)/LARGEUR,
			                   (blue1*(LARGEUR-i)+blue2*i)/LARGEUR);
			plot.setSectionPaint(i ,paint[i]);

		} 
		plot.setLabelOutlinePaint(null);
		plot.setLabelShadowPaint(null);
		plot.setLabelBackgroundPaint(Color.WHITE);
		plot.setBackgroundPaint(Color.WHITE);

		plot.setNoDataMessage("No data available");
	}

	protected static void exchange (String[]columnKeys, int i, int j)
	{
		String tempKey=columnKeys[j];
		columnKeys[j]=columnKeys[i];
		columnKeys[i]=tempKey;
	}

	protected static void exchange (double[]ySeries, int i, int j)
	{
		double tempVal=ySeries[j];
		ySeries[j]=ySeries[i];
		ySeries[i]=tempVal;
	}

	public void saveChart(java.io.OutputStream outputStream)
	{
		try
		{
			ChartUtilities.writeChartAsPNG(outputStream, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
