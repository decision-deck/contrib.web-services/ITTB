import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.Criterion;

public class PlotPerfTableFile {

	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public  void printFile()
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e){}
	}

	public void saveFile(String file)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos=new FileOutputStream(file);
			sortie.output(document,  fos);
			fos.close();     
		}
		catch (java.io.IOException e){}
	}

	public  void createFile(List<Criterion> criteria, 
	                        String encodedImage, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		Element criterionValue= new Element("criterionValue");
		racine.addContent(criterionValue);

		Attribute name =new Attribute("mcdaConcept",commentToPut);
		criterionValue.setAttribute(name);

		Element criteriaSet= new Element("criteriaSet");
		criterionValue.addContent(criteriaSet);

		for(Criterion criterion: criteria)
		{
			Element element = new Element("element");
			criteriaSet.addContent(element);

			Element criterionID = new Element("criterionID");
			element.addContent(criterionID);
			criterionID.setText(criterion.id());
		}  

		Element value=new Element ("value");
		criterionValue.addContent(value);
		Element image=new Element ("image");
		value.addContent(image);
		if(encodedImage!="")
			image.setText(encodedImage);
		else System.out.println ("encoded Image not detected");  
	}

	public  void createFileMultiplePlots(ArrayList<String > listIDs, 
	                                     String []encodedImage, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		Element criterionValue= new Element("criterionValue");
		racine.addContent(criterionValue);

		Attribute name =new Attribute("mcdaConcept",commentToPut);
		criterionValue.setAttribute(name);

		Element criteriaSet= new Element("criteriaSet");
		criterionValue.addContent(criteriaSet);

		for(int i=0;i<listIDs.size();i++)
		{
			Element element = new Element("element");
			criteriaSet.addContent(element);

			Element criterionID = new Element("criterionID");
			element.addContent(criterionID);
			criterionID.setText(listIDs.get(i).toString());
		}  

		for(int i=0;i<listIDs.size();i++)
		{
			Element value=new Element ("value");
			criterionValue.addContent(value);
			Element image=new Element ("image");
			value.addContent(image);
			if(encodedImage[i]!="")
				image.setText(encodedImage[i]);
			else System.out.println ("encoded Image not detected");  
		}
	}

}
