import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;


public class ImageConversion
{
	private String errorMessage = "";

	public String convertImage(java.io.InputStream inputStream, String format)
	{
		String encodedImage = "";
		try
		{
			BufferedImage image = ImageIO.read(inputStream);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			errorMessage = errorMessage.concat("\n Error in the image conversion: " + e.getMessage());
		}
		return encodedImage;

	}

	public String convertImage(String pathImage, String format)
	{
		File fileToDelete = new File(pathImage);
		try
		{
			return convertImage(new FileInputStream(fileToDelete), format);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			errorMessage = errorMessage.concat("\n Error in the image conversion: " + e.getMessage());
			return "";
		}
		finally
		{
			fileToDelete.delete();
		}
	}

	public void writeImageByLine(java.io.InputStream[] inputStream,String format, int size, java.io.OutputStream outputStream) 
	{  
		BufferedImage []images=new BufferedImage [size];

		for (int i=0;i<size;i++)
			try{
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {
				if(errorMessage.equals(""))
					errorMessage="\n Error in the image conversion: "+e.getMessage();
				else
					errorMessage=errorMessage.concat("\n Error in the image conversion: "+e.getMessage());
			}
		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			w+=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			x+=images[i].getWidth();
		}

		try {
			ImageIO.write(combined, format, outputStream);
		} catch (IOException e) {
			if(errorMessage.equals(""))
				errorMessage="\n Error in images combination (by line): "+e.getMessage();
			else
				errorMessage=errorMessage.concat("\n Error in in images combination (by line): "+e.getMessage());
		}
	}  

	public void writeImageByColumn (java.io.InputStream[] inputStream, String format, int size, java.io.OutputStream outputStream) 
	{  
		BufferedImage []images=new BufferedImage [size];
		for (int i=0;i<size;i++)
			try{			 	
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {

				e.printStackTrace();
				if(errorMessage.equals(""))
					errorMessage="\n Error in the image conversion: "+e.getMessage();
				else
					errorMessage=errorMessage.concat("\n Error in the image conversion: "+e.getMessage());
			}

		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			h+=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			y+=images[i].getHeight();			 
		}

		try {
			ImageIO.write(combined, format, outputStream);
		} catch (IOException e1) {
			if(errorMessage.equals(""))
				errorMessage="\n Error in images combination (by column): "+e1.getMessage();
			else
				errorMessage=errorMessage.concat("\n Error in in images combination (by column): "+e1.getMessage());
		}
	}  

	public void writeImageBySquare(java.io.InputStream[] inputStream, String format, int size, java.io.OutputStream outputStream) 
	{  
		BufferedImage []images=new BufferedImage [size];

		for (int i=0;i<size;i++)
			try{
				images[i] = ImageIO.read(inputStream[i]); 	
			} catch(Exception e) {
				e.printStackTrace();
				if(errorMessage.equals(""))
					errorMessage="\n Error in the image conversion: "+e.getMessage();
				else
					errorMessage=errorMessage.concat("\n Error in the image conversion: "+e.getMessage());
			}

		// create the new image, canvas size is the square of the size
		int maxNumberOfImagesByLine=(int) Math.ceil(Math.sqrt(size));
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();
		int maxWidth=w;
		w=w*maxNumberOfImagesByLine;

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		int maxHeight=h;

		h=h*maxNumberOfImagesByLine;

		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;

		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			x+=maxWidth;
			if(x>=w)
			{
				x=0;
				y+=maxHeight;
			}
		}

		try {
			ImageIO.write(combined, format, outputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			if(errorMessage.equals(""))
				errorMessage="\n Error in images combination (by square): "+e1.getMessage();
			else
				errorMessage=errorMessage.concat("\n Error in in images combination (by square): "+e1.getMessage());
		}

		try {
			ImageIO.write(combined, format, outputStream);
		} catch (IOException e1) {
			//e1.printStackTrace();
			if(errorMessage.equals(""))
				errorMessage="\n Error in images combination (by square): "+e1.getMessage();
			else
				errorMessage=errorMessage.concat("\n Error in in images combination (by square): "+e1.getMessage());
		}
	}  

	public String getImageErrorMessage()
	{
		return errorMessage;
	}

}
