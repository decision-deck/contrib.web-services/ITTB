import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_PerformanceTable;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class PlotNumPerfTable {

	protected static void createChart(String chartType, PerformanceTable perfTable,
	                                  Alternatives alternatives, Criterion criterion,
	                                  boolean color, Color color1,
	                                  Color color2, String chartTitle, String XaxisLabel,
	                                  String YaxisLabel, String order, String maxMin,
	                                  OutputStream outputStream)
	{
		final int nbrAlternatives = perfTable.nbAlternatives();
		String[] listAlternativesIDs = new String[nbrAlternatives];
		String[] listAlternativesNames = new String[nbrAlternatives];
		double[] ySeries = new double[nbrAlternatives];
		int idx=0;
		for (Alternative a: perfTable.alternatives())
		{
			listAlternativesIDs[idx] = a.id();
			// alternatives has the full info (performanceTable's alternatives only have ids)
			listAlternativesNames[idx] = alternatives.get(a.id()).name();
			ySeries[idx] = perfTable.getValue(a, criterion);
			idx++;
		}
		String title = "";
		final String id = criterion.id();
		if (criterion.name() != null && id != null)
			title = criterion.name() + " (" + id + ")";
		else
			title = id;

		if (chartType.equals("bc"))
		{
			/* Bar chart representation */
			BarChartPlot barChart = new BarChartPlot(ySeries, color1, color2, title, XaxisLabel, YaxisLabel,
			                                         listAlternativesIDs, listAlternativesNames,
			                                         order, maxMin);
			barChart.saveChart(outputStream);
		}
		else
		{
			/* Pie chart representation */
			PieChartPlot pieChart = new PieChartPlot(ySeries, color1, color2, title, XaxisLabel, YaxisLabel,
			                                         listAlternativesIDs, listAlternativesNames,
			                                         order, maxMin);
			pieChart.saveChart(outputStream);
		}
	}

	protected static void generateMultiplePlots(String chartType, PerformanceTable perfTable,
	                                              Alternatives alternatives, Criterion criter,
	                                              boolean color, Color color1, Color color2, String chartTitle,
	                                              String XaxisLabel, String YaxisLabel, String order, String maxMin,
	                                              OutputStream outputStream)
	{
		createChart(chartType, perfTable, alternatives, criter, color, color1, color2, chartTitle,
		            XaxisLabel, YaxisLabel, order, maxMin, outputStream);
	}

	protected static void generatePlot(String chartType, String plotsOrganisation, PerformanceTable performanceTable,
	                                     Alternatives alternatives, Criteria criteria,
	                                     boolean color, Color color1, Color color2, String chartTitle,
	                                     String XaxisLabel, String YaxisLabel, String order, String maxMin,
	                                     OutputStream pngStream)
	{
		int nbrCriteria = performanceTable.nbCriteria();
		ByteArrayOutputStream[] chart_png_image = new ByteArrayOutputStream[nbrCriteria];
		int idx = 0;
		for (Criterion c: performanceTable.criteria())
		{
			// criteria has the full info (performanceTable's criteria only have ids)
			final Criterion criterion = criteria.get(c.id());
			chart_png_image[idx] = new ByteArrayOutputStream();
			createChart(chartType, performanceTable, alternatives, criterion,
			            color, color1, color2, chartTitle, XaxisLabel, YaxisLabel, order, maxMin,
			            chart_png_image[idx]);
			idx++;
		}

		ImageConversion imageConvert = new ImageConversion();

		InputStream[] inputStream = new InputStream[nbrCriteria];
		for (int i = 0; i < nbrCriteria; i++)
			inputStream[i] = new ByteArrayInputStream(chart_png_image[i].toByteArray());

		if (plotsOrganisation.equals("By column"))
			imageConvert.writeImageByColumn(inputStream, "png", nbrCriteria, pngStream);

		else if (plotsOrganisation.equals("By line"))
			imageConvert.writeImageByLine(inputStream, "png", nbrCriteria, pngStream);

		else if (plotsOrganisation.equals("By square"))
		    imageConvert.writeImageBySquare(inputStream, "png", nbrCriteria, pngStream);
	}   	

	protected static Color[] getColorsFromOptions(boolean black_and_white, String colors1ToUse, String colors2ToUse)
	{
		Color[] colors = { Color.black, Color.black };
		if (black_and_white)
			return colors;

		Color c1 = null, c2 = null;
		try
		{
			c1 = (Color)Color.class.getField(colors1ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c1==null )
			return colors;

		try
		{
			c2 = (Color)Color.class.getField(colors2ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c2==null )
			return colors;

		colors[0] = c1;
		colors[1] = c2;
		return colors;
	}

	public static void main(String[] argv)
	{
		if (argv.length != 6)
		{
			System.err.println("Usage: alternatives.xml criteria.xml performanceTable.xml parameters.xml performanceTable.png messages.xml");
			System.err.println("       The 4 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathAlternatives = argv[0];
		final String pathCriteria = argv[1];
		final String pathPerfTable = argv[2];
		final String pathParameters = argv[3];
		final String pathOutputFile = argv[4];
		final String pathOutputMessage = argv[5];

		String tempErrorMessage="";
		String warningMessage="";

		if (!new File(pathParameters).exists() || !new File(pathAlternatives).exists()
				|| !new File(pathCriteria).exists() || !new File(pathPerfTable).exists())
		{
			tempErrorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction=new Main();	

		String errParam=mainFunction.checkEntryFile(pathParameters);

		String errAltern=mainFunction.checkEntryFile(pathAlternatives);

		String errCriter=mainFunction.checkEntryFile(pathCriteria);

		String errPerfTable=mainFunction.checkEntryFile(pathPerfTable);

		if (!"".equals(errParam))
			tempErrorMessage += "\nFatal Error: the file containing the parameters is not a valid XMCDA document.";

		if(!"".equals(errAltern))
			tempErrorMessage += "\nFatal Error: the file containing the alternatives tag is not a valid XMCDA document.";

		if(!"".equals(errCriter))
			tempErrorMessage += "\nFatal Error: the file containing the criteria tag is not a valid XMCDA document.";

		if(!"".equals(errPerfTable))
			tempErrorMessage += "\nFatal Error: the file containing the performanceTable tag is not a valid XMCDA document.";

		if(!"".equals(errParam) ||!"".equals(errAltern)||!"".equals(errCriter)||!"".equals(errPerfTable))
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		JDOM_Criteria jdomCriter= new JDOM_Criteria();
		mainFunction.PrepareParsing (pathCriteria);
		Criteria criteria = jdomCriter.parseCriteriaXML(mainFunction.getRacine());
		if(criteria==null)
			tempErrorMessage += "\nFatal Error: the file containing the criteria tag is erroneous or empty.";

		if(jdomCriter.getWarning()!="")
			warningMessage += "\n" + jdomCriter.getWarning();

		// Test en cas d'erreur
		mainFunction.PrepareParsing(pathAlternatives);
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		Alternatives alternatives = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (alternatives == null)
		    tempErrorMessage += "\nFatal Error: the file containing the alternatives tag is erroneous or empty.";

		if (jdomAlter.getWarning() != "")
		    warningMessage += "\n" + jdomAlter.getWarning();

		// Test en cas d'erreur
		mainFunction.PrepareParsing(pathPerfTable);
		JDOM_PerformanceTable jdomPerfTable = new JDOM_PerformanceTable();
		PerformanceTable performanceTable = jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());

		if (performanceTable == null)
			tempErrorMessage += "\nFatal Error: the file containing the performanceTable tag is erroneous or empty.";

		if (jdomPerfTable.getErrorMessage() != "")
			warningMessage = warningMessage.concat("\n" + jdomPerfTable.getErrorMessage());

		if (jdomPerfTable.getWarningMessage() != "")
			warningMessage = warningMessage.concat(jdomPerfTable.getWarningMessage());

		//ParseMethodParameters
		OptionsMethodParameters parseMethodParam=new OptionsMethodParameters();
		mainFunction.PrepareParsing (pathParameters);
		boolean statusParseMethod=parseMethodParam.parse(mainFunction.getRacine());

		if(!statusParseMethod)
			tempErrorMessage += "\nFatal Error: the file containing the parameters is erroneous or empty.";

		if(criteria==null || alternatives==null || performanceTable==null || !statusParseMethod)
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		performanceTable = mainFunction.run(performanceTable, criteria, alternatives);
		warningMessage = warningMessage.concat(mainFunction.getWarningMessage());
		tempErrorMessage = mainFunction.getErrorMessage();

		String chartType=parseMethodParam.getChartType();
		if (parseMethodParam.getError()!="")	
			warningMessage= warningMessage.concat(parseMethodParam.getError());

		boolean uniquePlot=parseMethodParam.getUniquePlot();
		String plotsOrganisation=parseMethodParam.getPlotsOrganisation();

		String order=parseMethodParam.getOrder();
		String maxMin=parseMethodParam.getMaxMin();

		boolean color =parseMethodParam.getColor();
		String colors1ToUse=parseMethodParam.getInitialColor();
		String colors2ToUse=parseMethodParam.getFinalColor();

		Color[] colors = getColorsFromOptions(!color, colors1ToUse, colors2ToUse);

		String chartTitle=parseMethodParam.getChartTitle();
		String XaxisLabel=parseMethodParam.getDomainAxisLabel();
		String YaxisLabel=parseMethodParam.getRangeAxisLabel();

		String image=null;

		if(performanceTable.nbAlternatives()==0 || performanceTable.nbCriteria()==0)
		{
			tempErrorMessage += "\nWe cannot run the algorithm since the criteria list or the alternatives list "
			                    + "of the performance table is empty.\n";
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createLogMessage(errCriter, errAltern, errPerfTable, warningMessage, tempErrorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}
				
		// D�rouler l'algorithme

		if (!uniquePlot)
		{
			String template = pathOutputFile.substring(0, pathOutputFile.indexOf('.')) + "-%02d.png";
			
			int idx = -1;
			ArrayList<String> criteriaIDs = new ArrayList<String>();
			for (Criterion c: performanceTable.criteria())
			{
				idx++;
				// criteria has the full info (performanceTable's criteria only have ids)
				final Criterion criterion = criteria.get(c.id());
				FileOutputStream png;
				try
				{
					png = new FileOutputStream(new File(String.format(template, idx+1)));
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
					continue;
				}
				generateMultiplePlots(chartType, performanceTable, alternatives, criterion,
				                      color, colors[0], colors[1], chartTitle,
				                      XaxisLabel, YaxisLabel, order, maxMin,
				                      png);
				criteriaIDs.add(criterion.id());
			}
		}
		else // (uniquePlot==true)
		{
			FileOutputStream outputStream = null;
			try
			{
				outputStream = new FileOutputStream(new File(pathOutputFile));
			}
			catch (FileNotFoundException e)
			{
				tempErrorMessage += "\nCould not open the output png file\n";
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createLogMessage(errCriter, errAltern, errPerfTable, warningMessage, tempErrorMessage);
				xmcdaMess.enregistre(pathOutputMessage);
				return;
			}
			generatePlot(chartType, plotsOrganisation, performanceTable, alternatives, criteria,
			             color, colors[0], colors[1], chartTitle, XaxisLabel, YaxisLabel,
			             order, maxMin, outputStream);
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessage(errCriter, errAltern, errPerfTable, warningMessage, tempErrorMessage);
		xmcdaMess.enregistre(pathOutputMessage);
	}

}
