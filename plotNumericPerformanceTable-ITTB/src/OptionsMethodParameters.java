import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class OptionsMethodParameters {

	static org.jdom2.Document document;
	private boolean color;
	private String chartType;
	private String color1ToUse;
	private String color2ToUse;
	private String chartTitle;
	private String XaxisLabel;
	private String YaxisLabel;
	private String arrangement;
	private boolean uniquePlot;
	private String order;
	private String maxMin;
	private String error;

	public  OptionsMethodParameters(){
		color=false;			//the default plot is a color plot
		uniquePlot=true;
		arrangement ="By column";
		chartType= "bc";		// bar chart is the default configuration
		color1ToUse="";
		color2ToUse="";
		chartTitle="";
		XaxisLabel="";
		YaxisLabel="";
		error="";
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		return document.getRootElement();
	}

	public boolean parse(Element racine)
	{	      
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator();

				while(i.hasNext())
				{
					Element courant = (Element)i.next();

					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("chart_type"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("barChart"))
							chartType= "bc";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("pieChart"))

							chartType= "pc";

						else
						{
							// �mettre un message d'erreur et dire que c'est bar chart par d�faut 
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("unique_plot"))
					{
						if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("true")))
							uniquePlot=true;
						else if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							uniquePlot= false;
						else
						{
							// Default value
							uniquePlot= true;
						}

					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("plots_display"))
					{
						if ( (courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("line")))
							arrangement= "By line";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("column")))
							arrangement= "By column";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("grid")))
							arrangement= "By square";	
						else
						{
							arrangement= "By column";
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order_by"))
					{
						if (courant.getChild("value").
								getChild("label").getValue().equals("name"))
							order= "by name";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("id"))
							order= "by id";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("values"))

							order= "by values";
						else
						{
							// �mettre un message d'erreur et dire que c'est by values d�faut 
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("order"))
					{		
						if (courant.getChild("value").
								getChild("label").getValue().equals("increasing"))
							maxMin= "increasing";
						else if (courant.getChild("value").
								getChild("label").getValue().equals("decreasing"))
							maxMin= "decreasing";
						else
						{
							// �mettre un message d'erreur 
							maxMin= "increasing";
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("use_color"))
					{
						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("false")))
							color=false;
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) &&
								(courant.getChild("value").
										getChild("label").getValue().equals("true")))
							color=true;
						else
						{
							color=false;
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("initial_color"))
					{
						color1ToUse=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("final_color"))
					{
						color2ToUse=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("chart_title"))
					{
						chartTitle=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("domain_axis"))
					{
						XaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("range_axis"))
					{
						YaxisLabel=courant.getChild("value").
								getChild("label").getValue();
					}
					else {// we have only the previous parameters in method parameters}
						error="Error: you have not entered the correct parameters in the GUI interface. " +
								"The default generated plot is a black and white bar chart.";
					}
				}
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;

		return statusOK;
	}

	public boolean getColor()
	{
		return color;
	}

	public String getChartType()
	{
		return chartType;
	}

	public String getInitialColor()
	{
		return color1ToUse;
	}

	public String getFinalColor()
	{
		return color2ToUse;
	}

	public String getChartTitle()
	{
		return chartTitle;
	}

	public String getDomainAxisLabel()
	{
		return XaxisLabel;
	}

	public String getRangeAxisLabel()
	{
		return YaxisLabel;
	}

	public String getPlotsOrganisation()
	{
		return arrangement;
	}

	public boolean getUniquePlot()
	{
		return uniquePlot;
	}

	public String getOrder()
	{
		return order;
	}

	public String getMaxMin()
	{
		return maxMin;
	}

	public String getError()
	{
		return error;
	}

}