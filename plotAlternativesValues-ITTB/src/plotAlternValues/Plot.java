package plotAlternValues;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.AlternativeValue;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesValues;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_AlternativesValues;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class Plot {

	private String warningMessage;

	public Plot()
	{
		warningMessage="";
	}

	public boolean checkFiles(Alternatives alter,AlternativesValues alternVal)
	{
		boolean result=true;
		int diff=0;
		if (alter.getIDs().size() != alternVal.size())
		{
			warningMessage += "\nPlease check the file containing the alternatives tag:" +
					"incoherence with the number of alternatives given in the file containing the alternativesValues tag.";
			return false;
		}
		ArrayList<String> list=new ArrayList<String>();
		for (AlternativeValue av: alternVal)
			list.add(av.getAlternativeValueId());

		for (String s : alter.getIDs())
		{
			if( ! list.contains(s) )
			{
				result=false;
				diff++;
			}
		}
		if(diff>1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the alternatives tag:" +
					"incoherence with the alternatives given " +
					"in the file containing the alternativesValues tag. There are "+diff+"different alternatives.");
		if(diff==1)
			warningMessage=warningMessage.concat("\nPlease check the file containing the alternatives tag:" +
					"incoherence with the alternatives given " +
					"in the file containing the alternativesValues tag. There is one different alternative.");
		return result;		
	}

	public ArrayList<String > getIntersectionAlternativesIDs(Alternatives alter,AlternativesValues alternVal)
	{
		ArrayList<String > listintersection=new ArrayList<String >();

		for (String t : alternVal.getListAlternativesIDs())
		{
			final Alternative a = alter.get(t);
			if (a!=null && a.isActive())
				listintersection.add(t);
		}
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>(new TreeSet<String>(listintersection));
		return listintersection;
	}

	public ArrayList<String > getIntersectionAlternativesNames(Alternatives alter,AlternativesValues alternVal,
	                                                           ArrayList<String > listintersectionIds)
	                                                           {
		ArrayList<String > listintersectionNames=new ArrayList<String >();

		for(int i=0; i< listintersectionIds.size();i++)
		{
			if (alter.getIDs().
					contains(listintersectionIds.get(i)))
			{
				int index=alter.getIDs().indexOf(listintersectionIds.get(i));
				if(alter.getNames().get(index)!=null)
					listintersectionNames.add(alter.getNames().get(index));
				else listintersectionNames.add(null);
			}
			else listintersectionNames.add(null);
		}
		return listintersectionNames;
	                                                           }

	public void plotAlternativesValuesBarChart(AlternativesValues alternativesValuesToPlot,Alternatives alter,
	                                             ArrayList <String>listintersectionIds,ArrayList <String>listintersectionNames,
	                                             String tempErrorMessage,
	                                             boolean color, Color color1, Color color2,
	                                             String chartTitle, String XaxisLabel, String YaxisLabel,String order, String maxMin,
	                                             File pngFile)
	{
		/* Bar chart representation of the AlternativesValues */
		double[] ySeriesAlternValues=new double[listintersectionIds.size()];

		for(int i=0;i<listintersectionIds.size();i++)
		{
			if (alternativesValuesToPlot.getListAlternativesIDs().
					contains(listintersectionIds.get(i)))
				ySeriesAlternValues[i]= alternativesValuesToPlot.getListAlternativesValues().
				get(alternativesValuesToPlot.getListAlternativesIDs().indexOf(listintersectionIds.get(i))).
				getRealValue();
		}

		BarChart barChartAlterVal= new  BarChart("Bar Chart",
		                                         ySeriesAlternValues, 
		                                         color1, color2,
		                                         chartTitle, XaxisLabel, YaxisLabel,alter,
		                                         listintersectionIds,listintersectionNames,
		                                         alternativesValuesToPlot,order,maxMin);
		barChartAlterVal.saveChart(pngFile);
	}

	public void plotAlternativesValuesPieChart(AlternativesValues alterValuesToPlot,Alternatives alter,
	                                             String tempErrorMessage,
	                                             boolean color, Color color1, Color color2,
	                                             String chartTitle,ArrayList  <String>listintersectionIds,
	                                             ArrayList <String>listintersectionNames,
	                                             String order,String maxMin,
	                                             File pngFile)
	{
		/* Pie chart representation of the AlternativesValues */
		double[] ySeriesAlterValues=new double[listintersectionIds.size()];

		for(int i=0;i<listintersectionIds.size();i++)
		{
			if (alterValuesToPlot.getListAlternativesIDs().
					contains(listintersectionIds.get(i)))
				ySeriesAlterValues[i]= alterValuesToPlot.getListAlternativesValues().
				get(alterValuesToPlot.getListAlternativesIDs().indexOf(listintersectionIds.get(i))).
				getRealValue();
		}

		PieChart pieChartAlternVal=new PieChart("PieChartDemo4",
		                                        ySeriesAlterValues, alterValuesToPlot,alter,
		                                        color1, color2,chartTitle,listintersectionIds, listintersectionNames,order,maxMin);
		pieChartAlternVal.saveChart(pngFile);
	}

	protected static Color[] getColorsFromOptions(boolean black_and_white, String colors1ToUse, String colors2ToUse)
	{
		Color[] colors = { Color.black, Color.black };
		if (black_and_white)
			return colors;

		Color c1 = null, c2 = null;
		try
		{
			c1 = (Color)Color.class.getField(colors1ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c1==null )
			return colors;

		try
		{
			c2 = (Color)Color.class.getField(colors2ToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c2==null )
			return colors;

		colors[0] = c1;
		colors[1] = c2;
		return colors;
	}

	public static void main(String[] argv)
	{
		String tempErrorMessage="";
		if ( argv.length != 5 )
		{
			System.err.println("Usage: alternatives.xml alternativesValues.xml parameters.xml alternativesValuesPlot.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathAlternatives=argv[0];
		final String pathAlternativesValues=argv[1];
		final String pathMethodParameters=argv[2];
		final String pathOutputFilePlot=argv[3];
		final String pathOutputMessage=argv[4];

		if( !new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists() )
		{
			tempErrorMessage="Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction=new Main();

		String errMethodParameters=mainFunction.checkEntryFile(pathMethodParameters);

		if(errMethodParameters!="")
			tempErrorMessage += "\nFatal error: the file containing the parameters is not a valid XMCDA document.";

		String errAlternatives=mainFunction.checkEntryFile(pathAlternatives);
		if(errAlternatives!="")
			tempErrorMessage += "\nFatal error: the file containing the alternatives tag is not a valid XMCDA document.";

		String errAlternativesValues=mainFunction.checkEntryFile(pathAlternativesValues);
		if(errAlternativesValues!="")
		{
			tempErrorMessage += "\nFatal error: the file containing the alternativesValues tag is not a valid XMCDA document.";
		}

		if( ! ("".equals(errMethodParameters) && "".equals(errAlternatives) && "".equals(errAlternativesValues) ) )
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Plot plot =new Plot();
		if(mainFunction.getWarningMessage()!="")
			plot.warningMessage=plot.warningMessage.concat(mainFunction.getWarningMessage());


		//Parsing XML file for Alternatives
		mainFunction.PrepareParsing (pathAlternatives);
		Alternatives alternatives=new Alternatives();
		JDOM_Alternatives jdomAlter= new JDOM_Alternatives();
		alternatives=jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if(alternatives==null)
		{
			tempErrorMessage +=  "\nFatal error: the file containing the alternatives tag is erroneous or empty.";
		}

		if(jdomAlter.getWarning()!="")
			plot.warningMessage += "\n"+jdomAlter.getWarning();

		//Parsing XML file for AlternativesValues
		mainFunction.PrepareParsing (pathAlternativesValues);
		AlternativesValues alternativesValues=new AlternativesValues();
		JDOM_AlternativesValues jdomAlternVal= new JDOM_AlternativesValues();
		alternativesValues=jdomAlternVal.parseAlternativesValuesXML(mainFunction.getRacine());
		if(jdomAlternVal.getWarningMessage()!="")
			plot.warningMessage += "\n"+jdomAlternVal.getWarningMessage();

		if(alternativesValues==null || alternativesValues.size()==0)
		{
			alternativesValues = null;
			tempErrorMessage += "\nFatal error: the file containing the alternativesValues tag is erroneous or empty.";
		}
		
		//ParseMethodParameters
		ParseMethodParameters parseMethodParam=new ParseMethodParameters();
		mainFunction.PrepareParsing (pathMethodParameters);
		boolean optionsSuccessfullyParsed=parseMethodParam.parse(mainFunction.getRacine());

		if(optionsSuccessfullyParsed==false)
		{
			tempErrorMessage += "\nFatal error: the file containing the parameters is erroneous or empty.";
		}
		if(alternatives==null || alternativesValues==null || ! optionsSuccessfullyParsed )
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			//.createLogMessageForAlternativesValPlot(errAlternatives, errAlternativesValues,plot.warningMessage, tempErrorMessage)
			return;
		}

		plot.checkFiles(alternatives,alternativesValues);

		//Intersection list
		ArrayList<String > listintersectionAlternatives=
				plot.getIntersectionAlternativesIDs(alternatives,alternativesValues);

		// Si l'intersection est vide on prend ce qu'on a dans la alternativesValues.xml
		if (listintersectionAlternatives.isEmpty())
		{
			plot.warningMessage += "\nOnly the alternatives IDs in the file containing the alternativesValues tag were considered. " +
					"Please check your input file containing the alternatives tag.";
			for (AlternativeValue av: alternativesValues)
				listintersectionAlternatives.add(av.getAlternativeValueId());
		}

		boolean color = parseMethodParam.getColor();
		String chartType=parseMethodParam.getChartType();
		if (parseMethodParam.getError()!="")	
			plot.warningMessage= plot.warningMessage.concat(parseMethodParam.getError());

		String colors1ToUse=parseMethodParam.getInitialColor();
		String colors2ToUse=parseMethodParam.getFinalColor();

		Color[] colors = getColorsFromOptions(!color, colors1ToUse, colors2ToUse);
		String order=parseMethodParam.getOrder();
		String maxMin=parseMethodParam.getMaxMin();

		String chartTitle=parseMethodParam.getChartTitle();
		String XaxisLabel=parseMethodParam.getDomainAxisLabel();
		String YaxisLabel=parseMethodParam.getRangeAxisLabel();

		if( listintersectionAlternatives.size()!=0 )
		{
			// D�rouler l'algorithme du plot
			if(chartType.equals("bc"))	
				plot.plotAlternativesValuesBarChart(alternativesValues,alternatives,
				                                                 listintersectionAlternatives,
				                                                 plot.getIntersectionAlternativesNames(alternatives, 
				                                                                                       alternativesValues, listintersectionAlternatives),
				                                                 tempErrorMessage,
				                                                 color, colors[0],colors[1],
				                                                 chartTitle,XaxisLabel, YaxisLabel,order, maxMin,
				                                                 new File(pathOutputFilePlot));

			// D�rouler l'algorithme du plot
			else if(chartType.equals("pc"))	
				plot.plotAlternativesValuesPieChart(alternativesValues,alternatives,
				                                                 tempErrorMessage,
				                                                 color, colors[0],colors[1],
				                                                 chartTitle,listintersectionAlternatives,
				                                                 plot.getIntersectionAlternativesNames(alternatives, 
				                                                                                       alternativesValues, listintersectionAlternatives),order, maxMin,
				                                                 new File(pathOutputFilePlot));
			else {
				tempErrorMessage += "\nError in chart type. Only Pie chart or Bar chart are available plots.";
			}
		}
		else
		{
			tempErrorMessage += "\n We cannot run the algorithm since the alternatives list  of the PROJECT is empty.\n";
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess= new XmcdaMessage();
		xmcdaMess.createLogMessageForAlternativesValPlot(errAlternatives,errAlternativesValues,
		                                                 plot.warningMessage,tempErrorMessage);
		xmcdaMess.enregistre(pathOutputMessage);
	}
}
