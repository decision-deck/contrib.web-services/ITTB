package plotAlternValues;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Timer;
import org.jfree.chart.ChartFactory;
//import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.SortOrder;

import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesValues;

/**
 * A simple demonstration application showing how to create a pie chart using data from a
 * {@link DefaultPieDataset}.  This chart has a lot of labels and rotates, so it is useful for
 * testing the label distribution algorithm.
 */
public class PieChart {
	private JFreeChart chart;

	public PieChart(final String title, double[] ySeries,
	                AlternativesValues alternativesValuesToPlot,Alternatives alter,
	                Color c1, Color c2,String chartTitle,
	                ArrayList  <String>listintersectionIds,
	                ArrayList  <String>listintersectionNames,String order, String maxMin) {
		String[] listIds=new  String[ySeries.length];
		for (int i=0;i<ySeries.length;i++)
			listIds[i]=listintersectionIds.get(i);

		String[] listNames=new  String[ySeries.length];
		for (int i=0;i<ySeries.length;i++)
			listNames[i]=listintersectionNames.get(i);

		String[] columnKeys =new  String[ySeries.length];	
		for(int i=0;i<ySeries.length;i++){columnKeys[i]="" ;}

		//// Initialization
		for(int i=0;i<ySeries.length;i++)
		{ 	
			if(order.equals("by id"))
			{

				if (listintersectionNames.get(i)!=null && listintersectionIds!=null)
					columnKeys[i]="("+listintersectionIds.get(i)+") "+listintersectionNames.
					get(i);
				else
					columnKeys[i]=listintersectionIds.get(i);
			}
			else
			{
				if (listintersectionNames.get(i)!=null && listintersectionIds!=null)

					columnKeys[i]=listintersectionNames.get(i)+" ("+listintersectionIds.get(i)+")";

				else
					columnKeys[i]=listintersectionIds.get(i);
			}
		}

		if(order.equals("by id")||order.equals("by name"))
		{
			for (int i=0;i<ySeries.length -1;i++)
			{
				for (int j=i+1;j<ySeries.length;j++)
				{

					if(maxMin.equals("increasing"))
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))<0)
						{
							// echange de columnkey
							exchange (columnKeys,i,j);       				
							// echange des valeurs
							exchange (ySeries,i,j);
						}
					}
					else
					{
						if((listIds[j].toLowerCase().compareTo(listIds[i].toLowerCase()))>0)
						{
							// echange de columnkey
							exchange (columnKeys,i,j);       				
							// echange des valeurs
							exchange (ySeries,i,j);
						}
					}
				}
			}
		}

		/*else if(order.equals("by name"))
        {
        	/// Ordonner les valeurs
        	for (int i=0;i<ySeries.length -1;i++)
        	{
        		for (int j=i+1;j<ySeries.length;j++)
        		{

        			if(maxMin.equals("increasing"))
        			{
        				if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))<0)
        				{

        				// echange de columnkey
        				exchange (columnKeys,i,j);       				
        				// echange des valeurs
        				exchange (ySeries,i,j);

        				}
        			}
        			else
        			{
        				if((listNames[j].toLowerCase().compareTo(listNames[i].toLowerCase()))>0)
            			{

            				// echange de columnkey
            				exchange (columnKeys,i,j);       				
            				// echange des valeurs
            				exchange (ySeries,i,j);

            			}
        			}
        		}
        	}
        }*/

		else if(order.equals("by values"))
		{
			/// Ordonner les valeurs
			for (int i=0;i<ySeries.length -1;i++)
			{
				for (int j=i+1;j<ySeries.length;j++)
				{
					if(maxMin.equals("increasing"))
					{

						if(ySeries[j]<ySeries[i])
						{
							// echange de columnkey
							exchange (columnKeys,i,j);       				
							// echange des valeurs
							exchange (ySeries,i,j);      				
						}
					}
					else
					{
						if(ySeries[j]>ySeries[i])
						{
							// echange de columnkey
							exchange (columnKeys,i,j);       				
							// echange des valeurs
							exchange (ySeries,i,j);      				
						}	
					}
				}
			}
		}

		DefaultPieDataset pieDataset = new DefaultPieDataset(); 
		for(int i=0;i<ySeries.length;i++)
			pieDataset.setValue(columnKeys[i], ySeries[i]);


		if(order.equals("by name") ||order.equals("by id"))
		{
			if(maxMin.equals("increasing"))
				pieDataset.sortByKeys(SortOrder.ASCENDING);
			else
				pieDataset.sortByKeys(SortOrder.DESCENDING);

		}
		else if (order.equals("by values"))
		{
			if(maxMin.equals("increasing"))
				pieDataset.sortByValues(SortOrder.ASCENDING);
			else
				pieDataset.sortByValues(SortOrder.DESCENDING);
		}

		// create the chart...
		chart = ChartFactory.createPieChart(
		                                    chartTitle,  // chart title
		                                    pieDataset,             // dataset
		                                    false,               // include legend
		                                    true,
		                                    false
				);

		chart.setBackgroundPaint(Color.white);

		final PiePlot plot = (PiePlot) chart.getPlot();
		plot.setBackgroundPaint(Color.white);
		plot.setCircular(true);
		plot.setLabelFont(new Font("Arial", Font.BOLD, 12));

		Paint []paint= new Paint [ySeries.length];

		int red1 = c1.getRed();
		int green1 = c1.getGreen();
		int blue1 = c1.getBlue();

		int red2 = c2.getRed();
		int green2 = c2.getGreen();
		int blue2 = c2.getBlue(); 

		int LARGEUR=ySeries.length;
		for(int i=0; i<ySeries.length; i++) {
			paint[i]=new Color((red1*(LARGEUR-i)+red2*i)/LARGEUR,
			                   (green1*(LARGEUR-i)+green2*i)/LARGEUR,
			                   (blue1*(LARGEUR-i)+blue2*i)/LARGEUR);
			plot.setSectionPaint(i ,paint[i]);

		} 
		plot.setLabelOutlinePaint(null);
		plot.setLabelShadowPaint(null);
		plot.setLabelBackgroundPaint(Color.WHITE);
		plot.setBackgroundPaint(Color.WHITE);

		plot.setNoDataMessage("No data available");

		// add the chart to a panel...
		/*final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);*/

		/*final Rotator rotator = new Rotator(plot);
        rotator.start();*/
	}

	protected static void exchange (String[]columnKeys, int i, int j)
	{
		String tempKey=columnKeys[j];
		columnKeys[j]=columnKeys[i];
		columnKeys[i]=tempKey;
	}

	protected static void exchange (double[]ySeries, int i, int j)
	{
		double tempVal=ySeries[j];
		ySeries[j]=ySeries[i];
		ySeries[i]=tempVal;
	}

	public void saveChart(String filepath)
	{
		File file = new File(filepath);
		try
		{
			ChartUtilities.saveChartAsPNG(file, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveChart(File file) {
		try {
			ChartUtilities.saveChartAsPNG(file, chart, 500, 270);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveChart(java.io.OutputStream outputStream)
	{
		try
		{
			ChartUtilities.writeChartAsPNG(outputStream, chart, 500, 270);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * The rotator.
	 *
	 */
	static class Rotator extends Timer implements ActionListener {

		private static final long serialVersionUID = 4386931737069481352L;

		/** The plot. */
		private PiePlot plot;

		/** The angle. */
		private int angle = 270;

		/**
		 * Constructor.
		 *
		 * @param plot  the plot.
		 */
		Rotator(final PiePlot plot) {
			super(100, null);
			this.plot = plot;
			addActionListener(this);
		}

		/**
		 * Modifies the starting angle.
		 *
		 * @param event  the action event.
		 */
		public void actionPerformed(final ActionEvent event) {
			this.plot.setStartAngle(angle);
			this.angle = this.angle + 1;
			if (this.angle == 360) {
				this.angle = 0;
			}
		}

	}    

}
