package plotAlternValues;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;


public class ImageToBase64
{
	private String errorMessage = "";

	public String convertImage(java.io.InputStream inputStream, String format)
	{
		String encodedImage = "";
		try
		{
			BufferedImage image = ImageIO.read(inputStream);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			errorMessage = errorMessage.concat("\n Error in the image conversion: " + e.getMessage());
		}
		return encodedImage;

	}

	public String convertImage(String pathImage, String format)
	{
		File fileToDelete = new File(pathImage);
		try
		{
			return convertImage(new FileInputStream(fileToDelete), format);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			errorMessage = errorMessage.concat("\n Error in the image conversion: " + e.getMessage());
			return "";
		}
		finally
		{
			fileToDelete.delete();
		}
	}


	public String getImageErrorMessage()
	{
		return errorMessage;
	}

}
