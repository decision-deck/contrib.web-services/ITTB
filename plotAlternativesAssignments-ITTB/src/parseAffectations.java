import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;


public class parseAffectations {
	
	
	private ArrayList<String >listAlternativeID ;
	private ArrayList<String >listAffectations ;
	private String warningMessage;  
	 
	 // Constructor
	   public parseAffectations (){
		   warningMessage="";
		   listAlternativeID =new ArrayList<String >();
		   listAffectations =new ArrayList<String >();
		  
	  }
	   ////////////////////////////////////////////////////////////////////////////////////////////////	 
	   ////////////////////////////////////////////////  ////////////////////////////////////////////////
	   @SuppressWarnings("unchecked")
	public boolean parseAlternativesAffectations(Element racine)
	   {		  
		   
		   boolean status =true;
		   if(racine.getChild("alternativesAffectations")!=null)
		   {
			   Element alternAffect= racine.getChild("alternativesAffectations");
			   if(alternAffect.getChildren("alternativeAffectation")!=null)
			   {
				   List listAlternAffectations = alternAffect.getChildren("alternativeAffectation");
				   Iterator i = listAlternAffectations.iterator();
				   while(i.hasNext())
				   {	        
					   Element courant = (Element)i.next();	           
					   if(courant.getChild("alternativeID")!=null && courant.getChild("categoryID")!=null)
					   {
						   if(!listAlternativeID.contains(courant.getChild("alternativeID").getValue()))
						   {
							   listAlternativeID.add(courant.getChild("alternativeID").getValue());
							   listAffectations.add(courant.getChild("categoryID").getValue());
						   }
						   else
						   {
							   // Warning, nous avons un duplicata
							   warningMessage=warningMessage.concat("\nWarning: duplicated alternativeID " +
							   		"("+courant.getChild("alternativeID").getValue()+
		                        ") in the entry file containing the 'alternativesAfefctations' tag." +
		                        " Only the first provided occurence is considered.");
						   }
					   }
	    	
					   else	    		 
						   status = false;  

				   } 
				   if(listAlternativeID.size()==0)
					   status =false;
			   }
			   else 
				   status =false;
		   }
		   else
			   status =false;
		   
		   return status;
	   }

	   ////////////////////////////////////////////////
	   ////////////////////////////////////////////////
	   public ArrayList<String > getAlternIDs()
	   {
		   return listAlternativeID ;
	   }
	   ////////////////////////////////////////////////
	   ////////////////////////////////////////////////
	   public ArrayList<String > getAffectations()
	   {
		   return listAffectations ;
	   }
	   ////////////////////////////////////////////////
	   ////////////////////////////////////////////////
	   public String getWarningMessage()
		{
			return warningMessage;
		}
	   ////////////////////////////////////////////////
	   ////////////////////////////////////////////////
}
