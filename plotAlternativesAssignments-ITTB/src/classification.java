import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.telecom_bretagne.utils.GraphViz;




public class classification
{

	private   Element racine = new Element("XMCDA");		 
	private org.jdom2.Document document = new Document(racine);

	public static final String XMCDA_2_0_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.0.0";

	public static final String XMCDA_2_0_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd";

	public static final String XMCDA_2_1_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.1.0";

	public static final String XMCDA_2_1_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd";

	public static final String XMCDA_2_2_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.2.0";

	public static final String XMCDA_2_2_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd";

	public static final String ACCEPTED_SCHEMA_LOCATIONS = XMCDA_2_0_0_NAMESPACE + " " + XMCDA_2_0_0_SCHEMA + " "
			+ XMCDA_2_1_0_NAMESPACE + " " + XMCDA_2_1_0_SCHEMA + " "
			+ XMCDA_2_2_0_NAMESPACE + " " + XMCDA_2_2_0_SCHEMA;

	public classification()
	{

	}

	public void PrepareParsing(String path)
	{

		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){
			//System.out.println(e.getStackTrace());
		}
		racine = document.getRootElement();

	}

	protected static String checkEntryFile(String path)
	{

		String error="";
		try {
			parseXML(path);

		} catch (JDOMException e) {

			error=error.concat("File is not well-formed: "+e.getMessage());
		} catch (IOException e) {

			error=error.concat("Could not check file because: "+e.getMessage());
		}
		return error;
	}

	protected static void parseXML(final String xml) throws JDOMException, IOException {
		SAXBuilder builder = 
				new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
		builder.setFeature(
				"http://apache.org/xml/features/validation/schema", true);

		builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
				ACCEPTED_SCHEMA_LOCATIONS);

		@SuppressWarnings("unused")
		Document doc= builder.build(xml);

	}

	protected static Color getColorFromOptions(boolean black_and_white, String colorToUse)
	{
		Color color = Color.black;
		if (black_and_white)
			return color;

		Color c = null;
		try
		{
			c = (Color)Color.class.getField(colorToUse.toLowerCase()).get(null);
		}
		catch (Exception e) { /* failed */ }

		if ( c==null )
			return color;



		color = c;

		return color;
	}


	protected  static ArrayList <String> searchClasses(ArrayList <String> affectations)
	{
		ArrayList <String> result= new ArrayList <String>();
		for(int i=0;i< affectations.size();i++)
			if(!result.contains( affectations.get(i)))
				result.add( affectations.get(i));
		return result;
	}

	protected static Polygon positions(Point p, Dimension size, int nbr)
	{
		// Les coordonn�es des diff�rents points du polygone
		int [] abscissa = new int[ nbr]; 
		int [] ordonates = new int[ nbr];

		// L'angle de r�f�rence entre 2 pointes du polygone r�gulier
		double delta = Math.PI * 2 /  nbr; 
		double angle = 0;
		for ( int i = 0; i < nbr; i++ ) {

			//On trace un polyg�ne r�gulier
			angle = delta * i + Math.PI / 2;
			abscissa[i] = (int)(size.width * Math.cos(angle) + 0.5) + p.x;
			ordonates[i] = p.y - (int)(size.height * Math.sin(angle) + 0.5);
		}

		return new Polygon(abscissa,ordonates, nbr);

	}

	protected static String convertImage(File image_png,String format) 
	{  

		String encodedImage="";

		try{

			BufferedImage image = ImageIO.read(image_png);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
			//System.out.println(encodedImage);
		} catch(Exception e) {

			e.printStackTrace();

		}
		//System.out.println(encodedImage);
		if ( image_png.delete() == false) 
			System.err.println("Warning: " +  image_png.getAbsolutePath() + " could not be deleted!");
		return encodedImage;
	} 

	protected static byte[] classifyAlternativesUsingGraphViz(ArrayList <String> alter,
			ArrayList <String> affectations,String colorToUse,String currentClass, String shape,
			String dotFilename)
	{

		GraphViz gv = new GraphViz();

		gv.addln(gv.start_graph(colorToUse, shape));
		char guillemet = '"';

		String titlePosition="l";
		//gv.addln("labeljust="+guillemet+titlePosition+guillemet+";");
		titlePosition="t";
		gv.addln("labelloc="+guillemet+titlePosition+guillemet+";");
		String title=currentClass;		
		gv.addln("label="+guillemet+title+guillemet+"; fontcolor="+colorToUse+";");

		String label="";
		for(int i =0;i<affectations.size();i++)
		{
			if(affectations.get(i).equals(currentClass))
			{
				if (label.equals(""))

					label=alter.get(i);
				else
					label=label+", "+alter.get(i);

			}
		}


		String nameLabel="label"+currentClass;
		gv.addln(nameLabel+" [label = "+guillemet+label+guillemet+", shape="+shape+", fontcolor="+colorToUse+"];");	 
		gv.addln(gv.end_graph());
		//System.out.println(gv.getDotSource()); 

		String type = "png";
		gv.createDotFile(new File(dotFilename));
		return gv.getGraph(gv.getDotSource(), type);
	}

	protected static String classifyAlternatives(ArrayList <String> alter,
			ArrayList <String> affectations,Color colorToUse,String currentClass, String shape)
	{
		String encodedImage="";
		int translation=10;
		int width = 400, height = 300;
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bi.createGraphics();  
		g.setColor(colorToUse); 
		Font fontText = new Font("Arial", Font.BOLD, 16);
		g.setFont(fontText);


		g.setStroke(new BasicStroke(3F));  
		if(shape.equals("rectangle"))
			g.drawRect(70,height/6,300, 200);
		else if(shape.equals("oval"))
			g.drawOval(70,height/6,300, 200);
		else if(shape.equals("diamond"))
		{
			int [] abscissa = {220+translation,50+translation,220+translation,370+translation}; 
			int [] ordonates = {50,150,250,150};
			/*int [] abscissa = {220,60,220,380}; 
	                  			 int [] ordonates = {50,150,250,150};*/
			/*int [] abscissa = {200,0,200,400}; 
	                  			 int [] ordonates = {10,150,290,150};*/

			Polygon pShape=new Polygon(abscissa,ordonates,4);
			g.drawPolygon(pShape);
		}
		else 
		{

		}
		int nbrOfAlternatives=0;
		for(int i =0;i<affectations.size();i++)
			if(affectations.get(i).equals(currentClass))
				nbrOfAlternatives++;
		int taille=nbrOfAlternatives*10;
		Polygon p=positions(new Point(width/2,height/2),new Dimension(taille, taille),nbrOfAlternatives);
		//g.drawPolygon(p);
		int delta=5;
		g.setColor(colorToUse); 
		for(int i =0;i<nbrOfAlternatives;i++)
		{
			//g.fillOval(p.xpoints[i]-delta, p.ypoints[i]-delta, 2*delta,2*delta);
			//ou bien

			g.drawLine(p.xpoints[i]-delta, p.ypoints[i]-delta+translation, p.xpoints[i]+delta, p.ypoints[i]+delta+translation);
			g.drawLine(p.xpoints[i]-delta, p.ypoints[i]+delta+translation, p.xpoints[i]+delta, p.ypoints[i]-delta+translation);


		}


		// Afficher alternativeID
		int j=0;
		for(int i =0;i<alter.size();i++)
		{

			if(affectations.get(i).equals(currentClass))
			{

				g.drawString(alter.get(i), p.xpoints[j], p.ypoints[j]-2*delta+translation);
				j++;
			}
		}

		if(j!=nbrOfAlternatives)
			System.out.println("erreur pour les classes!!!");


		// Mettre le titre pour la classe en cours		 
		g.drawString( currentClass, 0, (height/2)-3*translation);

		g.dispose();

		File file;
		try {

			file=File.createTempFile("classifyAltern_", ".png", new File(System.getProperty("java.io.tmpdir")));
			ImageIO.write(bi, "PNG", file);
			FileInputStream input= new FileInputStream(file);			 
			BufferedImage image = ImageIO.read(input);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			encodedImage = Base64.encode(baos.toByteArray());
			baos.close();
			input.close();
			if(!file.delete())
				System.err.println("Warning: " + file.getAbsolutePath() + " could not be deleted!");

		} catch (IOException ie) {
			ie.printStackTrace();
		}

		return encodedImage;
	}

	protected static void classifyUniquePlot(ArrayList <String> alter,ArrayList <String> affectations, ArrayList <String> classes, int[]ranks,
			Color colorToUse,String plotsOrganisation,String order, String shape, String pngFilename)
	{
		int translation=10;
		int width = 400, height = 300;		
		ByteArrayOutputStream []chart_png_image = new ByteArrayOutputStream[classes.size()];  
		for (int indexClass=0;indexClass< classes.size() ;indexClass++)
		{
			BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = bi.createGraphics();  
			g.setColor(colorToUse); 
			Font fontText = new Font("Arial", Font.BOLD, 16);
			g.setFont(fontText);


			g.setStroke(new BasicStroke(3F));  
			if(shape.equals("rectangle"))
				g.drawRect(70,height/6,300, 200);
			else if(shape.equals("oval"))
				g.drawOval(70,height/6,300, 200);
			else if(shape.equals("diamond"))
			{
				int [] abscissa = {220+translation,50+translation,220+translation,370+translation}; 
				int [] ordonates = {50,150,250,150};
				/*int [] abscissa = {220,60,220,380}; 
				 int [] ordonates = {50,150,250,150};*/
				/*int [] abscissa = {200,0,200,400}; 
				 int [] ordonates = {10,150,290,150};*/

				Polygon pShape=new Polygon(abscissa,ordonates,4);
				g.drawPolygon(pShape);
			}
			else 
			{

			}
			int nbrOfAlternatives=0;
			for(int i =0;i<affectations.size();i++)
				if(affectations.get(i).equals(classes.get(indexClass)))
					nbrOfAlternatives++;
			int taille=nbrOfAlternatives*10;
			Polygon p=positions(new Point(width/2,height/2),new Dimension(taille, taille),nbrOfAlternatives);
			//g.drawPolygon(p);
			int delta=5;
			g.setColor(colorToUse); 
			for(int i =0;i<nbrOfAlternatives;i++)
			{
				//g.fillOval(p.xpoints[i]-delta, p.ypoints[i]-delta, 2*delta,2*delta);
				//ou bien

				g.drawLine(p.xpoints[i]-delta, p.ypoints[i]-delta+translation, p.xpoints[i]+delta, p.ypoints[i]+delta+translation);
				g.drawLine(p.xpoints[i]-delta, p.ypoints[i]+delta+translation, p.xpoints[i]+delta, p.ypoints[i]-delta+translation);


			}


			// Afficher alternativeID
			int j=0;
			for(int i =0;i<alter.size();i++)
			{

				if(affectations.get(i).equals(classes.get(indexClass)))
				{

					g.drawString(alter.get(i), p.xpoints[j], p.ypoints[j]-2*delta+translation);
					j++;
				}
			}

			if(j!=nbrOfAlternatives)
				System.out.println("erreur pour les classes!!!");


			// Mettre le titre pour la classe en cours		 
			g.drawString( classes.get(indexClass), 0, (height/2)-3*translation);

			g.dispose();

			File file;
			try {

				file=File.createTempFile("classifyAltern_", ".png", new File(System.getProperty("java.io.tmpdir")));
				ImageIO.write(bi, "PNG", file);
				FileInputStream input= new FileInputStream(file);			 
				BufferedImage image = ImageIO.read(input);
				chart_png_image [indexClass] = new ByteArrayOutputStream();
				ImageIO.write(image, "png", chart_png_image [indexClass]);					 
				chart_png_image [indexClass].close();
				input.close();
				if(!file.delete())
					System.err.println("Warning: " + file.getAbsolutePath() + " could not be deleted!");

			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}

		// Mettre dans l'ordre souhaite
		orderClassesUniquePlot(classes,ranks,order,chart_png_image);

		InputStream[]inputStream = new InputStream[classes.size()];
		for (int i=0;i<classes.size();i++)
			inputStream[i]= new ByteArrayInputStream(chart_png_image[i].toByteArray());

		if(plotsOrganisation.equals("Line"))
			convertImageByLine(inputStream,"png",classes.size(), pngFilename);

		else if(plotsOrganisation.equals("Column"))
			convertImageByColumn(inputStream,"png",classes.size(), pngFilename);

		else  if(plotsOrganisation.equals("Grid"))
			convertImageByGrid(inputStream,"png",classes.size(), pngFilename);

		else{}
	}

	protected static void classifyUniquePlotUsingGraphViz(ArrayList <String> alter,ArrayList <String> affectations, ArrayList <String> classes, int[]ranks,
			String colorToUse,String plotsOrganisation,String order, String shape,
			String dotFilename, String pngFilename)
	{
		ByteArrayOutputStream []png_image = new ByteArrayOutputStream[classes.size()];
		for (int indexClass=0;indexClass< classes.size() ;indexClass++)
		{
			GraphViz gv = new GraphViz();				
			gv.addln(gv.start_graph(colorToUse, shape));
			char guillemet = '"';



			String titlePosition="t";
			gv.addln("labelloc="+guillemet+titlePosition+guillemet+";");
			String title=classes.get(indexClass);		
			gv.addln("label="+guillemet+title+guillemet+"; fontcolor="+colorToUse+";");

			String label="";
			for(int i =0;i<affectations.size();i++)
			{
				if(affectations.get(i).equals(classes.get(indexClass)))
				{
					if (label.equals(""))

						label=alter.get(i);
					else
						label=label+", "+alter.get(i);						
				}
			}

			String nameLabel="label"+classes.get(indexClass);
			gv.addln(nameLabel+" [label = "+guillemet+label+guillemet+", shape="+shape+", fontcolor="+colorToUse+"];");	 
			gv.addln(gv.end_graph());
			//System.out.println(gv.getDotSource()); 

			String type = "png";
			png_image[indexClass]= new ByteArrayOutputStream();
			try
			{
				gv.createDotFile(new File(dotFilename));
				png_image[indexClass].write(gv.getGraph(gv.getDotSource(), type));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Mettre dans l'ordre souhaite
		orderClassesUniquePlot(classes,ranks,order,png_image);

		InputStream[]inputStream = new InputStream[classes.size()];
		for (int i=0;i<classes.size();i++)
			inputStream[i] = new ByteArrayInputStream((png_image[i].toByteArray()));

		if(plotsOrganisation.equals("Line"))
			convertImageByLine(inputStream, "png", classes.size(), pngFilename);

		else if(plotsOrganisation.equals("Column"))
			convertImageByColumn(inputStream, "png", classes.size(), pngFilename);

		else  if(plotsOrganisation.equals("Grid"))
			convertImageByGrid(inputStream, "png", classes.size(), pngFilename);

		else{}

	}

	protected static void exchange (ByteArrayOutputStream []chart_png_image, int i, int j)
	{
		ByteArrayOutputStream temp=chart_png_image[j];
		chart_png_image[j]=chart_png_image[i];
		chart_png_image[i]=temp;
	}

	protected static void exchangeFiles (File []chart_png_image, int i, int j)
	{
		File temp=chart_png_image[j];
		chart_png_image[j]=chart_png_image[i];
		chart_png_image[i]=temp;
	}

	protected static void convertImageByLine (java.io.InputStream[] inputStream, String format, int size,
	                                            String pngFilename) 
	{  
		BufferedImage []images=new BufferedImage [size]; 
		for (int i=0;i<size;i++)
			try{		 	
				images[i] = ImageIO.read(inputStream[i]);					 	
			} catch(Exception e) {		
				//e.printStackTrace();  
			}

		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			w+=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			x+=images[i].getWidth();
		}

		try {
			FileOutputStream fos = new FileOutputStream(new File(pngFilename));
			ImageIO.write(combined, format, fos);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}  

	protected static void convertImageByColumn (java.io.InputStream []inputStream,String format, int size,
	                                            String pngFilename) 
	{  
		String encodedImage="";
		BufferedImage []images=new BufferedImage [size];
		for (int i=0;i<size;i++)
			try{			 	
				images[i] = ImageIO.read(inputStream[i]);
			} catch(Exception e) {
				e.printStackTrace();
			}

		// create the new image, canvas size is the max. of both image sizes
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			h+=images[i].getHeight();
		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;
		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			y+=images[i].getHeight();			 
		}

		try {
			FileOutputStream fos = new FileOutputStream(new File(pngFilename));
			ImageIO.write(combined, format, fos);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static void convertImageByGrid (java.io.InputStream[] inputStream, String format, int size,
	                                          String pngFilename) 
	{
		BufferedImage []images=new BufferedImage [size];

		for (int i=0;i<size;i++)
			try{

				images[i] = ImageIO.read(inputStream[i]); 	
			} catch(Exception e) {	
				e.printStackTrace();     
			}

		// create the new image, canvas size is the square of the size
		int maxNumberOfImagesByLine=(int) Math.ceil(Math.sqrt(size));
		int w=images[0].getWidth();
		for (int i=1;i<size;i++)	
			if(images[i].getWidth()>w)
				w=images[i].getWidth();
		int maxWidth=w;
		w=w*maxNumberOfImagesByLine;

		int h=images[0].getHeight();
		for (int i=1;i<size;i++)
			if(images[i].getHeight()>h)
				h=images[i].getHeight();
		int maxHeight=h;
		h=h*maxNumberOfImagesByLine;

		BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = combined.getGraphics();

		int x=0;
		int y=0;

		for (int i=0;i<size;i++)					 	
		{	 					 	 
			g.drawImage(images[i], x, y, null);
			x+=maxWidth;
			if(x>=w)
			{
				x=0;
				y+=maxHeight;
			}
		}

		try {
			FileOutputStream fos = new FileOutputStream(new File(pngFilename));
			ImageIO.write(combined, format, fos);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static void orderClasses(ArrayList<String> classes, int[] ranks, String order, byte[][] encodedImage)
	{
		if(order.equals("increasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())<0)
						exchangeImages (encodedImage,i,j);  
		}

		else if(order.equals("decreasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())>0)
						exchangeImages (encodedImage,i,j);  
		}

		else if(order.equals("categories"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if(ranks[j]<ranks[i])
						exchangeImages (encodedImage,i,j);  
		}
		else
		{

		}
	}

	protected static void orderClassesUniquePlot(ArrayList <String> classes,int []ranks ,String order,ByteArrayOutputStream []encodedImage)
	{
		if(order.equals("increasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())<0)
						exchangeBOS (encodedImage,i,j);  
		}

		else if(order.equals("decreasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())>0)
						exchangeBOS (encodedImage,i,j);  
		}

		else if(order.equals("categories"))
		{
			// Compl�ter
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if(ranks[j]<ranks[i])
						exchangeBOS (encodedImage,i,j);  
		}
		else
		{

		}
	}

	protected static void orderClassesUniquePlotUsingGraphViz(ArrayList <String> classes,int []ranks ,String order,File[]encodedImage)
	{
		if(order.equals("increasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())<0)
						exchangeFiles (encodedImage,i,j);  
		}

		else if(order.equals("decreasing"))
		{
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if((classes.get(j).toLowerCase()).compareTo(classes.get(i).toLowerCase())>0)
						exchangeFiles (encodedImage,i,j);  
		}

		else if(order.equals("categories"))
		{
			// Compl�ter
			for (int i=0;i< classes.size()-1 ;i++)
				for (int j=i+1;j< classes.size() ;j++)
					if(ranks[j]<ranks[i])
						exchangeFiles (encodedImage,i,j);  
		}
		else
		{

		}
	}

	protected static void exchangeImages (byte[][]chart_png_image, int i, int j)
	{
		byte[] temp=chart_png_image[j];
		chart_png_image[j]=chart_png_image[i];
		chart_png_image[i]=temp;
	}

	protected static void exchangeBOS (ByteArrayOutputStream []chart_png_image, int i, int j)
	{
		ByteArrayOutputStream temp=chart_png_image[j];
		chart_png_image[j]=chart_png_image[i];
		chart_png_image[i]=temp;
	}

	public static void main(String[] argv)
	{
		classification classf=new classification();

		String errorMessage = "";
		String warningMessage = "";

		if(argv.length!=6 && argv.length!=7)
		{
			System.err.println("Usage: alternatives.xml alternativesAssignments.xml [categories.xml] parameters.xml alternativesAssignments.dot alternativesAssignments.png messages.xml");
			System.err.println("       The 4 first files are inputs, the remaining ones are outputs.");
			System.err.println("       'categories.xml' may be omitted.  If supplied but the file does not exist, the program behaves as if it was not supplied.");
			System.exit(-1);
		}


		final String pathAlternatives = argv[0];
		final String pathAlternAffect = argv[1];
		final String pathCategories = argv.length == 7 && new File(argv[2]).exists() ? argv[2] : null;
		final String pathParameters = argv[argv.length - 4];
		final String pathOutputFileDot = argv[argv.length - 3];
		final String pathOutputFilePlot = argv[argv.length - 2];
		final String pathOutputMessage = argv[argv.length - 1];

		boolean stopRunning=false;
		boolean useCategories=true;

		if(argv.length==7)
		{
			if (!new File(pathAlternatives).exists()||!new File(pathAlternAffect).exists()||!new File(pathParameters).exists())
			{
				stopRunning =true;
				errorMessage="Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). " +
						"Please check your entry files.";
				// Output Log Message File
				StatusMessage xmcdaMess= new StatusMessage();
				xmcdaMess.createErrorMessage(errorMessage);
				xmcdaMess.save(pathOutputMessage);
				return;
			}
			if(pathCategories==null)
				useCategories = false;
		}

		//Check XMCDA: MethodParameters
		String errMethodParameters=checkEntryFile(pathParameters);
		if(errMethodParameters!="")
		{
			if(errorMessage.equals(""))
				errorMessage=errMethodParameters;
			else
				errorMessage=errorMessage.concat("\n"+errMethodParameters);
		}

		//Check XMCDA: XML file for Alternatives
		String errAltern =checkEntryFile(pathAlternatives);

		if(errAltern!="")
		{

			if(errorMessage.equals(""))
				errorMessage=errAltern;
			else
				errorMessage =errorMessage.concat("\n"+errAltern);
		}

		//Check XMCDA: XML file for AlternativesAffectations
		String errAlternAffect=checkEntryFile(pathAlternAffect);
		if(errAlternAffect!="")
		{
			if(errorMessage.equals(""))
				errorMessage=errAlternAffect;
			else
				errorMessage =errorMessage.concat("\n"+errAlternAffect);
		}


		String errCategories="";
		if(useCategories)
		{
			errCategories = checkEntryFile(pathCategories);	  				
			if(errCategories!="")
			{	  					
				if(errorMessage.equals(""))
					errorMessage=errCategories;
				else
					errorMessage =errorMessage.concat("\n"+errCategories);
			}

		}
		if(errMethodParameters!="" ||errAltern!=""||errAlternAffect!=""||errCategories!="")
		{

			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);

			xmcdaMess.save(pathOutputMessage);
			return;
		}

		//ParseMethodParameters
		ParseMethodParameters parseMethodParam=new ParseMethodParameters();
		classf.PrepareParsing (pathParameters);
		boolean statusParseMethod=parseMethodParam.parse(classf.getRacine());

		if(statusParseMethod==false)
		{
			//Error message
			if(errorMessage.equals(""))
				errorMessage="\nFatal error: the file containing the parameters is erroneous or empty.";
			else
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the parameters is erroneous or empty.");
		}

		if(parseMethodParam.getWarning()!="")
			warningMessage = warningMessage.concat("\n"+parseMethodParam.getWarning());



		//Parsing XML file for Alternatives
		classf.PrepareParsing (pathAlternatives);
		Alternatives alternativesToUse=new Alternatives();
		ParseAlternatives jdomAlter= new ParseAlternatives();
		alternativesToUse=jdomAlter.exploreAlternativesXML(classf.getRacine());
		//alternativesToUse.printAlternatives();
		if(alternativesToUse==null)
		{	
			//Error message
			if(errorMessage.equals(""))
				errorMessage="\nFatal Error: the file containing the alternatives " +
						"tag is erroneous or empty.";
			else
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
						" alternatives tag is erroneous or empty.");
		}


		if(jdomAlter.getWarning()!="")
			warningMessage= warningMessage.concat("\n"+jdomAlter.getWarning());

		//Parsing XML file for AlternativesAffectations
		classf.PrepareParsing (pathAlternAffect);

		parseAffectations alternAffect= new parseAffectations();
		boolean parseAlterAffect=alternAffect.parseAlternativesAffectations(classf.getRacine());
		ArrayList<String > listAlternIds=alternAffect.getAlternIDs();
		ArrayList<String > listAffectations=alternAffect.getAffectations();
		/*for (int i=0;i<listAlternIds.size();i++)
				System.out.println(i+": "+listAlternIds.get(i)+" "+listAffectations.get(i));*/
		if(!parseAlterAffect)
		{
			//Error message
			if(errorMessage.equals(""))
				errorMessage="\nFatal error: the file containing the alternativesAffectations " +
						"tag is erroneous or empty.";
			else
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
						" alternativesAffectations tag is erroneous or empty.");
		}
		if(alternAffect.getWarningMessage()!="")
			warningMessage= warningMessage.concat("\n"+alternAffect.getWarningMessage());


		if (!parseAlterAffect || alternativesToUse==null || !statusParseMethod)
		{
			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.save(pathOutputMessage);
			return;
		}


		CheckEntryFiles check=new CheckEntryFiles();
		boolean verif=check.checkIDs(alternativesToUse,listAlternIds);
		if(!verif)
			warningMessage= warningMessage.concat(check.getWarning());

		//Intersection list
		Alternatives intersectAlternatives=check.getIntersectionIDs(alternativesToUse,listAlternIds);
		ArrayList<String > listIntersectAffectations=check.getIntersectionAffectations(intersectAlternatives.getAlternativeIDs(),
				listAlternIds,listAffectations);


		//intersectAlternatives.printAlternatives();

		// Si l'intersection est vide on prend ce qu'on a dans alternativesAffectations
		if (intersectAlternatives.getAlternativeIDs().isEmpty())
		{
			warningMessage=warningMessage.concat("\nOnly the alternatives IDs " +
					"in the file containing the alternativesAffectations tag were considered. " +
					"Please check your input file containing the alternatives tag.");

			intersectAlternatives=new Alternatives();
			listIntersectAffectations=new ArrayList<String >();
			for (int i=0; i<listAlternIds.size();i++)
			{
				intersectAlternatives.addAlternative(new Alternative(listAlternIds.get(i)));
				intersectAlternatives.getAlternativeIDs().add(listAlternIds.get(i));
				listIntersectAffectations.add(listAffectations.get(i));
			} 

		}



		boolean uniquePlot=parseMethodParam.getUniquePlot();
		String plotsOrganisation=parseMethodParam.getPlotsOrganisation();
		String colorFromOptions=parseMethodParam.getColorToUse();
		String order=parseMethodParam.getOrder();
		String shape=parseMethodParam.getShape();


		if(useCategories && !(order.equals("categories")))
		{
			warningMessage=warningMessage.concat("\nError in the file containing the parameters: " +
					"the parameter 'order' must be equal to 'categories' in this case.");
			order="categories";
		}

		if(!useCategories && order.equals("categories"))
		{

			warningMessage=warningMessage.concat("\nError in the file containing the parameters: " +
					"the parameter 'order' cannot be equal to 'categories' when no categories are supplied.");
			order="increasing";
		}

		String plotTitle=parseMethodParam.getTitle();
		if(plotTitle.isEmpty())
			plotTitle="Alternatives assignments plot" ;


		main:
		{
			if(intersectAlternatives.getAlternativeIDs().size()==0 )
			{
				errorMessage=errorMessage.concat("\n We cannot run the algorithm " +
						"since the alternatives list  " +
						"of the PROJECT is empty.\n");
				break main;
			}

			if(useCategories && pathCategories==null)
			{
				// Il y a erreur dans method parameters
				errorMessage=errorMessage.concat("\n Inexistant entry file containing the categories tag. " +
						"Check also your parameter 'Use categories' in the entry file containing method parameters tag.");
				break main;
			}
			ArrayList <String> classes =searchClasses(listIntersectAffectations);
			// Derouler l'algorithme du plot
			boolean statusCateg=true;
			int []ranks=null;
			if(useCategories)
			{
				//ParseCategories
				ParseCategories parseCateg=new ParseCategories();
				classf.PrepareParsing (pathCategories);
				statusCateg=parseCateg.parse(classf.getRacine(),classes );
				ranks=parseCateg.getRanks();

				if(parseCateg.getWarning()!="")
					warningMessage = warningMessage.concat("\n"+parseCateg.getWarning());

				if(!statusCateg)
				{
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal error: the file containing the categories " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" categories tag is erroneous or empty.");
				}
			}

			if(!statusCateg)
				break main;

			byte encodedImage[][]=new byte[classes.size()][];

			// Output XMCDA File to plot the classes for the alternatives

			ArrayList<String > listAlternIdsOrdered= new ArrayList<String >();

			for (int i=0;i< intersectAlternatives.getAlternativeIDs().size();i++)
			{
				final int indexAltern=listAlternIds.indexOf(intersectAlternatives.getAlternativeIDs().get(i));
				listAlternIdsOrdered.add(listAlternIds.get(indexAltern));
			}

			if (!uniquePlot)
			{
				final String dot_template = pathOutputFileDot.substring(0, pathOutputFileDot.indexOf('.')) + "-%02d.dot";
				for (int i = 0; i < classes.size(); i++)
					// encodedImage[i]=classifyAlternatives(listAlternIdsOrdered,listIntersectAffectations,colorToUse,classes.get(i),shape);
					encodedImage[i] = classifyAlternativesUsingGraphViz(listAlternIdsOrdered, listIntersectAffectations,
					                                                    colorFromOptions.toLowerCase(), classes.get(i),
					                                                    shape, String.format(dot_template, i+1));
				orderClasses(classes, ranks, order, encodedImage);

				final String png_template = pathOutputFileDot.substring(0, pathOutputFilePlot.indexOf('.'))
				                            + "-%02d.png";
				FilePlotClasses.createFileMultiplePlots(encodedImage, plotTitle, png_template);
			} else // (uniquePlot==true)
			{
				// image=classifyUniquePlot(listAlternIdsOrdered,listIntersectAffectations,classes,ranks,colorToUse,plotsOrganisation,
				// order,shape);
				classifyUniquePlotUsingGraphViz(listAlternIdsOrdered, listIntersectAffectations, classes, ranks,
				                                colorFromOptions.toLowerCase(), plotsOrganisation, order, shape,
				                                pathOutputFileDot, pathOutputFilePlot);
			}
		}

		// Output Log Message File
		StatusMessage xmcdaMess = new StatusMessage();
		xmcdaMess.createLogMessage(warningMessage,errorMessage);		
		xmcdaMess.save(pathOutputMessage);
	}

	public Element getRacine()
	{
		return racine;
	}
}

