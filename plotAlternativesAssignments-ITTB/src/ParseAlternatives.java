

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import java.io.File;
import java.util.List;
import java.util.Iterator;
public class ParseAlternatives {

	   private Alternatives alternativesToTest;
	   private String pathXMLFile;
	   static org.jdom2.Document document;
	   private String warning;
	  
	 // Constructor
	   public ParseAlternatives()
	   {
		   warning="";
		   alternativesToTest=new Alternatives();
	   }
	   
	   /*********************************************************
	    *********************************************************
	    * @param path
	    * @return
	    */
	   public Element getRacine(String path) {
			  
			  
			  try
			     {			
					   SAXBuilder sxb = new SAXBuilder();
					  document = sxb.build(new File(path));
			     }
			     catch(Exception e){}
		    
			     Element racine = document.getRootElement();
			  
		  return racine;
		  }
		   
		/*********************************************************
		 *   
		 * @param path
		 */
	   public void setPath(String path)
		   {
			   this.pathXMLFile=path;
		   }
		   
		/*********************************************************
		 * 
		 * @return
		 */
	   public String getPath()
		   {
			   return pathXMLFile;
		   }
	   /*********************************************************
	    * 
	    * @return
	    */
		  
		  public Alternatives getAlternatives()
		  {
			  return alternativesToTest;
		  }
		  
		  /*********************************************************
		   * 
		   */
		  public void  printAlternatives()
		  {
			  System.out.println("There are "+alternativesToTest.
					  getListAlternatives().size()+" alternatives:");
			  for(int i=0;i<alternativesToTest.getListAlternatives().size();i++)
				  System.out.print(alternativesToTest.getListAlternatives().
						  get(i).getAlternativeId()+"\t");
			  System.out.println("");
		  }
	 
		  
	  @SuppressWarnings("unchecked")
	public Alternatives exploreAlternativesXML(Element racine)
	   {	      
		   
		  if(racine.getChild("alternatives")!=null)
		  {
		  Element alternatives = racine.getChild("alternatives");
		   //On cr�e une List contenant tous les noeuds "alternative" de l'Element racine
	      
		  if(alternatives.getChildren("alternative")!=null){
		  List listAlternatives = alternatives.getChildren("alternative");

	      //On cr�e un Iterator sur notre liste
	      Iterator i = listAlternatives.iterator();
	      while(i.hasNext())
	      {
	         Element courant = (Element)i.next();
	   
	    	  if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")!=null)
		         {
		         
		         
		        	 if(courant.getChild("active")!=null && 
		        			 courant.getChild("active").getValue().equals("false"))
		        	 {
		        		 //Inactive alternative, on l'ignore
		        		 warning=warning.concat("The alternative "+courant.getAttributeValue("id")+" was ignored " +
	        		 		"because it is not active: active=false.\n");
		        	 }
		        	 else 
		        	 {
		        		 if(!alternativesToTest.getAlternativeIDs().contains(courant.getAttributeValue("id")))
		        		 {
		        		 alternativesToTest.addAlternative(new Alternative
				        		 (courant.getAttributeValue("id"),courant.getAttributeValue("name")));  
		        		 alternativesToTest.getListAlternativesIDs().add(courant.getAttributeValue("id"));
		        		 }
		        		 else
		        		 {
		        			 // Duplicate alternative ID
		        			 warning=warning.concat("\nWarning: duplicated alternative ("+
		        			                        courant.getAttributeValue("id")+
		        			                        "). Only the first occurence is considered.");
		        		 }
		        	 }
		         }
		      
		      
		         else if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")==null )
		         {
		        	 
		        	 
		        	 if(courant.getChild("active")!=null && 
		        			 courant.getChild("active").getValue().equals("false"))
		        	 {
		        		 //Inactive alternative, on l'ignore
		        		 warning=warning.concat("The alternative "+courant.getAttributeValue("id")+" was ignored " +
		        		 		"because it is not active: active=false.\n");
		        	 }
		        	 else 
		        	 {
		        		 
		        		 if(!alternativesToTest.getAlternativeIDs().contains(courant.getAttributeValue("id")))
		        		 {
		        			 alternativesToTest.addAlternative(new Alternative
				        		 (courant.getAttributeValue("id")));
		        			 alternativesToTest.getListAlternativesIDs().add(courant.getAttributeValue("id"));
		        		 }
		        		 else
		        		 {
		        			// Duplicate alternative ID
		        			 warning=warning.concat("\nWarning: duplicated alternative ("+
		        			                        courant.getAttributeValue("id")+
		        			                        "). Only the first occurence is considered.");
		        		 }
		        	 }
		        	 
		         }
		         
		         else
		         {
		        	 // Warning, on n'a pas d'ID 
		         }
	         
	      }
	      
	      //alternativesToTest.printAlternatives();
	     
	      if(alternativesToTest.getAlternativeIDs().size()==0)
	    	  return null ;
	      else
	      return alternativesToTest;
	   }
	  
		  else
			  return null;
		  }
		  else
	  {
		  return null;
	  }
	   }

	  public String getWarning()
	  {
		  return warning;
	  }
	}
