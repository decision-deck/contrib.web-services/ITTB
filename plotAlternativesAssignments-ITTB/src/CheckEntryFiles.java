import java.util.ArrayList;

public class CheckEntryFiles {
	

	private String warningMessage;
	
	
	
	//Constructor
	
	public CheckEntryFiles()
	{
		warningMessage="";
	
		
	}
	public boolean checkIDs(Alternatives alternativesToUse,ArrayList<String >  list)
	{
		boolean result=true;// les listes sont coh�rentes
		int diff=0;
		int size=alternativesToUse.getAlternativeIDs().size();

		if(list.size()!=size)
		{
			
			warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
					" incoherence with the number of alternatives given " +
					"in the file containing the 'alternativesAffectations' tag.");
			result=false;
		}

		else //On a la m�me taille mais il faudra v�rifier si c'est les m�mes Ids
		{
			for (String s : list)
			{
				if(alternativesToUse.getAlternativeIDs().contains(s))
				{
					// OK
				}
				else
				{
					result=false;
					diff++;
				}
			}
			if(diff>1)
			
				warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
						"incoherence with the alternatives given " +
						"in the file containing the 'alternativesAffectations' tag. There are "+diff+" different alternatives.");
		
			else if(diff==1)
			{
				warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
				             						"incoherence with the alternatives given " +
				             						"in the file containing the 'alternativesAffectations' tag. There is one different alternative.");
			}
			else {}
		}
					
		return result;		
	}

	

	
	public String getWarning()
	{
		return warningMessage;
	}
	
	/***************************************************************
	 * 
	 * @return
	 */
	public Alternatives getIntersectionIDs(Alternatives alternativesToUse,ArrayList<String >  list)
	{
		Alternatives result=new Alternatives();
		for (String t : alternativesToUse.getListAlternativesIDs()) 
		{
		        if(list.contains(t)) 
		        {
		        	int index=alternativesToUse.getListAlternativesIDs().indexOf(t);
		        	result.addAlternative(new Alternative(alternativesToUse.getListAlternativesIDs().get(index),
		   		        	                           alternativesToUse.getListAlternativesNames().get(index)));
		        	result.getAlternativeIDs().add(alternativesToUse.getListAlternativesIDs().get(index));
		        }
		}
		/*for (String t : list) 
		{
		        if(alternativesToUse.getListAlternativesIDs().contains(t)) 
		        {
		        	int index=alternativesToUse.getListAlternativesIDs().indexOf(t);
		        	result.addAlternative(new Alternative(t,alternativesToUse.getListAlternativesNames().get(index)));
		        	result.getAlternativeIDs().add(t);
		        }
		}*/
		return result;
	}
	/***************************************************************
	 * 
	 * @return
	 */
	public ArrayList<String > getIntersectionAffectations(ArrayList<String > intersectAlternatives, ArrayList<String > listAlternIDs,
	                                                      ArrayList<String > listAffectations)
	{
		ArrayList<String >result=new ArrayList<String >();
		/*for (int i=0; i<listAlternIDs.size();i++)
			System.out.println(i+": "+listAlternIDs.get(i)+" "+ listAffectations.get((i)));*/
		for (int i=0;i< intersectAlternatives.size();i++)
		{
			
			int indexAltern=listAlternIDs.indexOf(intersectAlternatives.get(i));
			result.add(listAffectations.get(indexAltern));
			/*System.out.println(intersectAlternatives.get(i)+" "+listAlternIDs.
			                   get(indexAltern)+" "+listAffectations.get(indexAltern));*/
		}
		
		return result;

		
	}

	
}