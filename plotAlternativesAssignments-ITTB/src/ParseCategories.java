import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;


public class ParseCategories
{
	private String warning;
	private int[]ranks ;
	public ParseCategories()
	{
		warning="";
	}
	
	@SuppressWarnings("unchecked")
    public boolean parse(Element racine,ArrayList <String> classes)
	   {	      
		   
    	boolean status=true;   
    	ranks=new int[classes.size()];
		for (int i=0;i<ranks.length;i++)
			   ranks[i]=-1;

		   int indexTab=0;
		   if(racine.getChild("categories")!=null)
		   {
			
			   Element categories = racine.getChild("categories");
			   if (categories.getChildren("category")!=null){
				   
				   List listCategor= categories.getChildren("category");
				   Iterator i = listCategor.iterator();
				   while(i.hasNext())
				   {
					   Element courant = (Element)i.next();	 
					   if(courant.getChild("active")!=null && 
			        			 courant.getChild("active").getValue().equals("false"))
			        	 {
			        		 //Inactive alternative, on l'ignore
			        		 warning=warning.concat("The category "+courant.getAttributeValue("id")+" was ignored " +
		        		 		"because it is not active: active=false.\n");
			        	 }
					   else
					   {
					   if(courant.getChild("rank")!=null)
					   {

						   Element rank=courant.getChild("rank");
						   if(rank.getChild("integer")!=null)
						   {						  
							   String temp=courant.getAttributeValue("id");
								   if(classes.contains(temp))
								   {
									   int indexOfClass=classes.indexOf(temp);
									   ranks[indexOfClass]=Integer.parseInt(rank.getChild("integer").getValue());
									   indexTab++;
								   }
			
						   }
						   else
							   warning=warning.concat("Warning: undefined tag 'integer' for " +
												   		"category "+courant.getAttributeValue("id")+".");
					   }
					   else
						   warning=warning.concat("Warning: undefined tag 'rank' for " +
						   		"category "+courant.getAttributeValue("id")+".");
					
				   
					   }
				  
				   }
				   
				   /*if(indexTab!=ranks.length)
					   status=false;*/
				   }
			   else status=false;
		   }
		   
		   else status=false;
		   
		  return status;
		 
	   }
	
	public String getWarning()
	{
		return warning;
	}
	public int[]getRanks()
	{
		return ranks ;
	}
}
