import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilePlotClasses {

	private FilePlotClasses() {}

	public static void createFileMultiplePlots(byte[][] encodedImage,
	                                           String commentToPut, String pngTemplate) {
		for (int i = 0; i < encodedImage.length; i++) {
			if (encodedImage[i]==null)
				continue;
			try {
				FileOutputStream fos = new FileOutputStream(new File(String.format(pngTemplate, i+1)));
				fos.write(encodedImage[i]);
				fos.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}
