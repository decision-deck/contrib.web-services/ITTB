import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.util.List;
import java.util.Iterator;

public class  ParseMethodParameters {

		  
		   static org.jdom2.Document document;
		   private static final String DEFAULT_COLOR = "black";
		   private String colorToUse;
		   private String arrangement;
		   private boolean uniquePlot;
		   private String order;
		   private String warning;
		   //private boolean useCategories;
		   private String title;
		   private String shape;

		 // Constructor
		   public   ParseMethodParameters(){
			   uniquePlot=true;
			   colorToUse=DEFAULT_COLOR;
			   arrangement ="Column";
			   warning="";
			   order="";
			   //useCategories=false;
			   title="";
			   shape="";
				}
		   //////////////////////////////////////////////////////////////////////////////////
		   //////////////////////////////////////////////////////////////////////////////////
		   public Element getRacine(String path) {
			  
			  try
			     {			
					   SAXBuilder sxb = new SAXBuilder();
					  document = sxb.build(new File(path));
			     }
			     catch(Exception e){}
		    
			     Element racine = document.getRootElement();
			  
		  return racine;
		  }
		   
	
		   @SuppressWarnings("unchecked")
		public boolean parse(Element racine)
		   {	      
			   
			   
			   boolean statusOK=true;
			   if (racine.getChild("methodParameters")!=null)
			   {
			   
			   Element methodParameters = racine.getChild("methodParameters");
			   
			   if(methodParameters.getChildren("parameter")!=null)
			   {
			   List listParam = methodParameters.getChildren("parameter");
			   Iterator i = listParam.iterator();
		      
		     
		      while(i.hasNext())
		      {
		         Element courant = (Element)i.next();
		         
		 
		         /*if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("use_categories"))
		         {
		        	
		        	 if ( (courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
			        	 		getChild("label")!=null) &&
			        	 		(courant.getChild("value").
			        	 		getChild("label").getValue().equals("true")))
		        		 useCategories=true;
		        	 
		        	 else if ( (courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
			        	 		getChild("label")!=null) &&
			        	 		(courant.getChild("value").
			        	 		getChild("label").getValue().equals("false")))
		        		 useCategories= false;
		        	 else
		        	 {
		        		 // Default value
		        		 useCategories= false;
			        		 warning=warning.concat("\nThe parameter 'Use categories?' is set to the default value 'false'. " +
		 	 					"Check the entry file containing the parameters.");
		        	 }
		        	 
		         }
		         
		         else */if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("plot_title"))
		         {
		        	
		        	 if ( (courant.getChild("value")!= null) && (courant.getChild("value").getChild("label")!=null))
		        		 title=courant.getChild("value").getChild("label").getValue();
		        	 else
		        		 warning=warning.concat("\nThe parameter 'Plot title' is empty or erroneous. " +
	 	 					"Check the entry file containing the parameters.");

		         }
		         
		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("unique_plot"))
		         {
		        	
		        	 if ( (courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
			        	 		getChild("label")!=null) &&
			        	 		(courant.getChild("value").
			        	 		getChild("label").getValue().equals("true")))
		        		 uniquePlot=true;
		        	 
		        	 else if ( (courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
			        	 		getChild("label")!=null) &&
			        	 		(courant.getChild("value").
			        	 		getChild("label").getValue().equals("false")))
		        		 uniquePlot= false;
		        	 else
		        	 {
		        		 // Default value
			        		 uniquePlot= true;
			        		 warning=warning.concat("\nThe parameter 'Number of images' is set to the default value 'Multiple images'. " +
		 	 					"Check the entry file containing the parameters.");
		        	 }
		        	 
		         }
		         
		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("plots_display"))
		         {
		        	 if ( (courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
			        	 		getChild("label")!=null) &&
			        	 		(courant.getChild("value").
			        	 		getChild("label").getValue().equals("Line")))
		        		 arrangement= "Line";
		        	 else if ((courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
					        	 		getChild("label")!=null) &&
		        			 (courant.getChild("value").
			        	 		getChild("label").getValue().equals("Column")))
		        		 arrangement= "Column";

		        	 else if ((courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
					        	 		getChild("label")!=null) &&
		        			 (courant.getChild("value").
			        	 		getChild("label").getValue().equals("Grid")))
		        		 arrangement= "Grid";	
		        	 else
		        	 {
		        		 arrangement= "Column";
		        		 warning=warning.concat("\nThe parameter 'Plots arrangement' is set to the default value 'Column'. " +
 	 					"Check the entry file containing the parameters.");
		        	 }
		         
		         }
		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("order"))
		        		 {		
		        	 		
		        	 if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("increasing"))
		        		order= "increasing";
		        	 else if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("decreasing"))

		        		 order= "decreasing";
		        	 else if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("categories"))

		        		 order= "categories";
		        	
		        	 	
		        	 else
		        	 {
		        		// �mettre un message d'erreur 
		        		 order= "increasing";
		        		 warning=warning.concat("\nThe parameter 'Order:' is set to the default value 'increasing'. " +
 	 					"Check the entry file containing the parameters.");
		        	 }
		        	 
		        		/*if(useCategories && !(order.equals("categories")))
		        		{
		        			
		        			warning=warning.concat("\nError in the file containing the parameters: " +
		        	 		"the parameter 'order' must be equal to 'categories' in this case.");
		        			order="categories";
		        		}*/
		    				
		        		
		        		 }
		         
		        
		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("use_color"))
		         {
	        		 warning=warning.concat("\nThe parameter 'Use color' is now ignored");
		         }

		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("selected_color"))
		        		 {
		        	 	
		        	 		if ((courant.getChild("value")!= null) && 
		        			 (courant.getChild("value").
					        	 		getChild("label")!=null) )
		        	 		colorToUse=courant.getChild("value").
		        	 			getChild("label").getValue();
		        	 		if (colorToUse==null || "".equals(colorToUse.trim()))
		        	 			colorToUse = DEFAULT_COLOR;
		        	 		
		        		 }
		         
		         else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
		        		 equals("categories_shape"))
		        		 {
		        	 	
		        	 if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("rectangle"))
		        		shape= "rectangle";
		        	 else if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("oval"))

		        		 shape= "oval";
		        	 else if (courant.getChild("value").
			        	 		getChild("label").getValue().equals("diamond"))

		        		shape= "diamond";
		        	
		        	 	
		        	 else
		        	 {
		        		// �mettre un message d'erreur 
		        		 shape= "oval";
		        		 warning=warning.concat("\nThe parameter 'Categories shape:' is set to the default value 'oval'. " +
	 					"Check the entry file containing the parameters.");
		        	 }
		        	 
		        	 		
		        	 		
		        		 }
		         
		         
		         else {// we have only the previous parameters in method parameters}
		        	 //System.out.println("id:"+courant.getAttributeValue("id"));
		        	 warning=warning.concat("Error in the file containing the parameters: " +
		        	 		"you have not entered the correct parameters." );
		        	                        statusOK=false;
		        	 }
		         
		      }
			   }
			   else 
				   statusOK=false;
		      
		      }
			   
			   else 
				   statusOK=false;
			   

			   
			   return statusOK;
		   }
		   
		   public String getColorToUse()
		   {
			   return colorToUse;
		   }
		   
		   public String getPlotsOrganisation()
		   {
			   return arrangement;
		   }
		   public boolean getUniquePlot()
		   {
			   return uniquePlot;
		   }
		   public String getOrder()
		   {
			   return order;
		   }
		  
		 public String getWarning()
		 {
			 return warning;
		 }
		   
		  /*public boolean UseCategories()
		  {
			  return useCategories;
		  }*/
		  public String getTitle()
		  {
			  return title;
		  }
		  public String getShape()
		  {
			  return shape;
		  }
		}