
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Criterion;
import eu.telecom_bretagne.xmcda.PerformanceTable;



public class NumPerfTableFile
{
	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public void save(String fichier)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos=new FileOutputStream(fichier);
			sortie.output(document,  fos);
			fos.close();

		}
		catch (java.io.IOException e){}
	}

	public  void createOutputFile(PerformanceTable projectPerfTable,
	                              String typesInPerfTable[] , 
	                              ArrayList <String> listCriterionIdSFromParsing, 
	                              String commentToPut)
	{

		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		//projectPerfTable.printPerfTable_Config2();
		Element performanceTable = new Element("performanceTable");
		racine.addContent(performanceTable);
		Attribute name =new Attribute("mcdaConcept",commentToPut);
		performanceTable.setAttribute(name);	

		for(Alternative alternative: projectPerfTable.alternatives())
		{
			Element alternativePerformances  = new Element("alternativePerformances");
			performanceTable.addContent(alternativePerformances);

			Element alternativeID = new Element("alternativeID");
			alternativePerformances.addContent(alternativeID);
			alternativeID.setText(alternative.id());
			for(Criterion criterion: projectPerfTable.criteria())
			{
				Element performance = new Element("performance");
				alternativePerformances.addContent(performance);
				Element criterionID = new Element("criterionID");
				performance.addContent(criterionID);
				criterionID.setText(criterion.id());
				Element value = new Element("value");
				performance.addContent(value);
				// R�cup�rer le type
				int indexCriterion=listCriterionIdSFromParsing.indexOf(projectPerfTable.
				                                                       getCriterionIDs().get(j));


				String type=typesInPerfTable[indexCriterion];
				String index=String.valueOf(projectPerfTable.getMatrixObjectElement(i,j));
				if(type.equals("real"))
				{
					Element real = new Element("real");
					value.addContent(real);
					real.setText(index);
				}
				else if(type.equals("integer"))
				{
					Element integer = new Element("integer");
					value.addContent(integer);
					integer.setText(index);
				}
				else if(type.equals("label"))
				{
					Element label = new Element("label");
					value.addContent(label);
					label.setText(index);
				}
				else if(type.equals("boolean"))
				{
					Element Boolean = new Element("boolean");
					value.addContent(Boolean);
					Boolean.setText(index);
				}
				else
				{
					//Normalement je pr�vois un message d'erreur
				}
			}
		}	     
	}	   
}