import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class ParseCriteria
{

	private ArrayList<ArrayList<String>>  listTransformationLabels;

	private ArrayList<ArrayList<Integer>> listTransformationRanks;

	private boolean[]                     listStateTransforma;

	private String                        pathXMLFile;

	static org.jdom2.Document             document;

	private String                        warning;

	public ParseCriteria()
	{
		warning = "";
		listTransformationLabels = new ArrayList<ArrayList<String>>();
		listTransformationRanks = new ArrayList<ArrayList<Integer>>();
	}

	public Element getRacine(String path)
	{
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return document.getRootElement();
	}

	public void setPath(String path)
	{
		this.pathXMLFile = path;
	}

	public String getPath()
	{
		return pathXMLFile;
	}

	public boolean getTransformation(Element racine, ArrayList<String> listCriterionIds)
	{
		ArrayList<ArrayList<String>> listTransformationLabelsBis = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<Integer>> listTransformationRanksBis = new ArrayList<ArrayList<Integer>>();

		boolean status = true;
		listStateTransforma = new boolean[listCriterionIds.size()];
		for (int i = 0; i < listCriterionIds.size(); i++)
			listStateTransforma[i] = false;

		int[] indexCritreriaTab = new int[listCriterionIds.size()];
		int indexTab = 0;
		if (racine.getChild("criteria") != null)
		{

			Element criteria = racine.getChild("criteria");
			if (criteria.getChildren("criterion") != null)
			{
				Iterator<Element> i = criteria.getChildren("criterion").iterator();
				while ( i.hasNext() )
				{
					Element courant = (Element) i.next();
					if (listCriterionIds.contains(courant.getAttributeValue("id")))
					{
						int indexCriterion = listCriterionIds.indexOf(courant.getAttributeValue("id"));
						indexCritreriaTab[indexTab] = indexCriterion;
						indexTab++;
						ArrayList<String> currentlistTransformationLabels = new ArrayList<String>();
						ArrayList<Integer> currentlistTransformationRanks = new ArrayList<Integer>();

						if (courant.getChild("scale") != null)
						{
							// Le criterion actuel
							Element scale = courant.getChild("scale");
							if (scale.getChild("qualitative") != null)
							{
								Element qualitative = scale.getChild("qualitative");
								if (qualitative.getChildren("rankedLabel") != null)
								{
									Iterator<Element> j = qualitative.getChildren("rankedLabel").iterator();

									while ( j.hasNext() )
									{
										Element courantRankedLabel = (Element) j.next();
										Element label = courantRankedLabel.getChild("label");

										if (!currentlistTransformationLabels.contains(label.getValue().toString()))
										{
											currentlistTransformationLabels.add(label.getValue().toString());
											Element rank = courantRankedLabel.getChild("rank");
											currentlistTransformationRanks.add(Integer.parseInt(rank.getValue()));
											listStateTransforma[indexCriterion] = true;
										}
										else
										{
											// Le label existe d�j�
											warning = warning.concat("Warning: check your entry file containing the "
											                         + "criteria tag: duplicate label("
											                         + label.getValue()
											                         + "). Only the first occurence is considered.");
										}
									}
								}
								else
									warning = warning.concat("Warning: undefined tag 'rankedLabel' for " + "criterion "
									                         + courant.getAttributeValue("id") + ".");
							}
							else
								warning = warning.concat("Warning: undefined tag 'qualitative' for " + "criterion "
								                         + courant.getAttributeValue("id") + ".");
						}
						else
							warning = warning.concat("Warning: undefined tag 'scale' for " + "criterion "
							                         + courant.getAttributeValue("id") + ".");

						if (!currentlistTransformationLabels.isEmpty())
						{
							listTransformationLabelsBis.add(currentlistTransformationLabels);
							listTransformationRanksBis.add(currentlistTransformationRanks);
						}
						else
						{
							listTransformationLabelsBis.add(null);
							listTransformationRanksBis.add(null);
						}
					}
				}
			}
		}
		if (listTransformationLabelsBis.isEmpty())
			status = false;
		else
		{
			// Mettre dans le bon ordre
			for (int index = 0; index < listCriterionIds.size(); index++)
			{
				listTransformationLabels.add(listTransformationLabelsBis.get(indexCritreriaTab[index]));
				listTransformationRanks.add(listTransformationRanksBis.get(indexCritreriaTab[index]));
			}
		}
		return status;
	}

	public String getWarning()
	{
		return warning;
	}

	public ArrayList<ArrayList<String>> getLabels()
	{
		return listTransformationLabels;
	}

	public ArrayList<ArrayList<Integer>> getRanks()
	{
		return listTransformationRanks;
	}

	public boolean[] getListStateTransforma()
	{
		return listStateTransforma;
	}
}
