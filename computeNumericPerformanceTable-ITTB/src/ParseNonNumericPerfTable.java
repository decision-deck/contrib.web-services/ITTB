import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.GestionIncoherentFiles;
import eu.telecom_bretagne.xmcda.PerformanceTable;


public class ParseNonNumericPerfTable {

	static org.jdom2.Document document;

	public static final int size = 100;
	private static Object [][] matricePerf= new Object[size][size];
	private static int index_matrix_line=0;
	private static int index_matrix_column=0;
	private String type[];

	private PerformanceTable perfTableConfig2;
	private ArrayList <String> listCriterionIDInperformanceTable;
	private ArrayList <String> listAlternativeIDInperformanceTable;


	//Error message for the performance Table type
	private String errorMessage;
	private String warningMessage;
	private String warningFromRun;

	// Constructor
	public ParseNonNumericPerfTable()
	{
		perfTableConfig2=new PerformanceTable();
		listCriterionIDInperformanceTable= new  ArrayList <String>()  ;
		listAlternativeIDInperformanceTable=new  ArrayList <String>()  ;
		errorMessage="";
		warningMessage="";
		warningFromRun="";
		type=new String[1000];
	}

	public ArrayList <String> getCriterionIDInperformanceTable()
	{
		return listCriterionIDInperformanceTable;
	}

	public ArrayList <String> getAlternativeIDInperformanceTable()
	{
		return listAlternativeIDInperformanceTable;
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		return document.getRootElement();
	}

	public  PerformanceTable  exploreObjectsPerformanceTable(Element racine) 
	{	      
		if( racine.getChild("performanceTable")!=null){
			Element perfTabl = racine.getChild("performanceTable");

			if(perfTabl.getChildren("alternativePerformances")!=null)
			{
				Iterator<Element> i = perfTabl.getChildren("alternativePerformances").iterator();
				String temp="";
				int numberCriterMax=0;
				while(i.hasNext())
				{
					numberCriterMax=0;
					Element courant = (Element)i.next();

					if (listAlternativeIDInperformanceTable.contains(courant.
					                                                 getChild("alternativeID").getValue())){}
					else
					{ 		 
						listAlternativeIDInperformanceTable.add(courant.
						                                        getChild("alternativeID").getValue());
					}
					index_matrix_line=listAlternativeIDInperformanceTable.
							indexOf(courant.
							        getChild("alternativeID").getValue());

					Iterator<Element> j = courant.getChildren("performance").iterator();

					if (courant.getChildren().size()>numberCriterMax);
					numberCriterMax=courant.getChildren().size()-1;


					boolean alternativeIDOK=true; 
					int indexType=0;
					while(j.hasNext())
					{
						Element courantNiv2 = (Element)j.next();
						temp="";

						if(courantNiv2.getChild("value").
								getChild("label")!=null)
						{
							temp=courantNiv2.getChild("value").
									getChild("label").getValue();
							type[indexType]="label";
							indexType++;
						}
						else if(courantNiv2.getChild("value").
								getChild("boolean")!=null)
						{
							temp=courantNiv2.getChild("value").
									getChild("boolean").getValue();
							type[indexType]="boolean";
							indexType++;
						}
						else if(courantNiv2.getChild("value").
								getChild("real")!=null)
						{
							temp=courantNiv2.getChild("value").
									getChild("real").getValue();
							type[indexType]="real";
							indexType++;
						}
						else if(courantNiv2.getChild("value").
								getChild("integer")!=null)
						{
							temp=courantNiv2.getChild("value").
									getChild("integer").getValue();
							type[indexType]="integer";
							indexType++;
						}
						else
						{
							alternativeIDOK=false;
						}
						if (listCriterionIDInperformanceTable.contains(courantNiv2.
						                                               getChild("criterionID").getValue())){}
						else
						{
							listCriterionIDInperformanceTable.add(courantNiv2.
							                                      getChild("criterionID").getValue());
						}
						if(temp!="")
						{
							index_matrix_column=listCriterionIDInperformanceTable.
									indexOf(courantNiv2.
									        getChild("criterionID").getValue());
							matricePerf [index_matrix_line][index_matrix_column]=temp;
						}
					}
					if( ! alternativeIDOK )
					{
						listAlternativeIDInperformanceTable.remove(courant.
						                                           getChild("alternativeID").getValue()); 
						errorMessage=errorMessage.concat("Error in the file containing the performance " +
								"table tag. The value must be real, integer, label or boolean."+
								"We have eliminated the corresponding alternative:"+
								courant.getChild("alternativeID").getValue()+".");
					}
					// On passe � une autre alternative
				}

				i = perfTabl.getChildren("alternativePerformances").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					String id=courant.getChild("alternativeID").getValue();
					int sizeInferior=courant.getChildren().size()-1;
					if(listAlternativeIDInperformanceTable.contains(id))
					{
						if (sizeInferior<numberCriterMax)
						{
							listAlternativeIDInperformanceTable.remove(id); 
							warningMessage=warningMessage.concat
									("\nThe alternativeID "+id+" was removed because " +
											"the number of criteria is "+sizeInferior+
											" instead of "+numberCriterMax+".");
						}
					}
				}  
				perfTableConfig2=new PerformanceTable(listAlternativeIDInperformanceTable,
				                                      listCriterionIDInperformanceTable,matricePerf);
				return perfTableConfig2;
			}
			else return null;
		}
		else return null;
	}

	public PerformanceTable run(PerformanceTable 
	                            perfTableAfterParsingXML,Criteria criteriaToUse,
	                            Alternatives alternativesToUse )
	{
		/**************************************************************************/ 
		/*********La gestion d'incoh�rences entre les diff�rents fichiers**********/
		/**************************************************************************/ 
		GestionIncoherentFiles gest=new GestionIncoherentFiles(criteriaToUse,
		                                                       alternativesToUse, perfTableAfterParsingXML);

		boolean checkAlt=gest.checkAlternatives(alternativesToUse,
		                                        perfTableAfterParsingXML);
		boolean checkCrit=gest.checkCriteria(criteriaToUse,
		                                     perfTableAfterParsingXML);
		if (!checkAlt)
			warningFromRun=warningFromRun.concat(gest.
			                                     getWarningAlternatives());
		if(!checkCrit)
			warningFromRun=warningFromRun.concat(gest.getWarningCriteria());

		// Dans ce cas il faudra se mettre d'accord sur la perfTable finale

		// listintersectionAlternatives
		ArrayList<String > listintersectionAlternatives=
				gest.getIntersectionAlternativesIDs();

		// Si l'intersection est vide on prend ce qu'on a dans la perfTable
		if (listintersectionAlternatives.isEmpty())
		{
			warningFromRun=warningFromRun.concat("\nOnly the alternativesIDs " +
					"in the file containing the performance table tag were considered. " +
					"Please check your input file containing the alternatives tag.");
			listintersectionAlternatives=
					perfTableAfterParsingXML.getAlternativeIDs();
		}

		// listintersectionCriteria/
		ArrayList<String > listintersectionCriteria=
				gest.getIntersectionCriteriaIDs();

		// Si l'intersection est vide on prend ce qu'on a dans la perfTable
		if (listintersectionCriteria.isEmpty())
		{
			warningFromRun=warningFromRun.concat("\nOnly the criterionIDs " +
					"in the file containing the performance table tag were considered. " +
					"Please check your input file containing the criteria tag.");

			listintersectionCriteria=
					perfTableAfterParsingXML.getCriterionIDs();
		}

		// The matrix to consider in the project
		Object [][] matricePerf=new Object [listintersectionAlternatives.
		                                    size()][listintersectionCriteria.size()];
		for(int l=0;l<listintersectionAlternatives.size();l++)
		{
			for(int c=0;c<listintersectionCriteria.size();c++)
			{

				matricePerf[l][c]=perfTableAfterParsingXML.getMatrixObjectElement
						(perfTableAfterParsingXML.getAlternativeIDs().indexOf
						 (listintersectionAlternatives.get(l)),
						 perfTableAfterParsingXML.getCriterionIDs().indexOf
						 (listintersectionCriteria.get(c)));
			}
		}

		// Voici la PerformanceTable du projet
		perfTableAfterParsingXML=new PerformanceTable
				(listintersectionAlternatives,
				 listintersectionCriteria,matricePerf);

		return perfTableAfterParsingXML;
	}

	public String getError()
	{
		return errorMessage;
	}

	public String getWarning()
	{
		return warningMessage;
	}

	public String gerWarningFromRun()
	{
		return warningFromRun;
	}

	public  ArrayList <String>  getList(String id)
	{
		ArrayList <String> result=new  ArrayList <String>();
		if(id.equals("alternativeID"))
			result=listAlternativeIDInperformanceTable;
		else if(id.equals("criterionID"))
			result=listCriterionIDInperformanceTable;
		else
		{
			/*System.out.println("Error: which list you want" +
     		 		"from performance table?\n alternativeID? Or criterionID?");*/
		}
		return result;
	} 

	public String[] getTypes()
	{
		return type;
	}
}
