import java.io.File;
import java.util.ArrayList;

import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;



public class ToNumericTransform
{
	private String warningNumericTransform = "";

	public PerformanceTable NumericTransform(PerformanceTable test,
	                                         ArrayList<ArrayList<String>> listTransformationLabels,
	                                         ArrayList<ArrayList<Integer>> listTransformationRanks,
	                                         boolean[] listStateTransform)
	{
		int nbrAltern = test.getAlternativeIDs().size();
		int nbrCriter = test.getCriterionIDs().size();

		Object[][] matricePerf = new Object[nbrAltern][nbrCriter];
		for (int i = 0; i < nbrAltern; i++)
		{
			for (int j = 0; j < nbrCriter; j++)
			{
				matricePerf[i][j] = test.getMatrixObjectElement(i, j);
				// R�cup�rer la transformation relative � ce crit�re
				if (listStateTransform[j])
				{
					ArrayList<String> currentListLabel = listTransformationLabels.get(j);
					ArrayList<Integer> currentListRanks = listTransformationRanks.get(j);

					String temp = (String) matricePerf[i][j];
					if (currentListLabel.contains(temp))
						matricePerf[i][j] = currentListRanks.get(currentListLabel.indexOf(temp));
					else
						// message d'erreur pour dire pas de transformation sp�cifi�e pour telle valeur
						warningNumericTransform += "Error in the file containing the 'scale' tag: there is "
						                           + "no ranked label for the value" + matricePerf[i][j] + ".";
				}
				else
				{
					// Message d'erreur pour dire "pas de transformation pour le crit�re"
					if (i == 0)
					    // une seule fois car �a ne sert � rien de le refaire pour toutes les alternatives
					    warningNumericTransform += "\nWarning: No transformation was proposed for criterion" + " ("
					                               + test.getCriterionIDs().get(j) + "). "
					                               + "Please check the entry file containing the 'scale' tag.";
				}
			}
		}
		PerformanceTable result = new PerformanceTable(test.getAlternativeIDs(),
		                                               test.getCriterionIDs(), matricePerf);
		return result;
	}

	public static void main(String[] argv)
	{
		if (argv.length != 5 )
		{
			System.err.println("Usage: alternatives.xml criteria.xml performanceTable.xml performanceTable.xml messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}

		final String pathAlternatives = argv[0];
		final String pathCriteria = argv[1];
		final String pathPerfTable = argv[2];
		final String pathOutputFile = argv[3];
		final String pathOutputMessage = argv[4];

		String tempErrorMessage = "";
		String warningMessage = "";

		if (!new File(argv[0]).exists() || !new File(argv[1]).exists() || !new File(argv[2]).exists())
		{
			tempErrorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction = new Main();
		PerformanceTable test = new PerformanceTable();

		String errAltern = mainFunction.checkEntryFile(pathAlternatives);
		String errCriter = mainFunction.checkEntryFile(pathCriteria);
		String errPerfTable = mainFunction.checkEntryFile(pathPerfTable);

		if(!"".equals(errAltern))
			tempErrorMessage += "\nFatal Error: the file containing the alternatives tag is not a valid XMCDA document.";

		if("".equals(errCriter))
			tempErrorMessage += "\nFatal Error: the file containing the criteria tag is not a valid XMCDA document.";

		if("".equals(errPerfTable))
			tempErrorMessage += "\nFatal Error: the file containing the performanceTable tag is not a valid XMCDA document.";

		if(!"".equals(errAltern) || !"".equals(errCriter) || !"".equals(errPerfTable))
		{
			new XmcdaMessage().createErrorMessage(tempErrorMessage).enregistre(pathOutputMessage);
			return;
		}
		
		// criteria
		JDOM_Criteria jdomCriter = new JDOM_Criteria();
		mainFunction.PrepareParsing(pathCriteria);
		Criteria criter = jdomCriter.parseCriteriaXML(mainFunction.getRacine());

		if (jdomCriter.getWarning() != "")
			warningMessage = warningMessage.concat("\n" + jdomCriter.getWarning());

		if (criter == null)
			tempErrorMessage += "\nFatal Error: the file containing the criteria tag is erroneous or empty.";

		// alternatives
		mainFunction.PrepareParsing(pathAlternatives);
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		Alternatives alter = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (jdomAlter.getWarning() != "")
			warningMessage += "\n" + jdomAlter.getWarning();

		if (alter == null)
				tempErrorMessage += "\nFatal Error: the file containing the alternatives tag is erroneous or empty.";

		// performanceTable
		mainFunction.PrepareParsing(pathPerfTable);
		ParseNonNumericPerfTable jdomPerfTable = new ParseNonNumericPerfTable();
		PerformanceTable perfTable = jdomPerfTable.exploreObjectsPerformanceTable(mainFunction.getRacine());
		if (jdomPerfTable.getError() != "")
			warningMessage += "\n" + jdomPerfTable.getError();
		if (jdomPerfTable.getWarning() != "")
			warningMessage += "\n" + jdomPerfTable.getWarning();

		if (perfTable == null)
			tempErrorMessage += "\nFatal Error: the file containing the performanceTable tag is erroneous or empty.";

		if (alter==null || criter==null || perfTable == null )
		{
			XmcdaMessage.writeErrorMessage(tempErrorMessage, pathOutputMessage);
			return;
		}

		test = jdomPerfTable.run(perfTable , criter,alter);

		warningMessage = warningMessage.concat(jdomPerfTable.gerWarningFromRun());

		boolean statusParseMethod = true;
		String typesInPerfTable[] = jdomPerfTable.getTypes();
		if (test.getAlternativeIDs().size() != 0 && test.getCriterionIDs().size() != 0)
		{
			// D�rouler l'lgorithme
			ParseCriteria pc = new ParseCriteria();
			// mainFunction.PrepareParsing (pathCriteria);
			mainFunction.PrepareParsing(pathCriteria);
			statusParseMethod = pc.getTransformation(mainFunction.getRacine(), test.getCriterionIDs());

			if (statusParseMethod ==false)
				tempErrorMessage += "\n We cannot run the algorithm since the transformation is not specified.\n";
			else
			{
				// r�cup�rer la liste dex transformations
				ArrayList<ArrayList<String>> listTransformationLabels = pc.getLabels();
				ArrayList<ArrayList<Integer>> listTransformationRanks = pc.getRanks();
				boolean[] listStateTransform = pc.getListStateTransforma();

				ToNumericTransform numTrans=new ToNumericTransform();
				test =numTrans.NumericTransform(test, listTransformationLabels, listTransformationRanks,
				                                listStateTransform);

				warningMessage += numTrans.warningNumericTransform;
			}
		}
		else
			tempErrorMessage += "\n We cannot run the algorithm since the criteria list or the alternatives list "
			                    + "of the PROJECT is empty.\n";

		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessage(errCriter, errAltern, errPerfTable, warningMessage, tempErrorMessage);

		xmcdaMess.enregistre(pathOutputMessage);

		// Output XMCDA File to put the mean values

		if (test.getAlternativeIDs().size() != 0 && test.getCriterionIDs().size() != 0
				&& statusParseMethod == true)
		{
			NumPerfTableFile outputPerfTable = new NumPerfTableFile();
			outputPerfTable.createOutputFile(test, typesInPerfTable,
			                                 perfTable.getCriterionIDs(),
			                                 "Transformed numeric performance table");
			String pathOutputFile = argv[argv.length - 2];
			outputPerfTable.save(pathOutputFile);
		}
	}

}
