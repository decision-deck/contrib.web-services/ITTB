import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;


public class ParametersPerCriter
{
	private String warning;
	private boolean unique_transformation;
	private ArrayList <String> listCriter;
	private ArrayList <String> listTransform;
	private String global_transfo;
	public ParametersPerCriter()
	{
		warning="";
		listCriter=new ArrayList<String>();
		listTransform=new ArrayList<String>();	
		unique_transformation=true;
		global_transfo="";
	}

	public boolean parse(Element racine)
	{
		boolean statusOK=true;
		boolean enter=false;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null && methodParameters.getChildren("parameter").size()!=0)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator(); 
				while(i.hasNext())
				{
					Element courant = (Element)i.next();

					if (courant.getAttributeValue("id").equals("criteria_transformations"))
					{
						if(!enter)
						{
							enter=true;
							unique_transformation=false;
							Iterator<Element> j = courant.getChildren("value").iterator(); 
							while(j.hasNext())
							{

								Element courant2 = (Element)j.next();
								if (courant2!= null && courant2.getAttribute("id")!=null)
								{
									// Ajouter � la liste des crit�res
									if (listCriter.contains(courant2.getAttributeValue("id")))
									{
										// Error message
										warning=warning.concat("\nWarning: duplicate criterionID ("+courant2.getAttributeValue("id")+
												") in the entry file containing the method parameters tag. Only the first provided transformation is considered.");
									}
									else
									{
										listCriter.add(courant2.getAttributeValue("id"));

										if ( (courant2!= null) && (courant2.getChild("label")!=null) &&
												(courant2.getChild("label").getValue().equals("by_max")))
											listTransform.add("by max");
										else if ((courant2!= null) && (courant2.getChild("label")!=null) &&
												(courant2.getChild("label").getValue().equals("min_max")))
											listTransform.add("min max");
										else if ((courant2!= null) && (courant2.getChild("label")!=null) &&
												(courant2.getChild("label").getValue().equals("mean_stdv")))
											listTransform.add("by mean and stdv");
										else
										{
											// Default value
											listTransform.add("");
											warning=warning.concat("\nWarning: an error occured while " +
													"trying to get the transformation for criterion: "+courant2.getAttributeValue("id")+
													".");
										}
									}
								}
							}
						}
						else
						{
							warning=warning.concat("which parameter: 'Global transformation' or 'Per criteria transformations'?" +
									" The entry file containing the method parameters tag is erroneous");
							statusOK=false;
							//System.out.println("warning:"+warning);
						}
					}
					else  if (courant.getAttributeValue("id").equals("global_transformation"))
					{
						if(!enter)
						{
							enter=true;
							if(!unique_transformation)
							{
								warning=warning.concat("\nwhich parameter: 'Global transformation' or 'Per criteria transformations'?" +
										" The entry file containing the method parameters tag is erroneous");
								//System.out.println("warning:"+warning);
								statusOK=false;
							}
							else
							{
								if ((courant.getChild("value")!= null) && 
										(courant.getChild("value").
												getChild("label")!=null) &&
												(courant.getChild("value").
														getChild("label").getValue().equals("by_max")))
									global_transfo="by max";
								else if ((courant.getChild("value")!= null) && 
										(courant.getChild("value").getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("min_max")))
									global_transfo="min max";
								else if ((courant.getChild("value")!= null) && 
										(courant.getChild("value").getChild("label")!=null) &&
										(courant.getChild("value").
												getChild("label").getValue().equals("mean_stdv")))
									global_transfo="by mean and stdv";
								else
								{
									// Default value
									global_transfo="";
									warning=warning.concat("\nWarning: an error occured while " +
											"trying to get the parameter 'Global transformation'" +
											".");
								}
							} 
						}
						else
						{
							warning=warning.concat("\nwhich parameter: 'Global transformation' or 'Per criteria transformations'?" +
									" The entry file containing the method parameters tag is erroneous");
							statusOK=false;
							//System.out.println("warning:"+warning);
						}
					}
					else
						statusOK=false;
				}
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;

		// Si on n'a rien rajout� au final: (�a peut arriver)
		if (!unique_transformation && listCriter.size()==0)
		{
			warning=warning.concat("\nWarning: an error occured while " +
					"trying to get the transformation for each criterion. " +
					"Check the entry file containing the method parameters tag. It seems to be empty or erroneous.");
			statusOK=false;
		}
		return statusOK;
	}

	public String getWarning()
	{
		return warning;
	}

	public ArrayList <String> getListCriteria()
	{
		return listCriter;
	}
	public ArrayList <String> getAllTransforms()
	{
		return listTransform;
	}
	public boolean getUniqueTransformation()
	{
		return  unique_transformation;
	}
	public String getGlobalTransform()
	{
		return global_transfo;
	}

}
