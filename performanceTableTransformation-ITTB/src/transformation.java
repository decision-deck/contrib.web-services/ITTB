import java.io.File;
import java.util.ArrayList;

import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_PerformanceTable;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;


public class transformation
{
	protected float getMaximum(PerformanceTable test, int indexCriter)
	{
		float max=test.getMatrixElement(0,indexCriter);
		for(int i=0;i<test.getAlternativeIDs().size();i++)
		{
			float courant=test.getMatrixElement(i,indexCriter);
			if(courant>max)
				max=courant;
		}
		return max;
	}

	protected float getMinimum(PerformanceTable test, int indexCriter)
	{
		float min=test.getMatrixElement(0,indexCriter);
		for(int i=0;i<test.getAlternativeIDs().size();i++)
		{
			float courant=test.getMatrixElement(i,indexCriter);
			if(courant<min)
				min=courant;
		}
		return min;
	}

	protected float getMean(PerformanceTable test, int indexCriter)
	{
		float mean=0;
		int nbrAltern=test.getAlternativeIDs().size();
		for(int i=0;i<nbrAltern;i++)
			mean+=test.getMatrixElement(i,indexCriter);
		mean/=nbrAltern;
		return mean;
	}

	protected float getVariance(PerformanceTable test, int indexCriter, float mean)
	{
		float var=0;

		for(int i=0;i<test.getAlternativeIDs().size();i++)
		{
			float courant=test.getMatrixElement(i,indexCriter);
			var+=Math.pow((courant-mean),2);
		}
		return var;
	}

	protected float [][] divideByMax(PerformanceTable test, int indexCriter, boolean allTheCriteria,float [][] matrixToUse)
	{
		int nbrAltern=test.getAlternativeIDs().size();
		int nbrCriter=test.getCriterionIDs().size();
		float [][] matricePerf= new float [nbrAltern][nbrCriter];

		if(allTheCriteria)
		{
			for(int j=0;j<nbrCriter;j++)
			{
				float max=getMaximum(test,j);
				for(int i=0;i<nbrAltern;i++)				
					matricePerf[i][j]=test.getMatrixElement(i,j)/max;
			}
		}
		else
		{
			float max=getMaximum(test,indexCriter);
			for(int i=0;i<nbrAltern;i++)	
			{
				matrixToUse[i][indexCriter]=test.getMatrixElement(i,indexCriter)/max;
			}
		}
		if(allTheCriteria)
			return matricePerf;
		else return  matrixToUse;
	}

	protected float [][] minMax(PerformanceTable test,int indexCriter, boolean allTheCriteria,float [][] matrixToUse)
	{
		int nbrAltern=test.getAlternativeIDs().size();
		int nbrCriter=test.getCriterionIDs().size();
		float [][] matricePerf= new float [nbrAltern][nbrCriter];

		if(allTheCriteria)
		{
			for(int j=0;j<nbrCriter;j++)
			{
				float max=getMaximum(test,j);
				float min=getMinimum(test,j);

				for(int i=0;i<nbrAltern;i++)				
					matricePerf[i][j]=(test.getMatrixElement(i,j)-min)/(max-min);
			}
		}
		else
		{
			float max=getMaximum(test,indexCriter);
			float min=getMinimum(test,indexCriter);
			for(int i=0;i<nbrAltern;i++)	
			{
				matrixToUse[i][indexCriter]=(test.getMatrixElement(i,indexCriter)-min)/(max-min);
			}
		}
		if(allTheCriteria)
			return matricePerf;
		else return  matrixToUse;
	}

	protected float [][] meanStdv(PerformanceTable test,int indexCriter, boolean allTheCriteria,float [][] matrixToUse )
	{
		int nbrAltern=test.getAlternativeIDs().size();
		int nbrCriter=test.getCriterionIDs().size();
		float [][] matricePerf= new float [nbrAltern][nbrCriter];

		if(allTheCriteria)
		{
			for(int j=0;j<nbrCriter;j++)
			{
				float mean=getMean(test,j);
				float var=getVariance(test,j, mean);
				float stdv=(float) Math.sqrt(var);
				// Appliquer la loi normale
				for(int i=0;i<nbrAltern;i++)				
					matricePerf[i][j]=(float) ((1/(stdv*Math.sqrt(2*Math.PI)))*
							Math.exp(-(Math.pow((test.getMatrixElement(i,j)-mean),2))/(2*var)));	
			}
		}
		else
		{
			float mean=getMean(test,indexCriter);
			float var=getVariance(test,indexCriter, mean);
			float stdv=(float) Math.sqrt(var);
			// Appliquer la loi normale
			for(int i=0;i<nbrAltern;i++)				
				matrixToUse[i][indexCriter]=(float) ((1/(stdv*Math.sqrt(2*Math.PI)))*
						Math.exp(-(Math.pow((test.getMatrixElement(i,indexCriter)-mean),2))/(2*var)));	
		}

		if(allTheCriteria)
			return matricePerf;
		else return  matrixToUse;
	}

	public PerformanceTable uniqueTransform(PerformanceTable test,String transf_type)
	{
		int nbrAltern=test.getAlternativeIDs().size();
		int nbrCriter=test.getCriterionIDs().size();

		float [][] matricePerf= new float [nbrAltern][nbrCriter];
		for(int i=0;i<nbrAltern;i++)
			for(int j=0;j<nbrCriter;j++)
				matricePerf[i][j]=test.getMatrixElement(i,j);

		if(transf_type.equals("by max"))			
			matricePerf=divideByMax(test,Integer.MIN_VALUE,true,null);
		else if(transf_type.equals("min max"))
			matricePerf=minMax(test,Integer.MIN_VALUE,true,null);
		else if (transf_type.equals("by mean and stdv"))
			matricePerf=meanStdv(test,Integer.MIN_VALUE,true,null);// True =all the criteria have the same transformation
		else{}
		PerformanceTable result=new PerformanceTable(test.getAlternativeIDs(),test.getCriterionIDs(),matricePerf);
		return result;
	}

	public String UniqueTransformWarning(PerformanceTable test,String transf_type)
	{
		String warning="";

		if(transf_type.equals("by max")||transf_type.equals("min max")||transf_type.equals("by mean and stdv"))	{}	
		else
			// Message d'erreur
			warning="You have not entered a specific " +
					"transformation for criteria. No transformation was applied.";
		return warning;
	}

	public PerformanceTable MultipleTransform(PerformanceTable test,String warningMessage,
	                                          ArrayList <String> listCriter,ArrayList <String> listTransform)
	{
		int nbrAltern=test.getAlternativeIDs().size();
		int nbrCriter=test.getCriterionIDs().size();	
		float [][] matricePerf= new float [nbrAltern][nbrCriter];
		for(int i=0;i<nbrAltern;i++)
			for(int j=0;j<nbrCriter;j++)
				matricePerf[i][j]=test.getMatrixElement(i,j);

		for (int j=0; j<nbrCriter;j++)
		{
			if(listCriter.contains(test.getCriterionIDs().get(j)))
			{
				int index=listCriter.indexOf(test.getCriterionIDs().get(j));
				String transformation=listTransform.get(index);
				//System.out.println("Criterion : "+test.getCriterionIDs().get(j)+" Transformation: "+transformation);
				if(transformation.equals("by max"))			
					matricePerf=divideByMax(test,j,false,matricePerf );
				else if(transformation.equals("min max"))
					matricePerf=minMax(test,j,false,matricePerf);
				else if (transformation.equals("by mean and stdv"))
					matricePerf=meanStdv(test,j,false,matricePerf);
				else{}
			}
			else
			{
				// Message d'erreur
				warningMessage=warningMessage.concat("You have not entered a specific " +
						"transformation for criterion "+test.getCriterionIDs().get(j)+". No transformation was applied.");
			}
		}
		PerformanceTable result=new PerformanceTable(test.getAlternativeIDs(),test.getCriterionIDs(),matricePerf);
		return result;
	}

	public String MultipleTransformWarning(PerformanceTable test,
	                                       ArrayList <String> listCriter,ArrayList <String> listTransform)
	{
		String warningMessage="";
		int nbrCriter=test.getCriterionIDs().size();	
		for (int j=0; j<nbrCriter;j++)
		{
			if(listCriter.contains(test.getCriterionIDs().get(j)))
			{
				int index=listCriter.indexOf(test.getCriterionIDs().get(j));
				String transformation=listTransform.get(index);

				if(transformation.equals("by max")||transformation.equals("min max")||transformation.equals("by mean and stdv")){}
				else
				{
					// Message d'erreur
					warningMessage=warningMessage.concat("You have not entered a correct " +
							"transformation for criterion "+test.getCriterionIDs().get(j)+". No transformation was applied.");
				}
			}
			else
			{
				// Message d'erreur
				warningMessage=warningMessage.concat("You have not entered a specific " +
						"transformation for criterion "+test.getCriterionIDs().get(j)+". No transformation was applied.");
			}
		}
		return warningMessage;
	}

	public static void main(String[] argv)
	{
		boolean stopRunning=false;
		String tempErrorMessage="";
		String warningMessage="";

		if(argv.length==6)
			if(!new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists()||!new File(argv[3]).exists())
				stopRunning =true;
			else if(argv.length==7)
				if(!new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists()||!new File(argv[3]).exists()
						||!new File(argv[4]).exists())
					stopRunning =true;
				else
					stopRunning =true;

		if(	stopRunning)		
			tempErrorMessage="Error: unable to run the algorithm. " +
					"Reason: missing one (or more) entry file(s). " +
					"Please check your entry files.";


		if (stopRunning==false)
		{
			Main mainFunction=new Main();	
			PerformanceTable test=new PerformanceTable ();


			//ParseMethodParameters
			String pathParameters=argv[0];
			String pathAlternatives=argv[argv.length-5];
			String pathCriteria=argv[argv.length-4];
			String pathPerfTable=argv[argv.length-3];

			String errParam=mainFunction.checkEntryFile(pathParameters);
			String errAltern=mainFunction.checkEntryFile(pathAlternatives);
			String errCriter=mainFunction.checkEntryFile(pathCriteria);
			String errPerfTable=mainFunction.checkEntryFile(pathPerfTable);	

			if(errParam!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the methodParameters " +
						"tag is not a valid XMCDA document.");

			if(errAltern!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the alternatives " +
						"tag is not a valid XMCDA document.");

			if(errCriter!="")
			{
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteria " +
						"tag is not a valid XMCDA document.");
			}

			if(errPerfTable!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the performanceTable " +
						"tag is not a valid XMCDA document.");

			if(errParam!="" ||errAltern!=""||errCriter!=""||errPerfTable!="")
			{
				//In this case: stopRunning=true, we send the messages.xml output file
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createErrorMessage(tempErrorMessage);			
				String pathOutputMessage=argv[5];
				xmcdaMess.enregistre(pathOutputMessage);
			}
			else
			{
				ParametersPerCriter paramPerCriter=new ParametersPerCriter();
				mainFunction.PrepareParsing (pathParameters);// Le m�me fichier de argv[0]
				boolean statusUserParam=paramPerCriter.parse(mainFunction.getRacine());
				boolean uniqueTransform=paramPerCriter.getUniqueTransformation();
				if(paramPerCriter.getWarning()!="")
				{
					if(warningMessage.equals(""))
						warningMessage= "\n"+paramPerCriter.getWarning();
					else

						warningMessage= warningMessage.concat("\n"+paramPerCriter.getWarning());
				}
				if (!statusUserParam)
				{
					if(tempErrorMessage.equals(""))
					{
						if(paramPerCriter.getWarning()!="")
							tempErrorMessage="\nFatal Error: the file containing the methodParameters " +
									"tag is erroneous or empty ("+paramPerCriter.getWarning()+").";
						else
							tempErrorMessage="\nFatal Error: the file containing the" +
									" methodParameters tag is erroneous or empty.";
					}
					else
					{
						if(paramPerCriter.getWarning()!="")
							tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
									" methodParameters tag is erroneous or empty ("+paramPerCriter.getWarning()+").");
						else
							tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
									" methodParameters tag is erroneous or empty.");
					}
				}
				// Test d'erreur
				JDOM_Criteria jdomCriter= new JDOM_Criteria();
				mainFunction.PrepareParsing (pathCriteria);
				boolean parseCriterNull=false;
				Criteria criter=jdomCriter.parseCriteriaXML(mainFunction.getRacine());

				if(jdomCriter.getWarning()!="")
				{
					if(warningMessage.equals(""))
						warningMessage= "\n"+jdomCriter.getWarning();
					else

						warningMessage= warningMessage.concat("\n"+jdomCriter.getWarning());
				}
				if(criter!=null){}
				else
				{
					parseCriterNull=true;
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the criteria " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" criteria tag is erroneous or empty.");
				}

				// Test d'erreur
				mainFunction.PrepareParsing (pathAlternatives);
				JDOM_Alternatives jdomAlter= new JDOM_Alternatives();
				boolean parseAlternativesNull=false;
				Alternatives alter= jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
				if(jdomAlter.getWarning()!="")
				{
					if(warningMessage.equals(""))
						warningMessage= "\n"+jdomAlter.getWarning();
					else
						warningMessage= warningMessage.concat("\n"+jdomAlter.getWarning());
				}	
				if(alter!=null){}
				else
				{
					parseAlternativesNull=true;
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the alternatives " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" alternatives tag is erroneous or empty.");
				}
				// Test d'erreur
				mainFunction.PrepareParsing (pathPerfTable);
				JDOM_PerformanceTable jdomPerfTable= new JDOM_PerformanceTable();
				boolean parsePerfTableNull=false;
				PerformanceTable perfTable=jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());
				if(jdomPerfTable.getErrorMessage()!="")
					warningMessage= warningMessage.concat("\n"+jdomPerfTable.
					                                      getErrorMessage());

				if(perfTable!=null){}
				else
				{
					parsePerfTableNull=true;
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the performanceTable " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" performanceTable tag is erroneous or empty.");
				}

				if(parseCriterNull==false && parseAlternativesNull==false && parsePerfTableNull==false && statusUserParam==true)
				{
					test=mainFunction.run(test,pathCriteria,pathAlternatives,
					                      pathPerfTable);

					warningMessage=warningMessage.concat(mainFunction.getWarningMessage());
					tempErrorMessage=tempErrorMessage.concat(mainFunction.getErrorMessage());

					transformation transf=new transformation();

					if(test.getAlternativeIDs().size()!=0 
							&& test.getCriterionIDs().size()!=0)
					{
						// D�rouler l'lgorithme
						if(uniqueTransform)
						{
							String transf_type=paramPerCriter.getGlobalTransform();
							warningMessage=warningMessage.concat(transf.UniqueTransformWarning(test,transf_type));
							test= transf.uniqueTransform(test,transf_type);
							//test.printPerfTable_Config2();
						}
						else
							// chaque crit�re a une transformation	
						{
							warningMessage=warningMessage.concat(transf.MultipleTransformWarning
							                                     (test,paramPerCriter.getListCriteria(),paramPerCriter.getAllTransforms()));
							test= transf.MultipleTransform(test,warningMessage,paramPerCriter.getListCriteria(),paramPerCriter.getAllTransforms());
						}
					}
					else
					{
						tempErrorMessage=tempErrorMessage.concat("\n We cannot run the algorithm " +
								"since the criteria list or the alternatives list " +
								"of the PROJECT is empty.\n");
					}
					// Output Log Message File
					XmcdaMessage xmcdaMess= new XmcdaMessage();
					xmcdaMess.createLogMessage(errCriter,errAltern,
					                           errPerfTable,warningMessage,tempErrorMessage);

					String pathOutputMessage=argv[argv.length-1];
					xmcdaMess.enregistre(pathOutputMessage);

					// Output XMCDA File to put the mean values

					if(test.getAlternativeIDs().size()!=0 
							&& test.getCriterionIDs().size()!=0)
					{
						OutputPerfTableFile outputPerfTable= new OutputPerfTableFile();
						outputPerfTable.createOutputFile(test,"Transformed performance table");
						String pathOutputFile=argv[argv.length-2];
						outputPerfTable.save(pathOutputFile);
					}
				}
				else
				{
					// Output Log Message File
					XmcdaMessage xmcdaMess= new XmcdaMessage();
					xmcdaMess.createErrorMessage(tempErrorMessage);
					String pathOutputMessage=argv[argv.length-1];
					xmcdaMess.enregistre(pathOutputMessage);
				}
			}
		}//StopRunning =false
		else
		{
			//StopRunning =true
			// Output Log Message File
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createErrorMessage(tempErrorMessage);
			String pathOutputMessage=argv[argv.length-1];
			xmcdaMess.enregistre(pathOutputMessage);
		}
	}

}
