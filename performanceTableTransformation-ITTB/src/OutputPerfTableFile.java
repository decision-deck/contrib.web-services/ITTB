import java.io.FileOutputStream;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.PerformanceTable;

public class OutputPerfTableFile
{
	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public void save(String fichier)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos=new FileOutputStream(fichier);
			sortie.output(document,  fos);
			fos.close();

		}
		catch (java.io.IOException e){}
	}

	public  void createOutputFile(PerformanceTable 
	                              projectPerfTable,String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		//projectPerfTable.printPerfTable_Config2();
		Element performanceTable = new Element("performanceTable");
		racine.addContent(performanceTable);
		Attribute name =new Attribute("mcdaConcept",commentToPut);
		performanceTable.setAttribute(name);	



		int numberOfCriteria=projectPerfTable.getCriterionIDs().size();
		int numberOfAlternatives=projectPerfTable.getAlternativeIDs().size();
		/*for(int i=0;i<numberOfAlternatives;i++)
			   System.out.println(projectPerfTable.getAlternativeIDs().get(i));
		   for(int j=0;j<numberOfCriteria;j++)
			   System.out.println(projectPerfTable.getCriterionIDs().get(j));*/

		for(int i=0;i<numberOfAlternatives;i++)
		{
			Element alternativePerformances  = new Element("alternativePerformances");
			performanceTable.addContent(alternativePerformances);

			Element alternativeID = new Element("alternativeID");
			alternativePerformances.addContent(alternativeID);
			//System.out.println(projectPerfTable.getAlternativeIDs().get(i));
			alternativeID.setText(projectPerfTable.getAlternativeIDs().get(i));
			for(int j=0;j<numberOfCriteria;j++)
			{
				Element performance = new Element("performance");
				alternativePerformances.addContent(performance);
				Element criterionID = new Element("criterionID");
				performance.addContent(criterionID);
				//System.out.println(projectPerfTable.getCriterionIDs().get(j));
				criterionID.setText(projectPerfTable.
				                    getCriterionIDs().get(j));
				Element value = new Element("value");
				performance.addContent(value);
				Element real = new Element("real");
				value.addContent(real);
				String index=String.valueOf(projectPerfTable.getMatrixElement(i,j));
				real.setText(index);
			}
		}


	}

}
