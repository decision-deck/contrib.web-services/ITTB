import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.imageio.ImageIO;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.CriteriaComparisons;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_Criteria_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;

public class plotCriteriaComp {

	private String warningMessage = "";

	public boolean checkFiles(Criteria criter, CriteriaComparisons criterComp) {
		boolean result = true;
		int diff = 0;
		int size1 = criter.getCriteriaIDs().size();
		int size2 = criterComp.getListCriteriaIDs().size();

		if (size1 == size2) {
			ArrayList<String> list = new ArrayList<String>();
			for (int i = 0; i < size2; i++)
				list.add(criterComp.getListCriteriaIDs().get(i));
			for (String s : criter.getCriteriaIDs()) {
				if (list.contains(s)) {
					/* This is OK */} else {
					result = false;
					diff++;
				}
			}
			if (diff > 1)
				warningMessage = warningMessage.concat(
						"\nPlease check the file containing the criteria tag:" + "incoherence with the criteria given "
								+ "in the file containing the criteriaComparisons tag. There are " + diff
								+ " different criteria.");
			if (diff == 1)
				warningMessage = warningMessage.concat("\nPlease check the file containing the criteria tag:"
						+ "incoherence with the criteria given "
						+ "in the file containing the criteriaComparisons tag. There is one different criterion.");
		} else {
			result = false;
			warningMessage = warningMessage.concat("\nPlease check the file containing the criteria tag:"
					+ "incoherence with the number of criteria given "
					+ "in the file containing the criteriaComparisons tag.");
			warningMessage = warningMessage.concat(
					"\nIn order to run correctly, the program has " + "selected the criteria available in both files.");
		}
		return result;
	}

	public ArrayList<String> getIntersectionCriteriaIDs(Criteria criter, CriteriaComparisons criterComp) {

		ArrayList<String> listintersection = new ArrayList<String>();

		for (String t : criterComp.getListCriteriaIDs()) {
			if (criter.getCriteriaIDs().contains(t)) {
				listintersection.add(t);
			}
		}
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>(new TreeSet<String>(listintersection));
		return listintersection;
	}

	public String convertImage(File image_png, String format) {
		String encodedImage = "";
		try {
			BufferedImage image = ImageIO.read(image_png);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, format, baos);
			encodedImage = Base64.encode(baos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (image_png.delete() == false)
			System.err.println("Warning: " + image_png.getAbsolutePath() + " could not be deleted!");
		return encodedImage;
	}

	public void createGraphiz(ArrayList<String> criterIDs, CriteriaComparisons criterComp, String color,
			boolean valuedGraph, boolean transitiveReduction, String node_shape, File dotFile, File pngFile) {
		eu.telecom_bretagne.utils.GraphViz gv = new eu.telecom_bretagne.utils.GraphViz();
		gv.addln(gv.start_graph(color, node_shape));

		for (int i = 0; i < criterIDs.size(); i++) {
			int index_matrix_line = criterComp.getListCriteriaIDs().indexOf(criterIDs.get(i));

			for (int j = 0; j < criterComp.getListCriteriaIDs().size(); j++) {
				if (criterIDs.contains(criterComp.getListCriteriaIDs().get(j))) {
					if (criterComp.getMatrix()[index_matrix_line][j] != -2000) {
						if (valuedGraph == false)
							gv.addln(criterIDs.get(i) + " -> " + criterComp.getListCriteriaIDs().get(j) + ";");
						else {
							String label = Float.toString(criterComp.getMatrix()[index_matrix_line][j]);
							gv.addln(criterIDs.get(i) + " -> " + criterComp.getListCriteriaIDs().get(j) + " [label="
									+ label + ", fontcolor=" + color + "];");
						}
					}
				}
			}
		}

		for (int i = 0; i < criterIDs.size(); i++)
			gv.addln(criterIDs.get(i) + "[fontcolor=" + color + "]");

		gv.addln(gv.end_graph());

		gv.setTransitiveReduction(transitiveReduction);
		try {
			FileWriter fw = new FileWriter(dotFile);
			fw.write(gv.getDotSource());
			fw.close();
		} catch (java.io.IOException e) {
			e.printStackTrace();
		}
		gv.createImage(dotFile, "png", pngFile);
	}

	protected static String getColorFromOptions(boolean black_and_white, String colorToUse) {
		String color = "";
		if (black_and_white)
			color = "black";
		else
			color = colorToUse.toLowerCase();

		return color;
	}

	// Main function
	public static void main(String[] argv) {

		String errorMessage = "";
		if (argv.length != 6) {
			System.err.println(
					"Usage: criteria.xml criteriaMatrix.xml parameters.xml criteriaMatrix.dot criteriaMatrix.png messages.xml");
			System.err.println("       The 3 first files are inputs, the remaining ones are outputs");
			System.exit(-1);
		}
		if (!new File(argv[0]).exists() || !new File(argv[1]).exists() || !new File(argv[2]).exists()) {
			errorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). Please check your entry files.";

			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.enregistre(argv[5]);
		}

		String pathCriteria = argv[0];
		String pathCriteriaComparisons = argv[1];
		String pathMethodParameters = argv[2];
		String pathOutputDot = argv[3];
		String pathOutputPng = argv[4];
		String pathOutputMessage = argv[5];

		Main mainFunction = new Main();
		String errMethodParameters = mainFunction.checkEntryFile(pathMethodParameters);

		if (errMethodParameters != "")
			errorMessage = errorMessage
					.concat("\nFatal Error: the file containing the parameters " + "is not a valid XMCDA document.");

		String errCriteria = mainFunction.checkEntryFile(pathCriteria);
		if (errCriteria != "")
			errorMessage = errorMessage
					.concat("\nFatal Error: the file containing the criteria " + "tag is not a valid XMCDA document.");

		String errCriteriaComparisons = mainFunction.checkEntryFile(pathCriteriaComparisons);

		if (errCriteriaComparisons != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the criteriaComparisons "
					+ "tag is not a valid XMCDA document.");
		if (errMethodParameters != "" || errCriteria != "" || errCriteriaComparisons != "") {
			// In this case: stopRunning=true, we send the messages.xml output file
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			xmcdaMess.enregistre(pathOutputMessage);
		} else {
			plotCriteriaComp plot = new plotCriteriaComp();

			if (mainFunction.getWarningMessage() != "")
				plot.warningMessage = plot.warningMessage.concat(mainFunction.getWarningMessage());

			// Parsing XML file for Criteria
			mainFunction.PrepareParsing(pathCriteria);
			Criteria criteriaToUse = new Criteria();
			JDOM_Criteria jdomCriter = new JDOM_Criteria();
			boolean parseCriteriaNull = false;
			criteriaToUse = jdomCriter.parseCriteriaXML(mainFunction.getRacine());
			if (criteriaToUse != null) {
				/* This is OK */
			} else {
				parseCriteriaNull = true;
				// Error message
				if (errorMessage.equals(""))
					errorMessage = "\nFatal Error: the file containing the criteria " + "tag is erroneous or empty.";
				else
					errorMessage = errorMessage
							.concat("\nFatal Error: the file containing the" + " criteria tag is erroneous or empty.");
			}

			if (jdomCriter.getWarning() != "")
				plot.warningMessage = plot.warningMessage.concat("\n" + jdomCriter.getWarning());

			// Parsing XML file for CriteriaComparisons
			mainFunction.PrepareParsing(pathCriteriaComparisons);
			CriteriaComparisons criterCompaToUse = new CriteriaComparisons();
			JDOM_Criteria_Comparisons jdomCriterComp = new JDOM_Criteria_Comparisons();
			boolean parseCriterCompNull = false;
			criterCompaToUse = jdomCriterComp.exploreCriteriaComparisonsXML(mainFunction.getRacine());
			if (criterCompaToUse != null) {
				/* This is OK */
			}

			else {
				parseCriterCompNull = true;
				// Error message
				if (errorMessage.equals(""))
					errorMessage = "\nFatal Error: the file containing the criteriaComparisons "
							+ "tag is erroneous or empty.";
				else
					errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
							+ " criteriaComparisons tag is erroneous or empty.");
			}

			if (jdomCriterComp.getWarningMessage() != "")
				plot.warningMessage = plot.warningMessage.concat("\n" + jdomCriter.getWarning());

			// ParseMethodParameters
			MethodParamCriterComp parseMethodParam = new MethodParamCriterComp();
			mainFunction.PrepareParsing(pathMethodParameters);
			boolean statusParseMethod = parseMethodParam.parse(mainFunction.getRacine());

			if (parseMethodParam.getWarning() != "")
				plot.warningMessage = plot.warningMessage.concat(parseMethodParam.getWarning());

			if (!statusParseMethod) {
				if (errorMessage.equals(""))
					errorMessage = "\nFatal Error: the file containing the parameters " + "is erroneous or empty.";
				else
					errorMessage = errorMessage.concat(
							"\nFatal Error: the file containing the" + " methodParameters tag is erroneous or empty.");
			}
			if (parseCriterCompNull == false && parseCriteriaNull == false && statusParseMethod == true) {
				plot.checkFiles(criteriaToUse, criterCompaToUse);
				// Intersection list
				ArrayList<String> listintersectionCriterIDs = plot.getIntersectionCriteriaIDs(criteriaToUse,
						criterCompaToUse);

				// Si l'intersection est vide on prend ce qu'on a dans criteriaComparisons.xml
				if (listintersectionCriterIDs.isEmpty()) {
					plot.warningMessage = plot.warningMessage.concat("\nOnly the criteria IDs "
							+ "in the file containing the criteriaComparisons tag will be considered. "
							+ "Please check your input file containing the criteria tag.");
					for (int i = 0; i < criterCompaToUse.getListCriteriaIDs().size(); i++)
						listintersectionCriterIDs.add(criterCompaToUse.getListCriteriaIDs().get(i));
				}

				if (listintersectionCriterIDs.isEmpty()) {
					// Output Log Message File
					XmcdaMessage xmcdaMess = new XmcdaMessage();
					errorMessage = errorMessage
							.concat("Failure to run the algorithm because it seems that the file containing "
									+ "the criteriaComparisons tag is empty or erroneous.");
					xmcdaMess.createLogMessagePlotCriteriaComparisons(errCriteria, errCriteriaComparisons,
							plot.warningMessage, errorMessage);
					xmcdaMess.enregistre(pathOutputMessage);
				} else {
					// R�cup�rer les param�tres et d�rouler l'agorithme
					boolean color = parseMethodParam.getColor();
					String colorToUse = parseMethodParam.getColorToUse();
					// Merci S�bastien !!!!/////////////////////
					colorToUse = getColorFromOptions(!color, colorToUse);

					boolean valuedGraph = parseMethodParam.getValuedGraph();
					boolean transitiveReduction = parseMethodParam.getTransitiveReduction();

					// Parameter dependency
					if (valuedGraph == true)
						// On ne fait pas de r�duction transitive sur les graphes valu�s
						transitiveReduction = false;

					String node_shape = parseMethodParam.getShape();

					// Output Log Message File
					XmcdaMessage xmcdaMess = new XmcdaMessage();
					xmcdaMess.createLogMessagePlotCriteriaComparisons(errCriteria, errCriteriaComparisons,
							plot.warningMessage, errorMessage);
					xmcdaMess.enregistre(pathOutputMessage);

					plot.createGraphiz(listintersectionCriterIDs, criterCompaToUse, colorToUse, valuedGraph,
							transitiveReduction, node_shape, new File(pathOutputDot), new File(pathOutputPng));
				}
			} else {
				// Output Log Message File
				XmcdaMessage xmcdaMess = new XmcdaMessage();
				xmcdaMess.createErrorMessage(errorMessage);
				xmcdaMess.enregistre(pathOutputMessage);
			}
		}
	}

}
