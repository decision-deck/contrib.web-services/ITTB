﻿================================================================
 The web service « advancedPlotCriteriaComparisons-ITTB »
================================================================

Before running the java program in command line:
===================================================
You need to install Graphiz.
Then, you have to place the executable files dot.exe and tred.exe in your system.

Example:

"C:/Program Files/Graphviz 2.28/bin/dot.exe"
and 
"C:/Program Files/Graphviz 2.28/bin/tred.exe"

How to run the java program in command line?
===================================================
::
java –Djava.awt.headless =true -jar advancedPlotCriteriaComparisons-ITTB.jar methodGraphOptions.xml criteria.xml criteriaComparisons.xml criteriaComparisonsPlot.xml messages.xml
where:

- The first entry file corresponds to ``methodGraphOptions.xml``. There are many options to take into account. The generated graph can be valued or not. It can be transitive. Different shapes for the nodes are proposed (rectangle, square, ellipse, circle or diamond). Colors can be used.
- The second entry file corresponds to ``criteria.xml``
-  The third entry file corresponds to `` criteriaComparisons.xml``
- The first output file corresponds to the `` criteriaComparisonsPlot.xml`` file and contains the graph when it is generated. 
- The second output file corresponds to the ``messages.xml`` file. It is a list of messages generated by the algorithm. It contains information about the execution status as well as a warnings and errors when they occur.

How to run the tests?
=====================

The file ``methodPlotOptions.xml`` can be generated in Diviz. You have to choose the proposed options (valued graph, transitive graph, nodes shape, color) and put the entry files `` criteria.xml`` and `` criteriaComparisons.xml`` in the correct order.
