package eu.telecom_bretagne.xmcda;
import java.util.ArrayList;
import java.util.Iterator;

import org.jdom2.Element;


public class JDOM_AlternativesValues
{

	private String warningMessage;  

	public JDOM_AlternativesValues()
	{
		warningMessage = "";
	}

	public AlternativesValues parseAlternativesValuesXML(Element racine)
	{	
		if(racine.getChild("alternativesValues")==null)
			return null;

		final Element alternaVal = racine.getChild("alternativesValues");

		if(alternaVal.getChildren("alternativeValue")==null)
			return null;

		AlternativesValues alternativesValues = new AlternativesValues();

		Iterator<Element> i = alternaVal.getChildren("alternativeValue").iterator();

		String temp;
		final ArrayList<Element> values = new ArrayList<Element>();
		boolean alternativeIDOK;
		while(i.hasNext())
		{
			Element current_alternativeValue = (Element)i.next();
			temp="";
			values.clear();
			alternativeIDOK=true;

			final String current_alternativeID = current_alternativeValue.getChild("alternativeID").getValue();
			if ( current_alternativeID==null || "".equals(current_alternativeID))
			{
				warningMessage = warningMessage
				        .concat("\nalternativesValues tag: invalid empty alternativeID: ignored.");
				continue;
			}

			if ( current_alternativeValue.getChildren("values") != null )
			{
				for (Element values_elt: current_alternativeValue.getChildren("values"))
					for (Element value_elt: values_elt.getChildren("value")) //per the XMCDA v2 XML Schema, <values> has at least one <value>
						values.add(value_elt);
			}
			if ( current_alternativeValue.getChildren("value") != null )
			{
				for (Element value_elt: current_alternativeValue.getChildren("value"))
					values.add(value_elt);
			}

			if ( values.isEmpty() )
			{
				warningMessage = warningMessage
				        .concat("\nalternativesValues: no value has been found for alternative "
				                + current_alternativeID + ": this alternative is not taken into account.");
				continue;
			}
			if ( values.size() > 1 )
			    warningMessage = warningMessage
			            .concat("\nalternativesValues has more than one value for alternative "
			                    + current_alternativeID + ": only the first real or integer value will be displayed.");

			alternativeIDOK = false;
			// The original behavior was to take the first value only
			find_first_value:
				for ( Element value: values )
				{
					if(value.getChild("real")!=null)
					{
						temp=value.getChild("real").getValue();
						alternativeIDOK = true;
						break find_first_value;
					}
					else if(value.getChild("integer")!=null)
					{
						temp=value.getChild("integer").getValue();
						alternativeIDOK = true;
						break find_first_value;
					}
				}

			if (alternativeIDOK)
			{
				if (alternativesValues.getListAlternativesIDs().contains(current_alternativeID))
				{
					// Envoyer un warning pour dire que le crit�re existe deux fois
					/*warningMessage= warningMessage.concat("\nWarning: There are two identical alternativeID:" +
	        			 courant.
		    			 getChild("alternativeID").getValue()+
	        	 		" in the alternativesValues.xml file");*/
				}
				else
				{
					final Alternative alternative = new Alternative(current_alternativeID);
					alternativesValues.addAlternative(new AlternativeValue(alternative, Float.parseFloat(temp)));
				}
			}
			else
			{
				warningMessage = warningMessage
				        .concat("\nalternativesValues: no real value nor integer value has been found for "+
				                "alternative " + current_alternativeID + ": this alternative is not taken into account.");
			}
		}
		return  alternativesValues;
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

}
