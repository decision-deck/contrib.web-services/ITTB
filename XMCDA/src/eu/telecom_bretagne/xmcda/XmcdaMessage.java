package eu.telecom_bretagne.xmcda;

import java.io.FileOutputStream;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class XmcdaMessage
{

	// Nous allons commencer notre arborescence en cr�ant la racine XML
	// qui sera ici "xmcda:XMCDA".

	// private Namespace XMCDA=Namespace.getNamespace("xmcda");
	private Element           racine   = new Element("XMCDA");

	private Namespace         xsi      = Namespace
			.getNamespace("xsi", "http://www.w3.org/2001/" + "XMLSchema-instance");

	private Namespace         xmcda    = Namespace.getNamespace("xmcda", "http://www.decision-deck.org"
			+ "/2009/XMCDA-2.0.0");

	// private Element racine = new Element("xmcda",xsi);

	// On cr�e un nouveau Document JDOM bas� sur la racine que l'on vient de cr�er
	private org.jdom2.Document document = new Document(racine);

	public void affiche()
	{
		try
		{
			// On utilise ici un affichage classique avec getPrettyFormat()
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e)
		{}
	}

	public void enregistre(String fichier)
	{
		try
		{
			// On utilise ici un affichage classique avec getPrettyFormat()
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			// Remarquez qu'il suffit simplement de cr�er une instance de FileOutputStream
			// avec en argument le nom du fichier pour effectuer la s�rialisation.
			sortie.output(document, new FileOutputStream(fichier));
		}
		catch (java.io.IOException e)
		{}
	}


	public void createLogMessage(String errCriter, String errAltern, String errPerfTable, String warningMessage,
	                             String tempErrorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		if (warningMessage != "")
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}

		if (errCriter != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteria tag:\n" + errCriter);
			errorMessage.addContent(text2);
		}

		if (errPerfTable != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "performanceTable tag:\n" + errPerfTable);
			errorMessage.addContent(text2);
		}
	}

	public void createLogMessageForCriteriaValPlot(String errCriter, String errCriterValues, String warningMessage,
	                                               String tempErrorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		if (warningMessage != "")
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}

		if (errCriter != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteria tag:\n" + errCriter);
			errorMessage.addContent(text2);
		}

		if (errCriterValues != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "alternatives tag:\n" + errCriterValues);
			errorMessage.addContent(text2);
		}
	}

	public XmcdaMessage createLogMessageForAlternativesValPlot(String errAltern, String errAlternValues, String warningMessage,
	                                                   String tempErrorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		if (warningMessage != "")
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}

		if (errAltern != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "alternatives tag:\n" + errAltern);
			errorMessage.addContent(text2);
		}

		if (errAlternValues != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "alternativesValues tag:\n" + errAlternValues);
			errorMessage.addContent(text2);
		}
		return this;
	}

	public void createLogMessagePlotCriteriaFunctions_discrete(String errCriter, String errValueFunctions, String warningMessage,
	                                                           String tempErrorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{

			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		if (warningMessage != "")
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}

		if (errCriter != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteria tag:\n" + errCriter);
			errorMessage.addContent(text2);
		}

		if (errValueFunctions != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteria functions tag:\n" + errValueFunctions);
			errorMessage.addContent(text2);
		}
	}

	public void createLogMessagePlotAlternativesComparisons(String errAlternatives, String errAlternativesComparisons,
	                                                        String warningMessage, String tempErrorMessage)

	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{

			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "ERROR");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		warningMessage = warningMessage != null ? warningMessage.trim() : "";
		if (!"".equals(warningMessage))
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}


		errAlternatives = errAlternatives != null ? errAlternatives.trim() : "";
		if (!"".equals(errAlternatives))
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "alternatives tag:\n" + errAlternatives);
			errorMessage.addContent(text2);
		}

		errAlternativesComparisons = errAlternativesComparisons != null ? errAlternativesComparisons.trim() : "";
		if (errAlternativesComparisons != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "alternativesComparisons tag:\n"
					+ errAlternativesComparisons);
			errorMessage.addContent(text2);
		}
	}

	public XmcdaMessage createLogMessagePlotCriteriaComparisons(String errCriter, String errCriteriaComparisons,
	                                                            String warningMessage, String tempErrorMessage)

	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		// On cr�e un nouvel Element methodMessages et on l'ajoute
		// en tant qu'Element de racine
		Element logMessage = new Element("logMessage");
		methodMessages.addContent(logMessage);

		// On cr�e un nouvel Attribut name et on l'ajoute � methodMessages
		// gr�ce � la m�thode setAttribute
		Attribute executionStatus = new Attribute("name", "executionStatus");
		logMessage.setAttribute(executionStatus);

		// On cr�e un nouvel Element text, on lui assigne OK
		// et on l'ajoute en tant qu'Element de logMessage
		Element text = new Element("text");
		text.setText("OK");
		logMessage.addContent(text);

		if (tempErrorMessage != "")
		{

			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "ERROR");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(tempErrorMessage);
			fatalMess.addContent(text2);
		}

		if (warningMessage != "")
		{
			Element warningMess = new Element("logMessage");
			methodMessages.addContent(warningMess);
			Attribute error = new Attribute("name", "warning");
			warningMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(warningMessage);
			// text2.setText("Attention!!!");
			warningMess.addContent(text2);
		}

		if (errCriter != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteria tag:\n" + errCriter);
			errorMessage.addContent(text2);
		}

		if (errCriteriaComparisons != "")
		{
			Element errorMessage = new Element("errorMessage");
			methodMessages.addContent(errorMessage);
			Attribute error = new Attribute("name", "ERROR");
			errorMessage.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText("Error in the file containing the " + "criteriaComparisons tag:\n" + errCriteriaComparisons);
			errorMessage.addContent(text2);
		}
		return this;
	}

	public XmcdaMessage createErrorMessage(String errorMessage)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);
		racine.setAttribute("schemaLocation", "http://www.decision-deck.org/2009/XMCDA-2.0.0 "
				+ "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd", xsi);

		Element methodMessages = new Element("methodMessages");
		racine.addContent(methodMessages);

		Element logMessage = new Element("errorMessage");
		methodMessages.addContent(logMessage);

		Attribute executionStatus = new Attribute("name", "execution status");
		logMessage.setAttribute(executionStatus);

		Element text = new Element("text");
		text.setText("Execution failed.");
		logMessage.addContent(text);
		if (errorMessage != "")
		{
			Element fatalMess = new Element("errorMessage");
			methodMessages.addContent(fatalMess);
			Attribute error = new Attribute("name", "Error");
			fatalMess.setAttribute(error);
			Element text2 = new Element("text");
			text2.setText(errorMessage);
			fatalMess.addContent(text2);
		}
		return this;
	}

	static public void writeErrorMessage(String errorMessage, String filepath)
	{
		XmcdaMessage message = new XmcdaMessage();
		message.createErrorMessage(errorMessage);			
		message.enregistre(filepath);
	}

}
