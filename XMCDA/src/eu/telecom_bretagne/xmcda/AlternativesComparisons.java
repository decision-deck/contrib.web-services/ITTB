package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;
import java.util.List;


public class AlternativesComparisons extends Matrix<Alternative, Alternative, Float>
{
	public AlternativesComparisons()
	{
		super();
	}

	public AlternativesComparisons(AlternativesComparisons alternativeComparisons)
	{
		super(alternativeComparisons);
	}

	public void addAlternative(Alternative alternative, boolean ignoreDups)
	{
		addRow(alternative, ignoreDups);
		addColumn(alternative, ignoreDups);
	}

	public List<Cell<Alternative, Alternative, Float>> removeAlternative(Alternative alternative)
	{
		ArrayList<Cell<Alternative, Alternative, Float>> removedCells = new ArrayList<Matrix.Cell<Alternative,Alternative,Float>>();
		removedCells.addAll(removeRow(alternative));
		removedCells.addAll(removeColumn(alternative));
		return removedCells;
	}

	public boolean hasID(String id)
	{
		for (Alternative alternative: rows())
			if ( alternative.id().equals(id) )
				return true;
		return false;
	}
}
