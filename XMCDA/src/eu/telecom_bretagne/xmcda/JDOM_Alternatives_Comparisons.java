package eu.telecom_bretagne.xmcda;

import java.util.Iterator;

import org.jdom2.Element;


public class JDOM_Alternatives_Comparisons
{

	private AlternativesComparisons alternCompToTest = new AlternativesComparisons();

	private StringBuffer warningMessage = new StringBuffer();

	public  JDOM_Alternatives_Comparisons()
	{
	}

	public String getWarningMessage()
	{
		return warningMessage.toString();
	}

	public AlternativesComparisons exploreAlternativesComparisonsXML(Element racine, Alternatives alternatives)
	{
		return exploreAlternativesComparisonsXML(racine, alternatives, false);
	}

	public AlternativesComparisons exploreAlternativesComparisonsXML(final Element racine, final Alternatives alternatives, final boolean valued)
	{
		boolean error = false;
		boolean isValuedOk = true;
		boolean hasValuedCells = false, hasNonValuedCells = false;

		isValuedOk = true;

		if (racine.getChild("alternativesComparisons")==null)
			return null;

		Element alternComp = racine.getChild("alternativesComparisons");
		if(alternComp.getChild("pairs")==null)
			return null;

		Element pairs=alternComp.getChild("pairs");
		Iterator<Element> i = pairs.getChildren("pair").iterator();
		while(i.hasNext())
		{
			Element courant = (Element)i.next();
			if (courant.getChild("initial") == null || courant.getChild("terminal") == null
					|| courant.getChild("initial").getChild("alternativeID") == null
					|| courant.getChild("terminal").getChild("alternativeID") == null)
			{
				// Ignore the relation
				continue;
			}
			final String initialID = courant.getChild("initial").getChild("alternativeID").getValue();
			final String terminalID = courant.getChild("terminal").getChild("alternativeID").getValue();

			Alternative initial  = alternatives!=null ? alternatives.get(initialID) : null;
			Alternative terminal = alternatives!=null ? alternatives.get(terminalID) : null;
			if (initial==null)
			{
				initial = new Alternative(initialID);
				if (!alternCompToTest.hasID(initialID))
				{
					warningMessage = warningMessage.append("\nWarning: alternativesComparisons tag: alternative ID ")
							.append(initialID)
							.append(" in the 'initial' tag does not correspond to any declared alternative");
				}
			}
			if (terminal==null)
			{
				terminal = new Alternative(terminalID);
				if (!alternCompToTest.hasID(terminalID))
				{
					warningMessage = warningMessage.append("\nWarning: alternativesComparisons tag: alternative ID ")
							.append(terminalID)
							.append(" in the 'terminal' tag does not correspond to any declared alternative");
				}
			}

			if (!initial.isActive() || !terminal.isActive())
				continue;
			alternCompToTest.addAlternative(initial, true);
			alternCompToTest.addAlternative(terminal, true);

			// Update the matrix

			/*
			 * Treat NA the same as when there is no value: this is especially useful when
			 * XMCDA v3 is translated into v2 before being supplied to us, because XMCDA v3
			 * requires that there is at least a value for each cell in an
			 * alternativesMatrix.
			 */
			if (courant.getChild("value")==null || courant.getChild("value").getChild("NA") != null)
			{
				if (valued && isValuedOk)
				{
					hasNonValuedCells = true;
					if ( hasValuedCells )
						isValuedOk = false;
				}
				alternCompToTest.setValue(initial, terminal, 1f);
			}
			else if (courant.getChild("value")!=null 
					&& courant.getChild("value").getChild("real")!=null)
			{
				if (valued && isValuedOk)
				{
					hasValuedCells = true;
					if ( hasNonValuedCells )
						isValuedOk = false;
				}
				if(alternCompToTest.isSet(initial, terminal))
				{
					warningMessage = warningMessage
							.append("\nWarning: alternativesComparisons tag: more than one value for the pair")
							.append(" alternativeID (").append(initialID).append(") in the 'initial' tag")
							.append(" and alternativeID (").append(terminalID).append(") in the 'terminal' tag.");
					error = true;
					continue;
				}

				Float value = null;
				if (courant.getChild("value").getChild("real")!=null)
				{
					try
					{
						value = Float.parseFloat(courant.getChild("value").getChild("real").getValue());
					}
					catch (NumberFormatException e)
					{
						// ignore: value is null
					}
				}
				if (value == null)
				{
					warningMessage = warningMessage
							.append("\nError in the file containing the alternativesComparisons tag.")
							.append(" The value tag must be a valid real number within a <real> tag. Please check ")
							.append("this value for the pair consisting of alternativeID (").append(initialID)
							.append(") in the 'initial' tag").append(" and the alternativeID (").append(terminalID)
							.append(") in the 'terminal' tag.");
					error = true;
					continue;
				}
				alternCompToTest.setValue(initial,terminal, value);
			}
		}

		if ( valued && !isValuedOk )
		{
			error = true;
			warningMessage = warningMessage.append("\nError in the file containing the alternativesComparisons tag. The graph must be valued or non valued.");
		}
		if ( error )
			return null;

		return alternCompToTest;
	}

}
