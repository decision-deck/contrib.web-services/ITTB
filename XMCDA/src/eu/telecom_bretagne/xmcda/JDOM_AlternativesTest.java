package eu.telecom_bretagne.xmcda;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class JDOM_AlternativesTest
{
	private JDOM_Alternatives jdomAlternatives;

	@Before
	public void setUp() throws Exception {
		jdomAlternatives=new JDOM_Alternatives();
	}

	@After
	public void tearDown() throws Exception {
		jdomAlternatives=null;
	}

	@Test
	public void testJDOM_Alternatives() {
		assertNotNull("l'instance est cr��e", jdomAlternatives);
	}

	@Test
	public void testSetPath() {
		jdomAlternatives.setPath("data/ACUTA-in1-2/alternatives.xml");
		assertEquals("Is it OK for the path?", "data/ACUTA-in1-2/alternatives.xml",
				jdomAlternatives.getPath());
	}

	@Test
	public void testExploreAlternativesXML() {
		//fail("Not yet implemented");
		assertEquals("Is it OK for the parsing?",
				jdomAlternatives.exploreAlternativesXML(jdomAlternatives.getRacine
						("data/ACUTA-in1-2/alternatives.xml")),jdomAlternatives.getAlternatives());
	}
}
