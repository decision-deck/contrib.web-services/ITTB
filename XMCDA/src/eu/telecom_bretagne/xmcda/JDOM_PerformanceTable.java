package eu.telecom_bretagne.xmcda;

import java.io.File;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class JDOM_PerformanceTable
{
	static org.jdom2.Document  document;

	PerformanceTable perfTable = new PerformanceTable();

	// Error message for the performance Table type
	private String            errorMessage = "";

	private String            warningMessage = "";

	public Element getRacine(String path)
	{
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch (Exception e)
		{}

		return document.getRootElement();
	}

	public PerformanceTable explorePerformanceTable(Element racine)
	{
		if (racine.getChild("performanceTable") == null)
			return null;

		final Element perfTable_xml = racine.getChild("performanceTable");

		if (perfTable_xml.getChildren("alternativePerformances") == null)
			return null;

		String value;
		for ( Element courant: perfTable_xml.getChildren("alternativePerformances") )
		{
			Alternative alternative = new Alternative(courant.getChild("alternativeID").getValue());
			perfTable.addAlternative(alternative, true);
/*
			index_matrix_line = listAlternativeIDInperformanceTable.indexOf(courant.getChild("alternativeID")
			                                                                .getValue());
			tempAlternativePerformances = new AlternativePerformances(courant.getChild("alternativeID")
			                                                          .getValue());

			Iterator<Element> j = courant.getChildren("performance").iterator();
*/
			boolean alternativeIDOK = true;

			for ( Element performance: courant.getChildren("performance") )
			{
				value = "";
				if (performance.getChild("value").getChild("real") != null)
				{
					value = performance.getChild("value").getChild("real").getValue();
				}
				else if (performance.getChild("value").getChild("integer") != null)
				{
					value = performance.getChild("value").getChild("integer").getValue();
				}
				else
				{
					alternativeIDOK = false;
				}
				Criterion criterion = new Criterion(performance.getChild("criterionID").getValue());

				perfTable.addCriterion(criterion, true);
				value = value.trim();
				if ( value.length() != 0 )
					perfTable.setValue(alternative, criterion, Float.parseFloat(value));
			}

			if ( ! alternativeIDOK )
			{
				perfTable.removeAlternative(alternative);
				errorMessage = errorMessage.concat("Error in the file containing the performance "
						+ "table tag. The value is neither real nor integer."
						+ "We have eliminated the corresponding alternative:"
						+ alternative.id() + ".");
			}
		}

		// V�rifier que toutes les alternatives
		// retenues ont le m�me nombre de crit�res
		// TODO la fa�on propre de faire serait de mettre des valeurs N/A l� o� n'y a pas de valeurs
		
		for ( Alternative alternative: perfTable.alternatives())
		{
			for ( Criterion criterion: perfTable.criteria())
			{
				if ( ! perfTable.isSet(alternative, criterion) )
				{
					perfTable.removeAlternative(alternative);
					warningMessage = warningMessage.concat("\nThe alternativeID " + alternative.id() + " was removed because there are missing values");
				}
			}
		}
		return perfTable;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

}
