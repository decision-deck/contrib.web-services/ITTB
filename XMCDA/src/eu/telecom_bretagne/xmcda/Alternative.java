package eu.telecom_bretagne.xmcda;

import org.jdom2.Element;

public class Alternative
{
	final private String id;

	private String       name;

	private boolean      isActive    = true;

	private boolean      isReference = false;

	public Alternative(String id, boolean active)
	{
		this.id = id;
		this.isActive = active;
	}

	public Alternative(String id)
	{
		this.id = id;
		this.name = null;
	}

	public Alternative(String id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public String id()
	{
		return id;
	}

	public String name()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public boolean isActive()
	{
		return isActive;
	}

	public void setActive(boolean active)
	{
		this.isActive = active;
	}

	public boolean isReference()
	{
		return isReference;
	}

	public void setReference(boolean reference)
	{
		this.isReference = reference;
	}

	public static Alternative fromXML(Element xml)
	{
		final String id = xml.getAttributeValue("id");
		if (id == null) return null;
		Alternative alternative = new Alternative(xml.getAttributeValue("id"));
		alternative.setName(xml.getAttributeValue("name"));

		final String active = xml.getChildText("active");
		if ("false".equals(active) || "0".equals(active))
			alternative.setActive(false);

		final String reference = xml.getAttributeValue("reference");
		if ("true".equals(reference) || "1".equals(reference))
			alternative.setReference(true);
		return alternative;
	}

	/**
	 * Two alternatives are equal if they have the same id
	 */
	@Override
	public boolean equals(Object obj)
	{
		if ( obj==null || ! ( obj instanceof Alternative ) )
			return false;
		final Alternative alt = (Alternative) obj;
		return this.id.equals(alt.id);
	}

	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}

	@Override
	public String toString()
	{
		return id;
	}
}
