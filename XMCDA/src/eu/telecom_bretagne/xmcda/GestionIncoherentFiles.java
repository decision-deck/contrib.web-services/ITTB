package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;


public class GestionIncoherentFiles
{
	private Criteria         criter;

	private Alternatives     altern;

	private PerformanceTable perfTable;

	private String           warningMessageCriter;

	private String           warningMessageAltern;

	public GestionIncoherentFiles()
	{
	}

	public GestionIncoherentFiles(Criteria criter, Alternatives altern, PerformanceTable perfTable)
	{
		this.criter = criter;
		this.altern = altern;
		this.perfTable = perfTable;
		warningMessageCriter = "";
		warningMessageAltern = "";
	}

	public boolean checkAlternatives(Alternatives altern, PerformanceTable perfTable)
	{
		boolean result = true;// les listes sont coh�rentes
		int diff = 0, inactive = 0;
		if (altern.getNumberOfActiveAlternatives() != perfTable.nbRows())
		{
			warningMessageAltern = warningMessageAltern
					.concat("\nPlease check the file containing the alternatives tag:"
							+ "incoherence with the number of alternatives given "
							+ "in the file containing the performanceTable tag.");
			return false;
		}

		for (Alternative a: altern)
		{
			if ( ! perfTable.rows().contains(a) )
			{
				// On a un �l�ment diff�rent
				result = false;
				diff++;
			}
			else if ( ! a.isActive() )
			{
				result = false;
				inactive ++;warningMessageAltern = warningMessageAltern.concat("meuh");  // TODO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			}
		}
		if (diff > 1)
			warningMessageAltern = warningMessageAltern
			.concat("\nPlease check the file containing the alternatives tag:"
					+ "incoherence with the alternatives given "
					+ "in the file containing the performanceTable tag. There are " + diff
					+ " different alternatives.");

		if (diff == 1)
			warningMessageAltern = warningMessageAltern
			.concat("\nPlease check the file containing the alternatives tag:"
					+ "incoherence with the alternatives given "
					+ "in the file containing the performanceTable tag. There is one different alternative.");
		return result;
	}

	public boolean checkCriteria(Criteria criter, PerformanceTable perfTable)
	{
		boolean result = true;// les listes sont coh�rentes
		int diff = 0;
		if (criter.size() != perfTable.nbColumns())
		{
			warningMessageCriter = warningMessageCriter.concat("\nPlease check the file containing the criteria tag:"
					+ "incoherence with the number of criteria given "
					+ "in the file containing the performanceTable tag.");
			return false;
		}
		for (Criterion criterion: criter)
		{
			if ( ! perfTable.hasColumn(criterion))
			{
				// On a un �l�ment diff�rent
				result = false;
				diff++;
			}
		}

		if (diff > 1)
			warningMessageCriter = warningMessageCriter
			.concat("\nPlease check the file containing the criteria tag:"
					+ "incoherence with the criteria given "
					+ "in the file containing the performanceTable tag. There are " + diff
					+ " different criteria.");
		if (diff == 1)
			warningMessageCriter = warningMessageCriter
			.concat("\nPlease check the file containing the criteria tag:"
					+ "incoherence with the criteria given "
					+ "in the file containing the performanceTable tag. There is one different criterion.");

		return result;
	}

	public String getWarningCriteria()
	{
		return warningMessageCriter;
	}

	public String getWarningAlternatives()
	{
		return warningMessageAltern;
	}

}
