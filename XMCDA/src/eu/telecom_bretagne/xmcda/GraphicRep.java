package eu.telecom_bretagne.xmcda;

import java.util.Iterator;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.*;


public class GraphicRep
{
	public void representAlternativesValues(Alternatives alternativesToUse)
	{
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(2000, 2000);
		Canvas canvas = new Canvas(shell, SWT.NONE);
		canvas.setSize(200, 2000);
		canvas.setLocation(10, 10);
		shell.pack();
		shell.open();
		GC gc = new GC(canvas);

		// gc.setForeground(display.getSystemColor(SWT.COLOR_RED));
		gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
		gc.setLineWidth(2);

		int stepRect = 0;
		int stepLine = 0;

		int rectWidth = 50;
		int rectHeight = 40;

		int rectInitialx = 40;
		int rectInitialy = 40;

		int lineInitialx = rectInitialx + ( rectWidth / 2 );
		int lineInitialy = rectInitialx + rectInitialy;

		// int intialLinelength=rectInitialy+rectHeight+50;// y1=y+h
		int intialLinelength = rectInitialy + rectHeight + 50;// y1=y+h

		// d�calage dans l'abscisse afin de dessiner une fl�che
		int delta = 7;
		// int deltaArrowy=10;

		// Font font = new Font(display,"Arial",14,SWT.BOLD | SWT.ITALIC);
		Font font = new Font(display, "Arial", 14, SWT.BOLD);
		gc.setFont(font);

		Iterator<Alternative> alternative = alternativesToUse.iterator();
		while ( alternative.hasNext() )
		{
			Rectangle rect = new Rectangle(rectInitialx, rectInitialy + stepRect, rectWidth, rectHeight);

			gc.drawRectangle(rect);

			// On va mettre l'ID de chaque alternative
			gc.drawText(alternative.next().id(), 50, 50 + stepRect);

			if (alternative.hasNext()) // all except the last one
			{
				gc.drawLine(lineInitialx, lineInitialy + stepLine, lineInitialx, intialLinelength + stepLine);
				gc.drawLine(lineInitialx - delta, 110 + stepLine, lineInitialx, intialLinelength + stepLine);
				gc.drawLine(lineInitialx + delta, 110 + stepLine, lineInitialx, intialLinelength + stepLine);
			}

			stepRect += 90;
			stepLine += 90;
		}

		font.dispose();
		gc.dispose();

		while ( !shell.isDisposed() )
		{
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

}
