package eu.telecom_bretagne.xmcda;

import java.io.IOException;
import java.net.URL;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class MyValid
{

	public static final String XMCDA_2_0_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.0.0";

	public static final String XMCDA_2_0_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd";

	public static final String XMCDA_2_1_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.1.0";

	public static final String XMCDA_2_1_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd";

	public static final String XMCDA_2_2_0_NAMESPACE     = "http://www.decision-deck.org/2012/XMCDA-2.2.0";

	public static final String XMCDA_2_2_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd";

	public static final String XMCDA_2_2_1_NAMESPACE     = "http://www.decision-deck.org/2012/XMCDA-2.2.1";

	public static final String XMCDA_2_2_1_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd";

	public static final String XMCDA_2_2_2_NAMESPACE     = "http://www.decision-deck.org/2016/XMCDA-2.2.2";

	public static final String XMCDA_2_2_2_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.2.xsd";

	public static final String XMCDA_2_2_3_NAMESPACE     = "http://www.decision-deck.org/2019/XMCDA-2.2.3";

	public static final String XMCDA_2_2_3_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.3.xsd";

	public static final String ACCEPTED_SCHEMA_LOCATIONS;

	static
	{
		URL XMCDA2_0_0_url = getLocalXSD("xsd/XMCDA-2.0.0.xsd"),
				XMCDA2_1_0_url = getLocalXSD("xsd/XMCDA-2.1.0.xsd"),
				XMCDA2_2_0_url = getLocalXSD("xsd/XMCDA-2.2.0.xsd"),
				XMCDA2_2_1_url = getLocalXSD("xsd/XMCDA-2.2.1.xsd"),
				XMCDA2_2_2_url = getLocalXSD("xsd/XMCDA-2.2.2.xsd"),
				XMCDA2_2_3_url = getLocalXSD("xsd/XMCDA-2.2.3.xsd");
		// Use the local XSD if available, or the online version of them.
		ACCEPTED_SCHEMA_LOCATIONS = XMCDA_2_0_0_NAMESPACE + " "
				+ ( ( XMCDA2_0_0_url == null ) ? XMCDA_2_0_0_SCHEMA : XMCDA2_0_0_url.toString() )
				+ " " + XMCDA_2_1_0_NAMESPACE + " "
				+ ( ( XMCDA2_1_0_url == null ) ? XMCDA_2_1_0_SCHEMA : XMCDA2_1_0_url.toString() )
				+ " " + XMCDA_2_2_0_NAMESPACE + " "
				+ ( ( XMCDA2_2_0_url == null ) ? XMCDA_2_2_0_SCHEMA : XMCDA2_2_0_url.toString() )
				+ " " + XMCDA_2_2_1_NAMESPACE + " "
				+ ( ( XMCDA2_2_1_url == null ) ? XMCDA_2_2_1_SCHEMA : XMCDA2_2_1_url.toString() )
				+ " " + XMCDA_2_2_2_NAMESPACE + " "
				+ ( ( XMCDA2_2_2_url == null ) ? XMCDA_2_2_2_SCHEMA : XMCDA2_2_2_url.toString() )
				+ " " + XMCDA_2_2_3_NAMESPACE + " "
				+ ( ( XMCDA2_2_3_url == null ) ? XMCDA_2_2_3_SCHEMA : XMCDA2_2_3_url.toString() );
	}

	/**
	 * Searches a file within the local resources. The method catches every throwable that may be raised and simply
	 * prints a warning if this happens, along with a stack trace.
	 * 
	 * @param xsdFile the file to search
	 * @return the URL of the file, or {@code null} if it cannot be found
	 */
	static private URL getLocalXSD(String xsdFile)
	{
		URL url = null;
		try
		{
			url = MyValid.class.getClassLoader().getResource(xsdFile);
		}
		catch (Throwable t)
		{
			/* simply ignore */
			new Throwable("Could not find local XML Schema for " + xsdFile
			              + ": we will use the one available online. This is not an error", t).printStackTrace();
		}
		return url;
	}

	public void parseXML(final String xml) throws JDOMException, IOException
	{
		SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
		builder.setFeature("http://apache.org/xml/features/validation/schema", true);
		// Accepted schemaLocations are the following
		// Ref. http://www.w3.org/TR/xmlschema-0/#schemaLocation
		// http://xerces.apache.org/xerces2-j/properties.html#schema.external-schemaLocation
		builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
		                    ACCEPTED_SCHEMA_LOCATIONS);

		@SuppressWarnings("unused")
		Document doc = builder.build(xml);
		// The namespace URI for the document is: doc.getRootElement().getNamespaceURI();
	}
}
