package eu.telecom_bretagne.xmcda;

import java.util.Iterator;

import org.jdom2.Element;


public class JDOM_CriteriaValues {

	private String warningMessage;

	public JDOM_CriteriaValues()
	{
		warningMessage="";
	}

	public CriteriaValues parseCriteriaValuesXML(Element racine)
	{	
		if (racine.getChild("criteriaValues")==null)
			return null;

		Element critVal = racine.getChild("criteriaValues");
		if(critVal.getChildren("criterionValue")==null)
			return null;

		final CriteriaValues criteriaValues = new CriteriaValues();
		Iterator<Element> i = critVal.getChildren("criterionValue").iterator();

		String temp;
		boolean criterionIDOK; 
		while(i.hasNext())
		{
			Element courant = (Element)i.next();
			temp="";
			criterionIDOK=false;
			if ( courant.getChild("value") == null )
				criterionIDOK=false;
			else if (courant.getChild("value").getChild("real")!=null )
			{
				temp=courant.getChild("value").
						getChild("real").getValue();
				criterionIDOK=true;
			}
			else if (courant.getChild("value").getChild("integer")!=null )
			{
				temp=courant.getChild("value").
						getChild("integer").getValue();
				criterionIDOK=true;
			}

			if(criterionIDOK)
			{
				if ( criteriaValues.getCriteriaIDs().contains(courant.getChild("criterionID").getValue())){
					// Envoyer un warning pour dire que le crit�re existe deux fois
					/* warningMessage= warningMessage.concat("\nWarning: There are two identical criterionID:" +
	        			 courant.
		    			 getChild("criterionID").getValue()+
	        	 		" in the file containing the criteriaValues tag.");*/
				}
				else
					criteriaValues.add(new CriterionValue
					                   (new Criterion(courant.getChild("criterionID").getValue()), // TODO check qu'au retour on fait bien le match avec des criteria existants
					                    Float.parseFloat(temp)));
			}
			else
			{
				warningMessage= warningMessage.concat("\n Error in the file containing the criteriaValues tag." +
						" The value is neither real nor integer."+
						" We have eliminated the corresponding criterion:"+
						courant.getChild("criterionID").getValue()+".");
			}
		}

		if(criteriaValues.getCriteriaIDs().size()==0)
			return null;
		else
			return  criteriaValues;
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

}
