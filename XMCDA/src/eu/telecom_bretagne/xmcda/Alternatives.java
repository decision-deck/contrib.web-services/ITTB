package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author Sébastien Bigaret
 *
 */
public class Alternatives extends ArrayList<Alternative> implements Iterable<Alternative>
{
	public Alternatives() { }

	public Alternative get(String id)
	{
		for (Alternative alternative: this)
			if (alternative.id().equals(id))
				return alternative;
		return null;
	}

	public List<String> getIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		for (Alternative alternative: this)
			ids.add(alternative.id());
		return ids;
	}

	public List<Alternative> getListActiveAlternatives()
	{
		ArrayList<Alternative> activeAlternatives = new ArrayList<Alternative>(this);
		for (Alternative alternative: this)
			if ( ! alternative.isActive() )
				activeAlternatives.remove(alternative);
		return Collections.unmodifiableList(activeAlternatives);
	}

	public ArrayList<String> getNames()
	{
		ArrayList<String> names = new ArrayList<String>();
		for (Alternative alternative: this)
			names.add(alternative.name());
		return names;
	}

	public int getNumberOfActiveAlternatives()
	{
		int nb_active = 0;
		for (Alternative alternative: this)
			if ( alternative.isActive() )
				nb_active++;
		return nb_active;

	}

	public void printAlternatives()
	{
		System.out.println("This are the alternatives:");
		for (int i = 0; i < this.size(); i++)
		{
			System.out.println("Alternative" + ( i + 1 ) + ":\t" + "id = " + this.get(i).id()
			                   + "\t : " + this.get(i).name());
		}
	}

}
