package eu.telecom_bretagne.xmcda;

import java.util.List;
import java.util.stream.Collectors;


public class PerformanceTable extends Matrix<Alternative, Criterion, Float>
{
	public PerformanceTable()
	{
		super();
	}

	public PerformanceTable(PerformanceTable performanceTable)
	{
		super(performanceTable);
	}

	public void addCriterion(Criterion criterion, boolean ignoreDups)
	{
		super.addColumn(criterion, ignoreDups);
	}

	public void addAlternative(Alternative alternative, boolean ignoreDups)
	{
		super.addRow(alternative, ignoreDups);
	}

	public List<eu.telecom_bretagne.xmcda.Matrix.Cell<Alternative, Criterion, Float>> removeAlternative(Alternative alternative)
	{
		return super.removeRow(alternative);
	}

	public List<eu.telecom_bretagne.xmcda.Matrix.Cell<Alternative, Criterion, Float>> removeCriterion(Criterion criterion)
	{
		return super.removeColumn(criterion);
	}

	public List<Alternative> alternatives()
	{
		return rows();
	}

	public List<String> alternativesIDs()
	{
		return alternatives().stream().map(a->a.id()).collect(Collectors.toList());
	}
	
	public List<Criterion> criteria()
	{
		return columns();
	}

	public List<String> criteriaIDs()
	{
		return criteria().stream().map(a->a.id()).collect(Collectors.toList());
	}
	
	public int nbAlternatives()
	{
		return nbRows();
	}

	public int nbCriteria()
	{
		return nbColumns();
	}

	public void filterBy(Alternatives ref_alternatives)
	{
		for (Alternative a: alternatives() )
			if ( ! ref_alternatives.contains(a) || (ref_alternatives.contains(a) && ! ref_alternatives.get(a.id()).isActive() ) )
				this.removeAlternative(a);
	}

	public void filterBy(Criteria criteria)
	{
		for (Criterion c: criteria())
			if ( ! criteria.contains(c) )
				this.removeCriterion(c);
	}

}
