package eu.telecom_bretagne.xmcda;


public class AlternativeValue
{
	private Alternative alternative;

	private float  floatValue;

	public AlternativeValue(Alternative alternative, float alternVal)
	{
		this.alternative = alternative;
		this.floatValue = alternVal;
	}

	public Alternative getAlternative()
	{
		return alternative;
	}

	public String getAlternativeValueId()
	{
		return alternative.id();
	}

	public float getRealValue()
	{
		return floatValue;
	}

	public void setRealValue(float floatAlternVal)
	{
		this.floatValue = floatAlternVal;
	}

}
