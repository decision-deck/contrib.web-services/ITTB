package eu.telecom_bretagne.xmcda;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Value {

	enum type { INTEGER,REAL,INTERVAL,RATIONAL,LABEL };
	int v_int;
	float v_float;

	///Faire les m�thodes de cette classe plus tard avec plus de concentration
	public Object getValue() {
		Object result=null;
		type typeval=type.INTEGER;

		switch(typeval)
		{
			case INTEGER :
				result = getInteger();
				break;
			case REAL :
				result = getReal();
				break;
			case INTERVAL :
				// TO DO
				break;
			case RATIONAL :
				// TO DO
				break;
			case LABEL :
				// TO DO
				break;
		}
		return result;
	}

	public int getInteger()
	{
		try{
			BufferedReader fromDecider = 
					new BufferedReader(new InputStreamReader(System.in));
			System.out.println ("Please enter an integer:\n");
			v_int = Integer.parseInt(fromDecider.readLine());
		}
		catch(Exception e){
			System.err.print("Error : " + e.getMessage() );
		}
		return v_int;
	}

	public float getReal()
	{
		try{
			BufferedReader fromDecider = 
					new BufferedReader(new InputStreamReader(System.in));
			System.out.println ("Please enter a real:\n");
			v_float = (float)Double.parseDouble(fromDecider.readLine());
		}
		catch(Exception e){
			System.err.print("Error : " + e.getMessage() );
		}
		return  v_float;
	}

}
