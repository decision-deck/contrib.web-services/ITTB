package eu.telecom_bretagne.xmcda;

public class Point
{

	private float abscissa;

	private float ordinate;

	public Point(float abscissa, float ordinate)
	{
		this.abscissa = abscissa;
		this.ordinate = ordinate;
	}

	public float getAbscissa()
	{
		return abscissa;
	}

	public float getOrdinate()
	{
		return ordinate;
	}

}
