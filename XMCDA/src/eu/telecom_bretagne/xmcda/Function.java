package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;


public class Function
{
	private ArrayList<Point>listPoints;

	public Function()
	{
		listPoints= new ArrayList<Point>();
	}

	public ArrayList<Point> getPoints()
	{
		return listPoints;
	}

	public void addPoint(Point point)
	{
		listPoints.add(point);
	}
}
