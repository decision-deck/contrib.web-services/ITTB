/* 
 * Copyright (c) 2012-2013, Sébastien Bigaret
 * Permission to use within XMCDA
 */
package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class Matrix<ROW_T, COLUMN_T, VALUE_T>
{
	public static class Cell<R,C,V>
	{
		public final R row;
		public final C column;
		public final V value;
		public Cell(R row, C column, V value)
		{
			this.row = row;
			this.column = column;
			this.value = value;
		}
	}	
	protected static class Coord<R,C>
	{
		R row;
		C column;
		public Coord(R row, C column)
		{
			this.row = row;
			this.column = column;
		}
		@Override
		public boolean equals(Object obj)
		{
			if ( ! ( obj instanceof Coord<?, ?>) )
				return false;
			Coord<?,?> c = (Coord<?, ?>)obj;
			if ( ( this.row==null && c.row==null) || (this.row!=null && this.row.equals(c.row)) )
				return ( ( this.column==null && c.column==null ) || ( this.column!=null && this.column.equals(c.column) ));
			return false;
		}
		@Override
		public int hashCode()
		{
			int hashCode = 0;
			if (this.row!=null)
				hashCode = this.row.hashCode();
			if (this.column!=null)
				hashCode ^= this.column.hashCode();
			return hashCode;
		}
		@Override
		public String toString()
		{
			return "("+this.row+","+this.column+")";
		}
	}

	private LinkedHashSet<ROW_T> rows = new LinkedHashSet<ROW_T>();
	private LinkedHashSet<COLUMN_T> columns = new LinkedHashSet<COLUMN_T>();
	private Map<Coord<ROW_T,COLUMN_T>, VALUE_T> values = new HashMap<Coord<ROW_T,COLUMN_T>, VALUE_T>();
	private boolean frozen = false;

	public Matrix() { }

	public Matrix(List<ROW_T> rows, List<COLUMN_T> columns)
	{
		for (ROW_T row: rows)
			addRow(row, false);
		for (COLUMN_T column: columns)
			addColumn(column, false);
	}

	public Matrix(Matrix<ROW_T, COLUMN_T, VALUE_T> matrix)
	{
		this.rows = new LinkedHashSet<ROW_T>(matrix.rows);
		this.columns = new LinkedHashSet<COLUMN_T>(matrix.columns);
		for (ROW_T row: rows)
			for (COLUMN_T column: columns)
				if (matrix.isSet(row, column))
					this.setValue(row, column, matrix.getValue(row, column));
	}

	public List<ROW_T> rows()
	{
		return new ArrayList<ROW_T>(rows);
	}

	public List<COLUMN_T> columns()
	{
		return new ArrayList<COLUMN_T>(columns);
	}

	public int nbRows()
	{
		return rows.size();
	}

	public int nbColumns()
	{
		return columns.size();
	}

	/**
	 * Returns {@code true} if the matrix is square (same number of rows and columns).
	 * @return {@code true} if the matrix is square
	 */
	public boolean isSquare()
	{
		return nbColumns()==nbRows();
	}

	/**
	 * Sets the frozen state. A matrix is said to be frozen when its dimensions are frozen, i.e. when no row or column
	 * can be added or removed.
	 * 
	 * @param isFrozen
	 *            the frozen state
	 */
	public void setIsFrozen(boolean isFrozen)
	{
		this.frozen = isFrozen;
	}

	/**
	 * Returns {code true} if the matrix is in the frozen state. A matrix is said to be frozen when its dimensions are
	 * frozen, i.e. when no row or column can be added or removed.
	 * 
	 * @return {code true} if the matrix is in the frozen state
	 */
	public boolean isFrozen()
	{
		return this.frozen;
	}

	/**
	 * Adds a row to the matrix.<br/>
	 * If there exists a row {@code r} in the matrix such that parameter {@code (row==null ? r==null : row.equals(r))},
	 * then this method:
	 * <ul>
	 * <li>simply returns if {@code ignoreDups==true},
	 * <li>raises {@code IllegalArgumentException} if {@code ignoreDups==true}
	 * </ul>
	 * 
	 * @param row
	 *            the row to be added to the matrix
	 * @param ignoreDups
	 *            if {@code true}, adding a row that is already present in the matrix is silently ignored.
	 * @throws IllegalStateException
	 *             if the matrix is {@link #isFrozen() frozen}
	 * @throws IllegalArgumentException
	 *             if {@code ignoreDups} is {@code true} and the matrix already has a row equal to {@code row}.
	 */
	public void addRow(ROW_T row, boolean ignoreDups)
	{
		if (isFrozen())
			throw new IllegalStateException("Matrix is frozen");
		if ( !rows.add(row) && !ignoreDups )
			throw new IllegalArgumentException("Duplicate row");
	}

	/**
	 * Adds a column to the matrix.<br/>
	 * If there exists a column {@code c} in the matrix such that parameter
	 * {@code (column==null ? c==null : column.equals(c))}, then this method:
	 * <ul>
	 * <li>simply returns if {@code ignoreDups==true},
	 * <li>raises {@code IllegalArgumentException} if {@code ignoreDups==true}
	 * </ul>
	 * 
	 * @param column
	 *            the column to be added to the matrix
	 * @param ignoreDups
	 *            if {@code true}, adding a column that is already present in the matrix is silently ignored.
	 * @throws IllegalStateException
	 *             if the matrix is {@link #isFrozen() frozen}
	 * @throws IllegalArgumentException
	 *             if {@code ignoreDups} is {@code true} and the matrix already has a column equal to {@code column}.
	 */
	public void addColumn(COLUMN_T column, boolean ignoreDups)
	{
		if (isFrozen())
			throw new IllegalStateException("Matrix is frozen");
		if ( !columns.add(column) && !ignoreDups )
			throw new IllegalArgumentException("Duplicate column");
	}

	/**
	 * Removes a row and the associated values from the matrix, and returns the values associated to this row or
	 * {@code null} if no such row exists.
	 * 
	 * @param row
	 *            the row to be deleted
	 * @return the list of values associated to this row (may be empty), or {@code null} if no such row exists.
	 */
	public List<Cell<ROW_T,COLUMN_T,VALUE_T>> removeRow(ROW_T row)
	{
		if (isFrozen())
			throw new IllegalStateException("Matrix is frozen");
		if (!hasRow(row))
			return null;
		ArrayList<Cell<ROW_T,COLUMN_T,VALUE_T>> rowCells = new ArrayList<Matrix.Cell<ROW_T,COLUMN_T,VALUE_T>>();
		for (COLUMN_T column: columns)
			if (isSet(row, column))
				rowCells.add(new Cell<ROW_T, COLUMN_T, VALUE_T>(row, column, this.unset(row, column)));

		rows.remove(row);
		return rowCells;
	}

	/**
	 * Removes a column and the associated values from the matrix, and returns the values associated to this column or
	 * {@code null} if no such column exists.
	 * 
	 * @param column
	 *            the column to be deleted
	 * @return the list of values associated to this column (may be empty), or {@code null} if no such column exists.
	 */
	public List<Cell<ROW_T,COLUMN_T,VALUE_T>> removeColumn(COLUMN_T column)
	{
		if (isFrozen())
			throw new IllegalStateException("Matrix is frozen");
		if (!hasColumn(column))
			return null;
		ArrayList<Cell<ROW_T,COLUMN_T,VALUE_T>> columnCells = new ArrayList<Matrix.Cell<ROW_T,COLUMN_T,VALUE_T>>();
		for (ROW_T row: rows)
			if (isSet(row, column))
				columnCells.add(new Cell<ROW_T, COLUMN_T, VALUE_T>(row, column, this.unset(row, column)));

		columns.remove(column);
		return columnCells;
	}

	/**
	 * Returns {@code true} if matrix contains the supplied row.
	 * @param row 
	 * @return {@code true} if matrix contains the row.
	 */
	public boolean hasRow(ROW_T row)
	{
		return rows.contains(row);
	}

	/**
	 * Returns {@code true} if matrix contains the supplied column.
	 * @param column 
	 * @return {@code true} if matrix contains the column.
	 */
	public boolean hasColumn(COLUMN_T column)
	{
		return columns.contains(column);
	}

	/**
	 * Returns the list of the cells that have an assigned value (including those to which a null value is assigned)
	 * 
	 * @return the list of the cells having a value.
	 * @see #setValue(Object, Object, Object)
	 */
	public List<Cell<ROW_T,COLUMN_T,VALUE_T>> nonEmptyCells()
	{
		ArrayList<Cell<ROW_T, COLUMN_T, VALUE_T>> cells = new ArrayList<Matrix.Cell<ROW_T,COLUMN_T,VALUE_T>>();
		for (ROW_T row: rows)
			for (COLUMN_T column: columns)
				if (isSet(row, column))
				{
					cells.add(new Cell<ROW_T, COLUMN_T, VALUE_T>(row, column, getValue(row, column)));
				}
		return cells;
	}

	/**
	 * Assigns a value to the coordinates {@code (row, column} .<br/>
	 * The method returns the previous value assigned to the coordinates, or {@code null} if there was no such value.
	 * Since a matrix accepts null values, a return value of {@code null} means either that there was no value
	 * associated to {@code (row,column)}, or that a null value was stored at this coordinate: you may use
	 * {@link #isSet(Object, Object)} to distinguish between these two cases.
	 * 
	 * @param row
	 * @param column
	 * @param value
	 *            the value to be assigned
	 * @return the previous value assigned to the coordinates, or {@code null} if there was no such value.
	 */
	public VALUE_T setValue(ROW_T row, COLUMN_T column, VALUE_T value)
	{
		if (!hasRow(row))
			throw new IllegalArgumentException("Unknown row " + row == null ? "(null)" : row.toString());
		if (!hasColumn(column))
			throw new IllegalArgumentException("Unknown column " + column == null ? "(null)" : column.toString());
		return values.put(new Coord<ROW_T, COLUMN_T>(row, column), value);
	}

	/**
	 * Returns the value stored at the given coordinates, or {@code null} it there is no such value. Since a matrix
	 * accepts null values, a return value of {\code null} means either that there is no value associated to
	 * (row,column), or that a null value is stored at this coordinate: use {@link #isSet(Object, Object)} to
	 * distinguish between these two cases.
	 * 
	 * @param row
	 * @param column
	 * @return the corresponding value, or {@code null} if it is not set.
	 */
	public VALUE_T getValue(ROW_T row, COLUMN_T column)
	{
		return values.get(new Coord<ROW_T, COLUMN_T>(row, column));
	}

	/**
	 * Returns {@code true} if there is a value stored at the coordinates (row,column).
	 * @param row
	 * @param column
	 * @return {@code true} if a value is stored at coordinates (row,column)
	 */
	public boolean isSet(ROW_T row, COLUMN_T column)
	{
		return values.containsKey(new Coord<ROW_T, COLUMN_T>(row, column));
	}

	/**
	 * Removes the value associated at the requested coordinates, and returns the value that was previously stored, or
	 * {@code null} if there was no value stored at these coordinates. Since a matrix accepts null values, a return
	 * value of {\code null} means either that there were no value associated to (row,column), or that a null value was
	 * stored at this coordinate: use {@link #isSet(Object, Object)} to distinguish between these two cases.
	 * 
	 * @param row
	 * @param column
	 * @return the value previously stored at coordinates (row,column), or {@code null} it there was no such value.
	 */
	public VALUE_T unset(ROW_T row, COLUMN_T column)
	{
		return values.remove(new Coord<ROW_T, COLUMN_T>(row, column));
	}

}
