package eu.telecom_bretagne.xmcda;

import java.util.Iterator;
import org.jdom2.Element;


public class JDOM_Criteria_Comparisons {

	private CriteriaComparisons criterCompToTest  =new CriteriaComparisons();
	private String warningMessage;  

	public  JDOM_Criteria_Comparisons()
	{
		warningMessage="";
		criterCompToTest  =new CriteriaComparisons();
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

	public CriteriaComparisons exploreCriteriaComparisonsXML(Element racine)
	{

		String tempCriterID_initial="";
		String tempCriterID_terminal="";
		int initial_index=-1;
		int final_index=-1;

		if(racine.getChild("criteriaComparisons")!=null)
		{
			Element criterComp = racine.getChild("criteriaComparisons");
			if(criterComp.getChild("pairs")!=null)
			{ 
				Element pairs=criterComp.getChild("pairs");
				Iterator<Element> i = pairs.getChildren("pair").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					if(courant.getChild("initial")!=null && courant.getChild("terminal")!=null)
					{
						if(courant.getChild("initial").getChild("criterionID")!=null
								&&
								courant.getChild("terminal").getChild("criterionID")!=null)
						{
							tempCriterID_initial=courant.getChild("initial").
									getChild("criterionID").getValue();

							if( courant.getChild("initial").
									getChild("criterionID").getChild("active")!=null &&  
									courant.getChild("active").getValue().equals("false"))
							{
								//The initial is inactive --> Ignore this criterion ID
								warningMessage=warningMessage.concat("File containing the criteriaComparisons tag:" +
										" the criterion "+tempCriterID_initial+" was ignored " +
										"because it is not active: active=false.\n");
							}
							else
							{
								tempCriterID_terminal=courant.getChild("terminal").
										getChild("criterionID").getValue();

								if( courant.getChild("terminal").
										getChild("criterionID").getChild("active")!=null &&  
										courant.getChild("active").getValue().equals("false"))
								{
									//The initial is inactive --> Ignore this criterion ID
									warningMessage=warningMessage.concat("File containing the criteriaComparisons tag:" +
											" the criterion "+tempCriterID_terminal+" was ignored " +
											"because it is not active: active=false.\n");
								}
								else
								{
									if(criterCompToTest.getListCriteriaIDs().contains(tempCriterID_initial))
									{}
									else
										criterCompToTest.addCriterionID(tempCriterID_initial);

									initial_index=criterCompToTest.getListCriteriaIDs().
											indexOf(tempCriterID_initial);

									if(criterCompToTest.getListCriteriaIDs().contains(tempCriterID_terminal))
									{}
									else
										criterCompToTest.addCriterionID(tempCriterID_terminal);

									final_index=criterCompToTest.getListCriteriaIDs().
											indexOf(tempCriterID_terminal);

									// Update the matrix
									/*
									 * Treat NA the same as when there is no value: this is especially useful when
									 * XMCDA v3 is translated into v2 before being supplied to us, because XMCDA v3
									 * requires that there is at least a value for each cell in an
									 * alternativesMatrix.
									 */
									if (courant.getChild("value")==null || courant.getChild("value").getChild("NA") != null)
										criterCompToTest.updateMatrix( initial_index,final_index);
									else if (courant.getChild("value")!=null 
											&& courant.getChild("value").getChild("real")!=null)
									{
										if(criterCompToTest.getMatrix()[initial_index][final_index]==-2000)
											criterCompToTest.updateValuedMatrix( initial_index,final_index,
											                                     Float.parseFloat(courant.getChild("value").getChild("real").getValue()));
										else
										{
											warningMessage= warningMessage.concat("\nWarning: the file containing the criteriaComparisons tag: doubled pair for" +
													" criterionID ("+ tempCriterID_initial+")in the 'initial' tag" +
													" and criterionID ("+ tempCriterID_terminal+") in the 'terminal' tag."); 
										}  							  
									}

									else if (courant.getChild("value")!=null 
											&& courant.getChild("value").getChild("real")==null)
									{
										// Send a warning
										if (warningMessage.equals(""))
											warningMessage= "\nError in the file containing the criteriaComparisons tag." +
													" The value tag must be REAL. Please check " +
													"this value for the criterionID ("+ tempCriterID_initial+")in the 'initial' tag" +
													" and the criterionID ("+ tempCriterID_terminal+") in the 'terminal' tag.";
										else 
											warningMessage= warningMessage.concat("\nError in the file containing the " +
													"criteriaComparisons tag." +
													" The value tag must be REAL. Please check " +
													"this value for the criterionID ("+ tempCriterID_initial+")in the 'initial' tag" +
													" and the criterionID ("+ tempCriterID_terminal+") in the 'terminal' tag.");
									}
									else {/*Normally these are the only cases I should have*/}
								}
							}
						}
						else
						{
							// Ignore the relation
						}
					}
				}
				if(criterCompToTest.getListCriteriaIDs().size()==0)
					return null;
				else
					return criterCompToTest;
			}
			else return null;
		}
		else return null;
	}

}
