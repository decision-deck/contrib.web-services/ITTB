/**
 * 
 */
package eu.telecom_bretagne.xmcda;

/**
 * @author Sébastien Bigaret
 *
 */
public class Interval
{
	private Number lowerBound;
	private Number upperBound;
	
	public Interval(Number lowerBound, Number upperBound)
    {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
    }

	public boolean isWithin(float value)
	{
		boolean greater_than_lower = lowerBound==null ? true : value>=lowerBound.floatValue();
		boolean smaller_than_upper = upperBound==null ? true : value<=upperBound.floatValue();
		return greater_than_lower && smaller_than_upper;
	}

}
