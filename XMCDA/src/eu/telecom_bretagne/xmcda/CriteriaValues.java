package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CriteriaValues implements Iterable<CriterionValue>
{
	private ArrayList<CriterionValue> criteriaValues = new ArrayList<CriterionValue>();

	public CriteriaValues() { }

	/**
	 * Adds a criterion value to this object.
	 * @param criterionValue the CriterionValue to add
	 * @return a reference to this object
	 */
	public CriteriaValues add(CriterionValue criterionValue)
	{
		criteriaValues.add(criterionValue);
		return this;
	}

	public boolean remove(CriterionValue criterionValue)
	{
		return criteriaValues.remove(criterionValue);
	}

	public CriteriaValues filterBy(Criteria criteria)
	{
		CriteriaValues filtered = new CriteriaValues();
		for (CriterionValue criteriaValue: this.criteriaValues)
			if ( criteria.contains(criteriaValue.getCriterion()) )
				filtered.add(criteriaValue);
		return filtered;
	}

	public Criteria getCriteria()
	{
		Criteria criteria = new Criteria();
		for (CriterionValue criterionValue: criteriaValues)
			criteria.add(criterionValue.getCriterion());
		return criteria;
	}

	public ArrayList<String> getCriteriaIDs()
	{
		ArrayList<String> criteriaIDs = new ArrayList<String>();
		for (CriterionValue criterionValue: criteriaValues)
			criteriaIDs.add(criterionValue.getCriterion().id());
		return 	criteriaIDs;
	}

	public CriterionValue getCriterionValue(Criterion criterion)
	{
		for ( CriterionValue criterionValue: criteriaValues )
			if ( criterionValue.getCriterion().equals(criterion) )
				return criterionValue;
		return null;
	}
	
	public List<CriterionValue> getListCriteriaValues()
	{
		return Collections.unmodifiableList(criteriaValues);
	}

	public void printCriteriaValues()
	{
		System.out.println("These are the CriteriaValues:");
		for (int i= 0; i < criteriaValues.size(); i++)
		{
			System.out.println("CriterionValue"+(i+1)+":\t"+ 
					"id = "+ criteriaValues.get(i).getCriterion()+
					"\t, value = "+ criteriaValues.get(i).getRealValue());
		}
	}

	public int size()
	{
		return criteriaValues.size();
	}

	@Override
	public Iterator<CriterionValue> iterator()
	{
		return Collections.unmodifiableList(criteriaValues).iterator();
	}
}