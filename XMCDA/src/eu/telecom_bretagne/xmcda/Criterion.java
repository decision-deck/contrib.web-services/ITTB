package eu.telecom_bretagne.xmcda;


public class Criterion
{
	private String   id;

	private String   name;

	private boolean  active;

	//private Scale    scale;             // nominal, qualitative, quantitative

	private Function function;


	public Criterion(String criterionId, String criterionName)
	{
		this.id = criterionId;
		this.name = criterionName;
		active = true;
		function = new Function();
	}

	public Criterion(String criterionId)
	{
		this.id = criterionId;
		function = new Function();
	}

	public String id()
	{
		return id;
	}

	public String name()
	{
		return name;
	}

	public void name(String name)
	{
		this.name = name;
	}

	public boolean getActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public Function getCriterionFunction()
	{
		return function;
	}

	/**
	 * Two criteria are equal if they have the same id
	 */
	@Override
	public boolean equals(Object obj)
	{
		if ( obj==null || ! ( obj instanceof Criterion ) )
			return false;
		final Criterion criterion = (Criterion) obj;
		return this.id.equals(criterion.id);
	}

	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}

	@Override
	public String toString()
	{
		return id;
	}
}
