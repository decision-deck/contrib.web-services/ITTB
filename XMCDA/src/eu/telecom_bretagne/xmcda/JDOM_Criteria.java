package eu.telecom_bretagne.xmcda;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class JDOM_Criteria
{

	private Criteria criteriaToTest  =new Criteria();
	private String pathXMLFile;
	static org.jdom2.Document document;
	private String warning = "";

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		return document.getRootElement();
	}

	public void setPath(String path)
	{
		this.pathXMLFile=path;
	}

	public String getPath()
	{
		return pathXMLFile;
	}

	public Criteria getCriteria()
	{
		return criteriaToTest;
	}

	public void  printCriteria()
	{
		System.out.println("There are "+criteriaToTest.getListCriteria()
		                   .size()+" criteria:");
		for(int i=0;i<criteriaToTest.getListCriteria().size();i++)
			System.out.print(criteriaToTest.getListCriteria().
			                 get(i).id()+"\t");
		System.out.println("");
	}

	public Criteria parseCriteriaXML(Element racine)
	{	      
		if(racine.getChild("criteria")!=null)
		{
			Element criteria = racine.getChild("criteria");
			//On cr�e une List contenant tous les noeuds "criterion" de l'Element racine
			if (criteria.getChildren("criterion")!=null){

				Iterator<Element> i = criteria.getChildren("criterion").iterator();
				while(i.hasNext())
				{
					//On recr�e l'Element courant � chaque tour de boucle afin de
					//pouvoir utiliser les m�thodes propres aux Element :
					Element courant = (Element)i.next();
					//On affiche le nom de l'element courant
					/*System.out.println(courant.getAttributeValue("name"));*/

					//On affiche le id de l'element courant
					/*System.out.println(courant.getAttributeValue("id"));*/

					if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")!=null)
					{
						if(courant.getChild("active")!=null && 
								courant.getChild("active").getValue().equals("false"))
						{
							//Le criterion est inactif, on l'ignore
							warning=warning.concat("The criterion "+courant.getAttributeValue("id")+" was ignored " +
									"because it is not active: active=false.\n");
						}
						else 
							criteriaToTest.add(new Criterion 
							                   (courant.getAttributeValue("id"), courant.getAttributeValue("name")));       
					}
					else if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")==null)
					{
						if(courant.getChild("active")!=null && 
								courant.getChild("active").getValue().equals("false"))
						{
							//Le criterion est inactif, on l'ignore
							warning=warning.concat("The criterion "+courant.getAttributeValue("id")+" was ignored " +
									"because it is not active: active=false.\n");
						}
						else 
							criteriaToTest.add(new Criterion 
							                   (courant.getAttributeValue("id")));
					}
					else
					{
						// Warning, on n'a pas d'ID pour le criterion
					}
				}
				if(criteriaToTest.getCriteriaIDs().size()==0)
					return null;
				else
					return criteriaToTest;
			}
			else return null;
		}
		else
		{
			return null;
		}
	}

	public LinkedHashMap<Criterion, String> exploreCriteriaXML(Element racine, Criteria criter)
	{
		LinkedHashMap<Criterion, String> parsePreferenceDirectin=new LinkedHashMap<Criterion, String>();
		for (Criterion c: criter)
			parsePreferenceDirectin.put(c, "max");

		if(racine.getChild("criteria")==null)
			return parsePreferenceDirectin;
		Element criteria = racine.getChild("criteria");	      
		if(criteria.getChildren("criterion")==null)
			return parsePreferenceDirectin;

		Iterator<Element> i = criteria.getChildren("criterion").iterator();
		while(i.hasNext())
		{
			Element courant = (Element)i.next();
			if (courant.getAttributeValue("id")==null)
				continue;
			if(courant.getChild("active")!=null && 
					courant.getChild("active").getValue().equals("false"))
			{
				//Inactive criterion, on l'ignore							
				continue;
			}
			if(courant.getChild("scale")==null)
			{
				warning=warning.concat(" Error in the file containing the criteria tag: " +
						"no 'scale' detected for criterion ("+courant.getAttributeValue("id")+")." +
						" The preferenceDirection was set to 'max'.");
				continue;
			}
			if(courant.getChild("scale").getChild("quantitative")==null)
			{
				warning=warning.concat(" Error in the file containing the criteria tag: " +
						"no 'quantitative' detected for criterion ("+courant.getAttributeValue("id")+")." +
						" The preferenceDirection was set to 'max'.");
				continue;
			}
			if(courant.getChild("scale").getChild("quantitative").getChild("preferenceDirection")==null)
			{
				warning=warning.concat(" Error in the file containing the criteria tag: " +
						"no 'preferenceDirection' detected for criterion ("+courant.getAttributeValue("id")+")." +
						" The preferenceDirection was set to 'max'.");
				continue;
			}
			final String value = courant.getChild("scale").getChild("quantitative").getChild("preferenceDirection").getValue();
			if(value==null || !(value.equals("min")||value.equals("max")))
			{
				warning=warning.concat(" Error in the file containing the criteria tag: " +
						"the 'preferenceDirection' tag should be equal to 'min' or 'max'. " +
						"Check this tag for criterion ("+courant.getAttributeValue("id")+")." +
						" The preferenceDirection for this criterion was set to 'max'.");
				continue;
			}
			Criterion c = criter.get(courant.getAttributeValue("id"));
			if (c!=null)
				parsePreferenceDirectin.put(c, value);
		}
		return parsePreferenceDirectin;
	}

	public String getWarning()
	{
		return warning;
	}
}
