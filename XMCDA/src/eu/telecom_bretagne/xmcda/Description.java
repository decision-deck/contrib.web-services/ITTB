package eu.telecom_bretagne.xmcda;

public class Description {
	private String id;
	private String name;
	private String comment;


	public Description()
	{
		this.id=null;
		this.name=null;
		this.comment=null;
	}
	public Description(String id,String name,String comment)
	{
		this.id=id;
		this.name=name;
		this.comment=comment;
	}

	public Description(String id,String name)
	{
		this.id=id;
		this.name=name;
	}
}
