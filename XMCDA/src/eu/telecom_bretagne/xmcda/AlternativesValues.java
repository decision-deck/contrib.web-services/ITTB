package eu.telecom_bretagne.xmcda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class AlternativesValues implements Iterable<AlternativeValue>
{
	private ArrayList<AlternativeValue> alternativesValues;

	public AlternativesValues()
	{
		alternativesValues = new ArrayList<AlternativeValue>();
	}

	public boolean addAlternative(AlternativeValue altValToAdd)
	{
		return alternativesValues.add(altValToAdd);
	}

	public ArrayList<AlternativeValue> getListAlternativesValues()
	{
		return new ArrayList<AlternativeValue>(alternativesValues);
	}

	public ArrayList<String> getListAlternativesIDs()
	{
		ArrayList<String> listAlternativesID = new ArrayList<String>();
		for (int i = 0; i < alternativesValues.size(); i++)
			listAlternativesID.add(alternativesValues.get(i).getAlternativeValueId());
		return listAlternativesID;
	}

	public void filterBy(Alternatives ref_alternatives)
	{
		for (AlternativeValue av: getListAlternativesValues() )
		{
			Alternative a = av.getAlternative();
			if ( ! ref_alternatives.contains(a) || (ref_alternatives.contains(a) && ! ref_alternatives.get(a.id()).isActive() ) )
				alternativesValues.remove(av);
		}
	}

	public boolean contains(AlternativeValue alternativeValue)
	{
		return alternativesValues.contains(alternativeValue);
	}
	
	@Override
    public Iterator<AlternativeValue> iterator()
    {
		return Collections.unmodifiableList(alternativesValues).iterator();
    }

	public int size()
	{
		return alternativesValues.size();
	}
	
	public void printAlternativesValues()
	{
		System.out.println("These are the AlternativesValues:");
		for (int i = 0; i < alternativesValues.size(); i++)
		{
			System.out.println("AlternativeValue" + ( i + 1 ) + ":\t" + "id = "
					+ alternativesValues.get(i).getAlternativeValueId() + "\t, value = "
					+ alternativesValues.get(i).getRealValue());
		}
	}

}
