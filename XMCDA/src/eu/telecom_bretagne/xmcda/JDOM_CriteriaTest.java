package eu.telecom_bretagne.xmcda;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class JDOM_CriteriaTest {

	private JDOM_Criteria jdomCriteria;

	@Before
	public void setUp() throws Exception {
		jdomCriteria=new JDOM_Criteria();
	}

	@After
	public void tearDown() throws Exception {
		jdomCriteria=null;
	}

	@Test
	public void testJDOM_Criteria() {
		assertNotNull("l'instance est cr��e", jdomCriteria);
	}
	@Test
	public void testSetPath()
	{
		jdomCriteria.setPath("data/ACUTA-in1-2/criteria.xml");
		assertEquals("Is it OK for the path?", "data/ACUTA-in1-2/criteria.xml",
		             jdomCriteria.getPath());
	}

	@Test
	public void testParseCriteriaXML() {

		assertEquals("Is it OK for the parsing?",
		             jdomCriteria.parseCriteriaXML(jdomCriteria.getRacine
		                                           ("data/ACUTA-in1-2/criteria.xml")),jdomCriteria.getCriteria());

		//jdomCriteria.printCriteria();
	}

}
