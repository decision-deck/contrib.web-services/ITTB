package eu.telecom_bretagne.xmcda;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class Main
{
	static org.jdom2.Document document;
	static Element racine;

	private String warningMessage;
	private boolean activateMatrixInt;
	private boolean activateMatrixReal;
	private String tempErrorMessage;

	public Main(){
		activateMatrixInt=false;
		activateMatrixReal=false;
		warningMessage="";
		tempErrorMessage="";
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

	public String getErrorMessage()
	{
		return tempErrorMessage;
	}
	public String checkEntryFile(String path)
	{
		String error="";
		try {
			new MyValid().parseXML(path);
		} catch (JDOMException e) {
			error=error.concat("File is not well-formed: "+e.getMessage());
		} catch (IOException e) {
			error=error.concat("Could not check file because: "+e.getMessage());
		}
		return error;
	}

	public void PrepareParsing(String path)
	{
		try
		{
			//On cr�e une instance de SAXBuilder
			SAXBuilder sxb = new SAXBuilder();
			//On cr�e un nouveau document JDOM avec en argument le fichier XML
			//Le parsing est termin� ;)
			document = sxb.build(new File(path));
		}
		catch(Exception e){}
		//On initialise un nouvel �l�ment racine avec l'�l�ment racine du document.
		racine = document.getRootElement();
	}


	public PerformanceTable run(PerformanceTable performanceTable, Criteria criteria, Alternatives alternatives)
	{
		// La gestion d'incoh�rences entre les diff�rents fichiers
		GestionIncoherentFiles gest=new GestionIncoherentFiles(criteria,
		                                                       alternatives, performanceTable);

		boolean checkAlt = gest.checkAlternatives(alternatives, performanceTable);
		boolean checkCrit = gest.checkCriteria(criteria, performanceTable);
		if (!checkAlt)
		    warningMessage = warningMessage.concat(gest.getWarningAlternatives());
		if (!checkCrit)
		    warningMessage = warningMessage.concat(gest.getWarningCriteria());

		if ( checkAlt && checkCrit )
			return performanceTable;

		performanceTable = new PerformanceTable(performanceTable);
		if ( ! checkAlt )
		{
			List<?> intersection = performanceTable.alternatives();
			intersection.retainAll(alternatives.getListActiveAlternatives());
			// Si l'intersection est vide on prend ce qu'on a dans la perfTable
			if ( intersection.isEmpty())
			{
				warningMessage=warningMessage.concat("\nOnly the alternativesIDs " +
						"in the file containing the performance table tag were considered. " +
						"Please check your input file containing the alternatives tag.");
			}
			else
			{
				performanceTable.filterBy(alternatives);
			}
		}
		
		if ( ! checkCrit )
		{
			List<Criterion> intersection = performanceTable.criteria();
			intersection.retainAll(criteria.getListCriteria());

			// Si l'intersection est vide on prend ce qu'on a dans la perfTable
			if (intersection.isEmpty())
			{
				warningMessage=warningMessage.concat("\nOnly the criterionIDs " +
						"in the file containing the performance table tag were considered. " +
						"Please check your input file containing the criteria tag.");
			}
			else
			{
				performanceTable.filterBy(criteria);
			}
		}
		return performanceTable;
	}

	public boolean getActivateMatrixInt()
	{
		return activateMatrixInt;
	}

	public boolean getActivateMatrixReal()
	{
		return activateMatrixReal;
	}

	public Element getRacine()
	{
		return racine;
	}

}
