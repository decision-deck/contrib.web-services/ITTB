package eu.telecom_bretagne.xmcda;


import java.util.ArrayList;

public class CriteriaComparisons
{
	private ArrayList<String >listCriteriaID;
	private float[][]matrixRelations;

	// Normalement il faudra que je trouve une solution 
	//plus ad�quate pour l'initialisation de la matrice
	/////TO DO LATER:::::::::::::::::::::::::::::::::::::::
	private int size=100;

	public CriteriaComparisons()
	{
		listCriteriaID=new ArrayList<String>();
		matrixRelations= new float[size][size];
		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++)
				matrixRelations[i][j]=-2000;
	}

	public void addCriterionID(String CriteriaID)
	{
		listCriteriaID.add(CriteriaID);
	}

	public ArrayList<String > getListCriteriaIDs()
	{
		return 	listCriteriaID;
	}

	public void updateMatrix(int initial, int terminal)
	{
		matrixRelations[initial][terminal]=1;
	}

	public void updateValuedMatrix(int initial, int terminal,float value)
	{
		matrixRelations[initial][terminal]=value;
	}

	public float[][] getMatrix()
	{
		return matrixRelations;
	}

	public void setSize(int size)
	{
		this.size=size;
	}

	public int getSize()
	{
		return size;
	}
}
