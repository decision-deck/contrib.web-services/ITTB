/* 
 * Copyright (c) 2012-2013, Sébastien Bigaret
 * Permission to use within XMCDA
 */
package eu.telecom_bretagne.xmcda;

import java.util.List;

/**
 * @author Sébastien Bigaret
 *
 */
public class SingleDimensionMatrix <DIMENSION_T, VALUE_T>
    extends Matrix<DIMENSION_T, DIMENSION_T, VALUE_T>
{

	public void addDimension(DIMENSION_T dimension, boolean ignoreDups)
	{
		super.addRow(dimension, ignoreDups);
		super.addColumn(dimension, ignoreDups);
	}

	public List<DIMENSION_T> getDimensions()
	{
		return columns();
	}


	@Override
	public void addRow(DIMENSION_T line, boolean ignoreDups)
	{
		this.addDimension(line, ignoreDups);
	}

	@Override
	public void addColumn(DIMENSION_T column, boolean ignoreDups)
	{
		this.addDimension(column, ignoreDups);
	}

	/**
	 * Returns true
	 */
	@Override
	public boolean isSquare()
	{
		return true;
	}

}
