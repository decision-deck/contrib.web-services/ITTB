package eu.telecom_bretagne.xmcda;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class Criteria implements Iterable<Criterion>
{
	private final ArrayList<Criterion> criteria = new ArrayList<Criterion>();

	public Criteria() { }

	public void add(Criterion criterion)
	{
		criteria.add(criterion);
	}

	public void remove(Criterion criterion)
	{
		criteria.remove(criterion);
	}

	public ArrayList<String> getCriteriaIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		for (Criterion criterion: criteria)
			ids.add(criterion.id());
		return 	ids;
	}

	public Criterion get(String id)
	{
		for (Criterion criterion: criteria)
			if (criterion.id().equals(id))
				return criterion;
		return null;
	}

	public List<Criterion> getListCriteria()
	{
		return Collections.unmodifiableList(criteria);
	}

	public ArrayList<String> getListCriteriaNames()
	{
		ArrayList<String> names = new ArrayList<String>();
		for (Criterion criterion: criteria)
			names.add(criterion.name());
		return names;
	}

	public int size()
	{
		return criteria.size();
	}

	public void printCriteria()
	{
		System.out.println("This are the criteria:");
		for (int i= 0; i < criteria.size(); i++)
		{
			System.out.println("Criterion"+(i+1)+":\t"+ 
					"id = "+ criteria.get(i).id()+
					"\t, name = "+ criteria.get(i).name());
		}
	}

	public boolean contains(Criterion criterion)
	{
		return criteria.contains(criterion);
	}

	@Override
	public Iterator<Criterion> iterator()
	{
		return Collections.unmodifiableList(criteria).iterator();
	}
}
