package eu.telecom_bretagne.xmcda;

public class CriterionValue
{
	private final Criterion criterion;

	private float  floatValue;
	private Object object;

	public CriterionValue(Criterion criterion)
	{
		this.criterion = criterion;
	}

	public CriterionValue(Criterion criterion, float critVal)
	{
		this.criterion = criterion;
		this.floatValue = critVal;
	}

	public CriterionValue(Criterion criterion, Object object)
	{
		this.criterion = criterion;
		this.object = object;
	}

	public Criterion getCriterion()
	{
		return criterion;
	}

	public float getRealValue()
	{
		return floatValue;
	}

	public void setRealValue(float value)
	{
		this.floatValue = value;
	}

	public Object getObject()
	{
		return object;
	}

	public void setObject(Object object)
	{
		this.object = object;
	}
}
