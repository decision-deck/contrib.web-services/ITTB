package eu.telecom_bretagne.xmcda;

import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class JDOM_Alternatives
{
	private Alternatives alternativesToTest;
	private String pathXMLFile;
	static org.jdom2.Document document;
	private String warning;

	public JDOM_Alternatives()
	{
		warning="";
		alternativesToTest=new Alternatives();
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		Element racine = document.getRootElement();

		return racine;
	}

	public void setPath(String path)
	{
		this.pathXMLFile=path;
	}

	public String getPath()
	{
		return pathXMLFile;
	}

	public Alternatives getAlternatives()
	{
		return alternativesToTest;
	}


	public void  printAlternatives()
	{
		System.out.println("There are "+alternativesToTest.size()+" alternatives:");
		for(Alternative alternative: alternativesToTest)
			System.out.print(alternative.id()+"\t");
		System.out.println("");
	}


	public Alternatives exploreAlternativesXML(Element racine)
	{	      
		if(racine.getChild("alternatives")!=null)
		{
			Element alternatives = racine.getChild("alternatives");
			//On cr�e une List contenant tous les noeuds "alternative" de l'Element racine

			if(alternatives.getChildren("alternative")!=null){
				Iterator<Element> i = alternatives.getChildren("alternative").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();

					Alternative alternative = Alternative.fromXML(courant);
					if (alternative==null)
						// Warning, on n'a pas d'ID 
						continue;
					if ( ! alternative.isActive() )
					{
						warning=warning.concat("The alternative "+alternative.id()+" was ignored " +
								"because it is not active\n");
						//continue;
					}
					alternativesToTest.add(alternative);
				}

				if(alternativesToTest.getIDs().size()==0)
					return null ;
				else
					return alternativesToTest;
			}
			else
				return null;
		}
		else
		{
			return null;
		}
	}

	public String getWarning()
	{
		return warning;
	}
}
