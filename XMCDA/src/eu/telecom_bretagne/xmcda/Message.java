package eu.telecom_bretagne.xmcda;

public class Message
{

	private int number;
	private String message;

	public Message(int number,String message)
	{
		this.number=number;
		this.message=message;
	}
	public String getMessage (Message m)
	{
		return m.message;
	}
	public int getMessageNumber (Message m)
	{
		return m.number;
	}
	public void printMessage (Message m)
	{
		System.out.println("Message "+m.number+":\t"+m.message);
	}

}
