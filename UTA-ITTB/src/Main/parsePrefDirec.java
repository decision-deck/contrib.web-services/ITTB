package Main;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;

public class parsePrefDirec {
	
	private String warningMessage;
	
	 // Constructor
	   public   parsePrefDirec (){
		   warningMessage="";
		  
	  }
	   
	   @SuppressWarnings("unchecked")
	public  String [] parsePreferenceDirectin(Element racine,  ArrayList<String> criteriaIDs)
	   {	
			  
		   String []   preferenceDirections =new String [criteriaIDs.size()];
		   int nbrOfElements=0;
		   
		   if (racine.getChild("criteriaValues")!=null)
		  	{
			  Element critVal = racine.getChild("criteriaValues");
		  
			  if(critVal.getChildren("criterionValue")!=null){
			  List listCritValues =critVal.getChildren("criterionValue");
			  Iterator i = listCritValues.iterator();
	      
			  String temp;
			  boolean criterionIDOK; 
			  while(i.hasNext())
			  {
				  Element courant = (Element)i.next();
				  temp="";
				  criterionIDOK=true;
	           
				  if(courant.getChild("value").
	    			  getChild("label")!=null )
				  {
					  temp=courant.getChild("value").
					  getChild("label").getValue();
					  if(temp.equals("min")||temp.equals("max"))
					  {}
					  else
						  criterionIDOK=false;
		  		
				  }
				 
				  else
				  {
					  criterionIDOK=false;}

	         
				  if(criterionIDOK==true)
				  {
					  if (criteriaIDs.contains(courant.getChild("criterionID").getValue())){						  
						  preferenceDirections[criteriaIDs.indexOf(courant.getChild("criterionID").getValue())]=temp;
						  nbrOfElements++;
					  }
						  else
						  // Warning pour dire que le crit�re n'est pas dans la liste
						  warningMessage= warningMessage.concat("Warning: The criterion:"+courant.getChild("criterionID").getValue()+" was ignored in the entry file" +
						  		" 'preferenceDirection' since it was not found in the criteria list of the project.");
	         
				  }
				  else
				  {
	        	 warningMessage= warningMessage.concat("\n Error in the file containing the criteriaValues tag." +
		      		  		" The value must be a label 'min' or 'max'."+
		      		  		" We have eliminated the corresponding criterion:"+
		      		  		courant.getChild("criterionID").getValue()+".");
				  }
			  }
			  //criteriaValuesToTest.printCriteriaValues();
			  
			  if(nbrOfElements!=criteriaIDs.size())
				  return null; 
			  	else
			  		return  preferenceDirections;
		  	}

			  else return null;
		  	}
			  else
			  {
				  return null;
			  }
	   
	   }
	   

	   
	   @SuppressWarnings("unchecked")
    public  int [] parseSegments(Element racine,  ArrayList<String> criteriaIDs)
	   {	
			  
		   int []  segments =new int [criteriaIDs.size()];
		   int nbrOfElements=0;
		   
		   if (racine.getChild("criteriaValues")!=null)
		  	{
			  Element critVal = racine.getChild("criteriaValues");
		  
			  if(critVal.getChildren("criterionValue")!=null){
			  List listAlternValues =critVal.getChildren("criterionValue");
			  Iterator i = listAlternValues.iterator();
	      
			  int temp;
			  boolean criterionIDOK; 
			  while(i.hasNext())
			  {
				  Element courant = (Element)i.next();
				  temp=-1;
				  criterionIDOK=true;
	           
				  if(courant.getChild("value").
	    			  getChild("integer")!=null )
					  temp=Integer.parseInt(courant.getChild("value").getChild("integer").getValue());
				 
				  else
					  criterionIDOK=false;

	         
				  if(criterionIDOK==true)
				  {
					  if (criteriaIDs.contains(courant.getChild("criterionID").getValue())){						  
						  segments[criteriaIDs.indexOf(courant.getChild("criterionID").getValue())]=temp;
						  nbrOfElements++;
					  }
						  else
						  // Warning pour dire que le crit�re n'est pas dans la liste
						  warningMessage= warningMessage.concat("Warning: The criterion:"+courant.getChild("criterionID").getValue()+" was ignored in the entry file" +
						  		" 'preferenceDirection' since it was not found in the criteria list of the project.");
	         
				  }
				  else
				  {
	        	 warningMessage= warningMessage.concat("\n Error in the file containing the criteriaValues tag." +
		      		  		" The value must be a label 'min' or 'max'."+
		      		  		" We have eliminated the corresponding criterion:"+
		      		  		courant.getChild("criterionID").getValue()+".");
				  }
			  }
			  //criteriaValuesToTest.printCriteriaValues();
			  
			  if(nbrOfElements!=criteriaIDs.size())
				  return null; 
			  	else
			  		return  segments;
		  	}

			  else return null;
		  	}
			  else
			  {
				  return null;
			  }
	   
	   }
	   
	   
	   public String getWarningMessage()
		{
			return warningMessage;
		}
	   
	   
	   
}
