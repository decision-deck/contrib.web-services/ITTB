package Main;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import utafx.LinearFunction;



public class OutputValFunc
{
	public OutputValFunc()
	{
		
	}
	

	private   Element racine = new Element("XMCDA");		 
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
		      		"XMLSchema-instance");		 
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			 		"/2009/XMCDA-2.0.0");
	private org.jdom2.Document document = new Document(racine);


	public void save(String fichier)
	   {
	      try
	      {	        
	         XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	         sortie.output(document, new FileOutputStream(fichier));
	      }
	      catch (java.io.IOException e){}
	   }
	   
	@SuppressWarnings("unchecked")
    public  void createOutputFile(LinearFunction[] actuals ,ArrayList<String> listCriter)  
	   {
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
			   "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
			   "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
			   xsi);
	   
	   List contents= racine.getContent();
	   contents.clear();

	   Element criteria = new Element("criteria");
	   racine.addContent(criteria);
	   Attribute mcdaConcept =new Attribute("mcdaConcept","criteria");
	   criteria.setAttribute(mcdaConcept);

	   
	   for(int i=0;i<listCriter.size();i++)
	   {
		   Element criterion = new Element("criterion");
		   criteria.addContent(criterion);
		   Attribute id =new Attribute("id",listCriter.get(i));
		   criterion.setAttribute(id);
		  
	   
		   Element  criterionFunction=new Element ("criterionFunction");
		   criterion.addContent(criterionFunction);
	   
		   Element points=new Element ("points");
		   criterionFunction.addContent(points);
		   int number=actuals[i].getNoOfPoints();
		   for(int j=0;j<number;j++)
		   {
			   Element point=new Element ("point");
			   points.addContent(point);
			   
			   Element abscissa=new Element ("abscissa");
			   point.addContent(abscissa);
			   Element real=new Element ("real");
			   abscissa.addContent(real);
			   real.setText(Double.toString(actuals[i].getCharacteristicPoints()[j]));
			   
			   Element ordinate=new Element ("ordinate");
			   point.addContent(ordinate);
			   Element real2=new Element ("real");
			   ordinate.addContent(real2);
			   real2.setText(Double.toString(actuals[i].getValues()[j]));
			   
		   }
	   }
	 
	   }
	
              		   
  
               	   
}
