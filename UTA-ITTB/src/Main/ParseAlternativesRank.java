package Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;

public class ParseAlternativesRank {
	
	private String warningMessage;
	
	 // Constructor
	   public  ParseAlternativesRank (){
		   warningMessage="";
		  
	  }
	   
	   @SuppressWarnings("unchecked")
	public  double [] getRanks(Element racine,  ArrayList<String> alternativesIDs)
	   {	
			  
		   double []  ranks=new double [alternativesIDs.size()];
		   // Initialisation car les alternatives n'ont pas forcément toutes un rang
		   for(int r=0;r<alternativesIDs.size();r++)
			   ranks[r]=-1;
		   int nbrOfElements=0;
		   
		   if (racine.getChild("alternativesValues")!=null)
		  	{
			  Element alternVal = racine.getChild("alternativesValues");
		  
			  if(alternVal.getChildren("alternativeValue")!=null){
			  List listAlternValues =alternVal.getChildren("alternativeValue");
			  Iterator i = listAlternValues.iterator();
	      
			  double temp;
			  boolean alternativeIDOK; 
			  while(i.hasNext())
			  {
				  Element courant = (Element)i.next();
				  temp=-1;
				  alternativeIDOK=true;
	           
				  if(courant.getChild("value").getChild("integer")!=null )
					  temp=Double.parseDouble(courant.getChild("value").getChild("integer").getValue());			 
				  	else
					  alternativeIDOK=false;

	         
				  if(alternativeIDOK==true)
				  {
					  if (alternativesIDs.contains(courant.getChild("alternativeID").getValue())){						  
						  ranks[alternativesIDs.indexOf(courant.getChild("alternativeID").getValue())]=temp;
						  nbrOfElements++;
					  }
						  else
						  // Warning pour dire que l'alternative n'est pas dans la liste
						  warningMessage= warningMessage.concat("Warning: The alternative:"+courant.getChild("alternativeID").getValue()+" was ignored in the entry file" +
						  		" 'alternativesRank' since it was not found in the alternatives list of the project.");
	         
				  }
				  else
				  {
	        	 warningMessage= warningMessage.concat("\n Error in the file containing the alternativesValues tag." +
		      		  		" The value must be an integer."+
		      		  		" We have eliminated the corresponding alternative:"+
		      		  		courant.getChild("alternativeID").getValue()+".");
				  }
			  }
			  
			  return  ranks;
		  	}

			  else return null;
		  	}
			  else
			  {
				  return null;
			  }
	   
	   }
	   
	   
	   public String getWarningMessage()
		{
			return warningMessage;
		}
	   
	   
	   
}
