package Main;

import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;


public class ParseParameters
{
	private String warning;
	private boolean doPostOptimalAnalysis;
	
	public ParseParameters()
	{
		warning="";
		doPostOptimalAnalysis=false;
		
	}
	
	 @SuppressWarnings("unchecked")
    public boolean parse(Element racine)
		{
			boolean statusOK=true;
			 if (racine.getChild("methodParameters")!=null)
			   {
				 Element methodParameters = racine.getChild("methodParameters");
				 if(methodParameters.getChildren("parameter")!=null)
				 {
					 List listParam = methodParameters.getChildren("parameter");
					 Iterator i = listParam.iterator(); 
					 while(i.hasNext())
					 {
						 Element courant = (Element)i.next();

						if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
				        		 equals("post_optimality"))
				        		 {
				        	 	
				        	 		if ((courant.getChild("value")!= null) && 
				        			 (courant.getChild("value").
							        	 		getChild("boolean")!=null) && courant.getChild("value").
						        	 			getChild("boolean").getValue().equals("true"))
				        	 			doPostOptimalAnalysis=true;
				        	 		else if ((courant.getChild("value")!= null) && 
						        			 (courant.getChild("value").
									        	 		getChild("boolean")!=null) && courant.getChild("value").
								        	 			getChild("boolean").getValue().equals("false"))
						        	 			doPostOptimalAnalysis=false;
				        	 		
				        	 		else
				        	 		{
				        	 			warning=warning.concat("\nWarning: an error occured while " +
					   			        	 			 		"trying to get the parameter 'Post-optimality analysis:'" +
					   				        		 		". The default value is set to 'false'.");
				        	 		}
				        	 		
				        		 }
						 else {// we have only the previous parameters in method parameters}
							 
							 statusOK=false;
			        	 }
					 }
				 }
				 else 
					   statusOK=false;
			   }
			 else 
				 statusOK=false;
			
			 
			 return statusOK;
		}
	 
	public String getWarning()
	{
		return warning;
	}
	
	public boolean postOptimatily()
	{
		return doPostOptimalAnalysis;
	}
	
	
}
