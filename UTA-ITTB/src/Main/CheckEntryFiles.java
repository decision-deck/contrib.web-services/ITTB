package Main;



import java.util.ArrayList;
//import java.util.TreeSet;
import java.util.TreeSet;



public class CheckEntryFiles {
	

	private String warningMessageCriter;
	private String warningMessageAltern;
	
	
	
	//Constructor
	
	public CheckEntryFiles()
	{
		warningMessageCriter="";
		warningMessageAltern="";
	
		
	}
	public boolean checkIDs(ArrayList<String >  list, RunUta perfTable,String tag)
	{
		boolean result=true;// les listes sont coh�rentes
		int diff=0;
		int size=-1;
		ArrayList<String> listFromPerf=new ArrayList<String>();
		if(tag.equals("alternatives"))
		{
			size=perfTable.getListAlternatives().size();
			for (String s : perfTable.getListAlternatives())
				listFromPerf.add(s);
		
		}
		else 
		{
			size=perfTable.getListCriteria().size();
			for (String s : perfTable.getListCriteria())
				listFromPerf.add(s);
		}
		if(list.size()!=size)
		{
			
			warningMessageAltern=warningMessageAltern.concat("\nPlease check the file containing the "+tag+" tag:" +
					" incoherence with the number of "+tag+" given " +
					"in the file containing the performanceTable tag.");
			result=false;
		}

		else //On a la m�me taille mais il faudra v�rifier si c'est les m�mes Ids
		{
			for (String s : list)
			{
				if(listFromPerf.contains(s))
				{
					// OK
				}
				else
				{
					result=false;
					diff++;
				}
			}
			if(diff>1)
			
				warningMessageAltern=warningMessageAltern.concat("\nPlease check the file containing the "+tag+" tag:" +
						"incoherence with the "+tag+" given " +
						"in the file containing the performanceTable tag. There are "+diff+" different "+tag+".");
		
			if(diff==1)
			{
				String temp="";
				if(tag.equals("alternatives"))
					
					temp="alternative";
				else
					temp="criterion";
					
				warningMessageAltern=warningMessageAltern.concat("\nPlease check the file containing the "+tag+" tag:" +
				                         						"incoherence with the "+tag+" given " +
						"in the file containing the performanceTable tag. There is one different "+temp+".");
			}
		}
					
		return result;		
	}

	
	
	
	public String getWarningCriteria()
	{
		return warningMessageCriter;
	}
	
	public String getWarningAlternatives()
	{
		return warningMessageAltern;
	}
	
	/***************************************************************
	 * 
	 * @return
	 */
	public ArrayList<String > getIntersectionIDs(ArrayList<String >  list, RunUta perfTable,String tag)
	{
		ArrayList<String > listintersection=new ArrayList<String >();

		if(tag.equals("alternatives"))
		{
			for (String t : perfTable.getListAlternatives()) 
		        if(list.contains(t)) 
		        	listintersection.add(t);
			
		
			
		}
		else
		{
			for (String t : perfTable.getListCriteria()) 
		        if(list.contains(t)) 
		        	listintersection.add(t);
		}
		
		// use TreeSet to eliminate the duplicates...
		listintersection = new ArrayList<String>
			(new TreeSet<String>(listintersection));
		

		return listintersection;
	}

	
}