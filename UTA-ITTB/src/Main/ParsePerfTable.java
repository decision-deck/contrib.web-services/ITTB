package Main;


import org.jdom2.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class ParsePerfTable{

	
	 static org.jdom2.Document document;
	 
	 public static final int size = 100;
	 private static float [][] matricePerf= new float [size][size];
	 private static int index_matrix_line=0;
     private static int index_matrix_column=0;
    
     private RunUta perfTable;
     private ArrayList <String> listCriterionIDInperformanceTable;
     private ArrayList <String> listAlternativeIDInperformanceTable;
 
	 
	 //Error message for the performance Table type
	 private String errorMessage;
	 private String warningMessage;
	 
     // Constructor
     public ParsePerfTable()
     {
    	 perfTable=new RunUta();
    	 listCriterionIDInperformanceTable= new  ArrayList <String>()  ;
    	 listAlternativeIDInperformanceTable=new  ArrayList <String>()  ;
    	 errorMessage="";
    	 warningMessage="";
     }
	 
     public ArrayList <String> getCriterionIDInperformanceTable()
     {
    	 return listCriterionIDInperformanceTable;
     }
     
     public ArrayList <String> getAlternativeIDInperformanceTable()
     {
    	 return listAlternativeIDInperformanceTable;
     }
     
    public  float [][] getMatrix()
    {
    	return  matricePerf;
    }
     @SuppressWarnings("unchecked")
	public  RunUta  explorePerformanceTable(Element racine) 
	   {	      

    	 if( racine.getChild("performanceTable")!=null){
    		
    	 
    	 Element perfTabl = racine.getChild("performanceTable");
    	 
    	 if(perfTabl.getChildren("alternativePerformances")!=null){
    		 
    		
    	 List listAltPerF =perfTabl.getChildren("alternativePerformances");
    	 Iterator i = listAltPerF.iterator();
    	 String temp="";
    	  int numberCriterMax=0;
	      while(i.hasNext())
	      {
	         
	    	  numberCriterMax=0;
	    	  Element courant = (Element)i.next();
	    	  if (listAlternativeIDInperformanceTable.contains(courant.
	        		 getChild("alternativeID").getValue())){}    	  
	    	  else	 
	    		  listAlternativeIDInperformanceTable.add(courant.
 	        		 getChild("alternativeID").getValue());
	         
	    	 index_matrix_line=listAlternativeIDInperformanceTable.
             indexOf(courant.getChild("alternativeID").getValue());

	         List listPerfObjects =courant.getChildren("performance");
		     Iterator j = listPerfObjects.iterator();
		    
		     if (courant.getChildren().size()>numberCriterMax);
	    	  numberCriterMax=courant.getChildren().size()-1;	    	  
		     
		     boolean alternativeIDOK=true; 
		  
		     while(j.hasNext())
		      {
		    	  Element courantNiv2 = (Element)j.next();
		    	  temp="";
		    	 
		    	  if(courantNiv2.getChild("value").
		    			  getChild("real")!=null)
		    	  {
		    		  temp=courantNiv2.getChild("value").
 			  			getChild("real").getValue();  
		    	  }
		    	  else if(courantNiv2.getChild("value").
		    			  getChild("integer")!=null)
		    	  {
		    		  temp=courantNiv2.getChild("value").
			  			getChild("integer").getValue();
		    	  }
		    	  else
		    	  {
		    		 
		    		  alternativeIDOK=false;
		    	  }

		    	  if (listCriterionIDInperformanceTable.contains(courantNiv2.
			    			 getChild("criterionID").getValue())){}
		    	  
		    	  else
		    	  {
		    		  listCriterionIDInperformanceTable.add(courantNiv2.
			    			 getChild("criterionID").getValue());
		    	  }
		    	 if(temp!="")
		    	 {		    	  	 
		    		 index_matrix_column=listCriterionIDInperformanceTable.
		    		 indexOf(courantNiv2.
		    			 getChild("criterionID").getValue());
		    		 matricePerf [index_matrix_line][index_matrix_column]=
					 Float.parseFloat(temp);
		    	 
		    	 }
		    	 
		      }
			 if(alternativeIDOK){} 
		     else 
	    	  {
	    		  listAlternativeIDInperformanceTable.remove(courant.
	  	        		 getChild("alternativeID").getValue()); 
	    		  
	    		 
    		  errorMessage=errorMessage.concat("Error in the file containing the performance " +
    		  		"table tag. The value is neither real nor integer."+
    		  		"We have eliminated the corresponding alternative:"+
    		  		courant.getChild("alternativeID").getValue()+".");
    		  
    		    		  
	    	  }
			 
		   // On passe � une autre alternative
		      
	      }
	     
	      // V�rifier que toutes les alternatives 
	      //retenues ont le m�me nombre de crit�res
	      	i = listAltPerF.iterator();
		      while(i.hasNext())
		      {
		    	  Element courant = (Element)i.next();
		    	  String id=courant.getChild("alternativeID").getValue();
		    	  int sizeInferior=courant.getChildren().size()-1;
		    	 
		     if(listAlternativeIDInperformanceTable.contains(id))
		     {
		    	 
		    	  if (sizeInferior<numberCriterMax)
		    	  {
		    	  listAlternativeIDInperformanceTable.remove(id); 
		    	  
		    	  /*System.out.println("\n////The alternativeID "+id+" was removed because " +
			    	  		"the number of criteria is "+sizeInferior+
			    	  		" instead of "+numberCriterMax+".");*/
		    	  
		    	  warningMessage=warningMessage.concat
		    	  ("\nThe alternativeID "+id+" was removed because " +
		    	  		"the number of criteria is "+sizeInferior+
		    	  		" instead of "+numberCriterMax+".");
		    	  }

		     }

	   }  

	      perfTable=new RunUta(listAlternativeIDInperformanceTable,
	    		  listCriterionIDInperformanceTable,matricePerf);
	      
	      
	      //perfTableConfig2.printPerfTable_Config2();
	      return perfTable;  
	   }
     
    	 else return null;
    	 }
     else return null;
	 
	   }
     
     
     public String getErrorMessage()
     {
    	 return errorMessage;
     }
     public String getWarningMessage()
     {
    	 return warningMessage;
     }
     public  ArrayList <String>  getList(String id)
     {
     	 
    	 ArrayList <String> result=new  ArrayList <String>();
    	 if(id.equals("alternativeID"))
    		 result=listAlternativeIDInperformanceTable;
     	 else if(id.equals("criterionID"))
     		 result=listCriterionIDInperformanceTable;
     	 
     	 else
     	 {
     		 System.out.println("Error: which list you want" +
     		 		"from performance table?\n alternativeID? Or criterionID?");
     	 }
    	 
    	 return result;
     } 
}
