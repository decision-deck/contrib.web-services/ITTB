package Main;


//import helpers.AssertHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import utafx.Alternative;
import utafx.Criterion;
import utafx.LinearFunction;
import utafx.Ranking;
import utafx.UtaStarSolver;



public class RunUta
{
	static org.jdom2.Document document;
	static Element racine;
	
	private String error;
	private String warning;
	
	
	private ArrayList<String >listCriterionID;
	private ArrayList<String >listAlternativeID;
	private float [][] matricePerf;
	
	private static final String XMCDA_2_0_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.0.0";

	private static final String XMCDA_2_0_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd";

	private static final String XMCDA_2_1_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.1.0";

	private static final String XMCDA_2_1_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd";
	
	private static final String XMCDA_2_2_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.2.0";

	private static final String XMCDA_2_2_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd";

	private static final String ACCEPTED_SCHEMA_LOCATIONS = XMCDA_2_0_0_NAMESPACE + " " + XMCDA_2_0_0_SCHEMA + " "
	                                                       + XMCDA_2_1_0_NAMESPACE + " " + XMCDA_2_1_0_SCHEMA + " "
	                                                       + XMCDA_2_2_0_NAMESPACE + " " + XMCDA_2_2_0_SCHEMA;
	
	
	///////////////////////////////
	///////////////////////////////
	public RunUta()
	{
		error="";
		warning="";
		
		listCriterionID=new ArrayList<String >();
		listAlternativeID=new ArrayList<String >();
		matricePerf=new float [100][100];
		
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public RunUta(ArrayList<String>listAlt,
	                    			ArrayList<String>listCrit,float [][]matricePerfToUse)
	                    	{
	                    		
								error="";
								warning="";
								
								this.listCriterionID=new ArrayList<String >();
								this.listAlternativeID=new ArrayList<String >();
								for(int i=0;i<listAlt.size();i++)
									this.listAlternativeID.add(listAlt.get(i));
								for(int i=0;i<listCrit.size();i++)
									this.listCriterionID.add(listCrit.get(i));
	                    		matricePerf= new float [listAlt.size()][listCrit.size()];
	                    		 for(int l=0;l<listAlt.size();l++)
	                    			   for(int c=0;c<listCrit.size();c++)
	                    				   matricePerf[l][c]=matricePerfToUse[l][c];
	                    			
	                    	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	protected float getMatrixElement(int indexAlt,int indexCrit)
	{
		return matricePerf[indexAlt][indexCrit];
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getError()
	{
		return error;
		
	}
	//////////////////////////////
	//////////////////////////////
	public String getWarning()
	{
		return warning;
		
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	protected static String checkEntryFile(String path)
	{
		
		String error="";
		try {
				parseXML(path);
		
		} catch (JDOMException e) {
		
			error=error.concat("File is not well-formed: "+e.getMessage());
		} catch (IOException e) {
			
			error=error.concat("Could not check file because: "+e.getMessage());
		}
		return error;
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	protected static void  parseXML(final String xml) throws JDOMException, IOException {
	    SAXBuilder builder = 
	    	new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
	    builder.setFeature(
	    		"http://apache.org/xml/features/validation/schema", true);
	    
	    builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
		                    ACCEPTED_SCHEMA_LOCATIONS);
	
	    
	   
	  @SuppressWarnings("unused")
	Document doc= builder.build(xml);
	 
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	protected static  void PrepareParsing(String path)
	{
	
		try
     {
		   SAXBuilder sxb = new SAXBuilder();
		   document = sxb.build(new File(path));
     }
     catch(Exception e){}
     racine = document.getRootElement();
  
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public RunUta checkCoherentEntryFiles(RunUta perfTableAfterParsingXML,ArrayList<String > listAlternatives, ArrayList<String > listCriteria)
	{
	    
		String warning="";
		/*for(int i=0;i<listAlternatives.size();i++)
			System.out.println("Alternative: "+listAlternatives.get(i));
		for(int i=0;i< listCriteria.size();i++)
			System.out.println("Criterion: "+ listCriteria.get(i));
		
		for(int i=0;i<perfTableAfterParsingXML.getListAlternatives().size();i++)
			System.out.println("Perf table Alternative: "+perfTableAfterParsingXML.getListAlternatives().get(i));
		for(int i=0;i<perfTableAfterParsingXML.getListCriteria().size();i++)
			System.out.println("Perf Table criterion : "+perfTableAfterParsingXML.listCriterionID.get(i));*/
		
		/**************************************************************************/ 
		/*********La gestion d'incoh�rences entre les diff�rents fichiers**********/
		/**************************************************************************/ 
	   
		CheckEntryFiles check=new CheckEntryFiles();
		boolean checkAlt=check.checkIDs(listAlternatives,perfTableAfterParsingXML,"alternatives");
		boolean checkCrit=check.checkIDs(listCriteria,perfTableAfterParsingXML,"criteria");
		if (!checkAlt)
			warning=warning.concat(check.getWarningAlternatives());
		if(!checkCrit)
			warning=warning.concat(check.getWarningCriteria());
	                            	  
		// Dans ce cas il faudra se mettre d'accord sur la perfTable finale
	                            	  
		//////////////////listintersectionAlternatives//////////////////
	    ArrayList<String > listintersectionAlternatives=check.getIntersectionIDs(listAlternatives,perfTableAfterParsingXML,"alternatives");
	                            	  
	   // Si l'intersection est vide on prend ce qu'on a dans la perfTable
	    if (listintersectionAlternatives.isEmpty())
	    {
	    	warning=warning.concat("\nOnly the alternativesIDs " +
	    	                                     "in the file containing the performance table tag were considered. " +
	                            		  		"Please check your input file containing the alternatives tag.");
	                        
	    	
	    	for(int i=0;i<perfTableAfterParsingXML.getListAlternatives().size();i++)
	    	{
	    		//System.out.println(perfTableAfterParsingXML.getListAlternatives().get(i));
	    		listintersectionAlternatives.add(perfTableAfterParsingXML.getListAlternatives().get(i));
	    	}
	    }
	                            		  
	    //////////////////listintersectionCriteria//////////////////
	    ArrayList<String > listintersectionCriteria=check.getIntersectionIDs(listCriteria,perfTableAfterParsingXML,"criteria");
	                            	  
	    // Si l'intersection est vide on prend ce qu'on a dans la perfTable
	    if (listintersectionCriteria.isEmpty())
	    {
	    	warning=warning.concat("\nOnly the criterionIDs " +
	                              		  		"in the file containing the performance table tag were considered. " +
	                              		  		"Please check your input file containing the criteria tag.");
	    	
	    	for(int i=0;i<perfTableAfterParsingXML.getListCriteria().size();i++)
	    	{
	    		System.out.println(perfTableAfterParsingXML.getListCriteria().get(i));
	    		listintersectionCriteria.add(perfTableAfterParsingXML.getListCriteria().get(i));
	    	}
	    }
	                            	  
	                            	
	                            	  
	    ////////////TheMatrix to consider in the project/////////////

	    float [][] matricePerf=new float [listintersectionAlternatives.size()][listintersectionCriteria.size()];
	    for(int l=0;l<listintersectionAlternatives.size();l++)
	    	for(int c=0;c<listintersectionCriteria.size();c++)	                            			  
	    		matricePerf[l][c]=perfTableAfterParsingXML.getMatrixElement(perfTableAfterParsingXML.getListAlternatives().indexOf
	                          						   (listintersectionAlternatives.get(l)),perfTableAfterParsingXML.getListCriteria().indexOf
	                          						 (listintersectionCriteria.get(c)));

	                            	  
	    // ///////Voici la PerformanceTable du projet//////////////////
	                            	  
	                            	  
	    RunUta result=new RunUta(listintersectionAlternatives,listintersectionCriteria,matricePerf);
	    result.warning=warning;

	    return result;
	    }
	
	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	protected static double getMin(RunUta projectUta, int indexCriterion)
	{
		double min=projectUta.getMatrixElement(0, indexCriterion);
		for(int i=1; i<projectUta.getListAlternatives().size();i++)
		{
			double temp=projectUta.getMatrixElement(i, indexCriterion);
			if(temp<min)
			min=temp;
		}
		return min;
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	protected static double getMax(RunUta projectUta, int indexCriterion)
	{
		double max=projectUta.getMatrixElement(0, indexCriterion);
		for(int i=1; i<projectUta.getListAlternatives().size();i++)
		{
			double temp=projectUta.getMatrixElement(i, indexCriterion);
			if(temp>max)
			max=temp;
		}
		return max;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	protected static double getValue(String worstBest,RunUta projectUta, int indexCriterion, String preferenceDirection)
	{
		double bestWorst=0;
		if(worstBest.equals("worst"))
			{
				if(preferenceDirection.equals("min"))
					bestWorst=getMax(projectUta,indexCriterion);
				else if(preferenceDirection.equals("max"))
					bestWorst=getMin(projectUta,indexCriterion);
				else {/*Les seules possibilit�s dans mon programme sont 'min' et 'max'*/}
			}
			else if(worstBest.equals("best"))
			{
				if(preferenceDirection.equals("min"))
					bestWorst=getMin(projectUta,indexCriterion);
				else if(preferenceDirection.equals("max"))
					bestWorst=getMax(projectUta,indexCriterion);
				else {/*Les seules possibilit�s dans mon programme sont 'min' et 'max'*/}
				
			}
			else {/*Les seules possibilit�s dans mon programme sont 'worst' et 'best'*/}
			
		return bestWorst;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	protected static List<Criterion>  createCriteria(RunUta projectUta,int []segments,String [] preferenceDirection)
	{
		List<Criterion> criteria = new ArrayList<Criterion>();
		
		for(int i=0;i<projectUta.getListCriteria().size();i++)
		{
			double worstValue=getValue("worst",projectUta,i,preferenceDirection[i]);
			double bestValue=getValue("best",projectUta,i,preferenceDirection[i]);			
			criteria.add(new Criterion(worstValue, bestValue, segments[i]));
		}
		
		return criteria ;
		
	}
	///////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////
	protected static Alternative[]  createActions(RunUta projectUta,List<Criterion> criteria)
	{
		Alternative [] alternatives = new Alternative [projectUta.getListAlternatives().size()];
		
		for(int i=0;i<projectUta.getListAlternatives().size();i++)
		{
			
			int nbrOfCriteria=projectUta.getListCriteria().size();
			double []values=new double[nbrOfCriteria];
			for(int j=0;j<nbrOfCriteria;j++)
				values[j]=projectUta.getMatrixElement(i, j);
			Alternative alter = new Alternative(values, criteria);	
			alternatives[i]=alter;
			//System.out.println("i="+i+"Altern ID"+projectUta.getListAlternatives().get(i));
		}
		
		return alternatives ;
		
	}
	
	protected static int getNumberOfElements(double[]ranks)
	{
		int nbrOfElements=0;
		for(int i=0;i< ranks.length;i++)
			if(ranks[i]!=-1)
				nbrOfElements++;
		
	return nbrOfElements;
		
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	//protected static List<Alternative> getAlternativesForUTA(double []ranks,Alternative [] alternatives)
	protected static Alternative []getAlternativesForUTA(double []ranks,Alternative [] alternatives)
	{
		
		int nbrOfElements=getNumberOfElements(ranks);
		
		Alternative []result=new Alternative [nbrOfElements];
		//List<Alternative> result =new ArrayList <Alternative>();
		
		int index=0;
		for(int i=0;i< ranks.length;i++)
		{
			if(ranks[i]!=-1)
			{
				result[index]=alternatives[i];
				//result.add(alternatives[i]);
				index++;
			}
		}
		return result;
	}
	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////
	protected static double[] getRanksForUTA(double[]ranks)
	{
		int nbrOfElements=getNumberOfElements(ranks);		
		double []result=new double [nbrOfElements];
		int index=0;
		for(int i=0;i< ranks.length;i++)
		{
			if(ranks[i]!=-1)
			{
				result[index]=ranks[i];
				index++;
			}
		}
		return result;
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	protected static void ordonate(double[] ranksForUTA, Alternative [] alternativesWithRanks)
	{
		for(int i=0;i<ranksForUTA.length-1;i++)
		{
			for(int j=i+1;j<ranksForUTA.length;j++)
			{
				if(ranksForUTA[i]>ranksForUTA[j])
				{
					double temp=ranksForUTA[j];
					ranksForUTA[j]=ranksForUTA[i];
					ranksForUTA[i]=temp;
					Alternative tempAlt=alternativesWithRanks[j];
					alternativesWithRanks[j]=alternativesWithRanks[i];
					alternativesWithRanks[i]=tempAlt;
					
					           
					                       
				}
			}
		}
		
		/*for(int i=0;i<ranksForUTA.length;i++)
			System.out.println("ordonate ranksForUTA["+i+"]="+ranksForUTA[i]);*/
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	protected static void round (LinearFunction[] actuals,int number)
	{
		for (int i=0;i<actuals.length;i++)
		{
			//Pour chaque fonction
			int nbrOfPoints=actuals[i].getNoOfPoints();
			for (int j=0;j<nbrOfPoints;j++)
			{
				//Arrondir X
				double x=actuals[i].getPoints().get(j).getX();
				x=Math.round(x*100.)/100.;
				actuals[i].getPoints().get(j).setX(x);
				
				//Arrondir Y
				double y=actuals[i].getPoints().get(j).getY();
				y=Math.round(y*100.)/100.;
				actuals[i].getPoints().get(j).setY(y);
			}
			
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	//Main function
	public static void main(String[] argv)
	   {
		
		RunUta uta=new RunUta();
		boolean stopRunning = false;
		
	
		if (!new File(argv[0]).exists() || !new File(argv[1]).exists() || !new File(argv[2]).exists()||!new File(argv[3]).exists()||!new File(argv[4]).exists()||!new File(argv[5]).exists()
				|| argv.length != 8) stopRunning = true;


		if (stopRunning)
			uta.error = "Error: unable to run the algorithm. "
		                       + "Reason: missing one (or more) entry file(s). "
		                       + "Please check your entry files.";
	  
	   /*System.out.println(new File(argv[0]).exists() +" "+new File(argv[1]).exists()+" "+new File(argv[2]).exists()+" "+new File(argv[3]).exists() 
	                      +" "+new File(argv[4]).exists()+" "+new File(argv[5]).exists()+" "+ argv.length);*/
	   
		if (stopRunning == false)
		{
			
			
			String pathParameters = argv[0];
			String pathAlternatives = argv[1];
			String pathAlternativesRanking = argv[2];
			String pathCriteria = argv[3];
			String pathPerfTable = argv[4];
			//String pathPrefDirec = argv[argv.length - 4];
			String pathCriterSegments=argv[5];

			
			String errMethodParameters=checkEntryFile(pathParameters);
			String errAltern =checkEntryFile(pathAlternatives);
			String errAlternRanking =checkEntryFile(pathAlternativesRanking);
			String errCriter =checkEntryFile(pathCriteria);
			String errPerfTable = checkEntryFile(pathPerfTable);
			//String errPrefDirec = checkEntryFile(pathPrefDirec);
			String errCriterSegments = checkEntryFile(pathCriterSegments);
			
			/*if(errPrefDirec!="")
				uta.warning = uta.warning.concat("\n"+errPrefDirec);*/
			
			if(errMethodParameters!="")
				uta.error=uta.error.concat("\nFatal Error: the file containing the methodParameters " +
				"tag is not a valid XMCDA document.");
			
			if(errAltern!="")
				uta.error=uta.error.concat("\nFatal Error: the file containing the alternatives " +
				"tag is not a valid XMCDA document.");
			
			if(errAlternRanking!="")
				uta.error=uta.error.concat("\nFatal Error: the file containing the alternativesValues (alternativesRanking) " +
				"tag is not a valid XMCDA document.");
			
			if(errCriter!="")

				uta.error=uta.error.concat("\nFatal Error: the file containing the criteria " +
				"tag is not a valid XMCDA document.");
			
			if(errPerfTable!="")
				uta.error=uta.error.concat("\nFatal Error: the file containing the performanceTable " +
				"tag is not a valid XMCDA document.");
			
			if(errCriterSegments!="")
				uta.error=uta.error.concat("\nFatal Error: the file containing the criteriaValues (criteriaSegments) " +
				"tag is not a valid XMCDA document.");
			
			if(errMethodParameters!="" ||errAltern!=""||errAlternRanking!=""||errCriter!=""||errPerfTable!=""||errCriterSegments!="")
			{
				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createErrorMessage(uta.getError());
				String pathOutputMessage = argv[argv.length - 1];
				xmcdaMess.save(pathOutputMessage);
			}
			else
			{
			/*if(errPrefDirec!="")
			uta.warning = uta.warning.concat("\n"+errPrefDirec);*/
			
			
			
			// Parsing the file parameters.xml
			PrepareParsing(pathParameters);
			ParseParameters parseParam = new ParseParameters();
			boolean parseParametersNull = false;
			boolean statusParameters=parseParam.parse(racine);
			if(!statusParameters)
			{
				parseParametersNull = true;
				uta.error= "\nFatal Error: the file containing the method parameters "
	                   + "tag is erroneous or empty.";
			}
			if(parseParam.getWarning()!="")
			{
				if (uta.warning.equals(""))
					uta.warning = "\n" + parseParam.getWarning();
				else
					uta.warning = uta.warning.concat("\n" + parseParam.getWarning());
			}
			
			boolean doPostOptimalAnalysis=parseParam.postOptimatily();
			
			///////////////////////////////////////////Complete here please///////////////////////////////////////////////////////////////////////
			
			// Parsing the file alternatives.xml
			PrepareParsing(pathAlternatives);
			ParseAlternatives parseAlter = new ParseAlternatives();
			boolean parseAlternativesNull = false;
			ArrayList<String> alterIds = parseAlter.exploreAlternativesXML(racine);
			if (parseAlter.getWarning() != "")
			{

				if (uta.warning.equals(""))
					uta.warning = "\n" + parseAlter.getWarning();
				else
					uta.warning = uta.warning.concat("\n" + parseAlter.getWarning());
			}
			if ( alterIds!= null){}

			else
			{
				parseAlternativesNull = true;
				// Error message
				if (uta.error.equals(""))
					uta.error= "\nFatal Error: the file containing the alternatives "
					                   + "tag is erroneous or empty.";
				else
					uta.error= uta.error.concat("\nFatal Error: the file containing the"
					                                           + " alternatives tag is erroneous or empty.");
			}
			

			// Parsing the file criteria.xml
			PrepareParsing(pathCriteria);
			ParseCriteria parseCriter = new ParseCriteria();
			boolean parseCriterNull = false;
			ArrayList<String> criterIds = parseCriter.exploreCriteriaXML(racine);
			String [] parsePreferenceDirection=parseCriter.getPreferenceDirections();
			if (parseCriter.getWarning() != "")
			{

				if (uta.warning.equals(""))
					uta.warning = "\n" + parseCriter.getWarning();
				else
					uta.warning = uta.warning.concat("\n" + parseCriter.getWarning());
			}
			if (criterIds != null){}

			else
			{
				parseCriterNull = true;
				// Error message
				if (uta.error.equals(""))
					uta.error= "\nFatal Error: the file containing the criteria "
					                   + "tag is erroneous or empty: "+parseCriter.getWarning();
				else
					uta.error= uta.error.concat("\nFatal Error: the file containing the"
					                                           + " criteria tag is erroneous or empty: "+parseCriter.getWarning());
			}
			
			
			// Parsing the file performanceTable.xml
			PrepareParsing(pathPerfTable);
			ParsePerfTable jdomPerfTable = new ParsePerfTable();
			boolean parsePerfTableNull = false;
			RunUta perfTable = jdomPerfTable.explorePerformanceTable(racine);
			/*RunUta perfTable =new RunUta(jdomPerfTable.getAlternativeIDInperformanceTable(),
			                             jdomPerfTable.getCriterionIDInperformanceTable(),
			                             jdomPerfTable.getMatrix());*/
			/*for(int indexAlt=0;indexAlt<jdomPerfTable.getAlternativeIDInperformanceTable().size();indexAlt++)
				System.out.println(jdomPerfTable.getAlternativeIDInperformanceTable().get(indexAlt));*/
			if (jdomPerfTable.getErrorMessage() != "")
				uta.warning = uta.warning.concat("\n" + jdomPerfTable.getErrorMessage());
			if (jdomPerfTable.getWarningMessage() != "")
				uta.warning = uta.warning.concat("\n" + jdomPerfTable.getWarningMessage());

			if (perfTable != null){}
			else
			{
				parsePerfTableNull = true;
				// Error message
				if (uta.error.equals(""))
					uta.error ="\nFatal Error: the file containing the performanceTable "
					                   + "tag is erroneous or empty.";
				else
					uta.error = uta.error.concat("\nFatal Error: the file containing the"
					                                           + " performanceTable tag is erroneous or empty.");
			}
			
		
			
			if (parseCriterNull == false && parseAlternativesNull == false && parsePerfTableNull == false && parseParametersNull==false)
			{
				// Check coherent entry files
				
				/*for(int i=0;i<alterIds.size();i++)
					System.out.println("Alternative: "+alterIds.get(i));
				for(int i=0;i< criterIds.size();i++)
					System.out.println("Criterion: "+ criterIds.get(i));*/
				RunUta projectUta=uta.checkCoherentEntryFiles(perfTable, alterIds, criterIds);
				if(projectUta.getError()!="")
					projectUta.error=uta.error+"\n"+projectUta.getError();
				else
					projectUta.error=uta.error;
					
				if(projectUta.getWarning()!="")
					projectUta.warning=uta.warning+"\n"+projectUta.getWarning();
				else
					projectUta.warning=uta.warning;
					
				
				
				// D�rouler l'lgorithme
				
				//Parse preferenceDirection
				/*PrepareParsing(pathPrefDirec);
				parsePrefDirec parsePrefDirec = new parsePrefDirec();
				boolean parsePrefDirecNull = false;
				String [] parsePreferenceDirection=parsePrefDirec.parsePreferenceDirectin(racine,projectUta.getListCriteria() );
				if (parsePrefDirec.getWarningMessage()!= "")
					projectUta.warning = projectUta.warning.concat("\n" + parsePrefDirec.getWarningMessage());

				if (parsePreferenceDirection != null){
					//System.out.println("OK pref direc");
				}
				else
				{
					parsePrefDirecNull = true;
					//System.out.println("Failure pref direc");
					// Error message
					if (projectUta.error.equals(""))
						projectUta.error ="\nFatal Error: the entry file 'preferencesDirections' is erroneous or empty.";
					else
						projectUta.error = projectUta.error.concat("\nFatal Error: the entry file 'preferencesDirections' is erroneous or empty.");
				}*/
				
				
				//Parse segments
				PrepareParsing(pathCriterSegments);
				parsePrefDirec parseSegments = new parsePrefDirec();
				boolean parseSegmentsNull = false;
				int[] segments=parseSegments.parseSegments(racine,projectUta.getListCriteria() );
			
				if (parseSegments.getWarningMessage()!= "")
					projectUta.warning = projectUta.warning.concat("\n" + parseSegments.getWarningMessage());

				if (segments != null){
					//System.out.println("OK segments");
				}
				else
				{
					//System.out.println("Failure segments");
					parseSegmentsNull = true;
					// Error message
					if (projectUta.error.equals(""))
						projectUta.error ="\nFatal Error: the entry file 'criteriaSegments' is erroneous or empty.";
					else
						projectUta.error = projectUta.error.concat("\nFatal Error: the entry file 'criteriaSegments' is erroneous or empty.");
				}
				
				// Parsing the file alternativesRank.xml
				PrepareParsing(pathAlternativesRanking);
				ParseAlternativesRank parseAlterRank = new ParseAlternativesRank();
				boolean parseAlternativesRankNull = false;
				double[] ranks=parseAlterRank.getRanks(racine,projectUta.getListAlternatives());
				if (parseAlterRank.getWarningMessage()!= "")
					projectUta.warning = projectUta.warning.concat("\n" + parseAlterRank.getWarningMessage());
				if (ranks != null){}
				else
				{
					parseAlternativesRankNull = true;
					// Error message
					if (projectUta.error.equals(""))
						projectUta.error ="\nFatal Error: the entry file 'alternativesRanks' is erroneous or empty.";
					else
						projectUta.error = projectUta.error.concat("\nFatal Error: the entry file 'alternativesRanks' is erroneous or empty.");
				}
				
				
				if (projectUta.getListAlternatives().size() != 0 && projectUta.getListCriteria().size() != 0)
				{
					// D�rouler l'lgorithme
					
					
					//if(!parsePrefDirecNull)
					{
						// Dans ce cas je continue de d�rouler l'algorithme
						
						
					
						if(!parseSegmentsNull)
						{
							// Dans ce cas je continue de d�rouler l'algorithme
							UtaStarSolver starSolver = new UtaStarSolver(doPostOptimalAnalysis);
							
							// Create criteria
							List<Criterion> criteria =createCriteria(projectUta,segments,parsePreferenceDirection);
							// Create actions
							Alternative [] alternatives=createActions(projectUta, criteria);														
							// Create ranking
							
							

							if(!parseAlternativesRankNull)
							{
								
								// Dans ce cas je continue de d�rouler l'algorithme
								
								Alternative [] alternativesWithRanks=getAlternativesForUTA(ranks, alternatives);
								//List<Alternative> alternativesWithRanks=getAlternativesForUTA(ranks, alternatives);
								double[] ranksForUTA=getRanksForUTA(ranks);
								// Consid�rer juste les alternatives avec un ranking
								ordonate(ranksForUTA, alternativesWithRanks);
								Ranking<Alternative> ranking = new Ranking<Alternative>(ranksForUTA, alternativesWithRanks);
								/*for (int i = 0; i < alternativesWithRanks.length; i++)
									System.out.println("i:"+i+" alts:" +alternativesWithRanks[i]+" ranking:"+ ranksForUTA[i]);
								//System.out.println("ranksForUTA:"+ranksForUTA.length+"alternativesWithRanks:"+alternativesWithRanks.length);*/
																
								List<Alternative> alternativesWithRanksList=new ArrayList<Alternative>();
								for(int i=0;i<alternativesWithRanks.length;i++)
									alternativesWithRanksList.add(alternativesWithRanks[i]);
								
								
								
								/*System.out.println("Nbr Of criteria: "+criteria .size()+" Nbr Of rankedAlternatives:"+
								                   alternativesWithRanksList.size()+" Ranks:"+ranksForUTA.length);*/
								
								//EXAMPLE:
								// Create criteria
								/*Criterion price = new Criterion(30, 2, 2);
								Criterion time = new Criterion(40, 10, 3);
								Criterion comfort = new Criterion(0, 3, 3);

								List<Criterion> criteriaTest = new ArrayList<Criterion>();
								criteriaTest.add(price);
								criteriaTest.add(time);
								criteriaTest.add(comfort);

								// Create actions
								Alternative rer = new Alternative(new double[] { 3, 10, 1 }, criteriaTest);
								Alternative metro1 = new Alternative(new double[] { 4, 20, 2 }, criteriaTest);
								Alternative metro2 = new Alternative(new double[] { 2, 20, 0 }, criteriaTest);
								Alternative bus = new Alternative(new double[] { 6, 40, 0 }, criteriaTest);
								Alternative taxi = new Alternative(new double[] { 30, 30, 3 }, criteriaTest);
								for (int i = 0; i < criteriaTest.size(); i++) 
									System.out.println("Name:"+criteriaTest.get(i).getName());
									
								// Create ranking
								Ranking<Alternative> rankingTest = new Ranking<Alternative>(new double[] { 1, 2,3, 4, 5}, rer, metro1, metro2, bus, taxi);
								LinearFunction[] actuals = starSolver.solve(rankingTest, criteriaTest);
								double[][] expecteds = new double[3][];
								expecteds[0] = new double[] { 0, 0.5, 0.5 };
								expecteds[1] = new double[] { 0, 0.05, 0.05, 0.1 };
								expecteds[2] = new double[] { 0, 0, 0, 0.4 };

								for (int i = 0; i < expecteds.length; i++) 
									AssertHelper.assertArraysEqual(expecteds[i], actuals[i].getValues());
								
								
								
								//Normalement j'obtiens autant de LinearFunctions que le nombre de crit�res
								// Construction du fichier de sortie valueFunctions
								OutputValFunc output = new OutputValFunc ();
								
								ArrayList<String> testCriteria3=new ArrayList<String> ();
								testCriteria3.add("g1");
								testCriteria3.add("g2");
								testCriteria3.add("g3");
								
								System.out.println("Nbr Of criteria: "+criteriaTest.size()+" Nbr Of rankedAlternatives:"+
								                   rankingTest.getAlternatives().size()+" Ranks:"+rankingTest.getNumberOfRanks());
							
								output.createOutputFile(actuals, testCriteria3);*/
								LinearFunction[] actuals = starSolver.solve(ranking, criteria);
								//Arrondir � deux chiffres apr�s la virgule
								round(actuals,2);
								OutputValFunc output = new OutputValFunc ();							
								output.createOutputFile(actuals, projectUta.getListCriteria());
								
								String pathOutputFile = argv[argv.length - 2];
								output.save(pathOutputFile);								
							}
							
						}
					}
					
				}
				else
					projectUta.error =projectUta.error.concat("\n We cannot run the algorithm "
					                                           + "since the criteria list or the alternatives list "
					                                           + "of the PROJECT is empty.\n");
				
				
				if(!parseSegmentsNull && !parseAlternativesRankNull)
				{
					// Output Log Message File
					OutputMessage xmcdaMess = new OutputMessage();
					xmcdaMess.createLogMessage(projectUta.warning, projectUta.error);
					String pathOutputMessage = argv[argv.length - 1];
					xmcdaMess.save(pathOutputMessage);
				}
				
				else
				{
					// Output Log Message File
					OutputMessage xmcdaMess = new OutputMessage();
					xmcdaMess.createErrorMessage(projectUta.error);
					String pathOutputMessage = argv[argv.length - 1];
					xmcdaMess.save(pathOutputMessage);
				}
				
			}
			else
			{
				// Output Log Message File
				OutputMessage xmcdaMess = new OutputMessage();
				xmcdaMess.createErrorMessage(uta.getError());
				String pathOutputMessage = argv[argv.length - 1];
				xmcdaMess.save(pathOutputMessage);
			}

		}
		}// StopRunning =false
		else
		{
		// StopRunning =true
		OutputMessage xmcdaMess = new OutputMessage();
		xmcdaMess.createErrorMessage(uta.getError());
		String pathOutputMessage = argv[argv.length - 1];
		xmcdaMess.save(pathOutputMessage);

	}
	   
	   
	   }
	
	public ArrayList<String > getListAlternatives()
	{
		return listAlternativeID ;
	}
	public ArrayList<String > getListCriteria()
	{
		return listCriterionID ;
	}
}
