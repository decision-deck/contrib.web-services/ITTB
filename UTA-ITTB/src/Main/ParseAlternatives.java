package Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Element;

public class ParseAlternatives
{
	private String warning;
	private ArrayList<String> listAlternativesIDs;
	public ParseAlternatives()
	{
		warning="";
		listAlternativesIDs=new ArrayList<String>();
	}
	
	@SuppressWarnings("unchecked")
    public ArrayList<String> exploreAlternativesXML(Element racine)
	   {	      
		   
		  if(racine.getChild("alternatives")!=null)
		  {
			  Element alternatives = racine.getChild("alternatives");	    
			 
			  if(alternatives.getChildren("alternative")!=null){
				  List listAlternatives = alternatives.getChildren("alternative");
				  Iterator i = listAlternatives.iterator();
				  while(i.hasNext())
				  {
					  Element courant = (Element)i.next();
					  if (courant.getAttributeValue("id")!=null)
					  {
						  if(courant.getChild("active")!=null && 
								  courant.getChild("active").getValue().equals("false"))
						  {
							  //Inactive alternative, on l'ignore
							  warning=warning.concat("The alternative "+courant.getAttributeValue("id")+" was ignored " +
							  "because it is not active: active=false.\n");
						  }
						  else 
						  {
							  if(! listAlternativesIDs.contains(courant.getAttributeValue("id")))
								  listAlternativesIDs.add(courant.getAttributeValue("id"));
							  else
								  warning=warning.concat(" Warning: duplicated alternative "+courant.getAttributeValue("id")+".");
								  
						  }
					
					  }
					 
		         
					  else
					  {
						  // Warning, on n'a pas d'ID 
						  warning=warning.concat(" Error in the file containing the alternatives tag: no attribue 'id' detected.");
					  }
	         
				  }
	     
	     
				  if(listAlternativesIDs.size()==0)
					  return null ;
				  else
				  {
					  return listAlternativesIDs;
				  }
			  }
	  
			  else
			  {
				  return null;
			  }
		  	}
		  	else
		  	{
		  		return null;
		  	}

	   }
	

	 public String getWarning()
	  {
		  return warning;
	  }
	 public ArrayList<String> getAlternIds()
	 {
		 return listAlternativesIDs;
	 }
}
