package Main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Element;

public class ParseCriteria
{
	private String warning;
	private ArrayList<String> listCriteriaIDs;
	private String [] parsePreferenceDirectin;
	
	
	public ParseCriteria()
	{
		warning="";
		listCriteriaIDs=new ArrayList<String>();
		parsePreferenceDirectin=new String [1000];
	}
	
	@SuppressWarnings("unchecked")
    public ArrayList<String> exploreCriteriaXML(Element racine)
	   {	      
		   
		  
		
		if(racine.getChild("criteria")!=null)
		  {
			  Element criteria = racine.getChild("criteria");	      
			  if(criteria.getChildren("criterion")!=null){
				  List listCriteria = criteria.getChildren("criterion");
				  Iterator i = listCriteria.iterator();
				  while(i.hasNext())
				  {
					  Element courant = (Element)i.next();
					  if (courant.getAttributeValue("id")!=null)
					  {
						  if(courant.getChild("active")!=null && 
								  courant.getChild("active").getValue().equals("false"))
						  {
							  //Inactive criterion, on l'ignore
							  warning=warning.concat("The criterion "+courant.getAttributeValue("id")+" was ignored " +
							  "because it is not active: active=false.\n");
						  }
						  else 
						  {
							  
							  if(courant.getChild("scale")!=null)
							  {
							  
								  
								  if(courant.getChild("scale").getChild("quantitative")!=null)
								  {
									 
									  if(courant.getChild("scale").getChild("quantitative").getChild("preferenceDirection")!=null)
									  {
										 
										  if(courant.getChild("scale").getChild("quantitative").getChild("preferenceDirection").getValue().equals("min")||
												  courant.getChild("scale").getChild("quantitative").getChild("preferenceDirection").getValue().equals("max"))
										  {
										
											  if(! listCriteriaIDs.contains(courant.getAttributeValue("id")))
											  {
												  listCriteriaIDs.add(courant.getAttributeValue("id"));
												  parsePreferenceDirectin[ listCriteriaIDs.indexOf(courant.getAttributeValue("id"))]= courant.getChild("scale").
												  getChild("quantitative").getChild("preferenceDirection").getValue();
											  }
												  else
												  warning=warning.concat(" Warning: duplicated criterion "+courant.getAttributeValue("id")+". Only the first occurence is considered.");
										  }
										  else
										  {
											  warning=warning.concat(" Error in the file containing the criteria tag: " +
																  		"the 'preferenceDirection' tag should be equal to 'min' or 'max'. " +
																  		"Check this tag for criterion ("+courant.getAttributeValue("id")+"). This criterion was ignored.");  
										  }
										
									  }
									  else
									  {
										  warning=warning.concat(" Error in the file containing the criteria tag: " +
															  		"no 'preferenceDirection' detected for criterion ("+courant.getAttributeValue("id")+"). The criterion was ignored.");    
									  }
									 
								  }
								  
								  
								  
								  else
								  {
									  warning=warning.concat(" Error in the file containing the criteria tag: " +
														  		"no 'quantitative' detected for criterion ("+courant.getAttributeValue("id")+"). The criterion was ignored.");  
									
								  }
							  }
							  else
							  {
								  warning=warning.concat(" Error in the file containing the criteria tag: " +
								  		"no 'scale' detected for criterion ("+courant.getAttributeValue("id")+"). The criterion was ignored.");  
							  }
								  
						  }
					
					  }
					 
		         
					  else
					  {
						  // Warning, on n'a pas d'ID 
						  warning=warning.concat(" Error in the file containing the criteria tag: no attribue 'id' detected.");
					  }
	         
				  }
	     
	     
				  if(listCriteriaIDs.size()==0)
					  return null ;
				  else
					  return listCriteriaIDs;
			  }
	  
			  else
				  return null;
		  	}
		  	else
		  		return null;

	   }
	

	 public String getWarning()
	  {
		  return warning;
	  }
	 public ArrayList<String> getCriterIds()
	 {
		 return listCriteriaIDs;
	 }
	 
	 public String[]getPreferenceDirections()
	 {
		 return parsePreferenceDirectin;
	 }
}
