import java.io.FileOutputStream;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import eu.telecom_bretagne.xmcda.PerformanceTable;



public class OutputXmcdaFile {


	//Nous allons commencer notre arborescence en cr�ant la racine XML
	//qui sera ici "xmcda:XMCDA".
	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");

	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	//On cr�e un nouveau Document JDOM bas� sur la racine que l'on vient de cr�er
	static org.jdom2.Document document = new Document(racine);

	public  void affiche()
	{
		try
		{
			//On utilise ici un affichage classique avec getPrettyFormat()
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e){}
	}

	public void enregistre(String fichier)
	{
		try
		{
			//On utilise ici un affichage classique avec getPrettyFormat()
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			//Remarquez qu'il suffit simplement de cr�er une instance de FileOutputStream
			//avec en argument le nom du fichier pour effectuer la s�rialisation.
			FileOutputStream fos=new FileOutputStream(fichier);
			sortie.output(document,  fos);
			fos.close();

		}
		catch (java.io.IOException e){}
	}

	public  void createOutputFileMatrix(PerformanceTable 
	                                    projectPerfTable,float mean[],float max[],
	                                    float min[],float var[],float stdv[],float median[])
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		//On cr�e un nouvel Element meanValues et on l'ajoute
		//en tant qu'Element de racine
		Element criteriaMatrix = new Element("criteriaMatrix");
		racine.addContent(criteriaMatrix);

		int numberOfCriteria=projectPerfTable.getCriterionIDs().size();
		for(int i=0;i<numberOfCriteria;i++)
		{
			Element row = new Element("row");
			criteriaMatrix.addContent(row);

			Element criterionID = new Element("criterionID");
			row.addContent(criterionID);
			criterionID.setText(projectPerfTable.
			                    getCriterionIDs().get(i).toString());

			for(int j=0;j<6;j++)
			{
				Element column = new Element("column");
				row.addContent(column);

				Element criterionID2 = new Element("criterionID");
				column.addContent(criterionID2);

				Element value = new Element("value");
				column.addContent(value);

				Element real = new Element("real");
				value.addContent(real);

				float local=0;
				switch(j)
				{
					case 0: 
						local=mean[projectPerfTable.
						           getCriterionIDs().indexOf(projectPerfTable.
						                                     getCriterionIDs().get(i))];
						criterionID2.setText("mean");
						break;
					case 1:
						local=max[projectPerfTable.
						          getCriterionIDs().indexOf(projectPerfTable.
						                                    getCriterionIDs().get(i))];
						criterionID2.setText("max");
						break;
					case 2:
						local=min[projectPerfTable.
						          getCriterionIDs().indexOf(projectPerfTable.
						                                    getCriterionIDs().get(i))];
						criterionID2.setText("min");
						break;

					case 3:
						local=var[projectPerfTable.
						          getCriterionIDs().indexOf(projectPerfTable.
						                                    getCriterionIDs().get(i))];
						criterionID2.setText("variance");
						break;

					case 4:
						local=stdv[projectPerfTable.
						           getCriterionIDs().indexOf(projectPerfTable.
						                                     getCriterionIDs().get(i))];
						criterionID2.setText("Standard deviation");
						break;

					case 5:
						local=median[projectPerfTable.
						             getCriterionIDs().indexOf(projectPerfTable.
						                                       getCriterionIDs().get(i))];
						criterionID2.setText("Median");
						break;

					default:
						break;
				}
				String index=String.valueOf(local);
				real.setText(index);
			}
		}
	}

	public  void createOutputFile(PerformanceTable 
	                              projectPerfTable,float tab[],String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();
		//On cr�e un nouvel Element meanValues et on l'ajoute
		//en tant qu'Element de racine
		Element criteriaValues = new Element("criteriaValues");
		racine.addContent(criteriaValues);

		Attribute name =new Attribute("mcdaConcept",commentToPut);
		criteriaValues.setAttribute(name);

		int numberOfCriteria=projectPerfTable.getCriterionIDs().size();

		//On cr�e un nouvel Element meanValue et on l'ajoute
		//en tant qu'Element de meanValues
		for(int i=0;i<numberOfCriteria;i++)
		{
			Element criterionValue = new Element("criterionValue");
			criteriaValues.addContent(criterionValue);


			//On cr�e un nouvel Element criterionID et on l'ajoute
			//en tant qu'Element de meanValue
			Element criterionID = new Element("criterionID");
			criterionValue.addContent(criterionID);
			criterionID.setText(projectPerfTable.
			                    getCriterionIDs().get(i).toString());

			//On cr�e un nouvel Element meanValue et on l'ajoute
			//en tant qu'Element de criterionValue
			Element meanValue = new Element("value");
			criterionValue.addContent(meanValue);

			Element real = new Element("real");
			meanValue.addContent(real);

			String index=String.valueOf(tab[projectPerfTable.
			                                getCriterionIDs().indexOf(projectPerfTable.
			                                                          getCriterionIDs().get(i))]);

			real.setText(index);
		}
	}



}
