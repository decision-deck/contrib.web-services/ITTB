import java.io.File;

import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.Criteria;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Criteria;
import eu.telecom_bretagne.xmcda.JDOM_PerformanceTable;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.PerformanceTable;
import eu.telecom_bretagne.xmcda.XmcdaMessage;



public class AlgoFirst {

	private PerformanceTable projectPerfTable;

	public AlgoFirst(PerformanceTable projectPerfTable)
	{
		this.projectPerfTable=projectPerfTable;
	}

	public float [] maximum()
	{
		float []max=new float[projectPerfTable.
		                      getCriterionIDs().size()];
		int indexAlt, indexCrit;

		float element=projectPerfTable.getMatrixElement(0,0);

		// Pour chaque crit�re, on cherche le max parmi toutes les alternatives
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{

			indexCrit=projectPerfTable.getCriterionIDs().indexOf
					(projectPerfTable.getCriterionIDs().get(j));

			//Initialization
			max[j]=projectPerfTable.getMatrixElement(projectPerfTable.getAlternativeIDs().indexOf
			                                         (projectPerfTable.getAlternativeIDs().get(0)), indexCrit);

			for (int i=0;i< projectPerfTable.getAlternativeIDs().size();i++)
			{
				indexAlt=projectPerfTable.getAlternativeIDs().indexOf
						(projectPerfTable.getAlternativeIDs().get(i));

				//System.out.println("indexCrit ="+indexCrit+"indexAlt="+indexAlt);

				element=projectPerfTable.getMatrixElement(indexAlt, indexCrit);
				if (element>max[j])

				{
					max[j]=element;
					//System.out.println(element+">"+max[j]);
				}
			}

		}
		return max;
	}

	public float [] minimmum()
	{
		float []min=new float[projectPerfTable.
		                      getCriterionIDs().size()];
		int indexAlt, indexCrit;


		// Pour chaque crit�re, on cherche le max parmi toutes les alternatives
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{

			indexCrit=projectPerfTable.getCriterionIDs().indexOf
					(projectPerfTable.getCriterionIDs().get(j));

			//Initialization
			min[j]=projectPerfTable.getMatrixElement(
			                                         projectPerfTable.getAlternativeIDs().indexOf
			                                         (projectPerfTable.getAlternativeIDs().get(0)), indexCrit);

			for (int i=0;i< projectPerfTable.getAlternativeIDs().size();i++)
			{
				indexAlt=projectPerfTable.getAlternativeIDs().indexOf
						(projectPerfTable.getAlternativeIDs().get(i));


				if (projectPerfTable.getMatrixElement
						(indexAlt, indexCrit)<min[j])

					min[j]=projectPerfTable.getMatrixElement
					(indexAlt, indexCrit);
			}

		}
		return min;
	}

	public float [] variance(float moyenne[])
	{
		float []variance=new float[projectPerfTable.
		                           getCriterionIDs().size()];
		int indexAlt, indexCrit;

		// Pour chaque crit�re, on cherche le max parmi toutes les alternatives
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{
			//Initialization
			variance[j]=0;

			indexCrit=projectPerfTable.getCriterionIDs().indexOf
					(projectPerfTable.getCriterionIDs().get(j));

			for (int i=0;i< projectPerfTable.getAlternativeIDs().size();i++)
			{
				indexAlt=projectPerfTable.getAlternativeIDs().indexOf
						(projectPerfTable.getAlternativeIDs().get(i));
				variance[j]+=Math.pow(projectPerfTable.getMatrixElement
				                      (indexAlt, indexCrit)-moyenne[j],2);
			}

		}

		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{
			variance[j]/=projectPerfTable.getAlternativeIDs().size();
			// Pour arrondir � deux chiffres apr�s la virgule
			variance[j]= (float) (Math.floor(variance[j]*100.0)/100);

		}

		return variance;
	}

	public float [] standardDeviation(float [] variance)
	{
		float []stdDev=new float[projectPerfTable.
		                         getCriterionIDs().size()];

		//Initialization
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
			stdDev[j]=0;

		// Pour chaque crit�re, on cherche le max parmi toutes les alternatives
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)	
			stdDev[j]+=Math.sqrt(variance[j]);

		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)	
			stdDev[j]= (float) (Math.floor(stdDev[j]*100.0)/100);

		return stdDev;
	}

	public float [] median()
	{
		float []median=new float[projectPerfTable.
		                         getCriterionIDs().size()];
		int size=projectPerfTable.getAlternativeIDs().size();

		float temp;
		float []tri=new float[size];
		//Initialization
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
			median[j]=0;
		int indexCrit,indexAlt;	

		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{
			indexCrit=projectPerfTable.getCriterionIDs().indexOf
					(projectPerfTable.getCriterionIDs().get(j));

			for (int i=0;i< size;i++)
			{

				indexAlt=projectPerfTable.getAlternativeIDs().indexOf
						(projectPerfTable.getAlternativeIDs().get(i));

				//Initialization
				tri[i]=projectPerfTable.getMatrixElement
						(indexAlt, indexCrit);
			}


			for (int i=0;i< size-1;i++)
			{
				if (tri[i]>tri[i+1])
				{
					temp=tri[i];
					tri[i]=tri[i+1];
					tri[i+1]=temp;

				}
			}


			// Calcul de la m�diane � partir du tableau tri�

			// Si le nombre d'alternatives est pair
			if(size%2==0)
			{
				// On fait une moyenne
				if(size>2)
					median[j]=(float)((tri[size/2]+tri[((size/2)+1)])/2.);
				else 
					median[j]=(float)((tri[0]+tri[1])/2.);
				median[j]=(float)(Math.floor(median[j]*100.0)/100);
				//System.out.println("j="+j+"median="+median[j]);
			}
			else //Le nombre d'alternatives est impair
			{
				if(size>1)

					median[j]=tri[(size+1)/2];
				else
					median[j]=tri[0];

				median[j]=(float)(Math.floor(median[j]*100.0)/100);
				//System.out.println("j="+j+"median="+median[j]);
			}

		}



		return median;
	}

	public float [] mean()
	{

		int indexAlt, indexCrit;
		float []mean=new float[projectPerfTable.
		                       getCriterionIDs().size()];

		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
			mean[j]=0;
		// Pour chaque crit�re, on fait la moyenne sur toutes les alternatives
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{
			indexCrit=projectPerfTable.getCriterionIDs().indexOf
					(projectPerfTable.getCriterionIDs().get(j));

			for (int i=0;i< projectPerfTable.getAlternativeIDs().size();i++)
			{
				indexAlt=projectPerfTable.getAlternativeIDs().indexOf
						(projectPerfTable.getAlternativeIDs().get(i));

				mean [j]+=projectPerfTable.getMatrixElement(indexAlt, indexCrit);
			}
		}
		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)
		{
			mean[j]/=projectPerfTable.getAlternativeIDs().size();
			// Pour arrondir � deux chiffres apr�s la virgule
			mean[j]= (float) (Math.floor(mean[j]*100.0)/100);
		}
		return mean;	
	}

	public void printMean(float [] mean)
	{

		for (int j=0;j< projectPerfTable.getCriterionIDs().size();j++)			
			System.out.println("Mean:"+mean[j]);
	}
	public static void main(String[] argv)
	{
		boolean stopRunning=false;
		String tempErrorMessage="";
		String warningMessage="";

		if(argv[0]==null ||argv[1]==null ||argv[2]==null ||argv.length!=10 ||
				!new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists())
		{
			stopRunning =true;
			tempErrorMessage="Error: unable to run the algorithm. " +
					"Reason: missing one (or more) entry file(s). " +
					"Please check your entry files.";
		}

		if (stopRunning==false)
		{
			Main mainFunction=new Main();	
			PerformanceTable test=new PerformanceTable ();

			//String pathAlternatives="C:/temp/alternatives.xml"; 
			String pathAlternatives=argv[0];
			String errAltern=mainFunction.checkEntryFile(pathAlternatives);

			//String pathCriteria="C:/temp/criteria.xml"; 
			String pathCriteria=argv[1];	

			String errCriter=mainFunction.checkEntryFile(pathCriteria);

			//String pathPerfTable="C:/temp/performanceTable.xml"; 
			String pathPerfTable=argv[2];
			String errPerfTable=mainFunction.checkEntryFile(pathPerfTable);


			if(errAltern!="")
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the alternatives " +
						"tag is not a valid XMCDA document.");


			if(errCriter!="")
			{
				//System.out.println("errAlternativesValues: "+errAlternativesValues);
				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the criteria " +
						"tag is not a valid XMCDA document.");
			}

			if(errPerfTable!="")

				tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the performanceTable " +
						"tag is not a valid XMCDA document.");

			if(errAltern!=""||errCriter!=""||errPerfTable!="")
			{
				//In this case: stopRunning=true, we send the messages.xml output file
				XmcdaMessage xmcdaMess= new XmcdaMessage();
				xmcdaMess.createErrorMessage(tempErrorMessage);			
				String pathOutputMessage=argv[5];
				xmcdaMess.enregistre(pathOutputMessage);
			}
			else

			{


				// Test en cas d'erreur
				JDOM_Criteria jdomCriter= new JDOM_Criteria();
				mainFunction.PrepareParsing (pathCriteria);
				boolean parseCriterNull=false;
				Criteria criter=jdomCriter.parseCriteriaXML(mainFunction.getRacine());



				if(jdomCriter.getWarning()!="")
				{
					if(warningMessage.equals(""))
						warningMessage= "\n"+jdomCriter.getWarning();
					else

						warningMessage= warningMessage.concat("\n"+jdomCriter.getWarning());
				}

				if(criter!=null){}

				else
				{
					parseCriterNull=true;
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the criteria " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" criteria tag is erroneous or empty.");
				}

				// Test en cas d'erreur
				mainFunction.PrepareParsing (pathAlternatives);
				JDOM_Alternatives jdomAlter= new JDOM_Alternatives();
				boolean parseAlternativesNull=false;
				Alternatives alter= jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
				if(jdomAlter.getWarning()!="")
				{

					if(warningMessage.equals(""))
						warningMessage= "\n"+jdomAlter.getWarning();
					else
						warningMessage= warningMessage.concat("\n"+jdomAlter.getWarning());
				}
				if(alter!=null){}

				else
				{
					parseAlternativesNull=true;
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the alternatives " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" alternatives tag is erroneous or empty.");


				}
				// Test en cas d'erreur
				mainFunction.PrepareParsing (pathPerfTable);
				JDOM_PerformanceTable jdomPerfTable= new JDOM_PerformanceTable();
				boolean parsePerfTableNull=false;
				PerformanceTable perfTable=jdomPerfTable.explorePerformanceTable(mainFunction.getRacine());
				if(jdomPerfTable.getErrorMessage()!="")
					warningMessage= warningMessage.concat("\n"+jdomPerfTable.
					                                      getErrorMessage());

				if(jdomPerfTable.getWarningMessage()!="")
					warningMessage=warningMessage.concat(jdomPerfTable.
					                                     getWarningMessage());

				if(perfTable!=null){}
				else
				{
					parsePerfTableNull=true;
					//Error message
					if(tempErrorMessage.equals(""))
						tempErrorMessage="\nFatal Error: the file containing the performanceTable " +
								"tag is erroneous or empty.";
					else
						tempErrorMessage=tempErrorMessage.concat("\nFatal Error: the file containing the" +
								" performanceTable tag is erroneous or empty.");
				}
				if(parseCriterNull==false && parseAlternativesNull==false && parsePerfTableNull==false)
				{
					test=mainFunction.run(test,pathCriteria,pathAlternatives,
					                      pathPerfTable);
					warningMessage=mainFunction.getWarningMessage();
					tempErrorMessage=mainFunction.getErrorMessage();

					AlgoFirst algo=new AlgoFirst(test);
					float tabMean[]={};
					float tabVariance[]={};
					float tabStandardDeviation[]={};
					float tabMaximum[]={};
					float tabMinumum[]={};
					float tabMedian[]={};
					if(test.getAlternativeIDs().size()!=0 
							&& test.getCriterionIDs().size()!=0)
					{
						tabMean=algo.mean();
						tabMaximum=algo.maximum();
						tabMinumum=algo.minimmum();
						tabVariance=algo.variance(tabMean);
						tabStandardDeviation=algo.standardDeviation(tabVariance);
						tabMedian=algo.median();

						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();
						xmcdaMess.createLogMessage(errCriter,errAltern,
						                           errPerfTable,warningMessage,tempErrorMessage);

						String pathOutputMessage=argv[3];
						//String pathOutputMessage="C:/temp/log.xml"; 
						xmcdaMess.enregistre(pathOutputMessage);
					}
					else
					{
						tempErrorMessage=tempErrorMessage.concat("\n We cannot run the algorithm " +
								"since the criteria list or the alternatives list " +
								"of the PROJECT is empty.\n");

						// Output Log Message File
						XmcdaMessage xmcdaMess= new XmcdaMessage();
						xmcdaMess.createErrorMessage(tempErrorMessage);

						//xmcdaMess.affiche();

						//String pathOutputMessage="C:/temp/log.xml"; 
						String pathOutputMessage=argv[3];
						xmcdaMess.enregistre(pathOutputMessage);
					}

					// Output XMCDA File to put the mean values
					if(test.getAlternativeIDs().size()!=0 
							&& test.getCriterionIDs().size()!=0)
					{
						OutputXmcdaFile outputStatisticsFileMean= new OutputXmcdaFile();
						outputStatisticsFileMean.createOutputFile(test,tabMean,"mean values");
						String pathOutputFileMean=argv[4];
						outputStatisticsFileMean.enregistre(pathOutputFileMean);

						OutputXmcdaFile outputStatisticsFileMax= new OutputXmcdaFile();
						outputStatisticsFileMax.createOutputFile(test,tabMaximum,"maximum values");
						String pathOutputFileMax=argv[5];
						outputStatisticsFileMax.enregistre(pathOutputFileMax);

						OutputXmcdaFile outputStatisticsFileMin= new OutputXmcdaFile();
						outputStatisticsFileMin.createOutputFile(test,tabMinumum,"minimum values");
						String pathOutputFileMin=argv[6];
						outputStatisticsFileMin.enregistre(pathOutputFileMin);

						OutputXmcdaFile outputStatisticsFileVar= new OutputXmcdaFile();
						outputStatisticsFileVar.createOutputFile(test,tabVariance,"variance");
						String pathOutputFileVar=argv[7];
						outputStatisticsFileVar.enregistre(pathOutputFileVar);

						OutputXmcdaFile outputStatisticsFileStdv= new OutputXmcdaFile();
						outputStatisticsFileStdv.createOutputFile(test,
						                                          tabStandardDeviation,"standard deviation");
						String pathOutputMeanFileStdv=argv[8];
						outputStatisticsFileStdv.enregistre(pathOutputMeanFileStdv);

						OutputXmcdaFile outputStatisticsFileMedian= new OutputXmcdaFile();
						outputStatisticsFileMedian.createOutputFile(test,
						                                            tabMedian,"median values");
						String pathOutputMeanFileMedian=argv[9];
						outputStatisticsFileMedian.enregistre(pathOutputMeanFileMedian);
					}
				}
				else
				{
					// Output Log Message File
					XmcdaMessage xmcdaMess= new XmcdaMessage();
					xmcdaMess.createErrorMessage(tempErrorMessage);

					//xmcdaMess.affiche();

					//String pathOutputMessage="C:/temp/log.xml"; 
					String pathOutputMessage=argv[3];
					xmcdaMess.enregistre(pathOutputMessage);
				}
			}
		}//StopRunning =false
		else
		{
			//StopRunning =true
			// Output Log Message File
			XmcdaMessage xmcdaMess= new XmcdaMessage();
			xmcdaMess.createErrorMessage(tempErrorMessage);

			//xmcdaMess.affiche();

			//String pathOutputMessage="C:/temp/log.xml"; 
			String pathOutputMessage=argv[3];
			xmcdaMess.enregistre(pathOutputMessage);

		}
	}
}
