﻿================================================================
 The web service « sortAlternativesValues-ITTB »
================================================================

How to run the java program in command line?
===================================================
::

java –Djava.awt.headless =true -jar sortAlternativesValues-ITTB.jar alternatives.xml alternativesValues.xml categoriesLimits.xml affectations.xml messages.xml 
where:

-  The entry files `` alternatives.xml ``, ``alternativesValues.xml `` and `` categoriesLimits.xml `` are placed in this order. 
- The first output file corresponds to the `` affectations.xml`` file. 
- The second output file corresponds to the ``messages.xml`` file. It is a list of messages generated by the algorithm. It contains information about the execution status as well as a warnings and errors when they occur.

How to run the tests?
=====================

You have to put the three entry files ``alternatives.xml``, ``alternativesValues.xml`` and ``categoriesLimits.xml`` in the correct order.
