import java.util.ArrayList;


public class AlternativesValues {

	private ArrayList <AlternativeValue> listAltVal;
	
	public AlternativesValues(){
		listAltVal= new ArrayList<AlternativeValue>();
	}

	public ArrayList<AlternativeValue> addAlternative(AlternativeValue altValToAdd)
	{
		listAltVal.add(altValToAdd);
		return listAltVal;
	}

	public ArrayList <AlternativeValue> getListAlternativesValues()
	{
		return listAltVal; 
	}

	public ArrayList <String> getListAlternativesIDs()
	{
		ArrayList <String> listAlternativesID=new  ArrayList <String>();
		for (int i=0;i<listAltVal.size();i++)
			listAlternativesID.add(listAltVal.
					get(i).getAlternativeValueId());
		return 	listAlternativesID;
	}

	public void printAlternativesValues()
	{
		System.out.println("These are the AlternativesValues:");
		for (int i= 0; i < listAltVal.size(); i++)
		{
			System.out.println("AlternativeValue"+(i+1)+":\t"+ 
					"id = "+ listAltVal.get(i).getAlternativeValueId()+
					"\t, value = "+ listAltVal.get(i).getRealValue());
		}
	}

}
