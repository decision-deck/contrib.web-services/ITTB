import java.util.ArrayList;


public class AlternativesAffectations {

	private ArrayList <AlternativeAffectation> listAltAffect;
	
	public AlternativesAffectations(){
		listAltAffect= new ArrayList<AlternativeAffectation>();
		
	}
	
	public void addAffectation(AlternativeAffectation affectToAdd)
	{
		listAltAffect.add(affectToAdd);
	}

	public ArrayList <AlternativeAffectation> getListAlternativesValues()
	{
		return listAltAffect; 
	}

	public ArrayList <String> getAlternativesIDs()
	{
		ArrayList <String> listAlternativesID=new  ArrayList <String>();
		for (int i=0;i<listAltAffect.size();i++)
			listAlternativesID.add(listAltAffect.
					get(i).getAlternativeID());
		return 	listAlternativesID;
	 
	}

	public ArrayList <String> getCategoriesIDs()
	{
		ArrayList <String> listCategoriesIDs=new  ArrayList <String>();
		for (int i=0;i<listAltAffect.size();i++)
			listCategoriesIDs.add(listAltAffect.
					get(i).getCategoryID());
		return listCategoriesIDs;
	 
	}

	public void printAlternativesAffectations()
	{
		System.out.println("These are the AlternativesAffectations:");
		for (int i= 0; i < listAltAffect.size(); i++)
		{
			System.out.println("AlternativeAffectation"+(i+1)+":\t"+ 
					"alternativeID = "+ listAltAffect.get(i).getAlternativeID()+
					"\t, categoryID= "+ listAltAffect.get(i).getCategoryID());
		}
	}

}
