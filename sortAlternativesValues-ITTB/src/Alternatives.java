
import java.util.ArrayList;


public class Alternatives {
	
	private ArrayList <Alternative> listAlternatives;
	private ArrayList<String >listAlternativeID;
	
	public Alternatives(){
		listAlternatives= new ArrayList<Alternative>();
		listAlternativeID=new ArrayList<String>();
		
	}
	
	public void addAlternative(Alternative alternativeToAdd)
	{
		listAlternatives.add(alternativeToAdd);
	}
	

	public ArrayList<String> getAlternativeIDs()
	{
	
		return 	listAlternativeID;
	}
	
	public ArrayList <Alternative> getListAlternatives()
	{
		
		return listAlternatives; 
	}
	
	public ArrayList <String> getListAlternativesNames()
	{
		 ArrayList<String> listAlternNames=new ArrayList<String>();
			for (int i=0;i<listAlternatives.size();i++)
			{
				listAlternNames.add(listAlternatives.
						get(i).getAlternativeName());
			}
				return listAlternNames;
	}
	
	
	public ArrayList <String> getListAlternativesIDs()
	{

		return 	listAlternativeID;
	}
	
	public void printAlternatives()
	{
		System.out.println("This are the alternatives:");
		for (int i= 0; i < listAlternatives.size(); i++)
		{
			System.out.println("Alternative"+(i+1)+":\t"+ 
					"id = "+ listAlternatives.get(i).getAlternativeId()+
					"\t : "+ listAlternatives.get(i).getAlternativeName());
		}
	}

}
