import java.util.ArrayList;


public class CategoriesValues {

	private ArrayList <CategoryValue> listCategVal;

	public CategoriesValues(){
		listCategVal= new ArrayList<CategoryValue>();
	}

	public void  addCategory(CategoryValue categValToAdd)
	{
		listCategVal.add(categValToAdd);
	}

	public ArrayList <CategoryValue> getListCategoriesValues()
	{
		return listCategVal; 
	}

	public ArrayList <String> getListCategoriesIDs()
	{
		ArrayList <String> listCategIDs=new  ArrayList <String>();
		for (int i=0;i<listCategVal.size();i++)
			listCategIDs.add(listCategVal.
			                 get(i).getCategoryID());
		return 	listCategIDs;

	}

	public void printCategoriesValues()
	{
		System.out.println("These are the categoriesValues:");
		for (int i= 0; i < listCategVal.size(); i++)
		{
			System.out.println("categoryValue"+(i+1)+":\t"+ 
					"id = "+ listCategVal.get(i).getCategoryID()+
					"\t, upper bound = "+ listCategVal.get(i).getUpperBound()+
					"\t, lowerer bound = "+ listCategVal.get(i).getLowerBound());
		}
	}

}
