import java.util.Iterator;

import org.jdom2.Element;


public class ParseCategoriesValues {

	private CategoriesValues categVal  =new CategoriesValues();
	private String warningMessage;  

	public   ParseCategoriesValues(){
		warningMessage = "";
		categVal = new CategoriesValues();
	}

	public CategoriesValues parseCategoriesLimits(Element racine)
	{	
		if(racine.getChild("categoriesValues")!=null)
		{
			Element categoriesValues = racine.getChild("categoriesValues");

			if(categoriesValues.getChildren("categoryValue")!=null)
			{
				Iterator<Element> i = categoriesValues.getChildren("categoryValue").iterator();

				boolean categoryIDOK=true; 
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					float upper=-1;
					float lower=-1;
					categoryIDOK=true;
					if(courant.getChild("value") !=null)
					{
						if(courant.getChild("value").getChild("interval")!=null)
						{
							if(courant.getChild("value").getChild("interval").getChild("lowerBound")!=null &&  courant.getChild("value").getChild("interval").getChild("upperBound")!=null)
							{
								if(courant.getChild("value").getChild("interval").getChild("lowerBound").getChild("real")!=null &&
										courant.getChild("value").getChild("interval").getChild("upperBound").getChild("real")!=null )
								{
									if ( categVal.getListCategoriesIDs().contains(courant.getChild("categoryID").getValue())){
										warningMessage= warningMessage.concat("\nWarning: duplicated categoryID (" +
												courant.getChild("categoryID").getValue()+
												") in the file containing the 'categoriesValues' tag.");
									}  	        	                          	         
									else
									{
										upper=Float.parseFloat(courant.getChild("value").getChild("interval").
										                       getChild("upperBound").getChild("real").getValue());
										lower=Float.parseFloat(courant.getChild("value").getChild("interval").
										                       getChild("lowerBound").getChild("real").getValue());
										categVal.addCategory(new CategoryValue(courant.getChild("categoryID").getValue(), upper,lower));
									}
								}   	    	  
								else
									categoryIDOK=false;
							}
							else
								categoryIDOK=false;
						}
						else
							categoryIDOK=false;
					}
					else
						categoryIDOK=false;
				} 

				if(categVal.getListCategoriesIDs().size()!=0 && categoryIDOK)
					return  categVal;
				else
					return null;
			}
			else return null;
		}
		else
			return null;
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

}
