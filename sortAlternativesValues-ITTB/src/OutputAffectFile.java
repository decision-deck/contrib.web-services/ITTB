import java.io.FileOutputStream;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class OutputAffectFile {

	static Element racine = new Element("XMCDA");
	private  Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/" +
			"XMLSchema-instance");
	private  Namespace xmcda=Namespace.getNamespace("xmcda", "http://www.decision-deck.org" +
			"/2009/XMCDA-2.0.0");

	static org.jdom2.Document document = new Document(racine);

	public  void printFile()
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, System.out);
		}
		catch (java.io.IOException e){}
	}

	public void saveFile(String file)
	{
		try
		{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			FileOutputStream fos=new FileOutputStream(file);
			sortie.output(document,  fos);
			fos.close();     
		}
		catch (java.io.IOException e){}
	}


	public  void createFile(AlternativesAffectations affect, String commentToPut)
	{
		racine.setNamespace(xmcda);
		racine.addNamespaceDeclaration(xsi);
		racine.addNamespaceDeclaration(xmcda);	   
		racine.setAttribute("schemaLocation",
		                    "http://www.decision-deck.org/2009/XMCDA-2.0.0 " +
		                    		"http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd",
		                    		xsi);

		List<Content> contents= racine.getContent();
		contents.clear();

		Element alternativesAffectations= new Element("alternativesAffectations");
		racine.addContent(alternativesAffectations);

		Attribute name =new Attribute("mcdaConcept",commentToPut);
		alternativesAffectations.setAttribute(name);

		for(int i=0;i< affect.getAlternativesIDs().size();i++)
		{
			Element alternativeAffectation = new Element("alternativeAffectation");
			alternativesAffectations.addContent(alternativeAffectation);

			Element alternativeID = new Element("alternativeID");
			alternativeAffectation.addContent(alternativeID);
			alternativeID.setText(affect.getAlternativesIDs().get(i).toString());

			Element categoryID = new Element("categoryID");
			alternativeAffectation.addContent(categoryID);
			categoryID.setText(affect.getCategoriesIDs().get(i).toString());
		}  
	}      		   

}
