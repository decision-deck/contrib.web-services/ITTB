public class CheckEntryFiles {

	private String warningMessage = "";


	public boolean checkIDs(Alternatives altern,AlternativesValues alternVal)
	{
		boolean result=true;// les listes sont coh�rentes
		int diff=0;
		int size=alternVal.getListAlternativesIDs().size();

		if(altern.getAlternativeIDs().size()!=size)
		{
			warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
					" incoherence with the number of alternatives given " +
					"in the file containing the 'alternativesValues' tag.");
			result=false;
		}
		else //On a la m�me taille mais il faudra v�rifier si c'est les m�mes Ids
		{
			for (String s : altern.getAlternativeIDs())
			{
				if(alternVal.getListAlternativesIDs().contains(s))
				{
					// OK
				}
				else
				{
					result=false;
					diff++;
				}
			}
			if(diff>1)
				warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
						"incoherence with the alternatives given " +
						"in the file containing the 'alternativesValues' tag. There are "+diff+" different alternatives.");
			else if(diff==1)
			{
				warningMessage=warningMessage.concat("\nPlease check the file containing the 'alternatives' tag:" +
						"incoherence with the alternatives given " +
						"in the file containing the 'alternativesValues' tag. There is one different alternative.");
			}
		}
		return result;		
	}

	public String getWarning()
	{
		return warningMessage;
	}


	public AlternativesValues getIntersection(Alternatives altern,AlternativesValues alternVal)
	{
		AlternativesValues result=new AlternativesValues();
		for (String t : alternVal.getListAlternativesIDs()) 
		{
			if( altern.getAlternativeIDs().contains(t)) 
			{
				int index=alternVal.getListAlternativesIDs().indexOf(t);
				result.addAlternative(new AlternativeValue(alternVal.getListAlternativesIDs().get(index),
				                                           alternVal.getListAlternativesValues().get(index).getRealValue()));
			}
		}
		return result;
	}

}
