import java.util.Iterator;

import org.jdom2.Element;


public class ParseAlternativesValues {

	private AlternativesValues alternativesValuesToTest  =new AlternativesValues();
	private String warningMessage;  

	// Constructor
	public   ParseAlternativesValues(){
		warningMessage="";
		alternativesValuesToTest = new AlternativesValues();
	}

	//Ajouter cette m�thode � la classe JDOM_AlternativesValues
	public AlternativesValues parseAlternativesValuesXML(Element racine)
	{	
		if(racine.getChild("alternativesValues")!=null)
		{
			Element alternaVal = racine.getChild("alternativesValues");
			if(alternaVal.getChildren("alternativeValue")!=null)
			{
				Iterator<Element> i = alternaVal.getChildren("alternativeValue").iterator();

				String temp;
				boolean alternativeIDOK; 
				while(i.hasNext())
				{
					Element courant = (Element)i.next();
					temp="";
					alternativeIDOK=true;

					if(courant.getChild("value").
							getChild("real")!=null)
					{
						temp=courant.getChild("value").
								getChild("real").getValue();
					}
					else if(courant.getChild("value").
							getChild("integer")!=null)
					{
						temp=courant.getChild("value").
								getChild("integer").getValue();
					}
					else
					{
						alternativeIDOK=false;
					}

					if(alternativeIDOK==true && courant.
							getChild("alternativeID").getValue().length()!=0)
					{
						if ( alternativesValuesToTest.getListAlternativesIDs().contains(courant.
						                                                                getChild("alternativeID").getValue())){
							// Envoyer un warning pour dire que le crit�re existe deux fois
							/*warningMessage= warningMessage.concat("\nWarning: There are two identical alternativeID:" +
	        			 courant.
		    			 getChild("alternativeID").getValue()+
	        	 		" in the alternativesValues.xml file");*/
						}
						else
							alternativesValuesToTest.addAlternative(new AlternativeValue
							                                        (courant.getChild("alternativeID").getValue(), 
							                                         Float.parseFloat(temp)));
					}
					else
					{
						warningMessage= warningMessage.concat("\n Error in the file containing the alternativesValues tag." +
								" The value is neither real nor integer. "+
								"We have eliminated the corresponding alternative:"+
								courant.getChild("alternativeID").getValue()+".");
					}
				} 
				if(alternativesValuesToTest.getListAlternativesIDs().size()==0)
					return null;
				else
					return  alternativesValuesToTest;
			}
			else return null;
		}
		else
		{
			return null;
		}
	}

	public String getWarningMessage()
	{
		return warningMessage;
	}

}
