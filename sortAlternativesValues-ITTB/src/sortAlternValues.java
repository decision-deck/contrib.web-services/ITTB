import java.io.File;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class sortAlternValues
{
	private   Element racine = new Element("XMCDA");		 
	private org.jdom2.Document document = new Document(racine);

	private static final String XMCDA_2_0_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.0.0";

	private static final String XMCDA_2_0_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd";

	private static final String XMCDA_2_1_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.1.0";

	private static final String XMCDA_2_1_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd";

	private static final String XMCDA_2_2_0_NAMESPACE     = "http://www.decision-deck.org/2009/XMCDA-2.2.0";

	private static final String XMCDA_2_2_0_SCHEMA        = "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd";

	private static final String ACCEPTED_SCHEMA_LOCATIONS = XMCDA_2_0_0_NAMESPACE + " " + XMCDA_2_0_0_SCHEMA + " "
			+ XMCDA_2_1_0_NAMESPACE + " " + XMCDA_2_1_0_SCHEMA +" "
			+ XMCDA_2_2_0_NAMESPACE + " " + XMCDA_2_2_0_SCHEMA;


	public void PrepareParsing(String path)
	{
		try
		{
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){
			//System.out.println(e.getStackTrace());
		}
		racine = document.getRootElement();
	}

	protected static String checkEntryFile(String path)
	{
		String error="";
		try {
			parseXML(path);
		} catch (JDOMException e) {
			error=error.concat("File is not well-formed: "+e.getMessage());
		} catch (IOException e) {
			error=error.concat("Could not check file because: "+e.getMessage());
		}
		return error;
	}

	protected static void parseXML(final String xml) throws JDOMException, IOException {
		SAXBuilder builder = 
				new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
		builder.setFeature(
		                   "http://apache.org/xml/features/validation/schema", true);

		builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation",
		                    ACCEPTED_SCHEMA_LOCATIONS);

		@SuppressWarnings("unused")
		Document doc= builder.build(xml);
	}

	protected static String getCategoryID(float val,CategoriesValues categVal)
	{
		String categoryID=null;

		boolean stillSearch=true;
		int i=0;

		while(i<categVal.getListCategoriesIDs().size() && stillSearch)
		{
			float upperBound=categVal.getListCategoriesValues().get(i).getUpperBound();
			float lowerBound=categVal.getListCategoriesValues().get(i).getLowerBound();
			if( val>= lowerBound && val< upperBound)
			{
				stillSearch=false;
				categoryID=categVal.getListCategoriesValues().get(i).getCategoryID();
			}
			i++;
		}
		return categoryID;
	}

	protected static AlternativesAffectations getAffectations(AlternativesValues alternVal,CategoriesValues categVal)
	{
		AlternativesAffectations affect=new AlternativesAffectations();
		String categoryID=null;
		for(int i=0; i< alternVal.getListAlternativesIDs().size();i++)
		{
			categoryID=getCategoryID(alternVal.getListAlternativesValues().get(i).getRealValue(),categVal);
			if(categoryID!=null)
				affect.addAffectation(new AlternativeAffectation
				                      (alternVal.getListAlternativesIDs().get(i),categoryID));
		}
		return affect;
	}

	public static void main(String[] argv)
	{
		sortAlternValues tri = new sortAlternValues();
		String errorMessage = "";
		String warningMessage = "";

		String pathAltern=argv[0];
		String pathAlternVal=argv[1];
		String pathCategVal=argv[2];

		boolean stopRunning=false;

		if(argv[0]==null ||argv[1]==null ||argv[2]==null ||argv.length!=5||
				!new File(argv[0]).exists()||!new File(argv[1]).exists()||!new File(argv[2]).exists())
		{
			stopRunning =true;
			errorMessage="Error: unable to run the algorithm. Reason:  missing one (or more) entry file(s). " +
					"Please check your entry files.";
		}
		if (!stopRunning)
		{
			//ParseMethodParameters
			String errAltern = checkEntryFile(pathAltern);
			String errAlternVal =checkEntryFile(pathAlternVal);
			String errCateg=checkEntryFile(pathCategVal);

			if(errAltern!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the alternatives " +
						"tag is not a valid XMCDA document: "+errAltern);

			if(errAlternVal!="")
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the alternativesValues " +
						"tag is not a valid XMCDA document: "+errAlternVal);
			if(errCateg!="")
			{
				errorMessage=errorMessage.concat("\nFatal Error: the file containing the categoriesValues " +
						"tag is not a valid XMCDA document: "+errCateg);
			}

			if(errCateg!="" ||errAltern!=""||errAlternVal!="")
			{
				// Output Log Message File
				StatusMessage xmcdaMess= new StatusMessage();
				xmcdaMess.createErrorMessage(errorMessage);
				String pathOutputMessage=argv[argv.length - 1];
				xmcdaMess.save(pathOutputMessage);
			}
			else
			{
				//Parsing XML file for Alternatives
				tri.PrepareParsing (pathAltern);
				Alternatives alternativesToUse=new Alternatives();
				ParseAlternatives jdomAlter= new ParseAlternatives();
				boolean parseAlternativesNull=false;
				alternativesToUse=jdomAlter.exploreAlternativesXML(tri.getRacine());
				//alternativesToUse.printAlternatives();
				if(alternativesToUse!=null){/*This is OK*/}
				else
				{	
					parseAlternativesNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the alternatives " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" alternatives tag is erroneous or empty.");
				}
				if(jdomAlter.getWarning()!="")
					warningMessage= warningMessage.concat("\n"+jdomAlter.getWarning());

				//Parsing XML file for AlternativesValues
				tri.PrepareParsing (pathAlternVal);
				AlternativesValues alternValToUse=new AlternativesValues();
				ParseAlternativesValues parseAlternVal= new ParseAlternativesValues();
				boolean parseAlterValNull=false;
				alternValToUse=parseAlternVal.parseAlternativesValuesXML(tri.getRacine());
				//alternValToUse.printAlternativesValues();
				if(alternValToUse!=null){/*This is OK*/}
				else
				{	
					parseAlterValNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the 'alternativesValues' " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" 'alternativesValues' tag is erroneous or empty.");
				}
				if(parseAlternVal.getWarningMessage()!="")
					warningMessage= warningMessage.concat("\n"+parseAlternVal.getWarningMessage());

				//Parsing XML file for categoriesValues
				tri.PrepareParsing (pathCategVal);
				CategoriesValues categValToUse=new CategoriesValues();
				ParseCategoriesValues parseCategVal= new ParseCategoriesValues();
				boolean parseCategValNull=false;
				categValToUse=parseCategVal.parseCategoriesLimits(tri.getRacine());
				if(categValToUse!=null){/*This is OK*/}
				else
				{	
					parseCategValNull=true;
					//Error message
					if(errorMessage.equals(""))
						errorMessage="\nFatal Error: the file containing the 'categoriesValues' " +
								"tag is erroneous or empty.";
					else
						errorMessage=errorMessage.concat("\nFatal Error: the file containing the" +
								" 'categoriesValues' tag is erroneous or empty.");
				}
				if(parseCategVal.getWarningMessage()!="")
					warningMessage= warningMessage.concat("\n"+parseCategVal.getWarningMessage());

				if(!parseAlternativesNull && !parseAlterValNull && !parseCategValNull)
				{
					CheckEntryFiles check=new CheckEntryFiles();
					boolean verif=check.checkIDs(alternativesToUse,alternValToUse);
					if(!verif)
						warningMessage= warningMessage.concat(check.getWarning());

					//Intersection list
					AlternativesValues intersectionAlternVal=check.getIntersection(alternativesToUse,alternValToUse);
					//intersectionAlternVal.printAlternativesValues();

					// Si l'intersection est vide on prend ce qu'on a dans alternativesValues
					if (intersectionAlternVal.getListAlternativesIDs().isEmpty())
					{
						warningMessage=warningMessage.concat("\nOnly the alternatives IDs " +
								"in the file containing the 'alternativesValues' tag were considered. " +
								"Please check your input file containing the 'alternatives' tag.");

						intersectionAlternVal=new AlternativesValues();
						for (int i=0; i<alternValToUse.getListAlternativesIDs().size();i++)
							intersectionAlternVal.addAlternative(new AlternativeValue(alternValToUse.getListAlternativesIDs().get(i),
							                                                          alternValToUse.getListAlternativesValues().get(i).getRealValue()));
					}

					if(intersectionAlternVal.getListAlternativesIDs().size()!=0 )
					{
						AlternativesAffectations affect=getAffectations(intersectionAlternVal,categValToUse);

						if(!affect.getAlternativesIDs().isEmpty())
						{
							OutputAffectFile outputFile= new OutputAffectFile();
							outputFile.createFile(affect, "Sorted alternatives values" );
							String pathOutputFilePlot=argv[argv.length - 2];
							outputFile.saveFile(pathOutputFilePlot);
						}
						else
						{
							errorMessage=errorMessage.concat("\n The output file containing the AlternativesAffectations tag is empty. Please check your entry files.\n");
						}
					}
					else
					{
						errorMessage=errorMessage.concat("\n We cannot run the algorithm " +
								"since the alternatives list  " +
								"of the PROJECT is empty.\n");
					}

					// Output Log Message File
					StatusMessage xmcdaMess = new StatusMessage();
					xmcdaMess.createLogMessage(warningMessage,errorMessage);		
					String pathOutputMessage = argv[argv.length - 1];
					xmcdaMess.save(pathOutputMessage);
				}
				else
				{
					// Output Log Message File
					StatusMessage xmcdaMess= new StatusMessage();
					xmcdaMess.createErrorMessage(errorMessage);
					String pathOutputMessage=argv[argv.length - 1];
					xmcdaMess.save(pathOutputMessage);
				}
			}
		}
		else
		{
			// Output Log Message File
			StatusMessage xmcdaMess= new StatusMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage=argv[argv.length - 1];
			xmcdaMess.save(pathOutputMessage);
		}
	}

	public Element getRacine()
	{
		return racine;
	}

}
