public class Alternative {
	private String alternativeId;
	private String alternativeName;

	public Alternative (String alternativeId)
	{
		this.alternativeId=alternativeId;
		this.alternativeName=null;

	}

	public Alternative (String alternativeId, 
	                    String alternativeName)
	{
		this.alternativeId=alternativeId;
		this.alternativeName=alternativeName;
	}

	public String getAlternativeId() {
		return alternativeId;
	}
	public void setAlternativeId(String alternativeId) {
		this.alternativeId = alternativeId;
	}

	public String getAlternativeName() {
		return alternativeName;
	}
	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}

}