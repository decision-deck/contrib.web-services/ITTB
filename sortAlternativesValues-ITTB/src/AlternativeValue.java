

public class AlternativeValue {
	private String alternId;
	private float floatAlternVal;

	public AlternativeValue()
	{
	}

	public AlternativeValue(String alternId,float alternVal)
	{
		this.alternId=alternId;
		this.floatAlternVal=alternVal;
	}

	public String getAlternativeValueId() {
		return alternId;
	}

	public void setAlternativeValueId(String alternId) {
		this.alternId = alternId;
	}

	public float getRealValue() {
		return floatAlternVal;
	}

	public void setRealValue(float floatAlternVal) {
		this.floatAlternVal = floatAlternVal;
	}

}
