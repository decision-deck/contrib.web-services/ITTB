
public class CategoryValue
{
	private String categoryID;
	private float upperBound;
	private float lowerBound;
	public CategoryValue(String id,float up, float down)
	{
		this.categoryID=id;
		this.upperBound=up;
		this.lowerBound=down;
	}

	public String getCategoryID()
	{
		return categoryID;
	}

	public float getUpperBound()
	{
		return upperBound;
	}
	public float getLowerBound()
	{
		return lowerBound;
	}
}
