import java.io.File;
import java.util.Iterator;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
public class ParseAlternatives {

	private Alternatives alternativesToTest;
	private String pathXMLFile;
	static org.jdom2.Document document;
	private String warning;

	// Constructor
	public ParseAlternatives()
	{
		warning="";
		alternativesToTest=new Alternatives();
	}

	public Element getRacine(String path) {
		try
		{			
			SAXBuilder sxb = new SAXBuilder();
			document = sxb.build(new File(path));
		}
		catch(Exception e){}

		return document.getRootElement();
	}

	public void setPath(String path)
	{
		this.pathXMLFile=path;
	}


	public String getPath()
	{
		return pathXMLFile;
	}

	public Alternatives getAlternatives()
	{
		return alternativesToTest;
	}

	public void  printAlternatives()
	{
		System.out.println("There are "+alternativesToTest.
		                   getListAlternatives().size()+" alternatives:");
		for(int i=0;i<alternativesToTest.getListAlternatives().size();i++)
			System.out.print(alternativesToTest.getListAlternatives().
			                 get(i).getAlternativeId()+"\t");
		System.out.println("");
	}

	public Alternatives exploreAlternativesXML(Element racine)
	{	      
		if(racine.getChild("alternatives")!=null)
		{
			Element alternatives = racine.getChild("alternatives");
			//On cr�e une List contenant tous les noeuds "alternative" de l'Element racine

			if(alternatives.getChildren("alternative")!=null){
				Iterator<Element> i = alternatives.getChildren("alternative").iterator();
				while(i.hasNext())
				{
					Element courant = (Element)i.next();

					if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")!=null)
					{
						if(courant.getChild("active")!=null && 
								courant.getChild("active").getValue().equals("false"))
						{
							//Inactive alternative, on l'ignore
							warning=warning.concat("The alternative "+courant.getAttributeValue("id")+" was ignored " +
									"because it is not active: active=false.\n");
						}
						else 
						{
							if(!alternativesToTest.getAlternativeIDs().contains(courant.getAttributeValue("id")))
							{
								alternativesToTest.addAlternative(new Alternative
								                                  (courant.getAttributeValue("id"),courant.getAttributeValue("name")));  
								alternativesToTest.getListAlternativesIDs().add(courant.getAttributeValue("id"));
							}
							else
							{
								// Duplicate alternative ID
								warning=warning.concat("\nWarning: duplicated alternative ("+
										courant.getAttributeValue("id")+
										"). Only the first occurence is considered.");
							}
						}
					}
					else if (courant.getAttributeValue("id")!=null && courant.getAttributeValue("name")==null )
					{
						if(courant.getChild("active")!=null && 
								courant.getChild("active").getValue().equals("false"))
						{
							//Inactive alternative, on l'ignore
							warning=warning.concat("The alternative "+courant.getAttributeValue("id")+" was ignored " +
									"because it is not active: active=false.\n");
						}
						else 
						{
							if(!alternativesToTest.getAlternativeIDs().contains(courant.getAttributeValue("id")))
							{
								alternativesToTest.addAlternative(new Alternative
								                                  (courant.getAttributeValue("id")));
								alternativesToTest.getListAlternativesIDs().add(courant.getAttributeValue("id"));
							}
							else
							{
								// Duplicate alternative ID
								warning=warning.concat("\nWarning: duplicated alternative ("+
										courant.getAttributeValue("id")+
										"). Only the first occurence is considered.");
							}
						}
					}
					else
					{
						// Warning, on n'a pas d'ID 
					}
				}
				if(alternativesToTest.getAlternativeIDs().size()==0)
					return null ;
				else
					return alternativesToTest;
			}
			else
				return null;
		}
		else
		{
			return null;
		}
	}

	public String getWarning()
	{
		return warning;
	}

}
