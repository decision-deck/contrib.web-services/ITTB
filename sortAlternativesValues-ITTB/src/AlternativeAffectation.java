
public class AlternativeAffectation
{
	private String categoryID;
	private String alternativeID;
	public AlternativeAffectation(String alternId,String categID)
	{
    	this.categoryID=categID;
    	this.alternativeID=alternId;

	}
	
	public String getCategoryID()
	{
		return categoryID;
	}
	
	public String getAlternativeID()
	{
		return alternativeID;
	}

}
