import java.io.File;

import eu.telecom_bretagne.xmcda.Alternative;
import eu.telecom_bretagne.xmcda.Alternatives;
import eu.telecom_bretagne.xmcda.AlternativesComparisons;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives;
import eu.telecom_bretagne.xmcda.JDOM_Alternatives_Comparisons;
import eu.telecom_bretagne.xmcda.Main;
import eu.telecom_bretagne.xmcda.XmcdaMessage;


public class cutRelation
{
	private String warningMessage = "";

	protected void update_matrix_binary(AlternativesComparisons alternCompa, AlternativesComparisons binary,
	                                    final float threshold)
	{
		for (Alternative alternative_1: alternCompa.rows())
			for (Alternative alternative_2: alternCompa.rows())
				if (alternCompa.isSet(alternative_1, alternative_2))
				{
					final float value = alternCompa.getValue(alternative_1, alternative_2);
					binary.setValue(alternative_1, alternative_2, ( value < threshold ) ? 0f : 1f);
				}
	}

	protected void update_matrix_crisp(AlternativesComparisons alternCompa, AlternativesComparisons crisp,
	                                   float threshold)
	{
		for (Alternative alternative_1: alternCompa.rows())
			for (Alternative alternative_2: alternCompa.rows())
				if (alternCompa.isSet(alternative_1, alternative_2))
				{
					final float value = alternCompa.getValue(alternative_1, alternative_2);
					if (value < threshold)
						crisp.unset(alternative_1, alternative_2); // Couper la relation
					else
						// (value>=threshold)
						crisp.setValue(alternative_1, alternative_2, 1f);
				}
	}

	public AlternativesComparisons createBinaryRelation(AlternativesComparisons alternCompa, float threshold,
	                                                    boolean binary)
	{
		AlternativesComparisons result = new AlternativesComparisons();

		// Alternatives IDs
		for (Alternative alternative: alternCompa.rows())
			result.addAlternative(alternative, false);

		// Update matrix
		if (binary)
			update_matrix_binary(alternCompa, result, threshold);
		else
			update_matrix_crisp(alternCompa, result, threshold);

		return result;
	}

	public AlternativesComparisons createOtherRelation(AlternativesComparisons alternCompa, float threshold, float min,
	                                                   float max, float indeterminate)
	{
		// TODO check threshold positive?
		AlternativesComparisons result = new AlternativesComparisons();

		// Alternatives IDs
		for (Alternative alternative: alternCompa.columns())
			result.addAlternative(alternative, false);

		// Update matrix
		for (Alternative alternative_1: alternCompa.columns())
			for (Alternative alternative_2: alternCompa.columns())
				if (alternCompa.isSet(alternative_1, alternative_2))
				{
					final float value = alternCompa.getValue(alternative_1, alternative_2);
					if (value >= threshold)
						result.setValue(alternative_1, alternative_2, max);
					else if (value <= -threshold)
						result.setValue(alternative_1, alternative_2, min);
					else
						result.setValue(alternative_1, alternative_2, indeterminate);
				}

		return result;
	}


	public static void main(String[] argv)
	{
		String pathMethodParameters = argv[0];
		String pathAlternatives = argv[1];
		String pathAlternativesComparisons = argv[2];

		String errorMessage = "";
		if (argv.length != 5 || !new File(argv[0]).exists() || !new File(argv[1]).exists()
				|| !new File(argv[2]).exists())
		{
			errorMessage = "Error: unable to run the algorithm. Reason: missing one (or more) entry file(s). "
					+ "Please check your entry files.";

			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage = argv[4];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		Main mainFunction = new Main();
		String errParameters = mainFunction.checkEntryFile(pathMethodParameters);
		String errAlternatives = mainFunction.checkEntryFile(pathAlternatives);
		String errAlternativesComparisons = mainFunction.checkEntryFile(pathAlternativesComparisons);

		if (errParameters != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the methodParameters "
					+ "tag is not a valid XMCDA document:" + errParameters);

		if (errAlternatives != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternatives "
					+ "tag is not a valid XMCDA document:" + errAlternatives);

		if (errAlternativesComparisons != "")
			errorMessage = errorMessage.concat("\nFatal Error: the file containing the alternativesComparisons "
					+ "tag is not a valid XMCDA document:" + errAlternativesComparisons);

		if (errParameters != "" || errAlternatives != "" || errAlternativesComparisons != "")
		{
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage = argv[4];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		cutRelation relation = new cutRelation();

		if (mainFunction.getWarningMessage() != "")
			relation.warningMessage = relation.warningMessage.concat(mainFunction.getWarningMessage());

		// Parsing XML file for Alternatives
		mainFunction.PrepareParsing(pathAlternatives);
		Alternatives alternativesToUse = new Alternatives();
		JDOM_Alternatives jdomAlter = new JDOM_Alternatives();
		boolean parseAlternativesNull = false;
		alternativesToUse = jdomAlter.exploreAlternativesXML(mainFunction.getRacine());
		if (alternativesToUse == null)
		{
			parseAlternativesNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternatives " + "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
						+ " alternatives tag is erroneous or empty.");
		}

		if (jdomAlter.getWarning() != "")
			relation.warningMessage = relation.warningMessage.concat("\n" + jdomAlter.getWarning());


		// Parsing XML file for AlternativesComparisons
		mainFunction.PrepareParsing(pathAlternativesComparisons);
		AlternativesComparisons alternCompaToUse = new AlternativesComparisons();
		JDOM_Alternatives_Comparisons jdomAlterComp = new JDOM_Alternatives_Comparisons();

		boolean parseAlternCompNull = false;
		alternCompaToUse = jdomAlterComp.exploreAlternativesComparisonsXML(mainFunction.getRacine(), alternativesToUse);
		if (jdomAlterComp.getWarningMessage() != "")
			relation.warningMessage = relation.warningMessage.concat("\n" + jdomAlterComp.getWarningMessage());
		if (alternCompaToUse == null)

		{
			parseAlternCompNull = true;
			// Error message
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the alternativesComparisons "
						+ "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
						+ " alternativesComparisons tag is erroneous or empty.");
		}


		// ParseMethodParameters

		MethodParamCutRelation parseMethodParam = new MethodParamCutRelation();
		mainFunction.PrepareParsing(pathMethodParameters);
		boolean statusParseMethod = parseMethodParam.parse(mainFunction.getRacine());

		if (parseMethodParam.getWarning() != "")
			relation.warningMessage = relation.warningMessage.concat(parseMethodParam.getWarning());

		if (!statusParseMethod)
		{
			if (errorMessage.equals(""))
				errorMessage = "\nFatal Error: the file containing the methodParameters "
						+ "tag is erroneous or empty.";
			else
				errorMessage = errorMessage.concat("\nFatal Error: the file containing the"
						+ " methodParameters tag is erroneous or empty.");
		}
		if (parseAlternCompNull || parseAlternativesNull || !statusParseMethod)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			xmcdaMess.createErrorMessage(errorMessage);
			String pathOutputMessage = argv[4];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		if (alternCompaToUse.nbRows() == 0)
		{
			// Output Log Message File
			XmcdaMessage xmcdaMess = new XmcdaMessage();
			errorMessage = errorMessage
					.concat("Failure to run the algorithm because it seems that the file containing "
							+ "the alternativesComparisons tag is empty or erroneous.");
			xmcdaMess
			.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
			                                             errParameters + relation.warningMessage, errorMessage);

			String pathOutputMessage = argv[4];
			xmcdaMess.enregistre(pathOutputMessage);
			return;
		}

		// R�cup�rer le param�tre et d�rouler l'agorithme

		String cutType = parseMethodParam.getCutType();
		float threshold = parseMethodParam.getThreshold();
		String output_classical = parseMethodParam.getClassicalOutput();
		String output_bipolar = parseMethodParam.getBipolarOutput();

		float min = parseMethodParam.getMin();
		float max = parseMethodParam.getMax();
		float indeterminate = parseMethodParam.getIndeterminate();

		AlternativesComparisons alterCompResult = new AlternativesComparisons();
		String pathFile = argv[3];
		OutputRelationFile outputFile = new OutputRelationFile();

		if (cutType.equals("classical") && output_classical.equals("crisp"))
		{
			alterCompResult = relation.createBinaryRelation(alternCompaToUse, threshold, false);

			outputFile.createFile(alterCompResult, "output crisp relation", false);
			outputFile.saveFile(pathFile);
		}
		else if (cutType.equals("classical") && output_classical.equals("binary"))
		{
			alterCompResult = relation.createBinaryRelation(alternCompaToUse, threshold, true);

			outputFile.createFile(alterCompResult, "output binary relation", true);
			outputFile.saveFile(pathFile);
		}

		else if (cutType.equals("classical") && output_classical.equals("other binary"))
		{
			alterCompResult = relation.createOtherRelation(alternCompaToUse, threshold, min, max, indeterminate);

			outputFile.createFile(alterCompResult, "output other binary relation", true);
			outputFile.saveFile(pathFile);
		}
		else if (cutType.equals("bipolar") && output_bipolar.equals("bipolar"))
		{
			alterCompResult = relation.createOtherRelation(alternCompaToUse, threshold, -1, 1, 0);

			outputFile.createFile(alterCompResult, "output bipolar relation", true);
			outputFile.saveFile(pathFile);
		}
		else if (cutType.equals("bipolar") && output_bipolar.equals("other bipolar"))
		{
			alterCompResult = relation.createOtherRelation(alternCompaToUse, threshold, min, max, indeterminate);

			outputFile.createFile(alterCompResult, "output other bipolar relation", true);
			outputFile.saveFile(pathFile);
		}
		else
		{
			errorMessage = errorMessage.concat("\nError in file containing method parameters tag. "
					+ "Check 'Cut Type' and the corresponding output parameter.");
		}

		// Output Log Message File
		XmcdaMessage xmcdaMess = new XmcdaMessage();
		xmcdaMess.createLogMessagePlotAlternativesComparisons(errAlternatives, errAlternativesComparisons,
		                                                      relation.warningMessage, errorMessage);
		String pathOutputMessage = argv[4];
		xmcdaMess.enregistre(pathOutputMessage);
	}

}
