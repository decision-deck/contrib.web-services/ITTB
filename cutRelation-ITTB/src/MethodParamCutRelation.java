import java.util.Iterator;

import org.jdom2.Element;


public class MethodParamCutRelation {

	private String cutType;
	private float cutThreshold;
	private String output_classical;
	private String output_bipolar;
	private String warning;
	private float min;
	private float max;
	private float indeterminate;

	public MethodParamCutRelation()
	{
		cutType="classical";
		output_classical="binary";
		output_bipolar="bipolar";
		warning="";
		min =0;
		max=1;
		indeterminate=1/2;
	}

	public boolean parse(Element racine)
	{
		boolean statusOK=true;
		if (racine.getChild("methodParameters")!=null)
		{
			Element methodParameters = racine.getChild("methodParameters");
			if(methodParameters.getChildren("parameter")!=null)
			{
				Iterator<Element> i = methodParameters.getChildren("parameter").iterator(); 
				while(i.hasNext())
				{
					Element courant = i.next();
					if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("cut_type"))
					{

						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) && 
								(courant.getChild("value").getChild("label").getValue().equals("classical")) )
							cutType="classical";
						else if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").getChild("label")!=null) && 
								(courant.getChild("value").getChild("label").getValue().equals("bipolar")) )
							cutType="bipolar";

						else
						{
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Cut Type'" +
									". The default value is set to 'classical'.");
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("cut_threshold"))
					{

						if ((courant.getChild("value")!= null) && 
								(courant.getChild("value").
										getChild("real")!=null) )
							cutThreshold=Float.parseFloat(courant.getChild("value").
							                              getChild("real").getValue());

						else
						{
							warning=warning.concat("\nWarning: an error occured while " +
									"trying to get the parameter 'Cut threshold'" +
									". The default value is set to '0'.");
						}
					}

					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("classical_output"))
					{

						if(cutType.equals("classical")	)
						{
							if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").getChild("label")!=null) && 
									(courant.getChild("value").getChild("label").getValue().equals("crisp")) )
								output_classical="crisp";
							else if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").getChild("label")!=null) && 
									(courant.getChild("value").getChild("label").getValue().equals("classical_binary")) )
								output_classical="binary";
							else if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").getChild("label")!=null) && 
									(courant.getChild("value").getChild("label").getValue().equals("other_binary")) )
								output_classical="other binary";

							else
							{
								warning=warning.concat("\nWarning: an error occured while " +
										"trying to get the parameter 'Classical output'" +
										". The default value is set to 'crisp'.");
							}
						}
					}

					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("bipolar_output"))
					{


						if(cutType.equals("bipolar")	)
						{
							if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").getChild("label")!=null) && 
									(courant.getChild("value").getChild("label").getValue().equals("classical_bipolar")) )
								output_bipolar="bipolar";
							else if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").getChild("label")!=null) && 
									(courant.getChild("value").getChild("label").getValue().equals("other_bipolar")) )
								output_bipolar="other bipolar";


							else
							{
								warning=warning.concat("\nWarning: an error occured while " +
										"trying to get the parameter 'Bipolar output'" +
										". The default value is set to 'bipolar'.");
							}
						}
					}


					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("true"))
					{

						if(output_classical.equals("other binary")||output_bipolar.equals("other bipolar"))
						{
							if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").
											getChild("real")!=null) )
								max=Float.parseFloat(courant.getChild("value").
								                     getChild("real").getValue());

							else
							{
								warning=warning.concat("\nWarning: an error occured while " +
										"trying to get the parameter 'True'" +
										". The default value is set to '1'.");
							}
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("false"))
					{

						if(output_classical.equals("other binary")||output_bipolar.equals("other bipolar"))
						{
							if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").
											getChild("real")!=null) )
								min=Float.parseFloat(courant.getChild("value").
								                     getChild("real").getValue());

							else
							{
								warning=warning.concat("\nWarning: an error occured while " +
										"trying to get the parameter 'False'" +
										". The default value is set to '0'.");
							}
						}
					}
					else if (courant.getAttributeValue("id")!= null && courant.getAttributeValue("id").
							equals("indeterminate"))
					{

						if(output_classical.equals("other binary")||output_bipolar.equals("other bipolar"))
						{
							if ((courant.getChild("value")!= null) && 
									(courant.getChild("value").
											getChild("real")!=null) )
								indeterminate=Float.parseFloat(courant.getChild("value").
								                               getChild("real").getValue());

							else
							{
								warning=warning.concat("\nWarning: an error occured while " +
										"trying to get the parameter 'Indeterminate'" +
										". The default value is set to '1/2'.");
							}
						}
					}

					else {// we have only the previous parameter in method parameters
						warning=warning.concat("Error: you have not entered the correct parameter.");
					}
				}
			}
			else 
				statusOK=false;
		}
		else 
			statusOK=false;
		return statusOK;
	}


	public String getCutType()
	{
		return cutType;
	}
	public float getThreshold()
	{
		return cutThreshold;
	}

	public String getClassicalOutput()
	{
		return output_classical;
	}
	public String getBipolarOutput()
	{
		return output_bipolar;
	}
	public float getMin()
	{
		return min;
	}
	public float getMax()
	{
		return max;
	}
	public float getIndeterminate()
	{
		return indeterminate;
	}
	public String getWarning()
	{
		return warning;
	}

}
